<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("О бренде");
?><div style="text-align: justify;">
	<p>
		<b><span style="font-size: 14pt;">О бренде</span></b>
	</p>


	<p>
		<span style="font-size: 12pt;"><br>
		</span>
	</p>
	<p>
		<span style="font-size: 12pt;">Когда в продукт вкладываются личные ценности его создателей, получается бренд. И здесь не может быть мелочей, важно все от названия и идеи до выбора цвета моделей и эскизов принтов.</span><br>
	</p>
	<p>
 <br>
	</p>
 <span style="font-size: 12pt;"> </span>
	<p>
 <span style="font-size: 12pt;">
		Работа в креативной дизайнерской лаборатории сравнима со слаженным трудом пчел в улье, поэтому, первая часть имени бренда звучит как </span><b><span style="font-size: 12pt;">BEE</span></b><span style="font-size: 12pt;">. Слитное русскоязычное прочтение логотипа&nbsp;«</span><b><span style="font-size: 12pt;">BEE-RU»&nbsp;</span></b><span style="font-size: 12pt;">звучит просто и ясно, как глагол&nbsp;«</span><b><span style="font-size: 12pt;">беру» - «БЕРУ РУССКОЕ!»</span></b><span style="font-size: 12pt;">. Наш слоган -&nbsp;</span><b><span style="font-size: 12pt;">ОДЕЖДА для ЯРКИХ ЛЮДЕЙ с РУССКИМ ХАРАКТЕРОМ</span></b><span style="font-size: 12pt;">. Наша миссия - практичный и </span><b><span style="font-size: 12pt;">комфортный минимализм:&nbsp;</span></b><span style="font-size: 12pt;">чистота линий моделей, ясность форм и силуэтов, отсутствие чрезмерного графического наполнения; удобство и функциональность, прочность и долгий срок использования. </span>
	</p>
	<p>
 <br>
	</p>
 <span style="font-size: 12pt;"> </span>
	<p>
 <span style="font-size: 12pt;">
		В коллекциях моделей&nbsp;</span><b><span style="font-size: 12pt;">«BEE-BASIC»</span></b><span style="font-size: 12pt;">&nbsp; и&nbsp;</span><b><span style="font-size: 12pt;">«BEE-SPORT»</span></b><span style="font-size: 12pt;">&nbsp;соединяются два стиля: спорт и классика. Так рождается баланс &nbsp;- </span><b><span style="font-size: 12pt;">СПОРТИВНЫЙ CASUAL</span></b><span style="font-size: 12pt;">, уместный как на прогулке в парке, так и в городском офисе.&nbsp; Наши дизайнеры&nbsp; находятся в неустанном творческом поиске и постоянно дополняют действующий ассортимент более смелыми, сложными и нестандартными моделями, а также, лимитированными коллекциями «</span><b><span style="font-size: 12pt;">BEE-RU.</span></b><b><span style="font-size: 12pt;">NEW</span></b><b><span style="font-size: 12pt;">».</span></b><span style="font-size: 12pt;"> </span>
	</p>
	<p>
 <b><br>
 </b>
	</p>
 <span style="font-size: 12pt;"> </span>
	<p>
 <span style="font-size: 12pt;"> </span><b><span style="font-size: 12pt;">Комфортная, стильная, спортивная одежда марки</span></b><span style="font-size: 12pt;">«</span><b><span style="font-size: 12pt;">BEE-RU» </span></b><b><span style="font-size: 12pt;">не зависит от возрастных рамок! </span></b><span style="font-size: 12pt;">&nbsp;это постоянный источник </span><b><span style="font-size: 12pt;">ТВОЕГО ПОЗИТИВНОГО НАСТРОЯ и БОЛЬШОГО УДОВОЛЬСТВИЯ</span></b><span style="font-size: 12pt;">, идеальный способ </span><b><span style="font-size: 12pt;">ПОВЫШЕНИЯ ТВОЕЙ ЖИЗНЕННОГО ТОНУСА и САМООЦЕНКИ</span></b><span style="font-size: 12pt;"> !</span><span style="font-size: 12pt;"> </span>
	</p>
 <span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span>
</div>
 <span style="font-size: 12pt;"> </span> <span style="font-size: 12pt;"> </span><span style="font-size: 14pt;"> </span> <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>