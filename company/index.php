<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("О компании");
?><p>
 <span style="font-size: 12pt;"><b><span style="font-size: 14pt;">О компании</span></b></span><span style="font-weight: 400;"><br> </p>

<p>
 <span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span></span><span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span>
</p>
 <span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span>
<p>
 <span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span><span style="font-weight: 400;"><br>
 <span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span></span><span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span>
</p>
 <span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span>
<p>
 <span style="font-size: 12pt;">Новая торговая марка «</span><b><span style="font-size: 12pt;">BEE-RU»</span></b><span style="font-size: 12pt;">&nbsp;основана в 2018г., это&nbsp;яркая креативная российская дизайнерская лаборатория, в которой создается удобная и практичная спортивная&nbsp;</span><b><span style="font-size: 12pt;">ОДЕЖДА для молодых людей с РУССКИМ ХАРАКТЕРОМ</span></b><span style="font-size: 12pt;">. Ее создали и придумали талантливые люди, настроенные на позитивную активную волну, влюбленные в ритм большого города и его уникальные возможности. Смелые, стильные модели марки «</span><b><span style="font-size: 12pt;">BEE-RU»</span></b><span style="font-size: 12pt;">&nbsp;в популярном стиле&nbsp;</span><b><span style="font-size: 12pt;">SPORT casual </span></b><span style="font-size: 12pt;">по достоинству оценят сторонники&nbsp;свободного комфортного стиля в офисе и любители&nbsp;&nbsp;весело прогуляться с друзьями в парке, лихо прокатиться на скейте или роликах по городским улицам. Если ТЫ на позитиве, в движении и драйве, постоянном поиске - НАША ОДЕЖДА для ТЕБЯ !&nbsp;</span><b><span style="font-size: 12pt;">Наслаждайся РИТМОМ жизни и СВОБОДОЙ движений&nbsp;&nbsp;КАЖДЫЙ ДЕНЬ и в ЛЮБУЮ ПОГОДУ</span></b><span style="font-size: 12pt;">! </span>
</p>
<p>
 <br>
</p>
 <span style="font-size: 12pt;"> </span>
<p>
 <span style="font-size: 12pt;">
	Торговая марка «</span><b><span style="font-size: 12pt;">BEE-RU»&nbsp;</span></b><span style="font-size: 12pt;">предлагает две основные коллекции моделей.&nbsp;</span><b><span style="font-size: 12pt;">«BEE - BASIC»</span></b><span style="font-size: 12pt;">&nbsp;для поклонников яркого свободного спортивного стиля в одежде – необходимые для базового гардероба оригинальные футболки, практичные свитшоты, удобные толстовки, уютные спортивные платья, практичные стильные брюки, спортивные костюмы&nbsp;&nbsp;и линейка&nbsp;</span><b><span style="font-size: 12pt;">«BEE-SPORT»&nbsp;</span></b><span style="font-size: 12pt;">для занятий любительским и прогулочным спортом на улице и в зале - футболки и спортивные костюмы с динамичными светоотражающими ставками.&nbsp; </span>
</p>
<p>
 <br>
</p>
 <span style="font-size: 12pt;"> </span>
<p>
 <span style="font-size: 12pt;">Стильные мужские и женские футболки из коллекции</span><b><span style="font-size: 12pt;"> «BEE-BASIC»</span></b><span style="font-size: 12pt;">&nbsp;выполнены с круглым и v-образным вырезом горловины из органической кулирки - пенье супрем премиум с составом&nbsp;95% хлопка и 5% лайкры</span><b><span style="font-size: 12pt;">; </span></b><span style="font-size: 12pt;">имеют широкую палитру расцветок от основных классических -белого, черного, серого до ярких сложных цветов: бирюзово-зеленых, пастельно-желтых, нежных персиковых, глубоких оттенков синего, хаки и горчичного. В линию </span><b><span style="font-size: 12pt;">«BEE-BASIC»</span></b><span style="font-size: 12pt;">&nbsp;входят&nbsp; практичные свитшоты, удобные толстовки с графическими каучуковыми принтами, уютные и женственные спортивные платья, практичные стильные брюки и спортивные костюмы самых разнообразных расцветок. </span>
</p>
 <span style="font-size: 12pt;"> </span>
<p>
 <br>
</p>
<p>
 <span style="font-size: 12pt;">
	Коллекция концептуальных спортивных комплектов&nbsp;</span><b><span style="font-size: 12pt;">«BEE-SPORT»</span></b><span style="font-size: 12pt;">&nbsp;выполнена из комфортного, приятного телу, петельного футера 3х-нитки и позволяет комфортно чувствовать себя сразу в нескольких сезонах переменчивой погоды в России: весна, лето, осень. Светящиеся дерзкие полоски-рефлекторы эффектно украшают рукава свитшотов и спортивные брюки серии&nbsp; </span><b><span style="font-size: 12pt;">«BEE-SPORT»</span></b><span style="font-size: 12pt;">&nbsp;и обеспечивают твою безопасность&nbsp; во время вечерних пробежек, катания на скейтборде, велопрогулок по городу.&nbsp;Основные цвета комплектов&nbsp;- светлый серый меланж и глубокий темный антрацит. Смелые модели этой коллекции невозможно не заметить, их выделяют геометрические светоотражающие строчки, канты, вышивки и эффектные флуоресцентные принты !&nbsp;</span><b><span style="font-size: 12pt;">ТЫ НЕ ОСТАНЕШЬСЯ НЕЗАМЕЧЕННЫМ!</span></b><span style="font-size: 12pt;"> </span>
</p>
<p>
 <b><br>
 </b>
</p>
 <span style="font-size: 12pt;"> </span>
<p>
 <span style="font-size: 12pt;"> </span>
</p>
 <span style="font-size: 12pt;"> </span>
<p>
 <b><span style="font-size: 12pt;">Ты сам можешь дополнить и украсить </span></b><span style="font-size: 12pt;">наши модели оригинальными авторскими принтами из фирменной коллекции «</span><b><span style="font-size: 12pt;">PRINTBAR</span></b><span style="font-size: 12pt;">», разработанной талантливыми графическими дизайнерами компании. </span>
</p>
 <span style="font-size: 12pt;"> </span><br>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>