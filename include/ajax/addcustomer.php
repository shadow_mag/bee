<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

if (!$_POST['sum']) {
    exit();
}

$productId = 390;
$sum = $_POST['sum'];
$count = $_POST['count'];
$discount = $_POST['discount'];
$priceByItemWithDiscount = $sum/$count;
$priceByItem = ($sum + $discount)/$count;

CModule::IncludeModule('catalog');
CModule::IncludeModule('sale');

$result = CIBlockElement::GetByID($productId);
$product = $result->Fetch();

CSaleBasket::DeleteAll(CSaleBasket::GetBasketUserID());
Add2BasketByProductID($productId);

$basket = CSaleBasket::GetList([], ['FUSER_ID' => CSaleBasket::GetBasketUserID(), 'LID' => 's1', 'ORDER_ID' => 'NULL'], false, false, ['ID']);

while($items = $basket->Fetch()) {
	CSaleBasket::Update($items['ID'], [
		'PRODUCT_ID' => $productId,
		'PRICE' => $priceByItemWithDiscount,
		'CUSTOM_PRICE' => 'Y',
		'QUANTITY' => $count,
		'CAN_BUY' => 'Y',
		'NAME' => $product['NAME'],
		'CURRRENCY' => 'RUB',
		'DETAIL_PAGE_URL' => '/printbar.php',
		'LID' => 's1',
	]);
}

exit();