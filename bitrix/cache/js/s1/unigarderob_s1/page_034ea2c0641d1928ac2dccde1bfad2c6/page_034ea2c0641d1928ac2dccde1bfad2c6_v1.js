
; /* Start:"a:4:{s:4:"full";s:101:"/bitrix/templates/unigarderob_s1/components/bitrix/catalog.element/.default/script.js?155487088810693";s:6:"source";s:85:"/bitrix/templates/unigarderob_s1/components/bitrix/catalog.element/.default/script.js";s:3:"min";s:0:"";s:3:"map";s:0:"";}"*/
/*function UNISku ($oSettings) {
    var that = this;
    
    this.defaults = {
        'CATALOG':{
            'CONFIG': {
               'SHOW_QUANTITY': false,
               'SHOW_DISCOUNT_PERCENT': false,
               'SHOW_OLD_PRICE': false,
               'DISPLAY_COMPARE': false,
               'SHOW_SKU_PROPS': false,
               'OFFER_GROUP': false,
               'MAIN_PICTURE_MODE': 'IMG'
            },
            'VISUAL': {
               'ID': '',
               'CURRENT_PATH': '',
               'ONE_CLICK_BUY': ''
            },
            'DEFAULT_PICTURE': {
               'PREVIEW_PICTURE': undefined,
               'DETAIL_PICTURE': undefined
            },
            'PRODUCT': {
               'ID': '0',
               'NAME': ''
            },
            'OFFERS': [],
            'OFFER_SELECTED': '0',
            'TREE_PROPS': []
        },
        'EVENTS': {
            'onSelectProperty': function () {},
        }
    };
    
    this.settings = $.extend({}, this.defaults, $oSettings || {});
    
    function __inArray($oNeedle, $oHaystack, $bStrict) {
        var $bFounded = false; $bStrict = !!$bStrict;
        
        __forEach($oHaystack, function($oKey, $oValue) {
            if (__isObject($oNeedle) || __isArray($oNeedle)) {
                if (__equal($oNeedle, $oValue)) {
                    $bFounded = true;
                    return false;
                }
            } else {
                if ($bStrict)
                    if ($oValue === $oNeedle) {
                        $bFounded = true;
                        return false;
                    }
                    
                if (!$bStrict)
                    if ($oValue == $oNeedle) {
                        $bFounded = true;
                        return false;
                    }
            }
            
        });
        
        return $bFounded;
    }
    
    function __equal($oFirstObject, $oSecondObject) {        
        if (__isObject($oFirstObject) && __isObject($oSecondObject)) {
            var $arFirstObjectKeys = Object.keys($oFirstObject);
            var $arSecondObjectKeys = Object.keys($oSecondObject);
            
            if ($arFirstObjectKeys.length != $arSecondObjectKeys.length ) return false;
        
        	return !$arFirstObjectKeys.filter(function($sKey){
        		if (typeof $oFirstObject[$sKey] == "object" ||  Array.isArray($oFirstObject[$sKey])) {
        			return !__equal($oFirstObject[$sKey], $oSecondObject[$sKey]);
        		} else {
        			return $oFirstObject[$sKey] !== $oSecondObject[$sKey];
        		}
        	}).length;
        }
    	
        return false;
    }
    
    function __isFunction($oFunction) {
        return Object.prototype.toString.call($oFunction) == '[object Function]';
    }
    
    function __isArray($oArray) {
        return Object.prototype.toString.call($oArray) == '[object Array]';
    }
    
    function __isObject($oObject) {
        return Object.prototype.toString.call($oObject) == '[object Object]';
    }
    
    function __init()
    {
        __forEach(that.settings.CATALOG.OFFERS, function ($iOfferArrayID, $oOffer) {
            that.settings.CATALOG.OFFER_SELECTED = $oOffer.ID;
            if (__isFunction(that.settings.EVENTS['onSelectProperty']))
                    that.settings.EVENTS['onSelectProperty'](__getSettings());
            return false;
        });
    }
    
    function __forEach($oObject, callback)
    {
        if (__isFunction(callback)) {
            if (__isArray($oObject)) {
                var $iArrayIndex = 0;
                var $iArrayLength = $oObject.length;
                
                for ($iArrayIndex = 0; $iArrayIndex < $iArrayLength; $iArrayIndex++) {
                    var $oArrayEntry = $oObject[$iArrayIndex];
                    
                    if (callback($iArrayIndex, $oArrayEntry) == false)
                        break;
                }
            } else if (__isObject($oObject)) {
                for (var $sObjectIndex in $oObject) {
                    var $oObjectEntry = $oObject[$sObjectIndex];
                    
                    if (callback($sObjectIndex, $oObjectEntry) == false)
                        break;
                }
            }
        }
    }
    
    function __getSettings() {
        $oSettings = {
            'CATALOG': that.settings.CATALOG,
            'OFFER': that.GetOffer(),
            'PROPERTIES': {
                'DISPLAYED': [],
                'ENABLED': [],
                'DISABLED': []
            }
        };
        
        if ($oSettings['OFFER'] != null) {
            // ������������ ��������
            __forEach(that.settings.CATALOG.OFFERS, function ($iOfferArrayID, $oOffer) {
                __forEach($oOffer.TREE, function ($sTreeKey, $sTreeValue) {
                    $oProperty = {'KEY':$sTreeKey, 'VALUE':$sTreeValue};
                    // ���� �������� ��� � �������, �� ��������
                    if (!__inArray($oProperty))
                        $oSettings.PROPERTIES.DISPLAYED.push($oProperty);
                })
            });
            
            var $arProperties = []; // ���� �������
            var $arPropertiesSelected = []; // ��� ��������� ��������
            
            // ���� ��� ���� �������
            __forEach($oSettings['OFFER']['TREE'], function ($sTreeKey, $sTreeValue) {
                if (!__inArray($sTreeKey))
                    $arProperties.push($sTreeKey);
            });
            
            // �������� �� ���� ���������
            __forEach($arProperties, function ($iPropertyIndex, $sPropertyKey) {
                var $sCurrentProperty = $arProperties[$iPropertyIndex]; // ������������� �������
                // �������� �� ���� �������� ������������
                __forEach(that.settings.CATALOG.OFFERS, function ($iOfferIndex, $oOffer) {
                    var $bCompared = true; // ������������� ���������
                    // �������� �� ���� ��� ��������� ��������� � ���������� �� �� ���������� �������� ��������� �����������
                    __forEach($arPropertiesSelected, function ($iPropertySelectedIndex, $sPropertySelectedKey) {
                        if (that.settings.CATALOG.OFFERS[$iOfferIndex]['TREE'][$sPropertySelectedKey] != $oSettings.OFFER.TREE[$sPropertySelectedKey]) {
                            $bCompared = false; // ���� �������� �� ����� �� ������������� ��������� � �������
                            return false;
                        }
                    });
                    // ���� ��������� ������������, �� ��������� �������� �� ���������� ����� � ����������������
                    if ($bCompared == true) {
                        $oEnabledProperty = {'KEY':$sCurrentProperty, 'VALUE':that.settings.CATALOG.OFFERS[$iOfferIndex]['TREE'][$sCurrentProperty]};
                
                        if (!__inArray($oSettings.PROPERTIES.ENABLED, $oEnabledProperty))
                            $oSettings.PROPERTIES.ENABLED.push($oEnabledProperty);
                    } else {
                        $oDisabledProperty = {'KEY':$sCurrentProperty, 'VALUE':that.settings.CATALOG.OFFERS[$iOfferIndex]['TREE'][$sCurrentProperty]};
                        
                        if (!__inArray($oSettings.PROPERTIES.DISABLED, $oDisabledProperty))
                            $oSettings.PROPERTIES.DISABLED.push($oDisabledProperty);
                    }
                });
                // ��������� ������� �������� � ���������� �������� ��� �������� ���������
                $arPropertiesSelected.push($sCurrentProperty);
            });
        }
        // ���������� ��������� ���������
        return $oSettings;
    }
    
    this.constructor.prototype.SetOfferByID = function ($iOfferID) {
        __forEach(this.settings.CATALOG.OFFERS, function ($iOfferArrayID, $oOffer) {
            if ($oOffer.ID == $iOfferID) {
                that.settings.CATALOG.OFFER_SELECTED = $oOffer.ID;
                if (__isFunction(that.settings.EVENTS['onSelectProperty']))
                    that.settings.EVENTS['onSelectProperty'](__getSettings());
                return false;
            }
        }); 
    };
    
    this.constructor.prototype.GetOffer = function () {
        var $oCurrentOffer = null;
        
        // ���������, ���� ���� ��������� �������� �����������
        if (parseInt(this.settings.CATALOG.OFFER_SELECTED) > 0)
            __forEach(this.settings.CATALOG.OFFERS, function ($iOfferArrayID, $oOffer) {
                if ($oOffer.ID == that.settings.CATALOG.OFFER_SELECTED) {
                    $oCurrentOffer = $oOffer;
                    return false;
                }
            });
                
        return $oCurrentOffer;
    }
    
    this.constructor.prototype.SetOfferByProperty = function ($sPropKey, $sPropValue) {
        var $oOffer = this.GetOffer(); // �������� ������� �������� �����������
        var $oCompareTree = {}; // ������� ������ ������ ��� ������� ���������
        
        // ���������� ������ �� �������������� ���� �������� � ������ ��� ����������� �������� � ������ ���������
        __forEach($oOffer.TREE, function ($sTreeKey, $sTreeValue) {
            $oCompareTree[$sTreeKey] = $sTreeValue;
            if ($sTreeKey == $sPropKey) {
                $oCompareTree[$sTreeKey] = $sPropValue;
                return false;
            } 
        });
        
        // �������� ��������� ������� ���� �������� �����������
        __forEach(this.settings.CATALOG.OFFERS, function ($iOfferArrayID, $oOffer) {
            var $bCompared = true;
            // ���������� ������ ��������
            __forEach($oCompareTree, function ($sTreeKey, $sTreeValue) {
                if ($oOffer.TREE[$sTreeKey] != $sTreeValue) {
                    $bCompared = false;
                    
                    return false; // ������� �� �����
                }
            });
            // ���� ����� ����������, �� ������������� ��� �������
            if ($bCompared) {
                that.settings.CATALOG.OFFER_SELECTED = $oOffer.ID;
                
                if (__isFunction(that.settings.EVENTS['onSelectProperty']))
                    that.settings.EVENTS['onSelectProperty'](__getSettings());
                    
                return false; // ������� �� �����
            }
        });
    }
    
    __init();
}*/
/* End */
;; /* /bitrix/templates/unigarderob_s1/components/bitrix/catalog.element/.default/script.js?155487088810693*/
