<?
if($INCLUDE_FROM_CACHE!='Y')return false;
$datecreate = '001557161084';
$dateexpire = '001593161084';
$ser_content = 'a:2:{s:7:"CONTENT";s:3566:"	<div class="faq_block">
	<div class="title">Ответы на часто задаваемые вопросы</div>
							
			<div class="one_faq" id="bx_3218110189_313">
				<div>
					<span class="title_question_faq">Вопрос</span>	
					<span class="question_faq">Как сменить город для осуществления покупок?</span>
				</div>
				<div class="arrow_faq"></div>
				<div class="answer_faq">
					<div style="text-align: justify;">При первом запуске сайта Magazine отображается всплывающее окно приветствия, в котором можно выбрать свой город. Также регион можно сменить, щелкнув по установленному на данный момент городу в верхнем левом углу страницы сайта. Город можно выбрать из предложенного списка или используя поле &laquo;Введите свой город&raquo;.</div>

				</div>						
			</div>
								
			<div class="one_faq" id="bx_3218110189_312">
				<div>
					<span class="title_question_faq">Вопрос</span>	
					<span class="question_faq">Как отсортировать по цене результаты поиска?</span>
				</div>
				<div class="arrow_faq"></div>
				<div class="answer_faq">
					 Пока сортировка результатов поиска невозможна, но мы развиваемся &mdash; и скоро такая возможность появится.				</div>						
			</div>
								
			<div class="one_faq" id="bx_3218110189_311">
				<div>
					<span class="title_question_faq">Вопрос</span>	
					<span class="question_faq">Почему недоступна доставка выбранного товара?</span>
				</div>
				<div class="arrow_faq"></div>
				<div class="answer_faq">
					Доставка может быть недоступна, если выбранный товар есть в наличии только в магазине.				</div>						
			</div>
								
			<div class="one_faq" id="bx_3218110189_310">
				<div>
					<span class="title_question_faq">Вопрос</span>	
					<span class="question_faq">Почему недоступен самовывоз выбранного товара?</span>
				</div>
				<div class="arrow_faq"></div>
				<div class="answer_faq">
					<div> Данный товар является крупногабаритным или в выбранном регионе отсутствует магазин Magazine.</div>

.				</div>						
			</div>
								
			<div class="one_faq" id="bx_3218110189_309">
				<div>
					<span class="title_question_faq">Вопрос</span>	
					<span class="question_faq">Почему создалось несколько заказов в Корзине?</span>
				</div>
				<div class="arrow_faq"></div>
				<div class="answer_faq">
					Заказ разбивается на подзаказы, если выбранные товары есть в наличии в различных магазинах и отгружаются с различных складов. 				</div>						
			</div>
				 
			<input type="hidden" name="name_resume" value="Почему создалось несколько заказов в Корзине?" /> 
			<div class="uni-button solid_button send_question" onclick="Popups.AskQuestion()">Задать вопрос</div>
			</div>
					
";s:4:"VARS";a:2:{s:8:"arResult";a:7:{s:2:"ID";s:1:"8";s:14:"IBLOCK_TYPE_ID";s:7:"content";s:13:"LIST_PAGE_URL";s:43:"#SITE_DIR#/content/index.php?ID=#IBLOCK_ID#";s:15:"NAV_CACHED_DATA";N;s:4:"NAME";s:3:"FAQ";s:7:"SECTION";b:0;s:8:"ELEMENTS";a:5:{i:0;s:3:"313";i:1;s:3:"312";i:2;s:3:"311";i:3;s:3:"310";i:4;s:3:"309";}}s:18:"templateCachedData";a:5:{s:13:"additionalCSS";s:74:"/bitrix/templates/unigarderob_s1/components/bitrix/news.list/faq/style.css";s:9:"frameMode";b:1;s:8:"__NavNum";i:1;s:17:"__currentCounters";a:1:{s:28:"bitrix:system.pagenavigation";i:1;}s:13:"__editButtons";a:10:{i:0;a:5:{i:0;s:13:"AddEditAction";i:1;s:3:"313";i:2;s:194:"/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=8&type=content&ID=313&lang=ru&force_catalog=&filter_section=0&bxpublic=Y&from_module=iblock&return_url=%2Fhelp%2Ffaq%2Findex.php%3Fclear_cache%3DY";i:3;s:29:"Изменить вопрос";i:4;a:0:{}}i:1;a:5:{i:0;s:15:"AddDeleteAction";i:1;s:3:"313";i:2;s:145:"/bitrix/admin/iblock_list_admin.php?IBLOCK_ID=8&type=content&lang=ru&action=delete&ID=E313&return_url=%2Fhelp%2Ffaq%2Findex.php%3Fclear_cache%3DY";i:3;s:27:"Удалить вопрос";i:4;a:1:{s:7:"CONFIRM";s:123:"Будет удалена вся информация, связанная с этой записью. Продолжить?";}}i:2;a:5:{i:0;s:13:"AddEditAction";i:1;s:3:"312";i:2;s:194:"/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=8&type=content&ID=312&lang=ru&force_catalog=&filter_section=0&bxpublic=Y&from_module=iblock&return_url=%2Fhelp%2Ffaq%2Findex.php%3Fclear_cache%3DY";i:3;s:29:"Изменить вопрос";i:4;a:0:{}}i:3;a:5:{i:0;s:15:"AddDeleteAction";i:1;s:3:"312";i:2;s:145:"/bitrix/admin/iblock_list_admin.php?IBLOCK_ID=8&type=content&lang=ru&action=delete&ID=E312&return_url=%2Fhelp%2Ffaq%2Findex.php%3Fclear_cache%3DY";i:3;s:27:"Удалить вопрос";i:4;a:1:{s:7:"CONFIRM";s:123:"Будет удалена вся информация, связанная с этой записью. Продолжить?";}}i:4;a:5:{i:0;s:13:"AddEditAction";i:1;s:3:"311";i:2;s:194:"/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=8&type=content&ID=311&lang=ru&force_catalog=&filter_section=0&bxpublic=Y&from_module=iblock&return_url=%2Fhelp%2Ffaq%2Findex.php%3Fclear_cache%3DY";i:3;s:29:"Изменить вопрос";i:4;a:0:{}}i:5;a:5:{i:0;s:15:"AddDeleteAction";i:1;s:3:"311";i:2;s:145:"/bitrix/admin/iblock_list_admin.php?IBLOCK_ID=8&type=content&lang=ru&action=delete&ID=E311&return_url=%2Fhelp%2Ffaq%2Findex.php%3Fclear_cache%3DY";i:3;s:27:"Удалить вопрос";i:4;a:1:{s:7:"CONFIRM";s:123:"Будет удалена вся информация, связанная с этой записью. Продолжить?";}}i:6;a:5:{i:0;s:13:"AddEditAction";i:1;s:3:"310";i:2;s:194:"/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=8&type=content&ID=310&lang=ru&force_catalog=&filter_section=0&bxpublic=Y&from_module=iblock&return_url=%2Fhelp%2Ffaq%2Findex.php%3Fclear_cache%3DY";i:3;s:29:"Изменить вопрос";i:4;a:0:{}}i:7;a:5:{i:0;s:15:"AddDeleteAction";i:1;s:3:"310";i:2;s:145:"/bitrix/admin/iblock_list_admin.php?IBLOCK_ID=8&type=content&lang=ru&action=delete&ID=E310&return_url=%2Fhelp%2Ffaq%2Findex.php%3Fclear_cache%3DY";i:3;s:27:"Удалить вопрос";i:4;a:1:{s:7:"CONFIRM";s:123:"Будет удалена вся информация, связанная с этой записью. Продолжить?";}}i:8;a:5:{i:0;s:13:"AddEditAction";i:1;s:3:"309";i:2;s:194:"/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=8&type=content&ID=309&lang=ru&force_catalog=&filter_section=0&bxpublic=Y&from_module=iblock&return_url=%2Fhelp%2Ffaq%2Findex.php%3Fclear_cache%3DY";i:3;s:29:"Изменить вопрос";i:4;a:0:{}}i:9;a:5:{i:0;s:15:"AddDeleteAction";i:1;s:3:"309";i:2;s:145:"/bitrix/admin/iblock_list_admin.php?IBLOCK_ID=8&type=content&lang=ru&action=delete&ID=E309&return_url=%2Fhelp%2Ffaq%2Findex.php%3Fclear_cache%3DY";i:3;s:27:"Удалить вопрос";i:4;a:1:{s:7:"CONFIRM";s:123:"Будет удалена вся информация, связанная с этой записью. Продолжить?";}}}}}}';
return true;
?>