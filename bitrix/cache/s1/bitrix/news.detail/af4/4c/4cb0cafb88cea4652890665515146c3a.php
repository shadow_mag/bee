<?
if($INCLUDE_FROM_CACHE!='Y')return false;
$datecreate = '001557220153';
$dateexpire = '001593220153';
$ser_content = 'a:2:{s:7:"CONTENT";s:18668:"<div class="bx_news_detail">
    					<p style="text-align: justify;">
 <img width="410" alt="DSCF1173_1.jpg" src="/upload/medialibrary/b4e/b4e059397df7ca385f04f5de26121a58.jpg" height="229" align="left" style="margin:0px 15px 5px 0px;" title="DSCF1173_1.jpg"><span style="font-size: 12pt;"> </span>
</p>
<span style="font-size: 12pt;"> </span>
<p>
	<span style="font-size: 12pt;"> </span><span style="font-weight: 700; text-align: justify; font-size: 12pt;">Как выбрать качественную футболку для нанесения логотипа.</span><span style="font-size: 12pt;"> </span>
</p>
<span style="font-size: 12pt;"> </span>
<p>
	<span style="font-size: 12pt;"> </span><br>
	<span style="font-size: 12pt;"> </span>
</p>
<span style="font-size: 12pt;"> </span>
<p>
	<span style="font-size: 12pt;"> </span><span style="font-weight: 700; font-size: 12pt;">1. Плотность полотна</span><span style="font-size: 12pt;"> </span>
</p>
<span style="font-size: 12pt;"> </span>
<p>
	<span style="font-size: 12pt;"> </span><br>
	<span style="font-size: 12pt;"> </span>
</p>
<span style="font-size: 12pt;"> </span>
<p>
	<span style="font-size: 12pt;"> </span><span style="font-weight: 400; font-size: 12pt;">Обычно, эту цифру вам сообщат в первую очередь. Но о качестве футболки она ничего не говорит. Они сообщает только, сколько весит один квадратный метр полотна, из которого сделана футболка.</span><span style="font-size: 12pt;"> </span>
</p>
<span style="font-size: 12pt;"> </span>
<p>
	<span style="font-size: 12pt;"> </span><br>
	<span style="font-size: 12pt;"> </span>
</p>
<span style="font-size: 12pt;"> </span>
<p>
	<span style="font-size: 12pt;"> </span><span style="font-weight: 400; font-size: 12pt;">Индусы придумали этот миф много лет назад, когда наш потребитель особо в сырье не разбирался, и большинство привозимых в Россию футболок были либо стоком, либо шились из одинаково плохого полотна. И, чтобы как-то отличить по качеству ну совсем уж одноразовые долларовые футболки от почти одноразовых остальных, рассказали миру об этой якобы всё определяющей характеристике.</span><span style="font-size: 12pt;"> </span>
</p>
<span style="font-size: 12pt;"> </span>
<p>
	<span style="font-size: 12pt;"> </span><br>
	<span style="font-size: 12pt;"> </span>
</p>
<span style="font-size: 12pt;"> </span>
<p>
	<span style="font-size: 12pt;"> </span><span style="font-weight: 400; font-size: 12pt;">И с тех пор мы постоянно сталкиваемся с этим заблуждением: клиенты просят футболку плотностью 200-220 г/м2, считая, что это показатель хорошего качества.</span><span style="font-size: 12pt;"> </span>
</p>
<span style="font-size: 12pt;"> </span>
<p>
	<span style="font-size: 12pt;"> </span><br>
	<span style="font-size: 12pt;"> </span>
</p>
<span style="font-size: 12pt;"> </span>
<p>
	<span style="font-size: 12pt;"> </span><span style="font-weight: 400; font-size: 12pt;">При этом для себя они покупают футболки дорогих марок. А те - ну как на подбор - тонюсенькие.</span><span style="font-size: 12pt;"> </span>
</p>
<span style="font-size: 12pt;"> </span>
<p>
	<span style="font-size: 12pt;"> </span><br>
	<span style="font-size: 12pt;"> </span>
</p>
<span style="font-size: 12pt;"> </span>
<p>
	<span style="font-size: 12pt;"> </span><span style="font-weight: 400; font-size: 12pt;">Итак, </span><span style="font-weight: 700; font-size: 12pt;">правда</span><span style="font-weight: 400; font-size: 12pt;">: наиболее </span><span style="font-weight: 700; font-size: 12pt;">качественная футболка делается из тонкой пряжи</span><span style="font-weight: 400; font-size: 12pt;"> на вязальных машинах высокого класса (где иголки тонкие, и расположены максимально близко друг к другу). Полотно должно получаться тонкое, но плотное. Тогда и само изделие долговечно, и печать ложится идеально.</span><span style="font-size: 12pt;"> </span>
</p>
<span style="font-size: 12pt;"> </span>
<p>
	<span style="font-size: 12pt;"> </span><br>
	<span style="font-size: 12pt;"> </span>
</p>
<span style="font-size: 12pt;"> </span>
<p>
	<span style="font-size: 12pt;"> </span><span style="font-weight: 400; font-size: 12pt;">Если же пряжа толстая, на тонких иглах её не свяжешь. Поэтому используют вязальные машины низкого класса, полотно получается рыхлое и неоднородное. Такая футболка после стирки деформируется, а печать ложится неоднородно, «проваливается», а затем трескается, осыпается. Печатники знают - иногда, чтобы выровнять поверхность, приходится класть по 3-4 слоя подложки.</span><span style="font-size: 12pt;"> </span>
</p>
<span style="font-size: 12pt;"> </span>
<p>
	<span style="font-size: 12pt;"> </span><br>
	<span style="font-size: 12pt;"> </span>
</p>
<span style="font-size: 12pt;"> </span>
<p>
	<span style="font-size: 12pt;"> </span><span style="font-weight: 700; font-size: 12pt;">2. Страна-производитель</span><span style="font-size: 12pt;"> </span>
</p>
<span style="font-size: 12pt;"> </span>
<p>
	<span style="font-size: 12pt;"> </span><br>
	<span style="font-size: 12pt;"> </span>
</p>
<span style="font-size: 12pt;"> </span>
<p>
	<span style="font-size: 12pt;"> </span><span style="font-weight: 700; font-size: 12pt;">Правда</span><span style="font-weight: 400; font-size: 12pt;">: указанная на бирке страна-производитель почти ничего не скажет о качестве футболки.</span><span style="font-size: 12pt;"> </span>
</p>
<span style="font-size: 12pt;"> </span>
<p>
	<span style="font-size: 12pt;"> </span><br>
	<span style="font-size: 12pt;"> </span>
</p>
<span style="font-size: 12pt;"> </span>
<p>
	<span style="font-size: 12pt;"> </span><span style="font-weight: 400; font-size: 12pt;">Посмотрим правде в глаза, в Европе давно ничего не производится. А из Юго-восточной Азии в последнее время сложно что-то привезти легально. Поэтому, покупая однотонную футболку для нанесения логотипа, вы выбираете между:</span><span style="font-size: 12pt;"> </span>
</p>
<span style="font-size: 12pt;"> </span>
<p>
	<span style="font-size: 12pt;"> </span><br>
	<span style="font-size: 12pt;"> </span>
</p>
<span style="font-size: 12pt;"> </span>
<p>
	<span style="font-size: 12pt;"> </span><span style="font-weight: 400; font-size: 12pt;">- изделиями, которые заказываются европейцами в Бангладеш (реже Китае), привезены на европейский склад, а затем переправлены в Россию. Затраты на все эти путешествия, естественно, заложены в стоимость.</span><span style="font-size: 12pt;"> </span>
</p>
<span style="font-size: 12pt;"> </span>
<p>
	<span style="font-size: 12pt;"> </span><br>
	<span style="font-size: 12pt;"> </span>
</p>
<span style="font-size: 12pt;"> </span>
<p>
	<span style="font-size: 12pt;"> </span><span style="font-weight: 400; font-size: 12pt;">- футболками из Индии, Бангладеш или Китая: как правило, направленными сюда всякими правдами и неправдами.</span><span style="font-size: 12pt;"> </span>
</p>
<span style="font-size: 12pt;"> </span>
<p>
	<span style="font-size: 12pt;"> </span><br>
	<span style="font-size: 12pt;"> </span>
</p>
<span style="font-size: 12pt;"> </span>
<p>
	<span style="font-size: 12pt;"> </span><span style="font-weight: 400; font-size: 12pt;">- трикотажем, привезенным легально из Узбекистана</span><span style="font-size: 12pt;"> </span>
</p>
<span style="font-size: 12pt;"> </span>
<p>
	<span style="font-size: 12pt;"> </span><br>
	<span style="font-size: 12pt;"> </span>
</p>
<span style="font-size: 12pt;"> </span>
<p>
	<span style="font-size: 12pt;"> </span><span style="font-weight: 400; font-size: 12pt;">Итак, страна происхождения скажет вам скорее о легальности происхождения товара, о скорости его доставки (если вы подписали контракт на поставку - это может оказаться критичным). Но </span><span style="font-weight: 700; font-size: 12pt;">не о качестве.</span><span style="font-size: 12pt;"> </span>
</p>
<span style="font-size: 12pt;"> </span>
<p>
	<span style="font-size: 12pt;"> </span><br>
	<span style="font-size: 12pt;"> </span>
</p>
<span style="font-size: 12pt;"> </span>
<p>
	<span style="font-size: 12pt;"> </span><span style="font-weight: 700; font-size: 12pt;">3. Внешний вид, качество пошива</span><span style="font-size: 12pt;"> </span>
</p>
<span style="font-size: 12pt;"> </span>
<p>
	<span style="font-size: 12pt;"> </span><br>
	<span style="font-size: 12pt;"> </span>
</p>
<span style="font-size: 12pt;"> </span>
<p>
	<span style="font-size: 12pt;"> </span><span style="font-weight: 400; font-size: 12pt;">По внешнему виду можно уже многое сказать о качестве футболки.</span><span style="font-size: 12pt;"> </span>
</p>
<span style="font-size: 12pt;"> </span>
<p>
	<span style="font-size: 12pt;"> </span><br>
	<span style="font-size: 12pt;"> </span>
</p>
<span style="font-size: 12pt;"> </span>
<p>
	<span style="font-size: 12pt;"> </span><span style="font-weight: 400; font-size: 12pt;">На что обращать внимание:</span><span style="font-size: 12pt;"> </span>
</p>
<span style="font-size: 12pt;"> </span>
<p>
	<span style="font-size: 12pt;"> </span><br>
	<span style="font-size: 12pt;"> </span>
</p>
<span style="font-size: 12pt;"> </span>
<p>
	<span style="font-size: 12pt;"> </span><span style="font-weight: 400; font-size: 12pt;">- Пропорции. Ширина не должна быть больше длины, рукава одинаковые, плечи одинаковые, низ ровный. Померьте футболку. Если низ задирается, а крой перекашивается, закручивается в спираль - изделие некачественное.</span><span style="font-size: 12pt;"> </span>
</p>
<span style="font-size: 12pt;"> </span>
<p>
	<span style="font-size: 12pt;"> </span><br>
	<span style="font-size: 12pt;"> </span>
</p>
<span style="font-size: 12pt;"> </span>
<p>
	<span style="font-size: 12pt;"> </span><span style="font-weight: 400; font-size: 12pt;">- Цвет, усадка. Должен быть одинаковый во всей партии. Не поленитесь, постирайте футболку. Вам может открыться масса интересных подробностей.</span><span style="font-size: 12pt;"> </span>
</p>
<span style="font-size: 12pt;"> </span>
<p>
	<span style="font-size: 12pt;"> </span><br>
	<span style="font-size: 12pt;"> </span>
</p>
<span style="font-size: 12pt;"> </span>
<p>
	<span style="font-size: 12pt;"> </span><span style="font-weight: 400; font-size: 12pt;">- Разница между размерами. Возьмите несколько размеров, сложите их между собой. Каждый последующий должен быть больше предыдущего и по длине, и по ширине.</span><span style="font-size: 12pt;"> </span>
</p>
<span style="font-size: 12pt;"> </span>
<p>
	<span style="font-size: 12pt;"> </span><br>
	<span style="font-size: 12pt;"> </span>
</p>
<span style="font-size: 12pt;"> </span>
<p>
	<span style="font-size: 12pt;"> </span><span style="font-weight: 400; font-size: 12pt;">- Швы: ровные. Обязательна укрепляющая тесьма на горловине - от плеча к плечу.</span><span style="font-size: 12pt;"> </span>
</p>
<span style="font-size: 12pt;"> </span>
<p>
	<span style="font-size: 12pt;"> </span><br>
	<span style="font-size: 12pt;"> </span>
</p>
<span style="font-size: 12pt;"> </span>
<p>
	<span style="font-size: 12pt;"> </span><span style="font-weight: 400; font-size: 12pt;">- Рибана (резинка на горле). Немного растяните её и посмотрите на свет. Если увидите блестящие бесцветные нити внутри - значит, в резинку добавлена лайкра и после стирки она не растянется.</span><span style="font-size: 12pt;"> </span>
</p>
<span style="font-size: 12pt;"> </span>
<p>
	<span style="font-size: 12pt;"> </span><br>
	<span style="font-size: 12pt;"> </span>
</p>
<span style="font-size: 12pt;"> </span>
<p>
	<span style="font-size: 12pt;"> </span><span style="font-weight: 700; font-size: 12pt;">4. Качество полотна</span><span style="font-size: 12pt;"> </span>
</p>
<span style="font-size: 12pt;"> </span>
<p>
	<span style="font-size: 12pt;"> </span><br>
	<span style="font-size: 12pt;"> </span>
</p>
<span style="font-size: 12pt;"> </span>
<p>
	<span style="font-size: 12pt;"> </span><span style="font-weight: 400; font-size: 12pt;">Это </span><span style="font-weight: 700; font-size: 12pt;">основной момент</span><span style="font-weight: 400; font-size: 12pt;">, который должен быть определяющим при выборе футболки для печати. На качестве полотна экономят как известные брэнды, так и неизвестные. Быстро и правильно качество полотна могут определить далеко не все.</span><span style="font-size: 12pt;"> </span>
</p>
<span style="font-size: 12pt;"> </span>
<p>
	<span style="font-size: 12pt;"> </span><br>
	<span style="font-size: 12pt;"> </span>
</p>
<span style="font-size: 12pt;"> </span>
<p>
	<span style="font-size: 12pt;"> </span><span style="font-weight: 400; font-size: 12pt;">- Вид пряжи. Есть три основных уровня качества пряжи: openened (или о.е.), carded (кардная) и combed (гребенная, от англ. сomb - гребень, расчёска).</span><span style="font-size: 12pt;"> </span>
</p>
<span style="font-size: 12pt;"> </span>
<p>
	<span style="font-size: 12pt;"> </span><br>
	<span style="font-size: 12pt;"> </span>
</p>
<span style="font-size: 12pt;"> </span>
<p>
	<span style="font-size: 12pt;"> </span><span style="font-weight: 400; font-size: 12pt;">Первая - грубая, плохо очищенная от примесей. Нити получаются толстые, полотно рыхлое, с примесями мусора.</span><span style="font-size: 12pt;"> </span>
</p>
<span style="font-size: 12pt;"> </span>
<p>
	<span style="font-size: 12pt;"> </span><br>
	<span style="font-size: 12pt;"> </span>
</p>
<span style="font-size: 12pt;"> </span>
<p>
	<span style="font-size: 12pt;"> </span><span style="font-weight: 400; font-size: 12pt;">Кардная пряжа делается из более дорогого хлопка, он уже очищен от примесей, но ещё недостаточно мягок.</span><span style="font-size: 12pt;"> </span>
</p>
<span style="font-size: 12pt;"> </span>
<p>
	<span style="font-size: 12pt;"> </span><br>
	<span style="font-size: 12pt;"> </span>
</p>
<span style="font-size: 12pt;"> </span>
<p>
	<span style="font-size: 12pt;"> </span><span style="font-weight: 400; font-size: 12pt;">И третий уровень, </span><span style="font-weight: 700; font-size: 12pt;">высший сорт - это гребенная пряжа</span><span style="font-weight: 400; font-size: 12pt;">, делается из самых дорогих сортов, с длинным волокном, которое хорошенько прочёсывают, вытягивают и скручивают в тончайшие нити. Полотно получается тонким и нежным.</span><span style="font-size: 12pt;"> </span>
</p>
<span style="font-size: 12pt;"> </span>
<p>
	<span style="font-size: 12pt;"> </span><br>
	<span style="font-size: 12pt;"> </span>
</p>
<span style="font-size: 12pt;"> </span>
<p>
	<span style="font-size: 12pt;"> </span><span style="font-weight: 400; font-size: 12pt;">Все любят писать, что используют «гребенной суперчесанный хлопок». Причём пишут это даже про дешёвые промо-футболки. Скорее всего, это не так, потому что гребенная пряжа дорогая. Мы делаем из гребенной пряжи только футболки марки «Vivezza». Для остальных используем более дешёвую кардную.</span><span style="font-size: 12pt;"> </span>
</p>
<span style="font-size: 12pt;"> </span>
<p>
 <br>
</p> 	</div>";s:4:"VARS";a:2:{s:8:"arResult";a:12:{s:2:"ID";s:1:"2";s:9:"IBLOCK_ID";s:1:"1";s:4:"NAME";s:106:"Как выбрать качественную футболку для нанесения логотипа";s:17:"IBLOCK_SECTION_ID";N;s:6:"IBLOCK";a:88:{s:2:"ID";s:1:"1";s:3:"~ID";s:1:"1";s:11:"TIMESTAMP_X";s:19:"10.04.2019 09:34:50";s:12:"~TIMESTAMP_X";s:19:"10.04.2019 09:34:50";s:14:"IBLOCK_TYPE_ID";s:7:"content";s:15:"~IBLOCK_TYPE_ID";s:7:"content";s:3:"LID";s:2:"s1";s:4:"~LID";s:2:"s1";s:4:"CODE";s:23:"unigarderob_articles_s1";s:5:"~CODE";s:23:"unigarderob_articles_s1";s:4:"NAME";s:12:"Статьи";s:5:"~NAME";s:12:"Статьи";s:6:"ACTIVE";s:1:"Y";s:7:"~ACTIVE";s:1:"Y";s:4:"SORT";s:3:"500";s:5:"~SORT";s:3:"500";s:13:"LIST_PAGE_URL";s:14:"/help/article/";s:14:"~LIST_PAGE_URL";s:14:"/help/article/";s:15:"DETAIL_PAGE_URL";s:39:"#SITE_DIR#/help/article/#SECTION_CODE#/";s:16:"~DETAIL_PAGE_URL";s:39:"#SITE_DIR#/help/article/#SECTION_CODE#/";s:16:"SECTION_PAGE_URL";N;s:17:"~SECTION_PAGE_URL";N;s:18:"CANONICAL_PAGE_URL";N;s:19:"~CANONICAL_PAGE_URL";N;s:7:"PICTURE";N;s:8:"~PICTURE";N;s:11:"DESCRIPTION";s:0:"";s:12:"~DESCRIPTION";N;s:16:"DESCRIPTION_TYPE";s:4:"text";s:17:"~DESCRIPTION_TYPE";s:4:"text";s:7:"RSS_TTL";s:2:"24";s:8:"~RSS_TTL";s:2:"24";s:10:"RSS_ACTIVE";s:1:"Y";s:11:"~RSS_ACTIVE";s:1:"Y";s:15:"RSS_FILE_ACTIVE";s:1:"N";s:16:"~RSS_FILE_ACTIVE";s:1:"N";s:14:"RSS_FILE_LIMIT";N;s:15:"~RSS_FILE_LIMIT";N;s:13:"RSS_FILE_DAYS";N;s:14:"~RSS_FILE_DAYS";N;s:17:"RSS_YANDEX_ACTIVE";s:1:"N";s:18:"~RSS_YANDEX_ACTIVE";s:1:"N";s:6:"XML_ID";s:23:"unigarderob_articles_s1";s:7:"~XML_ID";s:23:"unigarderob_articles_s1";s:6:"TMP_ID";s:32:"ae778d475fdec2ac0a814f27d9a1113a";s:7:"~TMP_ID";s:32:"ae778d475fdec2ac0a814f27d9a1113a";s:13:"INDEX_ELEMENT";s:1:"Y";s:14:"~INDEX_ELEMENT";s:1:"Y";s:13:"INDEX_SECTION";s:1:"Y";s:14:"~INDEX_SECTION";s:1:"Y";s:8:"WORKFLOW";s:1:"N";s:9:"~WORKFLOW";s:1:"N";s:7:"BIZPROC";s:1:"N";s:8:"~BIZPROC";s:1:"N";s:15:"SECTION_CHOOSER";s:1:"L";s:16:"~SECTION_CHOOSER";s:1:"L";s:9:"LIST_MODE";N;s:10:"~LIST_MODE";N;s:11:"RIGHTS_MODE";s:1:"S";s:12:"~RIGHTS_MODE";s:1:"S";s:16:"SECTION_PROPERTY";s:1:"Y";s:17:"~SECTION_PROPERTY";s:1:"Y";s:14:"PROPERTY_INDEX";s:1:"N";s:15:"~PROPERTY_INDEX";s:1:"N";s:7:"VERSION";s:1:"1";s:8:"~VERSION";s:1:"1";s:17:"LAST_CONV_ELEMENT";s:1:"0";s:18:"~LAST_CONV_ELEMENT";s:1:"0";s:15:"SOCNET_GROUP_ID";N;s:16:"~SOCNET_GROUP_ID";N;s:16:"EDIT_FILE_BEFORE";N;s:17:"~EDIT_FILE_BEFORE";N;s:15:"EDIT_FILE_AFTER";N;s:16:"~EDIT_FILE_AFTER";N;s:13:"SECTIONS_NAME";s:14:"Разделы";s:14:"~SECTIONS_NAME";s:14:"Разделы";s:12:"SECTION_NAME";s:12:"Раздел";s:13:"~SECTION_NAME";s:12:"Раздел";s:13:"ELEMENTS_NAME";s:16:"Элементы";s:14:"~ELEMENTS_NAME";s:16:"Элементы";s:12:"ELEMENT_NAME";s:14:"Элемент";s:13:"~ELEMENT_NAME";s:14:"Элемент";s:11:"EXTERNAL_ID";s:23:"unigarderob_articles_s1";s:12:"~EXTERNAL_ID";s:23:"unigarderob_articles_s1";s:8:"LANG_DIR";s:1:"/";s:9:"~LANG_DIR";s:1:"/";s:11:"SERVER_NAME";s:23:"beeru.analyticumplus.ru";s:12:"~SERVER_NAME";s:23:"beeru.analyticumplus.ru";}s:13:"LIST_PAGE_URL";s:14:"/help/article/";s:14:"~LIST_PAGE_URL";s:14:"/help/article/";s:11:"SECTION_URL";s:0:"";s:7:"SECTION";a:1:{s:4:"PATH";a:0:{}}s:16:"IPROPERTY_VALUES";a:0:{}s:11:"TIMESTAMP_X";s:19:"06.05.2019 21:43:22";s:9:"META_TAGS";a:5:{s:5:"TITLE";s:106:"Как выбрать качественную футболку для нанесения логотипа";s:13:"ELEMENT_CHAIN";s:106:"Как выбрать качественную футболку для нанесения логотипа";s:13:"BROWSER_TITLE";s:106:"Как выбрать качественную футболку для нанесения логотипа";s:8:"KEYWORDS";s:0:"";s:11:"DESCRIPTION";s:0:"";}}s:18:"templateCachedData";a:3:{s:13:"additionalCSS";s:98:"/bitrix/templates/unigarderob_s1/components/bitrix/news/news/bitrix/news.detail/.default/style.css";s:9:"frameMode";b:1;s:8:"__NavNum";i:1;}}}';
return true;
?>