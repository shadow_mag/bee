<?
if($INCLUDE_FROM_CACHE!='Y')return false;
$datecreate = '001564740882';
$dateexpire = '001600740882';
$ser_content = 'a:2:{s:7:"CONTENT";s:8014:"<div class="contact">
	<div class="section section-left">
					<div class="field" itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
				<div class="title">Адрес:</div>
				<div class="uni-indents-vertical indent-10"></div>
				<div class="text">
					<span itemprop="postalCode">115088,</span>
					<span itemprop="addressLocality">Москва,</span>
					<span itemprop="streetAddress">3-й Угрешкий проезд, 8с9</span>
				</div>
			</div>
			<div class="uni-indents-vertical indent-50"></div>
							<div class="field">
				<div class="title">Телефон:</div>
				<div class="uni-indents-vertical indent-10"></div>
									<div class="text">+7 495 280-07-62</div>
									<div class="text">+7 977 687-81-61</div>
							</div>
			<div class="uni-indents-vertical indent-50"></div>
							<div class="field">
				<div class="title">Часы работы:</div>
				<div class="uni-indents-vertical indent-10"></div>
				<div class="text">С 9:00 до 18:00 (Прием заказов круглосуточно)</div>
			</div>
			<div class="uni-indents-vertical indent-50"></div>
							<div class="field">
				<div class="title">Электронная почта:</div>
				<div class="uni-indents-vertical indent-10"></div>
				<div class="text">sale@bee-ru.ru</div>
			</div>
			</div>
	<div class="section section-right">
					<div class="description">
				<span style="font-size: 14pt;"><b>Как добраться?</b></span><br>
 <br>
 <span style="font-size: 12pt;">Наш шоурум находится на территории БЦ&nbsp;"Techno-Loft" по адресу 3-й Угрешский проезд 8с9</span><span style="font-size: 12pt;">.&nbsp;</span><br>
 <br>
 <span style="font-size: 12pt;">Добраться можно на автомобиле, или по&nbsp;МЦК до&nbsp;станции&nbsp;Угрешская. Далее пешком&nbsp;650 метров</span><span style="font-size: 12pt;">&nbsp;по улице 3-й Угрешский проезд&nbsp; до проходной</span><span style="font-size: 12pt;">&nbsp;БЦ. Для прохода на территорию БЦ необходимо оформить пропуск у менеджера по телефону. При себе иметь документ удостоверяющий личность.<br>
 <br>
 <br>
 </span>			</div>
		<div class="clear"></div>
		<div class="uni-indents-vertical indent-20"></div>
							<div class="map">
				<script type="text/javascript">
function BX_SetPlacemarks_MAP_YANDEX_VIEW_7_31_342332723(map)
{
	if(typeof window["BX_YMapAddPlacemark"] != \'function\')
	{
		/* If component\'s result was cached as html,
		 * script.js will not been loaded next time.
		 * let\'s do it manualy.
		*/

		(function(d, s, id)
		{
			var js, bx_ym = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) return;
			js = d.createElement(s); js.id = id;
			js.src = "/bitrix/templates/unigarderob_s1/components/bitrix/map.yandex.view/.default/script.js";
			bx_ym.parentNode.insertBefore(js, bx_ym);
		}(document, \'script\', \'bx-ya-map-js\'));

		var ymWaitIntervalId = setInterval( function(){
				if(typeof window["BX_YMapAddPlacemark"] == \'function\')
				{
					BX_SetPlacemarks_MAP_YANDEX_VIEW_7_31_342332723(map);
					clearInterval(ymWaitIntervalId);
				}
			}, 300
		);

		return;
	}

	var arObjects = {PLACEMARKS:[],POLYLINES:[]};
	arObjects.PLACEMARKS[arObjects.PLACEMARKS.length] = BX_YMapAddPlacemark(map, {\'LON\':\'37.698322226701\',\'LAT\':\'55.712323728918\',\'TEXT\':\'\'});
}
</script>
<div class="bx-yandex-view-layout">
	<div class="bx-yandex-view-map">
<script type="text/javascript">

if (!window.GLOBAL_arMapObjects)
	window.GLOBAL_arMapObjects = {};

function init_MAP_YANDEX_VIEW_7_31_342332723()
{
	if (!window.ymaps)
		return;

	if(typeof window.GLOBAL_arMapObjects[\'MAP_YANDEX_VIEW_7_31_342332723\'] !== "undefined")
		return;

	var node = BX("BX_YMAP_MAP_YANDEX_VIEW_7_31_342332723");
	node.innerHTML = \'\';

	var map = window.GLOBAL_arMapObjects[\'MAP_YANDEX_VIEW_7_31_342332723\'] = new ymaps.Map(node, {
		center: [55.712323728918, 37.698322226701],
		zoom: 10,
		type: \'yandex#map\'
	});

	map.behaviors.enable("scrollZoom");
	map.behaviors.enable("dblClickZoom");
	map.behaviors.enable("drag");
	if (map.behaviors.isEnabled("rightMouseButtonMagnifier"))
		map.behaviors.disable("rightMouseButtonMagnifier");
	map.controls.add(\'zoomControl\');
	map.controls.add(\'miniMap\');
	map.controls.add(\'typeSelector\');
	map.controls.add(\'scaleLine\');
	window.bYandexMapScriptsLoaded = true;
	if (window.BX_SetPlacemarks_MAP_YANDEX_VIEW_7_31_342332723)
	{
		window.BX_SetPlacemarks_MAP_YANDEX_VIEW_7_31_342332723(map);
	}
}
function BXMapLoader_MAP_YANDEX_VIEW_7_31_342332723()
{
	if (null == window.bYandexMapScriptsLoaded)
	{
		function _wait_for_map(){
			if (window.ymaps && window.ymaps.Map)
				init_MAP_YANDEX_VIEW_7_31_342332723();
			else
				setTimeout(_wait_for_map, 50);
		}

		BX.loadScript(\'http://api-maps.yandex.ru/2.0/?load=package.full&mode=release&lang=ru-RU&wizard=bitrix\', _wait_for_map);
	}
	else
	{
		init_MAP_YANDEX_VIEW_7_31_342332723();
	}
}
	BX.ready(BXMapLoader_MAP_YANDEX_VIEW_7_31_342332723);

/* if map inits in hidden block (display:none)
*  after the block showed
*  for properly showing map this function must be called
*/
function BXMapYandexAfterShow(mapId)
{
	if(window.GLOBAL_arMapObjects[mapId] !== undefined)
		window.GLOBAL_arMapObjects[mapId].container.fitToViewport();
}

</script>
<div id="BX_YMAP_MAP_YANDEX_VIEW_7_31_342332723" class="bx-yandex-map" style="height: 500px; width: 600px;">загрузка карты...</div>	</div>
</div>
			</div>
			<div class="clear"></div>
							<div class="images uni_parent_col">
														<a rel="images_305" class="image uni_col uni-25 fancy" href="/upload/resize_cache/iblock/0f7/500_500_0/0f7338953e48b0e285459067ee2ccf9f.png">
						<img src="/upload/resize_cache/iblock/0f7/500_500_0/0f7338953e48b0e285459067ee2ccf9f.png" />
					</a>
																			<a rel="images_305" class="image uni_col uni-25 fancy" href="/upload/resize_cache/iblock/208/500_500_0/2084fecc247ad6510767d079b1bfd006.png">
						<img src="/upload/resize_cache/iblock/208/500_500_0/2084fecc247ad6510767d079b1bfd006.png" />
					</a>
																			<a rel="images_305" class="image uni_col uni-25 fancy" href="/upload/resize_cache/iblock/986/500_500_0/986514562e8d504828e9fa451923e121.png">
						<img src="/upload/resize_cache/iblock/986/500_500_0/986514562e8d504828e9fa451923e121.png" />
					</a>
																			<a rel="images_305" class="image uni_col uni-25 fancy" href="/upload/resize_cache/iblock/88b/500_500_0/88bc9f1ae060c5ecfcc6dc2897c0e458.png">
						<img src="/upload/resize_cache/iblock/88b/500_500_0/88bc9f1ae060c5ecfcc6dc2897c0e458.png" />
					</a>
																			<a rel="images_305" class="image uni_col uni-25 fancy" href="/upload/resize_cache/iblock/a4f/500_500_0/a4f40e9d41f4947b50eb7c5b018913e8.png">
						<img src="/upload/resize_cache/iblock/a4f/500_500_0/a4f40e9d41f4947b50eb7c5b018913e8.png" />
					</a>
																			<a rel="images_305" class="image uni_col uni-25 fancy" href="/upload/resize_cache/iblock/167/500_500_0/1674c4a052ec7a65e371b5eee1f4fea6.png">
						<img src="/upload/resize_cache/iblock/167/500_500_0/1674c4a052ec7a65e371b5eee1f4fea6.png" />
					</a>
																			<a rel="images_305" class="image uni_col uni-25 fancy" href="/upload/resize_cache/iblock/57e/500_500_0/57ea989c47e2aeec63d7cb43a828101a.png">
						<img src="/upload/resize_cache/iblock/57e/500_500_0/57ea989c47e2aeec63d7cb43a828101a.png" />
					</a>
																			<a rel="images_305" class="image uni_col uni-25 fancy" href="/upload/resize_cache/iblock/5eb/500_500_0/5eb292ad2e274d09a3cf665518609c37.png">
						<img src="/upload/resize_cache/iblock/5eb/500_500_0/5eb292ad2e274d09a3cf665518609c37.png" />
					</a>
												</div>
			<div class="clear"></div>
			</div>
	<div class="clear"></div>
</div>";s:4:"VARS";a:2:{s:8:"arResult";a:12:{s:2:"ID";s:3:"305";s:9:"IBLOCK_ID";s:1:"7";s:4:"NAME";s:54:"Москва, 3-й Угрешкий проезд, 8с9";s:17:"IBLOCK_SECTION_ID";N;s:6:"IBLOCK";a:88:{s:2:"ID";s:1:"7";s:3:"~ID";s:1:"7";s:11:"TIMESTAMP_X";s:19:"17.04.2019 22:06:01";s:12:"~TIMESTAMP_X";s:19:"17.04.2019 22:06:01";s:14:"IBLOCK_TYPE_ID";s:7:"content";s:15:"~IBLOCK_TYPE_ID";s:7:"content";s:3:"LID";s:2:"s1";s:4:"~LID";s:2:"s1";s:4:"CODE";s:23:"unigarderob_contacts_s1";s:5:"~CODE";s:23:"unigarderob_contacts_s1";s:4:"NAME";s:16:"Контакты";s:5:"~NAME";s:16:"Контакты";s:6:"ACTIVE";s:1:"Y";s:7:"~ACTIVE";s:1:"Y";s:4:"SORT";s:3:"500";s:5:"~SORT";s:3:"500";s:13:"LIST_PAGE_URL";s:10:"/contacts/";s:14:"~LIST_PAGE_URL";s:10:"/contacts/";s:15:"DETAIL_PAGE_URL";s:33:"#SITE_DIR#/contacts/#ELEMENT_ID#/";s:16:"~DETAIL_PAGE_URL";s:33:"#SITE_DIR#/contacts/#ELEMENT_ID#/";s:16:"SECTION_PAGE_URL";s:20:"#SITE_DIR#/contacts/";s:17:"~SECTION_PAGE_URL";s:20:"#SITE_DIR#/contacts/";s:18:"CANONICAL_PAGE_URL";N;s:19:"~CANONICAL_PAGE_URL";N;s:7:"PICTURE";N;s:8:"~PICTURE";N;s:11:"DESCRIPTION";s:0:"";s:12:"~DESCRIPTION";N;s:16:"DESCRIPTION_TYPE";s:4:"text";s:17:"~DESCRIPTION_TYPE";s:4:"text";s:7:"RSS_TTL";s:2:"24";s:8:"~RSS_TTL";s:2:"24";s:10:"RSS_ACTIVE";s:1:"Y";s:11:"~RSS_ACTIVE";s:1:"Y";s:15:"RSS_FILE_ACTIVE";s:1:"N";s:16:"~RSS_FILE_ACTIVE";s:1:"N";s:14:"RSS_FILE_LIMIT";N;s:15:"~RSS_FILE_LIMIT";N;s:13:"RSS_FILE_DAYS";N;s:14:"~RSS_FILE_DAYS";N;s:17:"RSS_YANDEX_ACTIVE";s:1:"N";s:18:"~RSS_YANDEX_ACTIVE";s:1:"N";s:6:"XML_ID";s:23:"unigarderob_contacts_s1";s:7:"~XML_ID";s:23:"unigarderob_contacts_s1";s:6:"TMP_ID";s:32:"6f2d1cdd290817eaa765e8435f500f1d";s:7:"~TMP_ID";s:32:"6f2d1cdd290817eaa765e8435f500f1d";s:13:"INDEX_ELEMENT";s:1:"Y";s:14:"~INDEX_ELEMENT";s:1:"Y";s:13:"INDEX_SECTION";s:1:"Y";s:14:"~INDEX_SECTION";s:1:"Y";s:8:"WORKFLOW";s:1:"N";s:9:"~WORKFLOW";s:1:"N";s:7:"BIZPROC";s:1:"N";s:8:"~BIZPROC";s:1:"N";s:15:"SECTION_CHOOSER";s:1:"L";s:16:"~SECTION_CHOOSER";s:1:"L";s:9:"LIST_MODE";N;s:10:"~LIST_MODE";N;s:11:"RIGHTS_MODE";s:1:"S";s:12:"~RIGHTS_MODE";s:1:"S";s:16:"SECTION_PROPERTY";s:1:"Y";s:17:"~SECTION_PROPERTY";s:1:"Y";s:14:"PROPERTY_INDEX";s:1:"N";s:15:"~PROPERTY_INDEX";s:1:"N";s:7:"VERSION";s:1:"1";s:8:"~VERSION";s:1:"1";s:17:"LAST_CONV_ELEMENT";s:1:"0";s:18:"~LAST_CONV_ELEMENT";s:1:"0";s:15:"SOCNET_GROUP_ID";N;s:16:"~SOCNET_GROUP_ID";N;s:16:"EDIT_FILE_BEFORE";N;s:17:"~EDIT_FILE_BEFORE";N;s:15:"EDIT_FILE_AFTER";N;s:16:"~EDIT_FILE_AFTER";N;s:13:"SECTIONS_NAME";s:14:"Разделы";s:14:"~SECTIONS_NAME";s:14:"Разделы";s:12:"SECTION_NAME";s:12:"Раздел";s:13:"~SECTION_NAME";s:12:"Раздел";s:13:"ELEMENTS_NAME";s:16:"Элементы";s:14:"~ELEMENTS_NAME";s:16:"Элементы";s:12:"ELEMENT_NAME";s:14:"Элемент";s:13:"~ELEMENT_NAME";s:14:"Элемент";s:11:"EXTERNAL_ID";s:23:"unigarderob_contacts_s1";s:12:"~EXTERNAL_ID";s:23:"unigarderob_contacts_s1";s:8:"LANG_DIR";s:1:"/";s:9:"~LANG_DIR";s:1:"/";s:11:"SERVER_NAME";s:23:"beeru.analyticumplus.ru";s:12:"~SERVER_NAME";s:23:"beeru.analyticumplus.ru";}s:13:"LIST_PAGE_URL";s:10:"/contacts/";s:14:"~LIST_PAGE_URL";s:10:"/contacts/";s:11:"SECTION_URL";s:0:"";s:7:"SECTION";a:1:{s:4:"PATH";a:0:{}}s:16:"IPROPERTY_VALUES";a:0:{}s:11:"TIMESTAMP_X";s:19:"01.08.2019 14:38:07";s:9:"META_TAGS";a:4:{s:5:"TITLE";s:54:"Москва, 3-й Угрешкий проезд, 8с9";s:13:"BROWSER_TITLE";s:0:"";s:8:"KEYWORDS";s:0:"";s:11:"DESCRIPTION";s:0:"";}}s:18:"templateCachedData";a:4:{s:13:"additionalCSS";s:102:"/bitrix/templates/unigarderob_s1/components/bitrix/news/contacts/bitrix/news.detail/.default/style.css";s:9:"frameMode";b:1;s:8:"__NavNum";i:1;s:17:"__currentCounters";a:2:{s:22:"bitrix:map.yandex.view";i:1;s:24:"bitrix:map.yandex.system";i:1;}}}}';
return true;
?>