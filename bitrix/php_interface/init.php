<?php
ini_set('display_errors','Off');
include_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/wsrubi.smtp/classes/general/wsrubismtp.php");

AddEventHandler("iblock", "OnBeforeIBlockElementUpdate","DoNotUpdate"); 
function DoNotUpdate(&$arFields) 
{ 
    if ($_REQUEST['mode']=='import') 
    { 
        // unset($arFields['PREVIEW_PICTURE']); 
        // unset($arFields['DETAIL_PICTURE']); 
        unset($arFields['PREVIEW_TEXT']);
        unset($arFields['DETAIL_TEXT']); 
        $arFields['DETAIL_TEXT_TYPE'] = 'html';
        $arFields['PREVIEW_TEXT_TYPE'] = 'html';
    } 
} 
AddEventHandler("iblock", "OnBeforeIBlockElementAdd","DoNotAdd"); 
function DoNotAdd(&$arFields) 
{ 
    if ($_REQUEST['mode']=='import') 
    { 
        // unset($arFields['PREVIEW_PICTURE']); 
        // unset($arFields['DETAIL_PICTURE']); 
        unset($arFields['PREVIEW_TEXT']);
        unset($arFields['DETAIL_TEXT']); 
        $arFields['DETAIL_TEXT_TYPE'] = 'html';
        $arFields['PREVIEW_TEXT_TYPE'] = 'html';
    } 
} 
