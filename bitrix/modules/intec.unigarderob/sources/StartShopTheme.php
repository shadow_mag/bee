<?
	$sSiteID = SITE_ID;
	$sSiteTheme = strToLower(static::getOptionValue($sSiteID,"COLOR_THEME"));
	
	if ($sSiteTheme == 'custom' || empty($sSiteTheme)) {
		$sCustomColor = static::getOptionValue($sSiteID, "CUSTOM_COLOR");	
	} else {
		$arOptions = static::getOptionsValue($sSiteID);
		$arTheme = $arOptions['COLOR_THEME']['VALUE'][$arOptions['COLOR_THEME']['ACTIVE_VALUE']];
		$sCustomColor = $arTheme['VALUE'];
	}
	
	if (CModule::IncludeModule('intec.startshop'))
		CStartShopTheme::SetColors(array(
			CStartShopVariables::$arThemeColors['startshop-color']['LESS_VARIABLE_NAME'] => $sCustomColor,
			CStartShopVariables::$arThemeColors['startshop-color-dark']['LESS_VARIABLE_NAME'] => CStartShopVariables::$arThemeColors['startshop-color-dark']['DEFAULT'],
			CStartShopVariables::$arThemeColors['startshop-color-input-text-standart-color']['LESS_VARIABLE_NAME'] => CStartShopVariables::$arThemeColors['startshop-color-input-text-standart-color']['DEFAULT'],
			CStartShopVariables::$arThemeColors['startshop-color-input-text-standart-background']['LESS_VARIABLE_NAME'] => CStartShopVariables::$arThemeColors['startshop-color-input-text-standart-background']['DEFAULT'],
		), SITE_ID);
?>