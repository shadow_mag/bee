<?
	$MESS['MODULE_NAME'] = "Uni Garderob";
    $MESS["MODULE_DESCRIPTION"] = "Мастер создания магазина одежды";
	$MESS["PARTNER_NAME"] = "Intec";
    $MESS["PARTNER_URI"] = "http://www.intecweb.ru";
    $MESS["SCOM_INSTALL_TITLE"] = "Установка модуля";
    $MESS["SCOM_UNINSTALL_TITLE"] = "Удаление модуля";
    $MESS["MODULE_GARDEROB_INSTALL_OK"] = "Поздравляем, модуль «Интернет магазин одежды UNI Garderob» успешно установлен!<br />
    Для установки готового сайта, пожалуйста перейдите <a href='/bitrix/admin/wizard_list.php?lang=ru'>в список мастеров</a> <br />и выберите пункт «Установить» в меню мастера intec:unigarderob";
    $MESS["MOD_UNINST_OK"] = "Удаление модуля успешно завершено";
    $MESS["OPEN_WIZARDS_LIST"] = "Открыть список мастеров";
    $MESS["MOD_BACK"] = "К списку модулей";
?>