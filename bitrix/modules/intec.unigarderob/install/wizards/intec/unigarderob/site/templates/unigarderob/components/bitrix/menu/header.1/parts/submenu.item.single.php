<div class="menu-header-1-subitem menu-header-1-subitem-level-<?=$iCurrentLevel?><?=!empty($arMenuItem['SELECTED']) ? ' ui-state-active' : ''?>">
    <a href="<?=$arMenuItem['LINK']?>" class="menu-header-1-subitem-wrapper ">
        <div class="uni-aligner-vertical"></div>
        <div class="menu-header-1-text solid_text">
            <?=$arMenuItem['TEXT']?>
        </div>
    </a>
</div>