<?$frame = $this->createFrame()->begin();?>
    <?
        $arJSBasketFly = array(
            "currency" => array(
                "format" => $arResult['CURRENCY']['FORMAT_STRING'],
                "delimiter" => array(
                    "decimals" => $arResult['CURRENCY']['DEC_POINT'],
                    "thousand" => empty($arResult['CURRENCY']['THOUSANDS_SEP']) ? ' ' : $arResult['CURRENCY']['THOUSANDS_SEP']
                ),
                "decimals" => $arResult['CURRENCY']['DECIMALS']
            ),
            "items" => array()
        );
        
        foreach ($arResult['ITEMS'] as $arItem) {
            if ($arItem['DELAY'] == 'Y') continue;
            $arJSBasketFly['items'][$arItem['ID']] = array(
                "quantity" => $arItem['QUANTITY'],
                "price" => $arItem['PRICE']
            );
        }
    ?>
    <script type="text/javascript">
        var UNIBasketFlySettings = <?=CUtil::PhpToJSObject($arJSBasketFly);?>;
        var UNIBasketFlyUpdate = function () {
            var $fSum = 0;
            
            var UNIBasketFlyFormatPrice = function ($fPrice) {
                return UNI.PHP.strReplace({
                        '#': UNI.PHP.numberFormat(
                            $fPrice,
                            UNIBasketFlySettings.currency.decimals,
                            UNIBasketFlySettings.currency.delimiter.decimals,
                            UNIBasketFlySettings.currency.delimiter.thousand
                        )
                    }, 
                    UNIBasketFlySettings.currency.format
                );
            }
            
            UNI.forEach(UNIBasketFlySettings.items, function ($sKey, $oObject) {
                $('.basket-fly-1 .basket-content .basket-sections .basket-section.basket .UNIBasketFlyItem' + $sKey + 'Quantity')
                    .val($oObject.quantity);
                
                $('.basket-fly-1 .basket-content .basket-sections .basket-section.basket .UNIBasketFlyItem' + $sKey + 'Total')
                    .html(UNIBasketFlyFormatPrice($oObject.price * $oObject.quantity));
                $fSum += parseFloat($oObject.price) * parseFloat($oObject.quantity);
            });
            
            $('.basket-fly-1 .basket-content .basket-sections .basket-section.basket .basket-sum')
                .html(UNIBasketFlyFormatPrice($fSum));
        }
    </script>
	<div class="basket-fly-1">
        <div class="basket-panel">
            <div class="basket-panel-wrapper">
                <div class="basket-element basket" onclick="oUNIBasketFly.setSectionBySelector('.basket');" title="<?=$arResult['TOTAL']['BASKET']['SUM_FORMATED']?>">
                    <?if($arResult['TOTAL']['BASKET']['COUNT'] > 0){?><div class="basket-text solid_text"><?=$arResult['TOTAL']['BASKET']['COUNT']?></div><?}?>
                    <div class="basket-icon"></div>
                </div>
                <div class="basket-delimiter"></div>
                <div class="basket-element wish-list" onclick="oUNIBasketFly.setSectionBySelector('.wish-list');">
                    <?if($arResult['TOTAL']['WISH_LIST']['COUNT'] > 0){?><div class="basket-text solid_text"><?=$arResult['TOTAL']['WISH_LIST']['COUNT']?></div><?}?>
                    <div class="basket-icon"></div>
                </div>
				<div class="basket-delimiter"></div>

					<div class="b_compare_fly">
						<?$APPLICATION->IncludeComponent("bitrix:catalog.compare.list", "fly", Array(
							"IBLOCK_TYPE" => "catalog",
								"IBLOCK_ID" => $arParams["IBLOCK_ID_COMPARE"],	
								"AJAX_MODE" => "N",	
								"AJAX_OPTION_JUMP" => "N",
								"AJAX_OPTION_STYLE" => "Y",	
								"AJAX_OPTION_HISTORY" => "N",	
								"DETAIL_URL" => "",
								"COMPARE_URL" => $arParams["COMPARE_URL"],
								"NAME" => $arParams["COMPARE_NAME"],	
								"AJAX_OPTION_ADDITIONAL" => "",	
								"TYPE" => $options["TYPE_BASKET"]["ACTIVE_VALUE"]
							),
							false
						);?>
					</div>

				<div class="basket-delimiter"></div>
				<div class="basket-element call" onclick="oUNIBasketFly.setSectionBySelector('#call-section');">
					<div class="basket-icon"></div>
				</div>
            </div>
        </div>
        <div class="basket-content">
            <div class="basket-content-wrapper">
                <div class="basket-sections">
                    <div class="basket-section basket"<?=$arResult['TOTAL']['BASKET']['COUNT'] == 0 ? ' style="width: 400px;"' : ''?>>
						<?if ($arResult['TOTAL']['BASKET']['COUNT'] > 0):?>
							<div class="basket-header solid_text">
								<?=GetMessage('BASKET_FLY_SECTIONS_BASKET_HEADER', array(
									'#COUNT#' => $arResult['TOTAL']['BASKET']['COUNT'],
									'#SUM#' => $arResult['TOTAL']['BASKET']['SUM_FORMATED']
								))?>
							</div>
							<div class="basket-body">
								<table class="basket-table">
									<tr class="basket-row basket-row-header">
										<td class="basket-column"></td>
										<td class="basket-column"><div class="basket-cell solid_text"><?=GetMessage('BASKET_FLY_SECTIONS_TABLE_HEADER_NAME');?></div></td>
										<td class="basket-column"><div class="basket-cell solid_text"><?=GetMessage('BASKET_FLY_SECTIONS_TABLE_HEADER_COUNT');?></div></td>
										<td class="basket-column"><div class="basket-cell solid_text"><?=GetMessage('BASKET_FLY_SECTIONS_TABLE_HEADER_PRICE');?></div></td>
										<td class="basket-column"><div class="basket-cell solid_text"><?=GetMessage('BASKET_FLY_SECTIONS_TABLE_HEADER_TOTAL');?></div></td>
										<td class="basket-column"></td>
									</tr>
									<?foreach ($arResult['ITEMS'] as $arItem):?>
										<?if ($arItem['DELAY'] == 'Y') continue;?>
										<?if (empty($arItem['PRODUCT'])) continue;?>
										<?
											$sPicture = SITE_TEMPLATE_PATH.'/images/noimg/no-img.png';
											
											if (!empty($arItem['DETAIL_PICTURE']))
												$sPicture = $arItem['DETAIL_PICTURE']['SRC'];
												
											if (!empty($arItem['PREVIEW_PICTURE']))
												$sPicture = $arItem['PREVIEW_PICTURE']['SRC'];
										?>
										<tr class="basket-row">
											<td class="basket-column"><div class="basket-cell">
												<a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="uni-image" style="display: block; width: 70px; height: 70px; overflow: hidden;">
													<div class="uni-aligner-vertical"></div>
													<img src="<?=$sPicture?>" alt="<?=$arItem['NAME']?>" />
												</a>
											</div></td>
											<td class="basket-column"><div class="basket-cell">
												<a href="<?=$arItem['DETAIL_PAGE_URL']?>"><?=$arItem['NAME'];?></a>
											</div></td>
											<td class="basket-column"><div class="basket-cell">
												<?
													$arJSNumeric = array(
														"VALUE" => floatval($arItem['QUANTITY']),
														"MINIMUM" => floatval($arItem['PRODUCT']['MEASURE_RATIO']),
														"MAXIMUM" => floatval($arItem['PRODUCT']['QUANTITY']),
														"RATIO" => floatval($arItem['PRODUCT']['MEASURE_RATIO']),
														"UNLIMITED" => !($arItem['PRODUCT']['QUANTITY_TRACE'] == 'Y' && $arItem['PRODUCT']['CAN_BUY_ZERO'] == 'N'),
														"VALUE_TYPE" => "FLOAT"
													);
												?>
												<div class="basket-numeric">
													<button class="basket-numeric-button basket-numeric-button-decrease" onclick="UNIBasketFlyNumeric<?=$arItem['ID']?>.Decrease();">-</button>
													<input class="basket-numeric-input UNIBasketFlyItem<?=$arItem['ID']?>Quantity" onchange="UNIBasketFlyNumeric<?=$arItem['ID']?>.SetValue($(this).val())" type="text" value="<?=$arItem['QUANTITY']?>" style="width: 40px;" />
													<button class="basket-numeric-button basket-numeric-button-increase" onclick="UNIBasketFlyNumeric<?=$arItem['ID']?>.Increase();">+</button>
												</div>
												<script type="text/javascript">
													var UNIBasketFlyNumeric<?=$arItem['ID']?> = new UNINumericUpDown(<?=CUtil::PhpToJSObject($arJSNumeric);?>);
													UNIBasketFlyNumeric<?=$arItem['ID']?>.Settings.EVENTS.onValueChange = function ($oNumeric) {
														UNIBasketFlySettings.items['<?=$arItem['ID']?>'].quantity = $oNumeric.GetValue();
														SaleActions.setQuantity(<?=intval($arItem['PRODUCT']['ID'])?>, $oNumeric.GetValue(), function ($sResponse) {
															UNIBasketFlySettings.items['<?=$arItem['ID']?>'].quantity = parseFloat($sResponse);
															UNIBasketFlyUpdate();
														});
														UNIBasketFlyUpdate();
													}
												</script>
											</div></td>
											<td class="basket-column"><div class="basket-cell basket-cell-nowrap">
												<?=$arItem['PRICE_FORMATED'];?>
											</td>
											<td class="basket-column"><div class="basket-cell basket-cell-nowrap UNIBasketFlyItem<?=$arItem['ID']?>Total">
												<?=CurrencyFormat($arItem['PRICE'] * $arItem['QUANTITY'], $arItem['CURRENCY']);?>
											</div></td>
											<td class="basket-column"><div class="basket-cell">
												<div class="basket-buttons-min">
													<div class="basket-buttons-min-wrapper">
														<div class="basket-buttons-min-button basket-buttons-min-button-wishlist">
															<div class="basket-buttons-min-button-icon" onclick="SaleActions.addToWishList('<?=$arItem['PRODUCT']['ID']?>', '<?=$arItem['CURRENCY']?>', 1, SaleWrapper.updateAfterBasketAction);"></div>
														</div>
														<div class="basket-buttons-min-button basket-buttons-min-button-delete">
															<div class="basket-buttons-min-button-icon" onclick="SaleActions.removeFromBasket('<?=$arItem['PRODUCT']['ID']?>', SaleWrapper.updateAfterBasketAction);"></div>
														</div>
													</div>
												</div>
											</div></td>
										</tr>
									<?endforeach;?>
								</table>
							</div>
							<div class="basket-buttons">
								<div class="basket-buttons-wrapper">
									<div class="basket-button-wrapper left">
										<div class="basket-button basket-button-gray" onclick="oUNIBasketFly.close();"><?=GetMessage('BASKET_FLY_BUTTONS_CONTINUE_BUY')?></div>
									</div>
									<?if (!empty($arParams['PATH_TO_BASKET'])):?>
										<div class="basket-button-wrapper right">
											<a href="<?=$arParams['PATH_TO_BASKET']?>" class="basket-button solid_button"><?=GetMessage('BASKET_FLY_BUTTONS_GO_BASKET')?></a>
										</div>
									<?endif;?>
									<?if (!empty($arParams['PATH_TO_ORDER'])):?>
										<div class="basket-button-wrapper right">
											<a href="<?=$arParams['PATH_TO_ORDER']?>" class="basket-button basket-button-gray"><?=GetMessage('BASKET_FLY_BUTTONS_GO_ORDER')?></a>
										</div>
									<?endif;?>
								</div>
							</div>
						<?else:?>
							<div class="basket-notify">
								<div class="basket-text"><?=GetMessage('BASKET_FLY_SECTIONS_BASKET_NOTIFY_TEXT')?></div>
								<div class="basket-element">
									<?if (!empty($arParams['CATALOG_URI'])):?>
										<a href="<?=$arParams['CATALOG_URI']?>" class="basket-button basket-button-gray"><?=GetMessage('BASKET_FLY_BUTTONS_GO_BUY')?></a>
									<?else:?>
										<div class="basket-button basket-button-gray" onclick="oUNIBasketFly.close();"><?=GetMessage('BASKET_FLY_BUTTONS_CLOSE')?></div>
									<?endif;?>
								</div>
							</div>
						<?endif;?>
					</div>
					<div class="basket-section wish-list"<?=$arResult['TOTAL']['WISH_LIST']['COUNT'] == 0 ? ' style="width: 400px;"' : ''?>>
								<?if ($arResult['TOTAL']['WISH_LIST']['COUNT'] > 0):?>
									<div class="basket-header solid_text">
										<?=GetMessage('BASKET_FLY_SECTIONS_WISH_LIST_HEADER', array(
											'#COUNT#' => $arResult['TOTAL']['WISH_LIST']['COUNT'],
											'#SUM#' => $arResult['TOTAL']['WISH_LIST']['SUM_FORMATED']
										))?>
									</div>
									<div class="basket-body">
										<table class="basket-table">
											<tr class="basket-row basket-row-header">
												<td class="basket-column"></td>
												<td class="basket-column"><div class="basket-cell solid_text"><?=GetMessage('BASKET_FLY_SECTIONS_TABLE_HEADER_NAME');?></div></td>
												<td class="basket-column"><div class="basket-cell solid_text"><?=GetMessage('BASKET_FLY_SECTIONS_TABLE_HEADER_COUNT');?></div></td>
												<td class="basket-column"><div class="basket-cell solid_text"><?=GetMessage('BASKET_FLY_SECTIONS_TABLE_HEADER_PRICE');?></div></td>
												<td class="basket-column"><div class="basket-cell solid_text"><?=GetMessage('BASKET_FLY_SECTIONS_TABLE_HEADER_TOTAL');?></div></td>
												<td class="basket-column"></td>
											</tr>
											<?foreach ($arResult['ITEMS'] as $arItem):?>
												<?if ($arItem['DELAY'] != 'Y') continue;?>
												<?if (empty($arItem['PRODUCT'])) continue;?>
												<?
													$sPicture = SITE_TEMPLATE_PATH.'/images/noimg/no-img.png';
													
													if (!empty($arItem['DETAIL_PICTURE']))
														$sPicture = $arItem['DETAIL_PICTURE']['SRC'];
														
													if (!empty($arItem['PREVIEW_PICTURE']))
														$sPicture = $arItem['PREVIEW_PICTURE']['SRC'];
												?>
												<tr class="basket-row">
													<td class="basket-column"><div class="basket-cell">
														<a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="uni-image" style="display: block; width: 70px; height: 70px; overflow: hidden;">
															<div class="uni-aligner-vertical"></div>
															<img src="<?=$sPicture?>" alt="<?=$arItem['NAME']?>" />
														</a>
													</div></td>
													<td class="basket-column"><div class="basket-cell">
														<a href="<?=$arItem['DETAIL_PAGE_URL']?>"><?=$arItem['NAME'];?></a>
													</div></td>
													<td class="basket-column"><div class="basket-cell">
													   <?=$arItem['QUANTITY']?>
													</div></td>
													<td class="basket-column"><div class="basket-cell basket-cell-nowrap">
														<?=$arItem['PRICE_FORMATED'];?>
													</div></td>
													<td class="basket-column"><div class="basket-cell basket-cell-nowrap UNIBasketFlyItem<?=$arItem['ID']?>Total">
														<?=CurrencyFormat($arItem['PRICE'] * $arItem['QUANTITY'], $arItem['CURRENCY']);?>
													</div></td>
													<td class="basket-column"><div class="basket-cell">
														<div class="basket-buttons-min">
															<div class="basket-buttons-min-wrapper">
																<div class="basket-buttons-min-button basket-buttons-min-button-basket">
																	<div class="basket-buttons-min-button-icon" onclick="SaleActions.addToBasket('<?=$arItem['PRODUCT']['ID']?>', '<?=$arItem['CURRENCY']?>', 1, SaleWrapper.updateAfterBasketAction);"></div>
																</div>
																<div class="basket-buttons-min-button basket-buttons-min-button-delete">
																	<div class="basket-buttons-min-button-icon" onclick="SaleActions.removeFromBasket('<?=$arItem['PRODUCT']['ID']?>', SaleWrapper.updateAfterBasketAction);"></div>
																</div>
															</div>
														</div>
													</div></td>
												</tr>
											<?endforeach;?>
										</table>
									</div>
									<div class="basket-buttons">
										<div class="basket-buttons-wrapper">
											<div class="basket-button-wrapper left">
												<div class="basket-button basket-button-gray" onclick="oUNIBasketFly.close();"><?=GetMessage('BASKET_FLY_BUTTONS_CONTINUE_BUY')?></div>
											</div>
											<?if (!empty($arParams['PATH_TO_BASKET'])):?>
												<div class="basket-button-wrapper right">
													<a href="<?=$arParams['PATH_TO_BASKET']?>?delay=y" class="basket-button solid_button"><?=GetMessage('BASKET_FLY_BUTTONS_GO_BASKET')?></a>
												</div>
											<?endif;?>
										</div>
									</div>
								<?else:?>
									<div class="basket-notify">
										<div class="basket-text"><?=GetMessage('BASKET_FLY_SECTIONS_WISH_LIST_NOTIFY_TEXT')?></div>
										<div class="basket-element">
											<div class="basket-button basket-button-gray" onclick="oUNIBasketFly.close();"><?=GetMessage('BASKET_FLY_BUTTONS_CLOSE')?></div>
										</div>
									</div>
								<?endif;?>
							</div>
					   
						<div class="basket-section section" id="call-section" style="width: 354px;">
						   <?$APPLICATION->IncludeComponent(
								"bitrix:form.result.new",
								"shop",
								Array(
									"AJAX_MODE" => "N",
									"SEF_MODE" => "N",
									"WEB_FORM_ID" => "1",
									"RESULT_ID" => $_REQUEST["RESULT_ID"],
									"START_PAGE" => "new",
									"SHOW_LIST_PAGE" => "N",
									"SHOW_EDIT_PAGE" => "N",
									"SHOW_VIEW_PAGE" => "N",
									"SUCCESS_URL" => "",
									"SHOW_ANSWER_VALUE" => "N",
									"SHOW_ADDITIONAL" => "N",
									"SHOW_STATUS" => "Y",
									"EDIT_ADDITIONAL" => "N",
									"EDIT_STATUS" => "N",
									"NOT_SHOW_FILTER" => array(),
									"NOT_SHOW_TABLE" => array(),
									"CHAIN_ITEM_TEXT" => "",
									"CHAIN_ITEM_LINK" => "",
									"IGNORE_CUSTOM_TEMPLATE" => "N",
									"USE_EXTENDED_ERRORS" => "Y",
									"CACHE_TYPE" => "A",
									"CACHE_TIME" => "3600",
									"AJAX_MODE" => "N",  // режим AJAX
									"AJAX_OPTION_SHADOW" => "N", // затемнять область
									"AJAX_OPTION_JUMP" => "Y", // скроллить страницу до компонента
									"AJAX_OPTION_STYLE" => "Y", // подключать стили
								)
							);?>
							<script type="text/javascript">
								$(document).ready(function(){
									$('#call-section form .inputtext').eq(1).mask("+7 (999) 999-9999");
								});
								$('.fly .close_button').click(function(){
									oUNIBasketFly.close();
								});
							</script>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
    <script type="text/javascript">
        if (oUNIBasketFly !== undefined) var iUNIBasketFlySection = oUNIBasketFly.section;
        var oUNIBasketFly = new UNIBasketFly({
            'animate': true,
            'animateTime': 500,
            'selectors': {
                'sections': '.basket-fly-1 .basket-content .basket-sections',
                'section': '.basket-fly-1 .basket-content .basket-sections .basket-section'
            }
        });
        
        if (iUNIBasketFlySection !== undefined) {
            oUNIBasketFly.settings.animate = false;
            oUNIBasketFly.setSectionByID(iUNIBasketFlySection);
            oUNIBasketFly.settings.animate = true;
            iUNIBasketFlySection = undefined;
        }
    </script>
<?$frame->end();?>