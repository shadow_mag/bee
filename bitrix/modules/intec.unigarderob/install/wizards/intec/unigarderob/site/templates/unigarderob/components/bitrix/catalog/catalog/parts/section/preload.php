<?if(!isset($arResult["VARIABLES"]["SECTION_ID"])){	
	$rsSections = CIBlockSection::GetList(
        array(),
        array(
            'IBLOCK_ID' => $arParams["IBLOCK_ID"],
            '=CODE' => $arResult["VARIABLES"]["SECTION_CODE"]
        )
    );
	if ($arSection = $rsSections->Fetch())
		$arResult["VARIABLES"]["SECTION_ID"] = $arSection["ID"];
    unset($rsSections, $arSection);
}?>