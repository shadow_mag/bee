<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

foreach ($arResult['ITEMS'] as $key => $item)
{
    if ($item['CAN_BUY'] != 'Y') {
        unset($arResult['ITEMS'][$key]);
        continue;
    }
        
	$rsElement = CIBlockElement::GetByID($item['PRODUCT_ID'])->GetNextElement();
	
	if ($rsElement)
	{
		$arElement = $rsElement->GetFields();
		$arElement['PROPERTIES'] = $rsElement->GetProperties();
		
		if (CModule::IncludeModule('catalog'))
		{
			$arElement = array_merge($arElement, CCatalogProduct::GetByID($arElement['ID']));
			$rsRatios = CCatalogMeasureRatio::getList(
				array(),
				array('PRODUCT_ID' => $arElement['ID']),
				false,
				false,
				array('PRODUCT_ID', 'RATIO')
			);
			
			$sRatio = 1;
			
			if ($arRatio = $rsRatios->Fetch())
			{
				$intRatio = (int)$arRatio['RATIO'];
				$dblRatio = doubleval($arRatio['RATIO']);
				$sRatio = ($dblRatio > $intRatio ? $dblRatio : $intRatio);
			}
			
			$arElement['MEASURE_RATIO'] = $sRatio;
		}
			
		$arResult['ITEMS'][$key]['PRODUCT'] = $arElement;
		
		if (!empty($arElement['PROPERTIES']['CML2_LINK']['VALUE']) && empty($arElement['PREVIEW_PICTURE']) && empty($arElement['DETAIL_PICTURE']))
		{
			$arLinkElement = CIBlockElement::GetByID($arElement['PROPERTIES']['CML2_LINK']['VALUE'])->GetNext();
			
			if ($arLinkElement) {
				if ($arLinkElement['PREVIEW_PICTURE'])
				{
					$CFile = CFile::GetFileArray($arLinkElement['PREVIEW_PICTURE']);
					$arResult['ITEMS'][$key]['PREVIEW_PICTURE'] = $CFile;
					$arElement['PREVIEW_PICTURE'] = $CFile;
				}
				
				if ($arLinkElement['DETAIL_PICTURE'])
				{
					$CFile = CFile::GetFileArray($arLinkElement['DETAIL_PICTURE']);
					$arResult['ITEMS'][$key]['DETAIL_PICTURE'] = $CFile;
					$arElement['DETAIL_PICTURE'] = $CFile;
				}
			}
		}
		else
		{
			if ($arElement['PREVIEW_PICTURE'])
			{
				$CFile = CFile::GetFileArray($arElement["PREVIEW_PICTURE"]);
				$arResult['ITEMS'][$key]['PREVIEW_PICTURE'] = $CFile;
				$arElement['PREVIEW_PICTURE'] = $CFile;
			}
			
			if ($arElement['DETAIL_PICTURE'])
			{
				$CFile = CFile::GetFileArray($arElement["DETAIL_PICTURE"]);
				$arResult['ITEMS'][$key]['DETAIL_PICTURE'] = $CFile;
				$arElement['DETAIL_PICTURE'] = $CFile;
			}
		}
	}
}

$arResult['TOTAL'] = array(
    'BASKET' => array(
        'SUM' => 0,
        'SUM_FORMATED' => '0',
        'COUNT' => 0
    ),
    'WISH_LIST' => array(
        'SUM' => 0,
        'SUM_FORMATED' => '0',
        'COUNT' => 0
    )
);

foreach ($arResult['ITEMS'] as $arItem) {
    $sBasketSection = 'BASKET';
    
    if ($arItem['DELAY'] == 'Y')
        $sBasketSection = 'WISH_LIST';
    
    $arResult['TOTAL'][$sBasketSection]['SUM'] += $arItem['PRICE'] * $arItem['QUANTITY'];
    $arResult['TOTAL'][$sBasketSection]['COUNT']++;
}

$sBaseCurrency = CCurrency::GetBaseCurrency();
$arResult['CURRENCY'] = CCurrencyLang::GetCurrencyFormat($sBaseCurrency);
$arResult['TOTAL']['BASKET']['SUM_FORMATED'] = CurrencyFormat($arResult['TOTAL']['BASKET']['SUM'], $sBaseCurrency);
$arResult['TOTAL']['WISH_LIST']['SUM_FORMATED'] = CurrencyFormat($arResult['TOTAL']['WISH_LIST']['SUM'], $sBaseCurrency);
?>