<?
	$MESS["CONTENT_TYPE_NAME"] = "Content";
	$MESS["CONTENT_ELEMENT_NAME"] = "Element";
	$MESS["CONTENT_SECTION_NAME"] = "Section";
	$MESS["CATALOG_TYPE_NAME"] = "Catalog";
	$MESS["CATALOG_ELEMENT_NAME"] = "Product";
	$MESS["CATALOG_SECTION_NAME"] = "Section";
	$MESS["REVIEWS_TYPE_NAME"] = "Reviews";
	$MESS["REVIEWS_ELEMENT_NAME"] = "Review";
	$MESS["REVIEWS_SECTION_NAME"] = "Section";
    $MESS["OFFERS_TYPE_NAME"] = "Offers";
	$MESS["OFFERS_ELEMENT_NAME"] = "Element";
	$MESS["OFFERS_SECTION_NAME"] = "Section";
?>