<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>
<?$this->setFrameMode(true);?>

<div class="compare-small-fly">
	<div class="compare-wrapper">
        <a href="<?=$arParams['COMPARE_URL']?>" class="compare-element compare">
            <div class="compare-element-wrapper">
                <div class="compare-icon"></div>
                <?$frame = $this->createFrame()->begin();?>
                    <?$iItemsCount = count($arResult);?>
                    <?if ($iItemsCount > 0):?>
                        <div class="compare-text"><?=$iItemsCount?></div>
                    <?endif;?>
                <?$frame->end();?>
            </div>
        </a>
    </div>
</div>