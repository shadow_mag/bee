<div class="price">
	<?if ($arResult['RATIO_PRICE']['VALUE'] == $arResult['RATIO_PRICE']['DISCOUNT_VALUE']):?>
		<div class="current"><?=$arResult['RATIO_PRICE']['PRINT_VALUE']?></div>
	<?else:?>
		<div class="current"><?=$arResult['RATIO_PRICE']['PRINT_DISCOUNT_VALUE']?></div>
		<?if ($arParams['SHOW_OLD_PRICE'] == 'Y'):?>
			<div class="old"><?=$arResult['RATIO_PRICE']['PRINT_VALUE']?></div>
		<?endif;?>
	<?endif;?>
</div>
<div class="uni-indents-vertical indent-25"></div>
<div class="order">
	<div class="valign"></div>
	<?if ($arResult['CAN_BUY']):?>
		<div class="count">
            <?
                $arJSNumeric = array(
                    'VALUE' => floatval($arResult['CATALOG_MEASURE_RATIO']),
                    'MINIMUM' => floatval($arResult['CATALOG_MEASURE_RATIO']),
                    'MAXIMUM' => floatval($arResult['CATALOG_QUANTITY']),
                    'RATIO' => floatval($arResult['CATALOG_MEASURE_RATIO']),
                    'UNLIMITED' => $arFlags['USE_COUNT_UNLIMITED'],
                    'VALUE_TYPE' => 'FLOAT'
                );
            ?>
			<button id="decrease" onclick="return <?=$sUniqueID?>ProductCount.Decrease()" class="uni-button">-</button>
			<input type="text" name="count" id="<?=$sUniqueID?>ProductCount" onchange="<?=$sUniqueID?>ProductCount.SetValue($(this).val())" value="<?=$arResult['CATALOG_MEASURE_RATIO']?>" />
			<button id="increase" onclick="return <?=$sUniqueID?>ProductCount.Increase()" class="uni-button">+</button>
            <script type="text/javascript">
                var <?=$sUniqueID?>ProductCount = new UNINumericUpDown(<?=CUtil::PhpToJSObject($arJSNumeric)?>);
                <?=$sUniqueID?>ProductCount.Settings.EVENTS.onValueChange = function ($oNumeric) {
                    $('#<?=$sUniqueID?>ProductCount').val($oNumeric.GetValue());
                }
    		</script>
		</div>
		<div class="buy-button-click">
            <?$sBuyBittonClickImage = !empty($arResult['DETAIL_PICTURE']['SRC']) ? $arResult['DETAIL_PICTURE']['SRC'] : $arResult['PREVIEW_PICTURE']['SRC']?>
            <?$arBuyButtonClickParams = array (
				"ID" => $arResult["ID"],
				"Name" => $arResult["NAME"],
				"IBlockType" => $arParams["IBLOCK_TYPE"],
				"IBlockID" => $arParams["IBLOCK_ID"],
				"PriceCurrent" => $arResult['MIN_PRICE']['PRINT_DISCOUNT_VALUE'],
				"PriceOld" => "",
				"Image" => $sBuyBittonClickImage,
                "Currency" => $arResult['MIN_PRICE']['CURRENCY'],
                "PriceID" => $arResult['MIN_BASIS_PRICE']['PRICE_ID'],
				"Price" => $arResult['MIN_PRICE']['VALUE']
			);?>
			<a 
				href="javascript:void(0);" 
				class="uni-button button" 
				onclick="Popups.OneClickBuy(<?=CUtil::PhpToJSObject($arBuyButtonClickParams);?>)"
			><?=GetMessage("ONE_CLICK_BUY")?></a>
		</div>
		<div class="buy-button">			
			<a rel="nofollow" class="uni-button solid_button button addToBasket addToBasket<?=$arResult["ID"]?>"
				onclick="SaleActions.addToBasket('<?=$arResult['ID']?>', '<?=$arResult['MIN_PRICE']['CURRENCY']?>', <?=$sUniqueID?>ProductCount.GetValue(), SaleWrapper.updateAfterBasketAction);"
			><?=GetMessage("CATALOG_ADD_TO_BASKET")?>
			</a>
			<a rel="nofollow" href="<?=$arParams["BASKET_URL"];?>" class="uni-button solid_button button removeFromBasket removeFromBasket<?=$arResult["ID"]?>" style="display: none;">
				<?=GetMessage("CATALOG_ADDED")?>
			</a>
		</div>
	<?endif;?>
	<div class="min-buttons">
		<?if($arParams["DISPLAY_COMPARE"] == "Y"):?>
			<div class="min-button compare">
				<div class="add addToCompare addToCompare<?=$arResult["ID"]?>"
					onclick="SaleActions.addToCompare('<?=$arResult['COMPARE_URL']?>', SaleWrapper.updateAfterCompareAction);"
					title="<?=GetMessage('COMPARE_TEXT_DETAIL')?>"					
				>
				</div>
				<div class="remove removeFromCompare removeFromCompare<?=$arResult["ID"]?>"
					style="display:none"
					onclick="SaleActions.removeFromCompare('<?=$arResult['COMPARE_REMOVE_URL']?>', SaleWrapper.updateAfterCompareAction);"
					title="<?=GetMessage('COMPARE_DELETE_TEXT_DETAIL')?>"
				>
				</div>
			</div>
		<?endif?>
		<?if ($arResult['CAN_BUY']):?>
			<div class="min-button like">
				<div class="add addToWishList addToWishList<?=$arResult["ID"]?>"								
					onclick="SaleActions.addToWishList('<?=$arResult['ID']?>', '<?=$arResult['MIN_PRICE']['CURRENCY']?>', <?=$sUniqueID?>ProductCount.GetValue(), SaleWrapper.updateAfterBasketAction);"
					title="<?=GetMessage('LIKE_TEXT_DETAIL')?>"
				>
				</div>
				<div class="remove removeFromWishList removeFromWishList<?=$arResult["ID"]?>"
					style="display:none"							
					onclick="SaleActions.removeFromWishList('<?=$arResult['ID']?>', SaleWrapper.updateAfterBasketAction);"
					title="<?=GetMessage('LIKE_DELETE_TEXT_DETAIL')?>"
				>
				</div>
			</div>
		<?endif;?>
	</div>
</div>