<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>
<?$this->setFrameMode(true);?>
<?
    $iGrid = intval($arParams['LINE_ELEMENT_COUNT']);
    
    if ($iGrid < 1) $iGrid = 1;
    if ($iGrid > 6) $iGrid = 6;
    
    $arGrid = array(
        '1' => 'uni-100',
        '2' => 'uni-50',
        '3' => 'uni-33',
        '4' => 'uni-25',
        '5' => 'uni-20',
        '6' => 'uni-16'
    );
?>
<?if (!empty($arResult['ITEMS'])):?>
    <?
        $sItemEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT");
    	$sItemDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE");
    	$arItemDeleteParams = array("CONFIRM" => GetMessage('CATALOG_DELETE_ELEMENT'));
    ?>
    <?if ($arParams["DISPLAY_TOP_PAGER"]):?>
        <div style="padding-top: 20px; padding-bottom: 20px;">
            <?include('parts/Paginator.php');?>
        </div>
    <?endif;?>
    <?$frame = $this->createFrame()->begin();?>
    <div class="catalog-section tile">
        <div class="clatalog-section-wrapper uni_parent_col">
            <?foreach ($arResult['ITEMS'] as $arItem):?>
                <?
                    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $sItemEdit);
            		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], $sItemDelete, $arItemDeleteParams);
            		$sItemEditAreaID = $this->GetEditAreaId($arItem['ID']);
                ?>
                <div class="product uni_col <?=$arGrid[$iGrid]?>" id="Product<?=$arItem['ID']?>">
                    <div class="product-wrapper">
                        <div class="image-normalizer"></div>
                        <div class="information-normalizer"></div>
                        <div class="shadow hover_shadow" id="<?=$sItemEditAreaID?>">
                            <div class="shadow-wrapper">
                                <div class="marks">
                                    <?if(!empty($arItem["MIN_PRICE"]["DISCOUNT_DIFF_PERCENT"]) && $arParams['SHOW_DISCOUNT_PERCENT'] == 'Y'){?>
                                        <span class="mark action">-<?=$arItem["MIN_PRICE"]["DISCOUNT_DIFF_PERCENT"];?>%</span>
                    				<?}?>
									<?if( $arItem["PROPERTIES"]["HIT"]["VALUE"] ){?>
										<span class="mark hit"><?=GetMessage('MARK_HIT')?></span>
									<?}?>			
									<?if( $arItem["PROPERTIES"]["NEW"]["VALUE"] ){?>
										<span class="mark new"><?=GetMessage('MARK_NEW')?></span>
									<?}?>
									<?if( $arItem["PROPERTIES"]["RECOMMEND"]["VALUE"] ){?>
										<span class="mark recommend"><?=GetMessage('MARK_RECOMEND')?></span>
									<?}?>
                                </div>
                                <div class="image" >
                                    <?if (empty($arItem['OFFERS'])):?>
                                        <a class="image-wrapper uni-image" href="<?=$arItem['DETAIL_PAGE_URL']?>">
                                            <div class="uni-aligner-vertical"></div>
                                            <img src="<?=$arItem['PICTURE']?>" alt="<?=htmlspecialcharsbx($arItem['NAME'])?>" title="<?=htmlspecialcharsbx($arItem['NAME'])?>"/>
                                        </a>
                                    <?else:?>
                                        <?include('parts/OffersImages.php')?>
                                    <?endif;?>
                                    <div class="uni-button-quickview" onclick="Popups.ProductQuickView.Open('<?=$arItem['DETAIL_PAGE_URL']?>',event)"><i></i>
										<?=GetMessage("QUICK_VIEW");?>
									</div>
                                </div>
                                <?if (empty($arItem['OFFERS'])):?>
                                    <div class="min-buttons">
                                        <div class="min-buttons-wrapper">
                                    		<?if($arParams["DISPLAY_COMPARE"] == "Y"):?>
                                    			<div class="min-button compare">
                                    				<div class="add addToCompare addToCompare<?=$arItem["ID"]?>"
                                    					onclick="SaleActions.addToCompare('<?=$arItem['COMPARE_URL']?>', SaleWrapper.updateAfterCompareAction);"
                                    					title="<?=GetMessage('COMPARE_TEXT_DETAIL')?>"					
                                    				>
                                    				</div>
                                    				<div  class="remove removeFromCompare removeFromCompare<?=$arItem["ID"]?>"
                                    					style="display:none"
                                    					onclick="SaleActions.removeFromCompare('<?=$arItem['COMPARE_REMOVE_URL']?>', SaleWrapper.updateAfterCompareAction);"
                                    					title="<?=GetMessage('COMPARE_DELETE_TEXT_DETAIL')?>"
                                    				>
                                    				</div>
                                    			</div>
                                    		<?endif?>
                                    		<?if ($arItem['CAN_BUY']):?>
                                    			<div class="min-button like">
                                    				<div class="add addToWishList addToWishList<?=$arItem["ID"]?>"								
                                    					onclick="SaleActions.addToWishList('<?=$arItem['ID']?>', '<?=$arItem['MIN_PRICE']['CURRENCY']?>', 1, SaleWrapper.updateAfterBasketAction);"	
                                    					title="<?=GetMessage('LIKE_TEXT_DETAIL')?>"
                                    				>
                                    				</div>
                                    				<div class="remove removeFromWishList removeFromWishList<?=$arItem["ID"]?>"
                                    					style="display:none"								
                                    					onclick="SaleActions.removeFromWishList('<?=$arItem['ID']?>', SaleWrapper.updateAfterBasketAction);"
                                    					title="<?=GetMessage('LIKE_DELETE_TEXT_DETAIL')?>"
                                    				>
                                    				</div>
                                    			</div>
                                    		<?endif;?>
                                        </div>
                                    </div>
                                <?else:?>
                                    <?include('parts/OffersMinButtons.php');?>
                                <?endif;?>
                                <div class="information">
                                    <?if (!empty($arItem['BRAND']['NAME'])):?>
                                        <a class="brand" href="<?=$arItem['BRAND']['DETAIL_PAGE_URL']?>"><?=$arItem['BRAND']['NAME']?></a>
                                    <?else:?>
                                        <div class="brand"></div>
                                    <?endif;?>
                                    <a class="name" href="<?=$arItem['DETAIL_PAGE_URL']?>"><?=$arItem['NAME']?></a>
                                    <?if (empty($arItem['OFFERS'])):?>
                                        <div class="price"><?=$arItem['MIN_PRICE']['PRINT_DISCOUNT_VALUE']?></div>
                                    <?elseif (!empty($arItem['MIN_PRICE'])):?>
                                        <div class="price SKUProductPrice"><?=GetMessage('CATALOG_PRICE_OFFERS', array('#PRICE#' => $arItem['MIN_PRICE']['PRINT_DISCOUNT_VALUE']))?></div>
										<div class="original SKUProductPriceOld"><?=$arItem['MIN_PRICE']['PRINT_VALUE']?></div>
									<?endif;?>
                                </div>
                                <div class="hideable">
                                    <?if (empty($arItem['OFFERS'])):?>
                                        <?include('parts/Order.php')?>
                                    <?else:?>
                                        <?include('parts/OffersProperties.php')?>
                                        <?include('parts/OrderWithOffers.php')?>
                                        <?include('parts/OffersScript.php')?>
                                    <?endif;?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?endforeach;?>
        </div>
    </div>
    <?$frame->end();?>      
    <?if ($arParams["DISPLAY_BOTTOM_PAGER"]):?>
        <div style="padding-top: 20px;">
            <?include('parts/Paginator.php');?>
        </div>
    <?endif;?>
<?endif;?>