<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
					<?if(!$isFrontPage){?>	
						<?if($flag_lm){?>
							</div><!--right_col-->
						<?}?>
					<?}?>
					<div class="clear"></div>
				</div> <!-- bx_content_section -->
			</div> <!-- worakarea_wrap_container workarea-->
		</div> <!-- workarea_wrap -->
		<div class="clear"></div>		
		<div class="bg_footer">
			<div class="footer">
				<div class="contacts left">
					<a href="/"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/logo_footer.php"), false);?></a>
           	</div>
				<div class="menu left">
               <div class="uni-indents-vertical indent-25"></div>
					<?$APPLICATION->IncludeComponent("bitrix:menu", "bottom_menu", array(
						"ROOT_MENU_TYPE" => "bottom",
						"MENU_CACHE_TYPE" => "A",
						"MENU_CACHE_TIME" => "600000",
						"MENU_CACHE_USE_GROUPS" => "N",
						"MENU_CACHE_GET_VARS" => array(
						),
						"MAX_LEVEL" => "2",
						"CHILD_MENU_TYPE" => "left",
						"USE_EXT" => "N",
						"DELAY" => "N",
						"ALLOW_MULTI_SELECT" => "N"
						), false
					);?>
					<div class="clear"></div>
				</div>
				<div class="phone-block right">
					<div class="uni-indents-vertical indent-25"></div>
					<div class="phone">
						<?$APPLICATION->IncludeComponent("bitrix:main.include","",Array(
								"AREA_FILE_SHOW" => "file", 
								"PATH" => SITE_DIR."include/company_phone.php", 
							)
						);?>
					</div>
					<div class="call_button">
						<span class="open_call" onclick="Popups.OrderCall()"><?=GetMessage("CALL_TEXT")?></span>
					</div>
				  <div class="uni-indents-vertical indent-25"></div>
					<div class="social_buttons">
						<?$facebookLink = $APPLICATION->GetFileContent($_SERVER["DOCUMENT_ROOT"].SITE_DIR."include/socnet_facebook.php");
						$twitterLink = $APPLICATION->GetFileContent($_SERVER["DOCUMENT_ROOT"].SITE_DIR."include/socnet_twitter.php");								
						$vkLink = $APPLICATION->GetFileContent($_SERVER["DOCUMENT_ROOT"].SITE_DIR."include/socnet_vk.php");
						$inLink  = $APPLICATION->GetFileContent($_SERVER["DOCUMENT_ROOT"].SITE_DIR."include/socnet_in.php");?>
						<ul>
							<?if ($inLink):?>
								<li class="in"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/socnet_in.php"), false);?></li>
							<?endif?>
							<?if ($facebookLink):?>
								<li class="fb"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/socnet_facebook.php"), false);?></li>
							<?endif?>
							<?if ($twitterLink):?>
								<li class="tw"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/socnet_twitter.php"), false);?></li>
							<?endif?>						
							<?if (LANGUAGE_ID=="ru" && $vkLink):?>
								<li class="vk"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/socnet_vk.php"), false);?></li>
							<?endif?>						
						</ul>	
					</div>
					<div class="logo-block">
						<?$APPLICATION->IncludeComponent("bitrix:main.include","",Array(
								"AREA_FILE_SHOW" => "file", 
								"PATH" => SITE_DIR."include/company_logo.php", 
							)
						);?>
                    </div>
				</div>
            <div class="clear"></div>
				<div class="uni-indents-vertical indent-25"></div>
				<div id="bx-composite-banner"></div>
			</div>
			<div class="copyright">
				<div class="copy_wrap clearfix">
					<div class="copy">
						<?$APPLICATION->IncludeComponent("bitrix:main.include","",Array(
								"AREA_FILE_SHOW" => "file", 
								"PATH" => SITE_DIR."include/copy.php", 
							)
						);?>
					</div>
					<div class="address">
						<?$APPLICATION->IncludeComponent("bitrix:main.include","",Array(
								"AREA_FILE_SHOW" => "file", 
								"PATH" => SITE_DIR."include/address.php", 
							)
						);?>
					</div>
					<div class="email">
						<?$APPLICATION->IncludeComponent("bitrix:main.include","",Array(
								"AREA_FILE_SHOW" => "file", 
								"PATH" => SITE_DIR."include/email.php", 
							)
						);?>
					</div>
				</div>
			</div>
		</div>
	</div><!--wrap-->
	<?if ($options['SHOW_BUTTON_TOP']['ACTIVE_VALUE'] == 'Y'):?>
		<div class="button_up solid_button">
			<i></i>
		</div>
	<?endif;?>
	<script type="text/javascript">
		$('.nbs-flexisel-nav-left').addClass('uni-slider-button-small').addClass('uni-slider-button-left').html('<div class="icon"></div>');
		$('.nbs-flexisel-nav-right').addClass('uni-slider-button-small').addClass('uni-slider-button-right').html('<div class="icon"></div>');
	</script>
</body>
</html>