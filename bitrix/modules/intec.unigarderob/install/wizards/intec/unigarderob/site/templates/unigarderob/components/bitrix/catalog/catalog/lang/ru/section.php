<?
$MESS["SECTION_SEARCH"] = "Найдено";
$MESS["SECTION_MODEL"] = "моделей";
$MESS["SECTION_SORT_TITLE"] = "Сортировать";
$MESS["SECTION_SORT_NAME"] = "По названию";
$MESS["SECTION_SORT_PRICE"] = "По цене";
$MESS["SECTION_SORT_POPULAR"] = "По популярности";
$MESS["SECTION_SORT_NONE"] = "Без сортировки";
$MESS["SECTION_SHOW"] = "Показать по";
$MESS["SECTION_VIEW_TITLE"] = "Вид";

$MESS["SECTION_SHOW_FILTER"] = "Фильтр";
$MESS["SECTION_HIDE_FILTER"] = "Скрыть фильтр";
$MESS["SECTION_SHOW_MENU"] = "Каталог";
$MESS["SECTION_HIDE_MENU"] = "Скрыть каталог";
?>