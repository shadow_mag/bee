<?$iCurrentDepthLevel++;?>
<?$iCurrentLevel++?>
<div class="menu-catalog-left-1-item<?=!empty($arMenuItem['SELECTED']) ? ' ui-state-active' : ''?>">
    <a class="menu-catalog-left-1-item-wrapper" href="<?=$arMenuItem['LINK']?>">
        <div class="uni-aligner-vertical"></div>
        <div class="menu-catalog-left-1-text">
            <?=$arMenuItem['TEXT']?>
        </div>
        <?if (is_numeric($arMenuItem['PARAMS']['ELEMENTS_COUNT'])):?>
            <div class="menu-catalog-left-1-count">
                <?=$arMenuItem['PARAMS']['ELEMENTS_COUNT']?>
            </div>
        <?endif;?>
    </a>
    <div class="menu-catalog-left-1-submenu menu-catalog-left-1-submenu-level-<?=$iCurrentLevel?>">    