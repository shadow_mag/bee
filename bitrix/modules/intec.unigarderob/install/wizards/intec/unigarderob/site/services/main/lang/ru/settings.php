<?
$MESS["MAIN_OPT_DESCRIPTION"] = "Описание страницы";
$MESS["MAIN_OPT_KEYWORDS"] = "Ключевые слова";
$MESS["MAIN_OPT_TITLE"] = "Заголовок окна браузера";
$MESS["MAIN_OPT_KEYWORDS_INNER"] = "Продвигаемые слова";

$MESS['TYPE_NAME'] = 'Оформление заказа через купить в 1 клик';
$MESS['USER_NAME'] = 'Ф.И.О. пользователя';
$MESS['USER_PHONE'] = 'Телефон пользователя';
$MESS['ORDER_ID'] = 'Номер заказа';
$MESS['TMP_ORDER_ID'] = 'Создан заказ номер';
$MESS['ORDER_DESCRIPTION'] = 'Описание заказа';
$MESS['ORDER_LIST'] = 'Состав заказа';
$MESS['PRICE'] = 'Сумма заказа';
$MESS['INFO'] = 'Информационное сообщение сайта';
$MESS['NEW_ORDER'] = 'Новый заказ';
$MESS['RUB'] = 'руб.';
?>