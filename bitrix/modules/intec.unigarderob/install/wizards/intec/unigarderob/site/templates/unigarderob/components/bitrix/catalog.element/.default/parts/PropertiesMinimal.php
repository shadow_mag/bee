<?$arPropertiesMinimal = $arResult['DISPLAY_PROPERTIES'];//array_slice($arResult['DISPLAY_PROPERTIES'], 0, 6);?>
<?if (!empty($arPropertiesMinimal)):?>
<div class="uni-indents-vertical indent-40"></div>
<div class="row">
    <div class="properties">
    	<?foreach ($arPropertiesMinimal as $arPropertyMinimal):?>
            <?if (!is_array($arPropertyMinimal['VALUE'])):?>
                <div class="property"><?=$arPropertyMinimal['NAME']?> &mdash; <?=$arPropertyMinimal['VALUE']?>;</div>
            <?else:?>
                <div class="property"><?=$arPropertyMinimal['NAME']?> &mdash; <?=implode(', ', $arPropertyMinimal['VALUE'])?>;</div>
            <?endif;?>
    	<?endforeach;?>
    </div>
</div>
<?endif;?>