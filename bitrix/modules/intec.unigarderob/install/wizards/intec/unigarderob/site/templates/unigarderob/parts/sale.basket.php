<div class="b_basket">
    <?$APPLICATION->IncludeComponent(
    	"bitrix:sale.basket.basket.small", 
    	".default", 
    	array(
    		"PATH_TO_BASKET" => SITE_DIR."personal/cart/",
    		"PATH_TO_ORDER" => SITE_DIR."personal/order/make/",
    		"SHOW_DELAY" => "Y",
    		"SHOW_NOTAVAIL" => "Y",
    		"SHOW_SUBSCRIBE" => "Y",
    		"TYPE_BASKET" => $options["TYPE_BASKET"]["ACTIVE_VALUE"],
    		"COMPONENT_TEMPLATE" => ".default",
    		"CATALOG_URI" => SITE_DIR."catalog/",
			"IBLOCK_TYPE_COMPARE" => "#CATALOG_IBLOCK_TYPE#",
			"IBLOCK_ID_COMPARE" => "#CATALOG_IBLOCK_ID#",
			"COMPARE_NAME" => "CATALOG_COMPARE_LIST",
			"COMPARE_URL" => SITE_DIR."catalog/compare.php"
    	),
    	false
    );?>
</div>