<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>
<?$this->setFrameMode(true);?>
<?global $options;?>
<?$sUniqueID = spl_object_hash($this);?>
<?
    $iGrid = intval($arParams['LINE_ELEMENT_COUNT']);
    
    if ($iGrid < 1) $iGrid = 1;
    if ($iGrid > 6) $iGrid = 6;
    
    $arGrid = array(
        '1' => 'uni-100',
        '2' => 'uni-50',
        '3' => 'uni-33',
        '4' => 'uni-25',
        '5' => 'uni-20',
        '6' => 'uni-16'
    );
?>
<?if (!empty($arResult['ITEMS'])):?>
    <?$frame = $this->createFrame()->begin();?>
    <div class="uni-product-slider" id="UNIProductSlider<?=$sUniqueID?>">
        <h3 class="header_grey"><?=$arParams['TITLE']?></h3>
        <div class="buttons">
            <div class="button button-left uni-slider-button-small uni-slider-button-left" onclick="return UNIProductSlider<?=$sUniqueID?>.SlideRight();">
                <div class="icon"></div>
            </div>
			<div class="button button-right uni-slider-button-small uni-slider-button-right" onclick="return UNIProductSlider<?=$sUniqueID?>.SlideLeft();">
                <div class="icon"></div>
            </div>	
        </div>
        <div class="indents-wrapper">
            <div class="indents">
            	<div class="slider">
                    <?
                		$strElementEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT");
                		$strElementDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE");
                		$arElementDeleteParams = array("CONFIRM" => GetMessage('CT_BCS_TPL_ELEMENT_DELETE_CONFIRM'));
                    ?>
            		<?foreach($arResult["ITEMS"] as $cell=>$arElement){
            		
            			$this->AddEditAction($arElement['ID'], $arElement['EDIT_LINK'], $strElementEdit);
            			$this->AddDeleteAction($arElement['ID'], $arElement['DELETE_LINK'], $strElementDelete, $arElementDeleteParams);
            		?>
            			<div class="element <?=$arGrid[$iGrid]?>" id="<?=$this->GetEditAreaId($arElement['ID']);?>">
        					<div class="wrapper hover_shadow">
        						<?if(!empty($arElement["DETAIL_PICTURE"]["SRC"])){
        							$file = CFile::ResizeImageGet($arElement["DETAIL_PICTURE"],array('width'=>300, 'height'=>300),"BX_RESIZE_IMAGE_PROPORTIONAL_ALT");
        									$src=$file['src'];
        								}else{
        									$src=SITE_TEMPLATE_PATH."/images/noimg/no-img.png";
        							}?>
        						<a href="<?=$arElement["DETAIL_PAGE_URL"]?>" class="image">	
        							<div class="uni-image">
        								<div class="uni-aligner-vertical"></div>
        								<img src="<?=$src?>" alt="<?=htmlspecialcharsbx($arElement['NAME'])?>" title="<?=htmlspecialcharsbx($arResult['NAME'])?>"/>
        							</div>
        						</a>
        						<div class="information">
        							<a class="name" href="<?=$arElement["DETAIL_PAGE_URL"]?>"><?=$arElement["NAME"]?></a>
        							<div class="price"><?=$arElement['MIN_PRICE']['PRINT_DISCOUNT_VALUE']?></div>
        						</div>
        						<div class="clear"></div>
        					</div>
            			</div>
            		<?}?>
            		<div class="clear"></div>		
            	</div>
            </div>
        </div>
    </div>
	<script type="text/javascript">
		var UNIProductSlider<?=$sUniqueID?> = new UNISlider({
            'SLIDER': '#UNIProductSlider<?=$sUniqueID?> .slider',
            'ELEMENT': '#UNIProductSlider<?=$sUniqueID?> .slider .element',
            'OFFSET': <?=$iGrid?>,
            'INFINITY_SLIDE': true,
            'ANIMATE': true,
            'ANIMATE_SPEED': 400,
            "EVENTS": {
                "onAdaptabilityChange": function ($oSlider) {
                    <?if ($options['ADAPTIV']['ACTIVE_VALUE'] == 'Y'):?>
                        $oSlider.Settings.OFFSET = Math.round($oSlider.GetSliderWidth() / $oSlider.GetElementWidth());
                    <?endif;?>
                    
                    if ($oSlider.Settings.OFFSET < $oSlider.GetElementsCount()) {
                        $('#UNIProductSlider<?=$sUniqueID?> .buttons').css('display', 'block');
                    } else {
                        $('#UNIProductSlider<?=$sUniqueID?> .buttons').css('display', 'none');
                    }  
                }            
            }                
		});
        
        $('.image-slider .list#ProductSlider .items .image').on('click', function () {
            $('.image-slider .list#ProductSlider .items .image').removeClass('selected');
            $(this).addClass('selected');
            $('.image-slider .image-box .slider-images#ProductSliderBox .image')
                .hide()
                .eq($(this).index()).show();
        }).eq(0).addClass('selected');
	</script>
    <?$frame->end();?>
<?endif;?>
