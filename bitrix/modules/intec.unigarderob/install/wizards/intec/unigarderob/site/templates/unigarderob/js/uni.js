var UNI = {};
UNI.PHP = {};
UNI.Controls = {};

UNI.isFunction = function ($oFunction) {
    return Object.prototype.toString.call($oFunction) === '[object Function]';
};
UNI.isArray = function ($oArray) {
    return Object.prototype.toString.call($oArray) === '[object Array]';
};
UNI.isObject = function ($oObject) {
    return Object.prototype.toString.call($oObject) === '[object Object]';
};
UNI.forEach = function ($oObject, callback) {
    if (UNI.isFunction(callback)) {
        if (UNI.isArray($oObject)) {
            var $iArrayIndex = 0;
            var $iArrayLength = $oObject.length;
            
            for ($iArrayIndex = 0; $iArrayIndex < $iArrayLength; $iArrayIndex++) {
                var $oArrayEntry = $oObject[$iArrayIndex];
                
                if (callback($iArrayIndex, $oArrayEntry) == false)
                    break;
            }
        } else if (UNI.isObject($oObject)) {
            for (var $sObjectIndex in $oObject) {
                var $oObjectEntry = $oObject[$sObjectIndex];
                
                if (callback($sObjectIndex, $oObjectEntry) == false)
                    break;
            }
        }
    }
};

UNI.PHP.strReplace = function ($oParameters, $sString) {
    if (!UNI.isObject($oParameters))
        return $sString;
        
    UNI.forEach($oParameters, function ($sKey, $sValue) {
        $sString = $sString.replace(new RegExp(RegExp.escape($sKey),'g'), $sValue);
    });
    
    return $sString;
}

UNI.PHP.numberFormat = function ( number, decimals, dec_point, thousands_sep ) {
	var i, j, kw, kd, km;

	if( isNaN(decimals = Math.abs(decimals)) ){
		decimals = 2;
	}
	if( dec_point == undefined ){
		dec_point = ",";
	}
	if( thousands_sep == undefined ){
		thousands_sep = ".";
	}

	i = parseInt(number = (+number || 0).toFixed(decimals)) + "";

	if( (j = i.length) > 3 ){
		j = j % 3;
	} else{
		j = 0;
	}

	km = (j ? i.substr(0, j) + thousands_sep : "");
	kw = i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands_sep);
	kd = (decimals ? dec_point + Math.abs(number - i).toFixed(decimals).replace(/-/, 0).slice(2) : "");

	return km + kw + kd;
}

RegExp.escape = function ($sString) {
    return $sString.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
}

function UNINumericUpDown($oSettings)
{
    var that = this;
    
    this.defaults = {
        'VALUE': 1,
        'MINIMUM': 1,
        'MAXIMUM': 1,
        'RATIO': 1,
        'UNLIMITED': false,
        'EVENTS': {
            'onValueChange': function () {}
        },
        'VALUE_TYPE': 'INTEGER'
    }
    
    this.Settings = $.extend({}, this.defaults, $oSettings || {});
    
    this.constructor.prototype.HandleValue = function ($nValue) {
        if (this.Settings.VALUE_TYPE == 'FLOAT') {
            return parseFloat($nValue);
        } else {
            return parseInt($nValue);
        } 
    }
    
	this.constructor.prototype.Increase = function () {
		return this.SetValue(this.HandleValue(this.Settings.VALUE) + this.HandleValue(this.Settings.RATIO));
	}
	
	this.constructor.prototype.Decrease = function () {
		return this.SetValue(this.HandleValue(this.Settings.VALUE) - this.HandleValue(this.Settings.RATIO));
	}
    
    this.constructor.prototype.GetValue = function () {
        if (parseInt(this.Settings.VALUE * 100) % 100 == 0) {
            return this.HandleValue(this.Settings.VALUE).toFixed(0);
        } else {
            if (parseInt(this.Settings.VALUE * 100) % 10 == 0) {
                return this.HandleValue(this.Settings.VALUE).toFixed(1);
            } else {
                return this.HandleValue(this.Settings.VALUE).toFixed(2);
            }
        } 
    }
	
	this.constructor.prototype.SetValue = function ($nValue) {
		$nValue = this.HandleValue($nValue);
        var $nNewValue = this.Settings.VALUE;
		
		if (isNaN($nValue) || $nValue <= this.HandleValue(this.Settings.MINIMUM)) {
			$nNewValue = this.HandleValue(this.Settings.MINIMUM);
		} else if ($nValue >= this.HandleValue(this.Settings.MAXIMUM) && this.Settings.UNLIMITED == false) {
            $nNewValue = this.HandleValue(this.Settings.MAXIMUM);
		} else {
			if ((Math.round($nValue * 100) % Math.round(this.Settings.RATIO * 100)) == 0) {
				$nNewValue = $nValue;
			} else {
				$nNewValue = this.HandleValue($nValue - ($nValue % this.Settings.RATIO));
			}
		}
        
        this.Settings.VALUE = $nNewValue;
        
        if (UNI.isFunction(this.Settings.EVENTS.onValueChange))
                this.Settings.EVENTS.onValueChange(this);
	}
}

function UNISlider ($oSettings) {
    var that = this;
    
    this.defaults = {
        'INFINITY_SLIDE': false,
        'SLIDER': '.slider',
        'ELEMENT': '.slide',
        'CURRENT': 0,
        'OFFSET': 1,
        'CUSTOM_CHANGE_RULE': null,
        'ANIMATE': false,
        'ANIMATE_SPEED': 500,
        'EVENTS': {
            'onSlideLeft': function () {},
            'onSlideRight': function () {},
            'onSlide': function () {},
            'onAdaptabilityChange': function () {},
            'onBeforSlide': function () {},
            'onAfterSlide': function () {}
        },
        'ADAPTABILITY': []
    }
    
    this.Settings = $.extend({}, this.defaults, $oSettings || {});
    
    this.constructor.prototype.GetMinSlide = function () {
        return 0;
    }
    
    this.constructor.prototype.GetMaxSlide = function () {
        return this.GetElementsCount() - this.Settings.OFFSET;
    }
    
    this.constructor.prototype.GetCurrentSlide = function () {
        return this.Settings.CURRENT;
    }
    
    this.constructor.prototype.GetElements = function () {
        return $(this.Settings.ELEMENT)
    }
    
    this.constructor.prototype.GetElementsCount = function () {
        return this.GetElements().size();
    }
    
    this.constructor.prototype.GetSliderWidth = function () {
        var $oSlider = $(this.Settings.SLIDER);
        var $fWidth = 0;
        
        if ($oSlider[0] !== undefined) 
            if (UNI.isFunction($oSlider[0].getBoundingClientRect)){
                var $oRectangle = $oSlider[0].getBoundingClientRect();
                $fWidth = parseFloat($oRectangle.right - $oRectangle.left);
            }
        
        if ($fWidth == 0)
            if ($oSlider.css('box-sizing') == 'border-box') {
                $fWidth = parseFloat($oSlider.outerWidth(false));
            } else {
                $fWidth = parseFloat($oSlider.width());
            }
            
        return $fWidth;
    }
    
    this.constructor.prototype.GetElementWidth = function () {
        var $oElements = $(this.Settings.ELEMENT);
        var $fWidth = 0;
        
        if ($oElements[0] !== undefined) 
            if (UNI.isFunction($oElements[0].getBoundingClientRect)){
                var $oRectangle = $oElements[0].getBoundingClientRect();
                $fWidth = parseFloat($oRectangle.right - $oRectangle.left);
            }
        
        if ($fWidth == 0)
            if ($oElements.css('box-sizing') == 'border-box') {
                $fWidth = parseFloat($oElements.outerWidth(false));
            } else {
                $fWidth = parseFloat($oElements.width());
            }
            
        return $fWidth;
    }
    
    this.constructor.prototype.SlideLeft = function () {
        this.SlideTo(this.Settings.CURRENT + 1);
        
        if (UNI.isObject(this.Settings.EVENTS))
            if (UNI.isFunction(this.Settings.EVENTS.onSlideLeft))
                this.Settings.EVENTS.onSlideLeft(this);
    }
    
    this.constructor.prototype.SlideRight = function () {
        this.SlideTo(this.Settings.CURRENT - 1);
        
        if (UNI.isObject(this.Settings.EVENTS))
            if (UNI.isFunction(this.Settings.EVENTS.onSlideRight))
                this.Settings.EVENTS.onSlideRight(this);
    }
    
    this.constructor.prototype.Slide = function ($iSlideNumber) {
        this.SlideTo($iSlideNumber);
        
        if (UNI.isObject(this.Settings.EVENTS))
            if (UNI.isFunction(this.Settings.EVENTS.onSlide))
                this.Settings.EVENTS.onSlide(this);
    }
    
    this.constructor.prototype.SlideTo = function ($iSlideNumber) {
        var $oSettings = {};
        $oSettings.SLIDE = {};
        $oSettings.SLIDE.CURRENT = this.GetCurrentSlide();
        $oSettings.SLIDE.NEXT = parseInt($iSlideNumber);
        $oSettings.BOUNDARIES = {};
        $oSettings.BOUNDARIES.MINIMUM = this.GetMinSlide();
        $oSettings.BOUNDARIES.MAXIMUM = this.GetMaxSlide();
        $oSettings.ELEMENT = {};
        $oSettings.ELEMENT.WIDTH = this.GetElementWidth();
        $oSettings.ANIMATE = this.Settings.ANIMATE;
        $oSettings.ANIMATE_SPEED = this.Settings.ANIMATE_SPEED;
        $oSettings.INFINITY_SLIDE = this.Settings.INFINITY_SLIDE;
        
        if (UNI.isFunction(this.Settings.EVENTS.onBeforeSlide))
            this.Settings.EVENTS.onBeforeSlide(this, $oSettings);
        
        if ($oSettings.SLIDE.NEXT > $oSettings.BOUNDARIES.MAXIMUM)
            if ($oSettings.INFINITY_SLIDE == true) {
                $oSettings.SLIDE.NEXT = $oSettings.BOUNDARIES.MINIMUM;
            } else {
                $oSettings.SLIDE.NEXT = $oSettings.BOUNDARIES.MAXIMUM;
            }
        
        if ($oSettings.SLIDE.NEXT < $oSettings.BOUNDARIES.MINIMUM)
            if ($oSettings.INFINITY_SLIDE == true) {
                $oSettings.SLIDE.NEXT = $oSettings.BOUNDARIES.MAXIMUM;
            } else {
                $oSettings.SLIDE.NEXT = $oSettings.BOUNDARIES.MINIMUM;
            }
        
        if (UNI.isFunction(this.Settings.CUSTOM_CHANGE_RULE)) {
            this.Settings.CUSTOM_CHANGE_RULE(this, $oSettings);
        } else {
            if ($oSettings.ANIMATE == true) {
                $(this.Settings.SLIDER).stop().animate({scrollLeft:$oSettings.SLIDE.NEXT * $oSettings.ELEMENT.WIDTH}, $oSettings.ANIMATE_SPEED);
            } else {
                $(this.Settings.SLIDER).scrollLeft($oSettings.SLIDE.NEXT * $oSettings.ELEMENT.WIDTH);
            }
        }
        
        if (UNI.isFunction(this.Settings.EVENTS.onAfterSlide))
            this.Settings.EVENTS.onAfterSlide(this, $oSettings);
        
        this.Settings.CURRENT = $oSettings.SLIDE.NEXT;
    }
    
    this.constructor.prototype.__ChangeRules = function () {
        var $iCurrentWidth = $(window).width();
        var $arRules = this.Settings.ADAPTABILITY;
        var $iRulesCount = $arRules.length;
        var $iCurrentRuleWidth = -1;
        var $oCurrentRule = {'WIDTH':'DEFAULT', 'SETTINGS':{}};
        var $bAnimate = this.Settings.ANIMATE;
        
        for (var $iRuleIndex = 0; $iRuleIndex < $iRulesCount; $iRuleIndex++) {
            if ($arRules[$iRuleIndex].WIDTH != 'DEFAULT') {
                var $iRuleWidth = parseInt($arRules[$iRuleIndex].WIDTH);
                
                if ($iRuleWidth > $iCurrentWidth && (($iRuleWidth < $iCurrentRuleWidth) || ($iCurrentRuleWidth < 0))) {
                    $iCurrentRuleWidth = $iRuleWidth;
                    $oCurrentRule = $arRules[$iRuleIndex];
                }
            } else {
                $oCurrentRule = $arRules[$iRuleIndex];
            }
        }
        
        if (UNI.isObject($oCurrentRule.SETTINGS))    
            for (var $sSettingKey in $oCurrentRule.SETTINGS)
                this.Settings[$sSettingKey] = $oCurrentRule.SETTINGS[$sSettingKey];
                
        if (UNI.isFunction($oCurrentRule.ACTION))
            $oCurrentRule.ACTION(this);
        
        this.Settings.ANIMATE = false;
        this.SlideTo(this.GetCurrentSlide());
        this.Settings.ANIMATE = $bAnimate;
        
        if (UNI.isObject(this.Settings.EVENTS))
            if (UNI.isFunction(this.Settings.EVENTS.onAdaptabilityChange))
                this.Settings.EVENTS.onAdaptabilityChange(this);
    }
    
    this.__ChangeRules();
    
    $(window).on('resize', function () {
        that.__ChangeRules();
    });
}
function UNISku ($oSettings) {
    this.defaults = {
        'CATALOG':{
            'CONFIG': {
               'SHOW_QUANTITY': false,
               'SHOW_DISCOUNT_PERCENT': false,
               'SHOW_OLD_PRICE': false,
               'DISPLAY_COMPARE': false,
               'SHOW_SKU_PROPS': false,
               'OFFER_GROUP': false,
               'MAIN_PICTURE_MODE': 'IMG'
            },
            'VISUAL': {
               'ID': '',
               'CURRENT_PATH': '',
               'ONE_CLICK_BUY': ''
            },
            'DEFAULT_PICTURE': {
               'PREVIEW_PICTURE': undefined,
               'DETAIL_PICTURE': undefined
            },
            'PRODUCT': {
               'ID': '0',
               'NAME': ''
            },
            'OFFERS': [],
            'OFFER_SELECTED': '0',
            'TREE_PROPS': []
        },
        'EVENTS': {
            'onChangeProperty': function () {},
        }
    };
    
    this.settings = $.extend({}, this.defaults, $oSettings || {});
    
    function __inArray($oNeedle, $oHaystack, $bStrict) {
        var $bFounded = false; $bStrict = !!$bStrict;
        
        UNI.forEach($oHaystack, function($oKey, $oValue) {
            if (UNI.isObject($oNeedle) || UNI.isArray($oNeedle)) {
                if (__equal($oNeedle, $oValue)) {
                    $bFounded = true;
                    return false;
                }
            } else {
                if ($bStrict)
                    if ($oValue === $oNeedle) {
                        $bFounded = true;
                        return false;
                    }
                    
                if (!$bStrict)
                    if ($oValue == $oNeedle) {
                        $bFounded = true;
                        return false;
                    }
            }
        });
        
        return $bFounded;
    }
    
    function __equal($oFirstObject, $oSecondObject) {        
        if (UNI.isObject($oFirstObject) && UNI.isObject($oSecondObject)) {
            var $arFirstObjectKeys = Object.keys($oFirstObject);
            var $arSecondObjectKeys = Object.keys($oSecondObject);
            
            if ($arFirstObjectKeys.length != $arSecondObjectKeys.length ) return false;
        
        	return !$arFirstObjectKeys.filter(function($sKey){
        		if (typeof $oFirstObject[$sKey] == "object" ||  Array.isArray($oFirstObject[$sKey])) {
        			return !__equal($oFirstObject[$sKey], $oSecondObject[$sKey]);
        		} else {
        			return $oFirstObject[$sKey] !== $oSecondObject[$sKey];
        		}
        	}).length;
        }
    	
        return false;
    }
    
    this.constructor.prototype.__init = function () {
        var that = this;
        UNI.forEach(this.settings.CATALOG.OFFERS, function ($iOfferArrayID, $oOffer) {
            that.settings.CATALOG.OFFER_SELECTED = $oOffer.ID;
            if (UNI.isFunction(that.settings.EVENTS['onChangeProperty']))
                    that.settings.EVENTS['onChangeProperty'](that.__getSettings());
            return false;
        });
    }
    
    this.constructor.prototype.__getSettings = function () {
        var that = this;
        
        $oSettings = {
            'CATALOG': that.settings.CATALOG,
            'OFFER': that.GetOffer(),
            'PROPERTIES': {
                'DISPLAYED': [],
                'ENABLED': [],
                'DISABLED': []
            }
        };
        
        if ($oSettings['OFFER'] != null) {
            // ������������ ��������
            UNI.forEach(that.settings.CATALOG.OFFERS, function ($iOfferArrayID, $oOffer) {
                UNI.forEach($oOffer.TREE, function ($sTreeKey, $sTreeValue) {
                    $oProperty = {'KEY':$sTreeKey, 'VALUE':$sTreeValue};
                    // ���� �������� ��� � �������, �� ��������
                    if (!__inArray($oProperty, $oSettings.PROPERTIES.DISPLAYED))
                        $oSettings.PROPERTIES.DISPLAYED.push($oProperty);
                })
            });
            
            var $arProperties = []; // ���� �������
            var $arPropertiesSelected = []; // ��� ��������� ��������
            
            // ���� ��� ���� �������
            UNI.forEach($oSettings['OFFER']['TREE'], function ($sTreeKey, $sTreeValue) {
                if (!__inArray($sTreeKey))
                    $arProperties.push($sTreeKey);
            });
            
            // �������� �� ���� ���������
            UNI.forEach($arProperties, function ($iPropertyIndex, $sPropertyKey) {
                var $sCurrentProperty = $arProperties[$iPropertyIndex]; // ������������� �������
                // �������� �� ���� �������� ������������
                UNI.forEach(that.settings.CATALOG.OFFERS, function ($iOfferIndex, $oOffer) {
                    var $bCompared = true; // ������������� ���������
                    // �������� �� ���� ��� ��������� ��������� � ���������� �� �� ���������� �������� ��������� �����������
                    UNI.forEach($arPropertiesSelected, function ($iPropertySelectedIndex, $sPropertySelectedKey) {
                        if (that.settings.CATALOG.OFFERS[$iOfferIndex]['TREE'][$sPropertySelectedKey] != $oSettings.OFFER.TREE[$sPropertySelectedKey]) {
                            $bCompared = false; // ���� �������� �� ����� �� ������������� ��������� � �������
                            return false;
                        }
                    });
                    // ���� ��������� ������������, �� ��������� �������� �� ���������� ����� � ����������������
                    if ($bCompared == true) {
                        $oEnabledProperty = {'KEY':$sCurrentProperty, 'VALUE':that.settings.CATALOG.OFFERS[$iOfferIndex]['TREE'][$sCurrentProperty]};
                
                        if (!__inArray($oEnabledProperty, $oSettings.PROPERTIES.ENABLED))
                            $oSettings.PROPERTIES.ENABLED.push($oEnabledProperty);
                    }
                });
                // ��������� ������� �������� � ���������� �������� ��� �������� ���������
                $arPropertiesSelected.push($sCurrentProperty);
            });
            
            // ������ ������� ���� ������������ ������� � ���� ���, ������������� � ENABLED, ��������� �� � DISABLED
            UNI.forEach($oSettings.PROPERTIES.DISPLAYED, function ($iPropertyIndex, $oProperty) {
                if (!__inArray($oProperty, $oSettings.PROPERTIES.ENABLED)) {
                    $oSettings.PROPERTIES.DISABLED.push($oProperty);
                }
            });
        }
        // ���������� ��������� ���������
        return $oSettings;
    }
    
    this.constructor.prototype.SetOfferByID = function ($iOfferID) {
        var that = this;
        UNI.forEach(this.settings.CATALOG.OFFERS, function ($iOfferArrayID, $oOffer) {
            if ($oOffer.ID == $iOfferID) {
                that.settings.CATALOG.OFFER_SELECTED = $oOffer.ID;
                if (UNI.isFunction(that.settings.EVENTS['onChangeProperty']))
                    that.settings.EVENTS['onChangeProperty'](that.__getSettings());
                return false;
            }
        }); 
    };
    
    this.constructor.prototype.GetOffer = function () {
        var that = this;
        var $oCurrentOffer = null;
        
        // ���������, ���� ���� ��������� �������� �����������
        if (parseInt(this.settings.CATALOG.OFFER_SELECTED) > 0)
            UNI.forEach(this.settings.CATALOG.OFFERS, function ($iOfferArrayID, $oOffer) {
                if ($oOffer.ID == that.settings.CATALOG.OFFER_SELECTED) {
                    $oCurrentOffer = $oOffer;
                    return false;
                }
            });
                
        return $oCurrentOffer;
    }
    
    this.constructor.prototype.SetOfferByProperty = function ($sPropKey, $sPropValue) {
        var that = this;
        var $oOffer = this.GetOffer(); // �������� ������� �������� �����������
        var $oCompareTree = {}; // ������� ������ ������ ��� ������� ���������
                
        if ($oOffer !== null && $oOffer !== undefined) {
            var $bFounded = false;
            
            // ���������� ������ ������ ��� ����������� �������� � ������ ���������
            UNI.forEach($oOffer.TREE, function ($sTreeKey, $sTreeValue) {
                $oCompareTree[$sTreeKey] = $sTreeValue;
                if ($sTreeKey == $sPropKey) {
                    $oCompareTree[$sTreeKey] = $sPropValue;
                } 
            });
            
            // �������� ��������� ������� ���� �������� �����������
            UNI.forEach(this.settings.CATALOG.OFFERS, function ($iOfferArrayID, $oOffer) {
                var $bCompared = true;
                // ���������� ������ ��������
                UNI.forEach($oCompareTree, function ($sTreeKey, $sTreeValue) {
                    if ($oOffer.TREE[$sTreeKey] != $sTreeValue) {
                        $bCompared = false;
                        
                        return false; // ������� �� �����
                    }
                });
                // ���� ����� ����������, �� ������������� ��� �������
                if ($bCompared) {
                    that.settings.CATALOG.OFFER_SELECTED = $oOffer.ID;
                    //ConsoleHere
                    console.log(that.__getSettings());
                    
                    if (UNI.isFunction(that.settings.EVENTS['onChangeProperty']))
                        that.settings.EVENTS['onChangeProperty'](that.__getSettings());
                    
                    $bFounded = true;
                    return false; // ������� �� �����
                }
            });
            
            // ���� �� ����� ���������� �� ����������, �� �������� ����� ������ ���������� �������� � ����������� �����������
            if ($bFounded) return;
            $oCompareTree = {}; // ���������� ������ ���������
            
            // ���������� ������ �� �������������� ���� �������� � ������ ��� ����������� �������� � ������ ���������
            UNI.forEach($oOffer.TREE, function ($sTreeKey, $sTreeValue) {
                $oCompareTree[$sTreeKey] = $sTreeValue;
                if ($sTreeKey == $sPropKey) {
                    $oCompareTree[$sTreeKey] = $sPropValue;
                    return false;
                } 
            });
            
            // �������� ��������� ������� ���� �������� �����������
            UNI.forEach(this.settings.CATALOG.OFFERS, function ($iOfferArrayID, $oOffer) {
                var $bCompared = true;
                // ���������� ������ ��������
                UNI.forEach($oCompareTree, function ($sTreeKey, $sTreeValue) {
                    if ($oOffer.TREE[$sTreeKey] != $sTreeValue) {
                        $bCompared = false;
                        
                        return false; // ������� �� �����
                    }
                });
                // ���� ����� ����������, �� ������������� ��� �������
                if ($bCompared) {
                    that.settings.CATALOG.OFFER_SELECTED = $oOffer.ID;
                    
                    if (UNI.isFunction(that.settings.EVENTS['onChangeProperty']))
                        that.settings.EVENTS['onChangeProperty'](that.__getSettings());
                        
                    return false; // ������� �� �����
                }
            });
        }
    }
    
    this.__init();
}