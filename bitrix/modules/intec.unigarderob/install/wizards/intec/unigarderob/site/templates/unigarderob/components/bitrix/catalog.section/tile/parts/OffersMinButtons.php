<?foreach ($arItem['OFFERS'] as $arOffer):?>
    <div class="min-buttons SKUMinButtons SKUMinButtons<?=$arOffer['ID']?>" style="display: none;">
        <div class="min-buttons-wrapper">
    		<?if($arParams["DISPLAY_COMPARE"] == "Y"):?>
    			<div class="min-button compare">
    				<div class="add addToCompare addToCompare<?=$arOffer["ID"]?>"
    					onclick="SaleActions.addToCompare('<?=$arOffer['COMPARE_URL']?>', SaleWrapper.updateAfterCompareAction);"
    					title="<?=GetMessage('COMPARE_TEXT_DETAIL')?>"					
    				>
    				</div>
    				<div  class="remove removeFromCompare removeFromCompare<?=$arOffer["ID"]?>"
    					style="display:none"
    					onclick="SaleActions.removeFromCompare('<?=$arOffer['COMPARE_REMOVE_URL']?>', SaleWrapper.updateAfterCompareAction);"
    					title="<?=GetMessage('COMPARE_DELETE_TEXT_DETAIL')?>"
    				>
    				</div>
    			</div>
    		<?endif?>
    		<?if ($arOffer['CAN_BUY']):?>
    			<div class="min-button like">
    				<div class="add addToWishList addToWishList<?=$arOffer["ID"]?>"								
    					onclick="SaleActions.addToWishList('<?=$arOffer['ID']?>', '<?=$arOffer['MIN_PRICE']['CURRENCY']?>', 1, SaleWrapper.updateAfterBasketAction);"	
    					title="<?=GetMessage('LIKE_TEXT_DETAIL')?>"
    				>
    				</div>
    				<div class="remove removeFromWishList removeFromWishList<?=$arOffer["ID"]?>"
    					style="display:none"								
    					onclick="SaleActions.removeFromWishList('<?=$arOffer['ID']?>', SaleWrapper.updateAfterBasketAction);"
    					title="<?=GetMessage('LIKE_DELETE_TEXT_DETAIL')?>"
    				>
    				</div>
    			</div>
    		<?endif;?>
        </div>
    </div>
<?endforeach;?>