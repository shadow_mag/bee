<?
    $arJSParams = array(
		'PRODUCT_TYPE' => $arResult['CATALOG_TYPE'],
		'PRODUCT' => array(
			'ID' => $arItem['ID'],
			'NAME' => $arItem['~NAME']
		),
		'OFFERS' => $arItem['JS_OFFERS'],
		'OFFER_SELECTED' => $arResult['OFFERS_SELECTED'],
		'TREE_PROPS' => $arSkuProps
	);
?>
<script type="text/javascript">
    var SKUProduct<?=$arItem['ID']?> = new UNISku({
        'CATALOG': <?=CUtil::PhpToJSObject($arJSParams, false, true);?>,
        'EVENTS': {
            'onChangeProperty': function ($oParameters) {
                $oProduct = $oParameters.CATALOG.PRODUCT;
                $oOffer = $oParameters.OFFER;
                $arPropDisplayed = $oParameters.PROPERTIES.DISPLAYED;
                $arPropDisabled = $oParameters.PROPERTIES.DISABLED;
                $arPropEnabled = $oParameters.PROPERTIES.ENABLED;
                $arPropSelected = $oParameters.OFFER.TREE;
                
                $('#Product' + $oProduct.ID + ' .offers.properties .property .values .value')
                    .removeClass('selected')
                    .addClass('hidden')
                    .removeClass('enabled')
                    .removeClass('disabled');
                
                $('#Product' + $oProduct.ID + ' .SKUBuyButton').hide();
                $('#Product' + $oProduct.ID + ' .SKUMinButtons').hide();
                
                for (var $iNumber = 0; $iNumber < $arPropDisplayed.length; $iNumber++) {
                    $('#Product' + $oProduct.ID + ' .SKUProductProperty' + $arPropDisplayed[$iNumber]['KEY'] + '' + $arPropDisplayed[$iNumber]['VALUE'])
                        .addClass('hidden');
                }
                
                for (var $iNumber = 0; $iNumber < $arPropDisabled.length; $iNumber++) {
                    $('#Product' + $oProduct.ID + ' .SKUProductProperty' + $arPropDisabled[$iNumber]['KEY'] + '' + $arPropDisabled[$iNumber]['VALUE'])
                        .removeClass('hidden')
                        .addClass('disabled');
                }
                
                for (var $iNumber = 0; $iNumber < $arPropEnabled.length; $iNumber++) {
                    $('#Product' + $oProduct.ID + ' .SKUProductProperty' + $arPropEnabled[$iNumber]['KEY'] + '' + $arPropEnabled[$iNumber]['VALUE'])
                        .removeClass('hidden')
                        .addClass('enabled');
                }
                
                for (var $sKey in $arPropSelected) {
                    $('#Product' + $oProduct.ID + ' .SKUProductProperty' + $sKey + '' + $arPropSelected[$sKey])
                        .addClass('selected');
                }
                
                $('#Product' + $oProduct.ID + ' .SKUProductImage').hide();
                $('#Product' + $oProduct.ID + ' .SKUProductImage' + $oOffer.ID).show();
                $('#Product' + $oProduct.ID + ' .SKUMinButtons' + $oOffer.ID).css('display', 'inline-block');
                $('#Product' + $oProduct.ID + ' .SKUBuyButton' + $oOffer.ID).css('display', 'block');
                
                if ($oOffer.PRICE.VALUE == $oOffer.PRICE.DISCOUNT_VALUE) {
                    $('#Product' + $oProduct.ID + ' .SKUProductPrice').html($oOffer.PRICE.PRINT_VALUE);
					$('#Product' + $oProduct.ID + ' .SKUProductPriceOld').hide().html('');
                } else {
                    $('#Product' + $oProduct.ID + ' .SKUProductPrice').html($oOffer.PRICE.PRINT_DISCOUNT_VALUE);
					$('#Product' + $oProduct.ID + ' .SKUProductPriceOld').html($oOffer.PRICE.PRINT_VALUE);
				}
            }
        }
    })
</script>