<?if ($arItem['CAN_BUY']):?>
    <div class="button-buy-click">
        <?$sBuyBittonClickImage = !empty($arItem['DETAIL_PICTURE']['SRC']) ? $arItem['DETAIL_PICTURE']['SRC'] : $arItem['PREVIEW_PICTURE']['SRC']?>
        <?$arBuyButtonClickParams = array (
			"ID" => $arItem["ID"],
			"Name" => $arItem["NAME"],
			"IBlockType" => $arParams["IBLOCK_TYPE"],
			"IBlockID" => $arParams["IBLOCK_ID"],
			"PriceCurrent" => $arItem['MIN_PRICE']['PRINT_DISCOUNT_VALUE'],
			"PriceOld" => "",
			"Image" => $sBuyBittonClickImage,
            "Currency" => $arItem['MIN_PRICE']['CURRENCY'],
            "PriceID" => $arItem['MIN_BASIS_PRICE']['PRICE_ID'],
			"Price" => $arItem['MIN_PRICE']['VALUE']
		);?>
		<a 
			href="javascript:void(0);" 
			class="uni-button button" 
			onclick="Popups.OneClickBuy(<?=CUtil::PhpToJSObject($arBuyButtonClickParams);?>)"
		><?=GetMessage("CATALOG_ONE_CLICK_BUY")?></a>
    </div>
    <div class="button-buy">
        <a rel="nofollow" class="uni-button solid_button button addToBasket addToBasket<?=$arItem["ID"]?>"
			onclick="SaleActions.addToBasket('<?=$arItem['ID']?>', '<?=$arItem['MIN_PRICE']['CURRENCY']?>', <?=$arItem['CATALOG_MEASURE_RATIO']?>, SaleWrapper.updateAfterBasketAction);"
		><?=GetMessage("CATALOG_ADD")?>
		</a>
		<a rel="nofollow" href="<?=$arParams["BASKET_URL"];?>" class="uni-button solid_button button removeFromBasket removeFromBasket<?=$arItem["ID"]?>" style="display: none;">
			<?=GetMessage("CATALOG_ADDED")?>
		</a>
    </div>
<?endif;?>
<div class="min-buttons">
    <div class="min-buttons-wrapper">
		<?if($arParams["DISPLAY_COMPARE"] == "Y"):?>
			<div class="min-button compare">
				<div class="add addToCompare addToCompare<?=$arItem["ID"]?>"
					onclick="SaleActions.addToCompare('<?=$arItem['COMPARE_URL']?>', SaleWrapper.updateAfterCompareAction);"
					title="<?=GetMessage('COMPARE_TEXT_DETAIL')?>"					
				>
				</div>
				<div  class="remove removeFromCompare removeFromCompare<?=$arItem["ID"]?>"
					style="display:none"
					onclick="SaleActions.removeFromCompare('<?=$arItem['COMPARE_REMOVE_URL']?>', SaleWrapper.updateAfterCompareAction);"
					title="<?=GetMessage('COMPARE_DELETE_TEXT_DETAIL')?>"
				>
				</div>
			</div>
		<?endif?>
		<?if ($arItem['CAN_BUY']):?>
			<div class="min-button like">
				<div class="add addToWishList addToWishList<?=$arItem["ID"]?>"								
					onclick="SaleActions.addToWishList('<?=$arItem['ID']?>', '<?=$arItem['MIN_PRICE']['CURRENCY']?>', 1, SaleWrapper.updateAfterBasketAction);"
					title="<?=GetMessage('LIKE_TEXT_DETAIL')?>"
				>
				</div>
				<div class="remove removeFromWishList removeFromWishList<?=$arItem["ID"]?>"
					style="display:none"							
					onclick="SaleActions.removeFromWishList('<?=$arItem['ID']?>', SaleWrapper.updateAfterBasketAction);"
					title="<?=GetMessage('LIKE_DELETE_TEXT_DETAIL')?>"
				>
				</div>
			</div>
		<?endif;?>
    </div>
</div>