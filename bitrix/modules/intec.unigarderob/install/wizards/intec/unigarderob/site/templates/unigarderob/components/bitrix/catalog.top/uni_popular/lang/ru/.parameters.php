<?
	$MESS["UNI_POPULAR_TITLE"] = "Текст заголовока";
	$MESS["UNI_POPULAR_TITLE_DEFAULT"] = "Популярные товары";
    $MESS["UNI_POPULAR_LINE_ELEMENT_COUNT"] = "Количество отображаемых элементов";
    $MESS["UNI_POPULAR_USE_SLIDER"] = "Использовать слайдер";
    $MESS["UNI_POPULAR_USE_ARROWS"] = "Отображать стрелки прокрутки";
    $MESS["UNI_POPULAR_USE_POINTS"] = "Отображать точки прокрутки";
?>