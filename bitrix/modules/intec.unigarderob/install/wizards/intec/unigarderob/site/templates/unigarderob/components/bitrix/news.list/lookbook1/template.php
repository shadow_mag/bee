<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>
<?$this->setFrameMode(true);?>
<?$sUniqueID = 'uni-lookboks-2'.spl_object_hash($this);?>
<?if (!empty($arResult["ITEMS"])) {?>
	<?$href = str_replace("#SITE_DIR#/", SITE_DIR, $arResult["LIST_PAGE_URL"]);?>
	<div class="uni-lookboks-2" id="<?=$sUniqueID?>">
		<div class="uni-lookboks-wrapper" id="<?=$sUniqueID.'tabs'?>">
			<div class="tabs">
				<h3 class="header_grey ">
					Lookbook
				</h3>
			</div>
            <div style="padding-top: 15px; clear: both;"></div>

				<div id="<?=$sUniqueID.'tab'.$arItems["XML_ID"]?>">
					<?$GLOBALS["arr".$arItems["XML_ID"]] = array("PROPERTY_".$arItems["PROPERTY_CODE"]."_VALUE" => $arItems["VALUE"]);?>
					<?$APPLICATION->IncludeComponent(
						"bitrix:catalog.top", 
						"uni_lookbook", 
						array(
							"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
							"IBLOCK_ID" => $arParams["IBLOCK_ID"],
							"ELEMENT_SORT_FIELD" => "RAND",
							"ELEMENT_SORT_ORDER" => "asc",
							"FILTER_NAME" => "arr".$arItems["XML_ID"],
							"HIDE_NOT_AVAILABLE" => "N",
							"ELEMENT_COUNT" => $arParams["ELEMENT_COUNT"],
							"LINE_ELEMENT_COUNT" => $arParams["LINE_ELEMENT_COUNT"],
							"OFFERS_SORT_FIELD" => "sort",
                    		"OFFERS_SORT_ORDER" => "asc",
                    		"OFFERS_SORT_FIELD2" => "id",
                    		"OFFERS_SORT_ORDER2" => "desc",
                    		"OFFERS_LIMIT" => "0",
                            "OFFERS_PROPERTY_CODE" => $arParams['OFFERS_PROPERTY_CODE'],
                            "PRICE_CODE" => $arParams['PRICE_CODE'],
							"CACHE_TYPE" => "A",
							"CACHE_TIME" => "36000000",
							"CACHE_GROUPS" => "Y",
							"DISPLAY_COMPARE" => $arParams["DISPLAY_COMPARE"],
							"COMPARE_NAME" => $arParams["COMPARE_NAME"],
							"SEF_MODE" => "N",
                            "USE_SLIDER" => "Y",
                            "USE_SLIDER_ARROWS" => "Y",
                            "USE_SLIDER_POINTS" => "Y",
						),
						$component,
						array("HIDE_ICONS" => "Y")
					);?>
				</div>
				<div class="see_all"><a href="<?=$href?>"><?=GetMessage("ALL_LOOKBOOK")?>&nbsp;<span style="font-size:16px;">&#8250;</span></a></div>
		</div>
	</div>
	<script>
		$(document).ready(function(){
			$("#<?=$sUniqueID.'tabs'?>").tabs();
		})
	</script>
<?}?>
