<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();

if (!CModule::IncludeModule("highloadblock"))
	return;

if (!WIZARD_INSTALL_DEMO_DATA)
	return;

$COLOR_ID = $_SESSION["ESHOP_HBLOCK_COLOR_ID"];
unset($_SESSION["ESHOP_HBLOCK_COLOR_ID"]);

$BRAND_ID = $_SESSION["ESHOP_HBLOCK_BRAND_ID"];
unset($_SESSION["ESHOP_HBLOCK_BRAND_ID"]);

//adding rows
WizardServices::IncludeServiceLang("references.php", LANGUAGE_ID);

use Bitrix\Highloadblock as HL;
global $USER_FIELD_MANAGER;

if ($COLOR_ID)
{
	$hldata = HL\HighloadBlockTable::getById($COLOR_ID)->fetch();
	$hlentity = HL\HighloadBlockTable::compileEntity($hldata);

	$entity_data_class = $hlentity->getDataClass();
	$arColors = array(
		"PURPLE" => "colors/Purple.jpg",
		"BROWN" => "colors/Brown.jpg",
		"SEE" => "colors/Cyan.jpg",
		"BLUE" => "colors/Blue.jpg",
		"ORANGERED" => "colors/Orange-Red.jpg",
		"REDBLUE" => "colors/Blue-Red.jpg",
		"RED" => "colors/Red.jpg",
		"GREEN" => "colors/Green.jpg",
		"WHITE" => "colors/White.jpg",
		"BLACK" => "colors/Black.jpg",
		"PINK" => "colors/Pink.jpg",
		"AZURE" => "colors/Blue.jpg",
		"JEANS" => "colors/Blue-Jeans.jpg",
		"FLOWERS" => "colors/Black-White-Flowers.jpg",
		"GRAY" => "colors/Gray.png",
		"SILVER" => "colors/Silver.png",
		"GOLD" => "colors/Gold.jpg",
		"123" => "colors/Yellow.png",
		"321" => "colors/Orange.jpg",
		"blue_in_line" => "colors/BlueInLine.jpg",
		"etnic" => "colors/Etnic.jpg",
		"natural" => "colors/Natural.jpg"
	);
	$sort = 0;
	foreach($arColors as $colorName=>$colorFile)
	{
		$sort+= 100;
		$arData = array(
			'UF_NAME' => GetMessage("WZD_REF_COLOR_".$colorName),
			'UF_FILE' =>
				array (
					'name' => ToLower($colorName).".jpg",
					'type' => 'image/jpeg',
					'tmp_name' => WIZARD_ABSOLUTE_PATH."/site/services/highloadblock/".$colorFile
				),
			'UF_SORT' => $sort,
			'UF_DEF' => ($sort > 100) ? "0" : "1",
			'UF_XML_ID' => ToLower($colorName)
		);
		$USER_FIELD_MANAGER->EditFormAddFields('HLBLOCK_'.$COLOR_ID, $arData);
		$USER_FIELD_MANAGER->checkFields('HLBLOCK_'.$COLOR_ID, null, $arData);

		$result = $entity_data_class::add($arData);
	}
}

if ($BRAND_ID)
{
	$hldata = HL\HighloadBlockTable::getById($BRAND_ID)->fetch();
	$hlentity = HL\HighloadBlockTable::compileEntity($hldata);

	$entity_data_class = $hlentity->getDataClass();
	$arBrands = array(
		"COMPANY1" => "brands/cm-01.png",
		"COMPANY2" => "brands/cm-02.png",
		"COMPANY3" => "brands/cm-03.png",
		"COMPANY4" => "brands/cm-04.png",
		"BRAND1" => "brands/bn-01.png",
		"BRAND2" => "brands/bn-02.png",
		"BRAND3" => "brands/bn-03.png",
	);
	$sort = 0;
	foreach($arBrands as $brandName=>$brandFile)
	{
		$sort+= 100;
		$arData = array(
			'UF_NAME' => GetMessage("WZD_REF_BRAND_".$brandName),
			'UF_FILE' =>
				array (
					'name' => ToLower($brandName).".png",
					'type' => 'image/png',
					'tmp_name' => WIZARD_ABSOLUTE_PATH."/site/services/highloadblock/".$brandFile
				),
			'UF_SORT' => $sort,
			'UF_DESCRIPTION' => GetMessage("WZD_REF_BRAND_DESCR_".$brandName),
			'UF_FULL_DESCRIPTION' => GetMessage("WZD_REF_BRAND_FULL_DESCR_".$brandName),
			'UF_XML_ID' => ToLower($brandName)
		);
		$USER_FIELD_MANAGER->EditFormAddFields('HLBLOCK_'.$BRAND_ID, $arData);
		$USER_FIELD_MANAGER->checkFields('HLBLOCK_'.$BRAND_ID, null, $arData);

		$result = $entity_data_class::add($arData);
	}
}
?>