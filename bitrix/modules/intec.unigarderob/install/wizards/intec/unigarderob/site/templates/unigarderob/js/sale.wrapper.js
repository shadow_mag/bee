SaleWrapper = {};
SaleWrapper.updateAfterCompareAction = function ($sRespone) {
    SaleActions.updateButtons(SaleWrapper.updateButtons);
    SaleActions.updateCompare(SaleWrapper.updateCompare);
}
SaleWrapper.updateAfterBasketAction = function ($sRespone) {
    SaleActions.updateButtons(SaleWrapper.updateButtons);
    SaleActions.updateBasket(SaleWrapper.updateBasket);
}
SaleWrapper.updateButtons = function ($sResponse) {
    $($sResponse).appendTo('head');
}
SaleWrapper.updateCompare = function ($sResponse) {
    $('.b_compare').replaceWith($sResponse);
}
SaleWrapper.updateCompareFly = function ($sResponse) {
    $('.b_compare_fly').replaceWith($sResponse);
}
SaleWrapper.updateBasket = function ($sResponse) {
    $('.b_basket').replaceWith($sResponse);
}