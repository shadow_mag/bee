<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arServices = Array(
	"main" => Array(
		"NAME" => GetMessage("SERVICE_MAIN_SETTINGS"),
		"STAGES" => Array(
			"files.php",
			"urlrewrite.php",
			"search.php",
			"template.php",
			"theme.php",
			"settings.php",
		),
	),
	"highloadblock" => Array(
		"NAME" => GetMessage("SERVICE_HIGHLOADBLOCK"),
		"STAGES" => Array(
			"references.php",
			"references2.php",
		)
	),
	"iblock" => Array(
		"NAME" => GetMessage("SERVICE_IBLOCK_DEMO_DATA"),
		"STAGES" => Array(
			"types.php", // ���� ����������
			"articles.php", // ������
			"banners.php", // ������ � ������
			"brands.php", // ������
			"catalog.php", // �������
			"catalog_offers.php", // �������� �����������
			"catalog_reviews.php", // ������ � �������
			"contacts.php", // ��������
			"faq.php", // �������
			"jobs.php", // ��������
			"lookbook.php", // Lookbook
			"news.php", // �������
			"popular_slider.php", // ���������� ���������
			"reviews.php", // ������
			"services.php", // ������
			"services_reviews.php", // ������ �� �������
			"shares.php", // �����
			"slider.php", // ������� �������
			"staffs.php", // ����������
			"final.php" // ����� ��������� ���� ����������
		),
	),
	"sale" => Array(
		"NAME" => GetMessage("SERVICE_SALE_DEMO_DATA"),
		"STAGES" => Array(
			"locations.php",
			"step1.php",
			"step2.php",
			"step3.php"
		),
	),
	"catalog" => Array(
		"NAME" => GetMessage("SERVICE_CATALOG_SETTINGS"),
		"STAGES" => Array(
			"index.php",
		),
	),
	"form" => array(
		"NAME" => GetMessage("SERVICE_FORM_DEMO_DATA"),
		"STAGES" => array(
			"call.php",
			"service.php",
			"faq.php",
			"resume.php",
		)
	)
);
?>