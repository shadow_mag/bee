<table class="prices" cellspacing="0" cellpadding="0" border="0">
	<tr class="header">
		<td></td>
		<?if ($arResult['CATALOG_QUANTITY_TRACE'] == 'Y'):?><td><?=GetMessage('PRODUCT_SKULIST_HAVE')?></td><?endif;?>
		<?foreach($arResult['SKU_PROPS'] as $arSkuProp):?>
			<?foreach($arResult['OFFERS'] as $arOffer):?>
				<?if (!empty($arOffer['TREE']['PROP_'.$arSkuProp['ID']])):?>
					<td><?=$arSkuProp['NAME']?></td>
					<?break;?>
				<?endif;?>
			<?endforeach;?>
		<?endforeach;?>
		<td><?=GetMessage('PRODUCT_SKULIST_PRICE')?></td>
		<td></td>
		<td></td>
        <td></td>
		<td></td>
	</tr>
	<?foreach($arResult['OFFERS'] as $arOffer):?>
		<?if (!empty($arOffer['PREVIEW_PICTURE'])):?>
			<?$photo = CFile::ResizeImageGet($arOffer['PREVIEW_PICTURE'], array('width' => 170, 'height' => 170), BX_RESIZE_IMAGE_PROPORTIONAL)?>							
		<?else:?>
			<?$photo = array()?>
			<?if (!empty($arResult['DETAIL_PICTURE'])):?>
				<?$photo['src'] = $arResult['DETAIL_PICTURE']['SRC']?>
			<?elseif(!empty($arResult['PREVIEW_PICTURE'])):?>
				<?$photo['src'] = $arResult['PREVIEW_PICTURE']['SRC']?>
			<?else:?>
				<?$photo['src'] = SITE_TEMPLATE_PATH.'/images/noimg/noimg_quadro.jpg'?>
			<?endif;?>
		<?endif;?>
		<tr class="rows">
			<td>
				<div class="image">
					<div class='valign'></div>
					<img src="<?=$photo['src']?>" />
				</div>
			</td>
			<?if ($arResult['CATALOG_QUANTITY_TRACE'] == 'Y'):?><td>
				<div class="state available" style="<?=$arOffer['CATALOG_AVAILABLE'] != 'Y'?'display: none;':''?>">
					<div class="icon"></div>
					<?=GetMessage('PRODUCT_HAVE')?>
					<?if ($arParams['SHOW_MAX_QUANTITY'] == 'Y' && $arOffer['CATALOG_QUANTITY'] > 0):?>
						<?=$arOffer['CATALOG_QUANTITY']?> <?=$arOffer['CATALOG_MEASURE_NAME']?>.
					<?endif;?>
				</div>
				<div class="state unavailable" style="<?=$arOffer['CATALOG_AVAILABLE'] == 'Y'?'display: none;':''?>">
					<div class="icon"></div>
					<?=GetMessage('PRODUCT_NOT_HAVE')?>
				</div>
			</td><?endif;?>
			<?foreach($arResult['SKU_PROPS'] as $arSkuProp):?>
				<?$value = $arSkuProp['VALUES'][$arOffer['TREE']['PROP_'.$arSkuProp['ID']]]['NAME']?>
				<?if (!empty($value)):?>
					<td><div><span class="sku-adaptiv-title"><?=$arSkuProp['NAME']?>:</span> <?=$value?></div></td>
				<?endif;?>
			<?endforeach;?>
			<td class="price"><div><?=$arOffer['RATIO_PRICE']['PRINT_DISCOUNT_VALUE']?></div></td>
			<td style="width: 130px; white-space: nowrap;">
				<?if ($arOffer['CAN_BUY']):?>
					<div class="count" id="count<?=$arOffer['ID']?>" style="margin-left: 5px; margin-right: 5px;">
                        <?
                            $arJSNumeric = array(
                                'VALUE' => floatval($arOffer['CATALOG_MEASURE_RATIO']),
                                'MINIMUM' => floatval($arOffer['CATALOG_MEASURE_RATIO']),
                                'MAXIMUM' => floatval($arOffer['CATALOG_QUANTITY']),
                                'RATIO' => floatval($arOffer['CATALOG_MEASURE_RATIO']),
                                'UNLIMITED' => ($arOffer['CATALOG_CAN_BUY_ZERO'] == "Y" || $arOffer['CATALOG_QUANTITY_TRACE'] == "N"),
                                'VALUE_TYPE' => 'FLOAT'
                            );
                        ?>
						<button id="decrease" onclick="return SKUProductCount<?=$arOffer['ID']?>.Decrease();" class="uni-button">-</button>
						<input type="text" name="count" id="SKUProductCount<?=$arOffer['ID']?>" onchange="return SKUProductCount<?=$arOffer['ID']?>.SetValue($(this).val());" value="<?=$arOffer['CATALOG_MEASURE_RATIO']?>" />
						<button id="increase" onclick="return SKUProductCount<?=$arOffer['ID']?>.Increase();" class="uni-button">+</button>
                        <script type="text/javascript">
                            var SKUProductCount<?=$arOffer['ID']?> = new UNINumericUpDown(<?=CUtil::PhpToJSObject($arJSNumeric)?>);
                            SKUProductCount<?=$arOffer['ID']?>.Settings.EVENTS.onValueChange = function ($oNumeric) {
                                $('#SKUProductCount<?=$arOffer['ID']?>').val($oNumeric.GetValue());
                            };
                		</script>
					</div>
				<?endif;?>
			</td>
            <td style="width: 180px;">
                <?if ($arOffer['CAN_BUY']):?>
                    <div class="buy-button-click" style="margin-left: 5px; margin-right: 5px;">
                        <?
							$off_det = is_array($arOffer['DETAIL_PICTURE']) ? $arOffer['DETAIL_PICTURE']['SRC'] : CFile::GetPath($arOffer['DETAIL_PICTURE']);
							$off_prev = is_array($arOffer['PREVIEW_PICTURE']) ? $arOffer['PREVIEW_PICTURE']['SRC'] : CFile::GetPath($arOffer['PREVIEW_PICTURE']);
						?>
            			<?$sBuyBittonClickImage = !empty($off_det) ? $off_det : $off_prev?>
                        <?$sBuyBittonClickImage = empty($sBuyBittonClickImage) ? $arResult['DETAIL_PICTURE']['SRC'] : $sBuyBittonClickImage?>
                        <?$sBuyBittonClickImage = empty($sBuyBittonClickImage) ? $arResult['PREVIEW_PICTURE']['SRC'] : $sBuyBittonClickImage?>
            			<?$arBuyButtonClickParams = array (
            				"ID" => $arOffer["ID"],
            				"Name" => $arResult["NAME"],
            				"IBlockType" => $arParams["IBLOCK_TYPE"],
            				"IBlockID" => $arResult["IBLOCK_ID"],
            				"PriceCurrent" => $arOffer['MIN_PRICE']['PRINT_DISCOUNT_VALUE'],
            				"PriceOld" => "",
            				"Image" => $sBuyBittonClickImage,
                            "Currency" => $arOffer['MIN_PRICE']['CURRENCY'],
                            "PriceID" => $arOffer['MIN_PRICE']['PRICE_ID'],
            				"Price" => $arOffer['MIN_PRICE']['VALUE']
            			);?>
            			<a 
            				href="javascript:void(0);" 
            				class="uni-button button" 
            				onclick="Popups.OneClickBuy(<?=CUtil::PhpToJSObject($arBuyButtonClickParams);?>)"
							style="width: 206px;"
            			>
            				<span><?=GetMessage("ONE_CLICK_BUY")?></span>
            			</a>
            		</div>
                <?endif;?>
            </td>
			<td style="width: 134px;">
				<?if ($arOffer['CAN_BUY']):?>
					<div class="buy-button" style="margin-right: 5px; margin-left: 5px;">			
						<a rel="nofollow" class="uni-button solid_button button addToBasket addToBasket<?=$arOffer["ID"]?>" style="width: 174px;"
							onclick="SaleActions.addToBasket('<?=$arOffer['ID']?>', '<?=$arOffer['MIN_PRICE']['CURRENCY']?>', SKUProductCount<?=$arOffer['ID']?>.GetValue(), SaleWrapper.updateAfterBasketAction);"><?=GetMessage("CATALOG_ADD_TO_BASKET")?>
						</a>
						<a rel="nofollow" href="<?=$arParams["BASKET_URL"];?>" class="uni-button solid_button button removeFromBasket removeFromBasket<?=$arOffer["ID"]?>" style="width: 174px; display: none;">
							<?=GetMessage("CATALOG_ADDED")?>
						</a>
					</div>
				<?endif;?>
			</td>
			<td style="width: 88px; white-space: nowrap; text-align: right;">
                <div style="overflow: hidden;">
    				<?if($arParams["DISPLAY_COMPARE"]=="Y"):?>
    					<div class="min-buttons" style="height: 40px;">
                            <div class="uni-aligner-vertical"></div>
    						<?if ($arOffer['CAN_BUY']):?>
    							<div class="min-button like">
    								<div class="add addToWishList addToWishList<?=$arOffer["ID"]?>"								
    									onclick="SaleActions.addToWishList('<?=$arOffer['ID']?>', '<?=$arOffer['MIN_PRICE']['CURRENCY']?>', SKUProductCount<?=$arOffer['ID']?>.GetValue(), SaleWrapper.updateAfterBasketAction);"
    									title="<?=GetMessage('LIKE_TEXT_DETAIL')?>"
    								>
    								</div>
    								<div class="remove removeFromWishList removeFromWishList<?=$arOffer["ID"]?>"
    									style="display:none"						
    									onclick="SaleActions.removeFromWishList('<?=$arOffer['ID']?>', SaleWrapper.updateAfterBasketAction);"
    									title="<?=GetMessage('LIKE_DELETE_TEXT_DETAIL')?>"
    								>
    								</div>
    							</div>
    						<?endif;?>
    						<div class="min-button compare">
    							<div class="add addToCompare addToCompare<?=$arOffer["ID"]?>"
    								onclick="SaleActions.addToCompare('<?=$arOffer['COMPARE_URL']?>', SaleWrapper.updateAfterCompareAction);"
    								title="<?=GetMessage('COMPARE_TEXT_DETAIL')?>"
    							>
    							</div>
    							<div  class="remove removeFromCompare removeFromCompare<?=$arOffer["ID"]?>"
    								style="display:none"
    								onclick="SaleActions.removeFromCompare('<?=$arOffer['COMPARE_REMOVE_URL']?>', SaleWrapper.updateAfterCompareAction);"
    								title="<?=GetMessage('COMPARE_DELETE_TEXT_DETAIL')?>"
    							>
    							</div>
    						</div>
    					</div>
    				<?endif?>
                </div>
			</td>
		</tr>
	<?endforeach;?>
</table>