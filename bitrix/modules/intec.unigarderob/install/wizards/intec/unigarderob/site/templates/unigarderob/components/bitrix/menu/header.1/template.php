<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?global $options;?>
<?$arBaseMenuItem = current($arResult);?>
<?$iBaseDepthLevel = $arBaseMenuItem['DEPTH_LEVEL'];?>
<?$iCurrentDepthLevel = $iBaseDepthLevel;?>
<?$iCurrentLevel = 0;?>
<?unset($arBaseMenuItem); reset($arResult);?>
<div class="menu-header-1 <?=$options['TYPE_TOP_MENU']['ACTIVE_VALUE']?><?=$options["MENU_WIDTH_SIZE"]["ACTIVE_VALUE"] == 'Y' ? ' size-wide' : ' size-limited'?>">
    <div class="menu-header-1-wrapper">
        <div class="menu-header-1-wrapper-wrapper">
            <div class="menu-header-1-wrapper-wrapper-wrapper">
                <?foreach ($arResult as $arMenuItem):?>
                    <?if ($arMenuItem["DEPTH_LEVEL"] == $iBaseDepthLevel){?>
                        <?if ($arMenuItem["DEPTH_LEVEL"] == $iCurrentDepthLevel && !$arMenuItem['IS_PARENT']) {?>
                            <?include('parts/menu.item.single.php');?>
                        <?} else if ($arMenuItem["DEPTH_LEVEL"] == $iCurrentDepthLevel && $arMenuItem["IS_PARENT"]) {?>
                            <?include('parts/menu.item.submenu.start.php');?>
                        <?} else if ($arMenuItem["DEPTH_LEVEL"] < $iCurrentDepthLevel && !$arMenuItem["IS_PARENT"]) {?>
                            <?include('parts/anymenu.item.submenu.end.php');?>
                            <?include('parts/menu.item.single.php');?>
                        <?} else if ($arMenuItem["DEPTH_LEVEL"] < $iCurrentDepthLevel && $arMenuItem["IS_PARENT"]) {?>
                            <?include('parts/anymenu.item.submenu.end.php');?>
                            <?include('parts/menu.item.submenu.start.php');?>
                        <?}?>
                    <?} else {?>
                        <?if ($arMenuItem["DEPTH_LEVEL"] == $iCurrentDepthLevel && !$arMenuItem['IS_PARENT']) {?>
                            <?include('parts/submenu.item.single.php');?>
                        <?} else if ($arMenuItem["DEPTH_LEVEL"] == $iCurrentDepthLevel && $arMenuItem["IS_PARENT"]) {?>
                            <?include('parts/submenu.item.submenu.start.php');?>
                        <?} else if ($arMenuItem["DEPTH_LEVEL"] < $iCurrentDepthLevel && !$arMenuItem["IS_PARENT"]) {?>
                            <?include('parts/anymenu.item.submenu.end.php');?>
                            <?include('parts/submenu.item.single.php');?>
                        <?} else if ($arMenuItem["DEPTH_LEVEL"] < $iCurrentDepthLevel && $arMenuItem["IS_PARENT"]) {?>
                            <?include('parts/anymenu.item.submenu.end.php');?>
                            <?include('parts/submenu.item.submenu.start.php');?>
                        <?}?>
                    <?}?>
                <?endforeach;?>
                <?while ($iCurrentDepthLevel != $iBaseDepthLevel):?>
                    <?if ($iCurrentDepthLevel != $iBaseDepthLevel + 1):?>
                        <?include('parts/submenu.item.submenu.end.php');?>
                    <?else:?>
                        <?include('parts/menu.item.submenu.end.php');?>
                    <?endif;?>
                    <?$iCurrentDepthLevel--;?>
                <?endwhile;?>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $('.menu-header-1 .menu-header-1-item .menu-header-1-item-wrapper').mouseenter(function () {
                var $oMenu = $(this).children('.menu-header-1-submenu.menu-header-1-submenu-level-1');
                
                if ($oMenu.size() > 0) {
                    var $iCurrentHeight = $oMenu.css({'display':'block'}).height();
                    var $iHeight = $oMenu.css({'display':'block', 'height':'auto'}).height();
                    
                    $oMenu.css({'left':'auto'});
                    $oMainMenu = $(this).parent().parent().parent().parent();
                    $iMainMenuWidth = $oMainMenu.width();
                    $iMenuWidth = $oMenu.position().left + parseInt($oMenu.css('max-width'));
                    
                    if ($iMenuWidth > $iMainMenuWidth) {
                        $iNewMenuPosition = $oMenu.position().left + $(this).width() - parseInt($oMenu.css('max-width'));
                        $oMenu.css({'left':$iNewMenuPosition + 'px'})
                    }
                        
                    $oMenu.css({'height':$iCurrentHeight + 'px'});
                    $oMenu.stop().animate({'height': $iHeight + 'px'}, 50, function () {
                        $(this).css({'height': 'auto'});
                    });  
                }  
            }).mouseleave(function () {
                var $oMenu = $(this).children('.menu-header-1-submenu.menu-header-1-submenu-level-1');
                $oMenu.stop().animate({'height': '0px'}, 50, function () {
                    $(this).css({'display':'none', 'height':'0px'});
                });
            });
    </script>
</div>