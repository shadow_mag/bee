<?if (!$arFlags['SHOW_OFFERS']){ // Если нет OFFERS?>
    <div class="price minimal">
		<?if ($arResult['RATIO_PRICE']['VALUE'] == $arResult['RATIO_PRICE']['DISCOUNT_VALUE']):?>
			<div class="current"><?=$arResult['RATIO_PRICE']['PRINT_VALUE']?></div>
		<?else:?>
			<div class="current"><?=$arResult['RATIO_PRICE']['PRINT_DISCOUNT_VALUE']?></div>
			<?if ($arParams['SHOW_OLD_PRICE'] == 'Y'):?>
				<div class="old"><?=$arResult['RATIO_PRICE']['PRINT_VALUE']?></div>
			<?endif;?>
		<?endif;?>
	</div>
	<div class="order">		
		<?if ($arResult['CAN_BUY']):?>
			<div class="buy-button buy-button-qv" style="display:inline-block">			
				<a rel="nofollow" class="uni-button solid_button button addToBasket addToBasket<?=$arResult["ID"]?>"
					onclick="SaleActions.addToBasket('<?=$arResult['ID']?>', '<?=$arResult['MIN_PRICE']['CURRENCY']?>', 1, SaleWrapper.updateAfterBasketAction);"
				><?=GetMessage("CATALOG_ADD_TO_BASKET")?>
				</a>
				<a rel="nofollow" href="<?=$arParams["BASKET_URL"];?>" class="uni-button solid_button button removeFromBasket removeFromBasket<?=$arResult["ID"]?>" style="display: none;">
					<?=GetMessage("CATALOG_ADDED")?>
				</a>
			</div>
		<?endif;?>
		<div class="min-buttons min-buttons-qv " style="display:inline-block">
			<?if($arParams["DISPLAY_COMPARE"] == "Y"):?>
				<div class="min-button compare">
					<div class="add addToCompare addToCompare<?=$arResult["ID"]?>"
						onclick="SaleActions.addToCompare('<?=$arResult['COMPARE_URL']?>', SaleWrapper.updateAfterCompareAction);"
						title="<?=GetMessage('COMPARE_TEXT_DETAIL')?>"					
					>
					</div>
					<div class="remove removeFromCompare removeFromCompare<?=$arResult["ID"]?>"
						style="display:none"
						onclick="SaleActions.removeFromCompare('<?=$arResult['COMPARE_REMOVE_URL']?>', SaleWrapper.updateAfterCompareAction);"
						title="<?=GetMessage('COMPARE_DELETE_TEXT_DETAIL')?>"
					>
					</div>
				</div>
			<?endif?>
			<?if ($arResult['CAN_BUY']):?>
				<div class="min-button like">
					<div class="add addToWishList addToWishList<?=$arResult["ID"]?>"								
						onclick="SaleActions.addToWishList('<?=$arResult['ID']?>', '<?=$arResult['MIN_PRICE']['CURRENCY']?>', 1, SaleWrapper.updateAfterBasketAction);"
						title="<?=GetMessage('LIKE_TEXT_DETAIL')?>"
					>
					</div>
					<div class="remove removeFromWishList removeFromWishList<?=$arResult["ID"]?>"
						style="display:none"							
						onclick="SaleActions.removeFromWishList('<?=$arResult['ID']?>', SaleWrapper.updateAfterBasketAction);"
						title="<?=GetMessage('LIKE_DELETE_TEXT_DETAIL')?>"
					>
					</div>
				</div>
			<?endif;?>
		</div>
	</div>
<?}else{?>
    <div class="price minimal">
		<div class="current" id="SKUPriceCurrent">
			<?=$arResult['MIN_PRICE']['PRINT_DISCOUNT_VALUE']?>
		</div>
		<?if ($arParams['SHOW_OLD_PRICE'] == 'Y'):?>
			<div class="old" id="SKUPriceOld"<?=!$arFlags['SHOW_PRICE_DISCOUNT'] ? ' style="display: none;"' : ''?>><?=$arResult['MIN_PRICE']['PRINT_VALUE']?></div>
		<?endif;?>
	</div>
	<div class="order">
		<?foreach ($arResult['OFFERS'] as $arOffer):?>
			<div class="buy-button buy-button-qv SKUBuyButton<?=$arOffer['ID']?>" style="display: none;">	
				<a rel="nofollow" class="uni-button solid_button button addToBasket addToBasket<?=$arOffer["ID"]?>"
						onclick="SaleActions.addToBasket('<?=$arOffer['ID']?>', '<?=$arOffer['MIN_PRICE']['CURRENCY']?>', 1, SaleWrapper.updateAfterBasketAction);"
					><?=GetMessage("CATALOG_ADD_TO_BASKET")?>
				</a>
				<a rel="nofollow" href="<?=$arParams["BASKET_URL"];?>" class="uni-button solid_button button removeFromBasket removeFromBasket<?=$arOffer["ID"]?>" style="display: none;">
					<?=GetMessage("CATALOG_ADDED")?>
				</a>
			</div>
			<div class="min-buttons min-buttons-qv SKUMinButtons<?=$arOffer['ID']?>" style="display: none;">
				<?if($arParams["DISPLAY_COMPARE"]=="Y"):?>
					<div class="min-button compare">
						<div class="add addToCompare addToCompare<?=$arOffer["ID"]?>"
							onclick="SaleActions.addToCompare('<?=$arOffer['COMPARE_URL']?>', SaleWrapper.updateAfterCompareAction);"	
							title="<?=GetMessage('COMPARE_TEXT_DETAIL')?>"
						>
						</div>
						<div  class="remove removeFromCompare removeFromCompare<?=$arOffer["ID"]?>"
							style="display:none"
							onclick="SaleActions.removeFromCompare('<?=$arOffer['COMPARE_REMOVE_URL']?>', SaleWrapper.updateAfterCompareAction);"
							title="<?=GetMessage('COMPARE_DELETE_TEXT_DETAIL')?>"
						>
						</div>
					</div>
				<?endif?>
				<?if ($arOffer['CAN_BUY']):?>
					<div class="min-button like">
						<div class="add addToWishList addToWishList<?=$arOffer["ID"]?>"								
							onclick="SaleActions.addToWishList('<?=$arOffer['ID']?>', '<?=$arOffer['MIN_PRICE']['CURRENCY']?>', 1, SaleWrapper.updateAfterBasketAction);"	
							id="like_<?=$arOffer["ID"]?>"
							title="<?=GetMessage('LIKE_TEXT_DETAIL')?>"
						>
						</div>
						<div class="remove removeFromWishList removeFromWishList<?=$arOffer["ID"]?>"
							style="display:none"								
							onclick="SaleActions.removeFromWishList('<?=$arOffer['ID']?>', SaleWrapper.updateAfterBasketAction);"
							title="<?=GetMessage('LIKE_DELETE_TEXT_DETAIL')?>"
						>
						</div>
					</div>
				<?endif;?>
			</div>
		<?endforeach;?>
	</div>
<?}?>