<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?$arBaseMenuItem = current($arResult);?>
<?$iBaseDepthLevel = $arBaseMenuItem['DEPTH_LEVEL'];?>
<?$iCurrentDepthLevel = $iBaseDepthLevel;?>
<?$iCurrentLevel = 0;?>
<?unset($arBaseMenuItem); reset($arResult);?>
<div class="menu-catalog-left-1">
    <div class="menu-catalog-left-1-wrapper">
        <?foreach ($arResult as $arMenuItem):?>
            <?if ($arMenuItem["DEPTH_LEVEL"] == $iBaseDepthLevel){?>
                <?if ($arMenuItem["DEPTH_LEVEL"] == $iCurrentDepthLevel && !$arMenuItem['IS_PARENT']) {?>
                    <?include('parts/menu.item.single.php');?>
                <?} else if ($arMenuItem["DEPTH_LEVEL"] == $iCurrentDepthLevel && $arMenuItem["IS_PARENT"]) {?>
                    <?include('parts/menu.item.submenu.start.php');?>
                <?} else if ($arMenuItem["DEPTH_LEVEL"] < $iCurrentDepthLevel && !$arMenuItem["IS_PARENT"]) {?>
                    <?include('parts/anymenu.item.submenu.end.php');?>
                    <?include('parts/menu.item.single.php');?>
                <?} else if ($arMenuItem["DEPTH_LEVEL"] < $iCurrentDepthLevel && $arMenuItem["IS_PARENT"]) {?>
                    <?include('parts/anymenu.item.submenu.end.php');?>
                    <?include('parts/menu.item.submenu.start.php');?>
                <?}?>
            <?} else {?>
                <?if ($arMenuItem["DEPTH_LEVEL"] == $iCurrentDepthLevel && !$arMenuItem['IS_PARENT']) {?>
                    <?include('parts/submenu.item.single.php');?>
                <?} else if ($arMenuItem["DEPTH_LEVEL"] == $iCurrentDepthLevel && $arMenuItem["IS_PARENT"]) {?>
                    <?include('parts/submenu.item.submenu.start.php');?>
                <?} else if ($arMenuItem["DEPTH_LEVEL"] < $iCurrentDepthLevel && !$arMenuItem["IS_PARENT"]) {?>
                    <?include('parts/anymenu.item.submenu.end.php');?>
                    <?include('parts/submenu.item.single.php');?>
                <?} else if ($arMenuItem["DEPTH_LEVEL"] < $iCurrentDepthLevel && $arMenuItem["IS_PARENT"]) {?>
                    <?include('parts/anymenu.item.submenu.end.php');?>
                    <?include('parts/submenu.item.submenu.start.php');?>
                <?}?>
            <?}?>
        <?endforeach;?>
        <?while ($iCurrentDepthLevel != $iBaseDepthLevel):?>
            <?if ($iCurrentDepthLevel != $iBaseDepthLevel + 1):?>
                <?include('parts/submenu.item.submenu.end.php');?>
            <?else:?>
                <?include('parts/menu.item.submenu.end.php');?>
            <?endif;?>
            <?$iCurrentDepthLevel--;?>
        <?endwhile;?>
    </div>
</div>