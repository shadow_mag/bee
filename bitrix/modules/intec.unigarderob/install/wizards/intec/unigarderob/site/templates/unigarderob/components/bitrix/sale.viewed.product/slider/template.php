<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?$this->setFrameMode(true);?>
<?global $options;?>
<?$sUniqueID = spl_object_hash($this);?>
<?
    $iGrid = intval($arParams['LINE_ELEMENT_COUNT']);
    
    if ($iGrid < 1) $iGrid = 1;
    if ($iGrid > 6) $iGrid = 6;
    
    $arGrid = array(
        '1' => 'uni-100',
        '2' => 'uni-50',
        '3' => 'uni-33',
        '4' => 'uni-25',
        '5' => 'uni-20',
        '6' => 'uni-16'
    );
?>
<?if (!empty($arResult)):?>
    <?$frame = $this->createFrame()->begin();?>
    <div class="uni-product-viewed-slider" id="UNIProductViewedSlider<?=$sUniqueID?>">
        <h3 class="header_grey"><?=GetMessage('UNI_PRODUCT_VIEWED_PRODUCT_TITLE')?></h3>
        <div class="buttons">
            <div class="button button-left uni-slider-button-small uni-slider-button-left" onclick="return UNIProductViewedSlider<?=$sUniqueID?>.SlideRight();">
                <div class="icon"></div>
            </div>
			<div class="button button-right uni-slider-button-small uni-slider-button-right" onclick="return UNIProductViewedSlider<?=$sUniqueID?>.SlideLeft();">
                <div class="icon"></div>
            </div>	
        </div>
        <div class="indents-wrapper">
            <div class="indents">
            	<div class="slider">
                    <?
                		$strElementEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT");
                		$strElementDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE");
                		$arElementDeleteParams = array("CONFIRM" => GetMessage('CT_BCS_TPL_ELEMENT_DELETE_CONFIRM'));
                    ?>
            		<?foreach($arResult as $arElement){
            		
            			$this->AddEditAction($arElement['ID'], $arElement['EDIT_LINK'], $strElementEdit);
            			$this->AddDeleteAction($arElement['ID'], $arElement['DELETE_LINK'], $strElementDelete, $arElementDeleteParams);
            		?>
            			<div class="element <?=$arGrid[$iGrid]?>" id="<?=$this->GetEditAreaId($arElement['ID']);?>">
        					<div class="wrapper hover_shadow">
        						<?if(!empty($arElement["PICTURE"]["src"])){

        									$src=$arElement["PICTURE"]["src"];
        								}else{
        									$src=SITE_TEMPLATE_PATH."/images/noimg/no-img.png";
        							}?>
        						<a href="<?=$arElement["DETAIL_PAGE_URL"]?>" class="image">	
        							<div class="uni-image">
        								<div class="uni-aligner-vertical"></div>
        								<img src="<?=$src?>" />
        							</div>
        						</a>
        						<div class="information">
        							<a class="name" href="<?=$arElement["DETAIL_PAGE_URL"]?>"><?=$arElement["NAME"]?></a>
        							<div class="price"><?=$arElement['PRICE_FORMATED']?></div>
        						</div>
        						<div class="clear"></div>
        					</div>
            			</div>
            		<?}?>
            		<div class="clear"></div>		
            	</div>
            </div>
        </div>
    </div>
	<script type="text/javascript">
		var UNIProductViewedSlider<?=$sUniqueID?> = new UNISlider({
            'SLIDER': '#UNIProductViewedSlider<?=$sUniqueID?> .slider',
            'ELEMENT': '#UNIProductViewedSlider<?=$sUniqueID?> .slider .element',
            'OFFSET': <?=$iGrid?>,
            'INFINITY_SLIDE': true,
            'ANIMATE': true,
            'ANIMATE_SPEED': 400,
            "EVENTS": {
                "onAdaptabilityChange": function ($oSlider) {
                    <?if ($options['ADAPTIV']['ACTIVE_VALUE'] == 'Y'):?>
                        $oSlider.Settings.OFFSET = Math.round($oSlider.GetSliderWidth() / $oSlider.GetElementWidth());
                    <?endif;?>
                    
                    if ($oSlider.Settings.OFFSET < $oSlider.GetElementsCount()) {
                        $('#UNIProductViewedSlider<?=$sUniqueID?> .buttons').css('display', 'block');
                    } else {
                        $('#UNIProductViewedSlider<?=$sUniqueID?> .buttons').css('display', 'none');
                    }  
                }            
            }                
		});
        
        $('.image-slider .list#ProductSlider .items .image').on('click', function () {
            $('.image-slider .list#ProductSlider .items .image').removeClass('selected');
            $(this).addClass('selected');
            $('.image-slider .image-box .slider-images#ProductSliderBox .image')
                .hide()
                .eq($(this).index()).show();
        }).eq(0).addClass('selected');
	</script>
    <?$frame->end();?>
<?endif;?>