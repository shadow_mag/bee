<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>
<?
    use Bitrix\Main\Loader;
    $bIBlockExists = (!empty($arCurrentValues['IBLOCK_ID']) && (int)$arCurrentValues['IBLOCK_ID'] > 0);
    $bCatalogIncluded = Loader::includeModule('catalog');
    $arPriceTypes = array();
    
    if ($bCatalogIncluded) {
    	$arPriceTypes = CCatalogIBlockParameters::getPriceTypesList();
    } else {
    	$arPriceTypes = array();
    }
    
    $arProperty_Offers = array();
    $arProperty_OffersWithoutFile = array();
    if ($bCatalogIncluded && $bIBlockExists)
    {
    	$arOffers = CCatalogSKU::GetInfoByProductIBlock($arCurrentValues['IBLOCK_ID']);
        
    	if (!empty($arOffers))
    	{
    		$oPropertyIterator = Bitrix\Iblock\PropertyTable::getList(array(
    			'select' => array('ID', 'IBLOCK_ID', 'NAME', 'CODE', 'PROPERTY_TYPE', 'MULTIPLE', 'LINK_IBLOCK_ID', 'USER_TYPE'),
    			'filter' => array('=IBLOCK_ID' => $arOffers['IBLOCK_ID'], '=ACTIVE' => 'Y', '!=ID' => $arOffers['SKU_PROPERTY_ID']),
    			'order' => array('SORT' => 'ASC', 'NAME' => 'ASC')
    		));
    		while ($arProperty = $oPropertyIterator->fetch())
    		{
    			$cPropertyCode = (string)$arProperty['CODE'];
    			if ($cPropertyCode == '')
    				$cPropertyCode = $arProperty['ID'];
    			$sPropertyName = '['.$cPropertyCode.'] '.$arProperty['NAME'];
    
    			$arProperty_Offers[$cPropertyCode] = $sPropertyName;
    			if ($property['PROPERTY_TYPE'] != Bitrix\Iblock\PropertyTable::TYPE_FILE)
    				$arProperty_OffersWithoutFile[$cPropertyCode] = $sPropertyName;
    		}
    		unset($cPropertyCode, $sPropertyName, $arProperty, $oPropertyIterator);
    	}
    }

    $arTemplateParameters = array(
        "PRICE_CODE" => array(
    		"NAME" => GetMessage("UNI_LABELS_PRICE_CODE"),
    		"TYPE" => "LIST",
    		"MULTIPLE" => "Y",
    		"VALUES" => $arPriceTypes,
    	)
    );
    
    if ($bCatalogIncluded) {
        $arTemplateParameters['OFFERS_PROPERTY_CODE'] = array(
    		"NAME" => GetMessage("UNI_LABELS_OFFERS_PROPERTY_CODE"),
    		"TYPE" => "LIST",
    		"MULTIPLE" => "Y",
    		"VALUES" => $arProperty_Offers,
    	);
    }
?>