<div class="basket-small-1<?=!empty($arParams['TYPE_BASKET'])?' '.$arParams['TYPE_BASKET']:''?><?=$arParams['TYPE_BASKET'] == 'fly'?' basket-hidden':''?>">
	<div class="basket-wrapper">
        <a href="<?=$arParams['PATH_TO_BASKET']?>?delay=y" class="basket-element wish-list">
            <div class="basket-element-wrapper">
                <div class="basket-icon"></div>
                <?$frame = $this->createFrame()->begin();?>
                    <?if ($arResult['TOTAL']['WISH_LIST']['COUNT'] > 0):?>
                        <div class="basket-text"><?=$arResult['TOTAL']['WISH_LIST']['COUNT']?></div>
                    <?endif;?>
                <?$frame->end();?>
            </div>
        </a>
        <a href="<?=$arParams['PATH_TO_BASKET']?>" class="basket-element basket">
            <div class="basket-element-wrapper">
                <div class="basket-icon"></div>
                <?$frame = $this->createFrame()->begin();?>
                    <?if ($arResult['TOTAL']['BASKET']['COUNT'] > 0):?>
                        <div class="basket-text"><?=$arResult['TOTAL']['BASKET']['COUNT']?></div>
                    <?endif;?>
                <?$frame->end();?>
            </div>
            <?$frame = $this->createFrame()->begin();?>
                <?if ($arResult['TOTAL']['BASKET']['COUNT'] > 0):?>
                    <div class="basket-sum">
                        <?=$arResult['TOTAL']['BASKET']['SUM_FORMATED']?>
                    </div>
                <?endif;?>
            <?$frame->end();?>
        </a>
    </div>
</div>