$(document).ready(function(){
	resize();
	function resize() {
		var size = $('.bg_footer').outerHeight();
		$('body').css('padding-bottom', (size+50) + 'px');
	}
	$(window).resize(function(){
		 resize();
	})
	$('.fancy').fancybox();	
	$("input[name='PERSONAL_PHONE']").mask("+7 (999) 999-9999");
	$('.button_up').click(function() {
		$('body, html').animate({
			scrollTop: 0
      }, 1000);
   });
})
$(window).scroll(function(){
	var top_show = 300 ;
	if($(this).scrollTop() > top_show) {
		$('.button_up').fadeIn();
	} 
	else {
		$('.button_up').fadeOut();
	}
});