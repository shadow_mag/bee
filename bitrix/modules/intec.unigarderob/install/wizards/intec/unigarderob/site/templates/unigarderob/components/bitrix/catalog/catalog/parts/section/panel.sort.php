<?if ($arFlags['SHOW_PANEL']):?>
    <?
    	$sSort = null;
        $sOrder = 'asc';
        $sOrderInvert = 'desc';
        $sView = $options["CATALOG_SECTION_DEFAULT_VIEW"]["ACTIVE_VALUE"];
    	
        if (isset($_GET['sort']))
            if (in_array($_GET['sort'], array('name', 'price', 'popular')))
                $sSort = $_GET['sort'];
              
        if (isset($_GET['order']))
            if (in_array($_GET['order'], array('asc', 'desc')))
                $sOrder = $_GET['order'];
    
        if ($sOrder == 'desc')
            $sOrderInvert = 'asc';
    
    	if (isset($_COOKIE['view']))
    		if (in_array($_COOKIE['view'], array('tile', 'list')))
                $sView = $_COOKIE['view'];
    	
        if (isset($_GET['view']))
            if (in_array($_GET['view'], array('tile', 'list'))) {
                $sView = $_GET['view'];
                setcookie("view", $sView, time()+60*60*24*7, '/');
            }	
    ?>
    <div class="uni-panel-sort">
        <div class="uni-panel-sort-wrapper">
            <div class="part sort">
                <div class="part-wrapper">
					<?/*
                    <div class="caption">
                        <div class="uni-aligner-vertical"></div>
                        <?=GetMessage('SECTION_SORT_TITLE')?>
                    </div>
					*/?>
                    <div class="element">
                        <div class="uni-aligner-vertical"></div>
                        <div class="element-wrapper">
                            <a class="sort-item<?=$sSort == 'name' ? ' ui-state-active' : ''?> <?=$sOrder?>" href="<?=
                                $sSort == 'name' ?
                                $APPLICATION->GetCurPageParam('sort=name&order='.$sOrderInvert, array('sort', 'order')) :
                                $APPLICATION->GetCurPageParam('sort=name&order='.$sOrder, array('sort', 'order'))                      
                            ?>"><span class="sort-item-text"><?=GetMessage('SECTION_SORT_NAME')?></span><div class="sort-item-icon"></div></a>
                        </div>
                    </div>
                    <div class="element">
                        <div class="uni-aligner-vertical"></div>
                        <div class="element-wrapper">
                            <a class="sort-item<?=$sSort == 'price' ? ' ui-state-active' : ''?> <?=$sOrder?>" href="<?=
                                $sSort == 'price' ?
                                $APPLICATION->GetCurPageParam('sort=price&order='.$sOrderInvert, array('sort', 'order')) :
                                $APPLICATION->GetCurPageParam('sort=price&order='.$sOrder, array('sort', 'order'))                      
                            ?>"><span class="sort-item-text"><?=GetMessage('SECTION_SORT_PRICE')?></span><div class="sort-item-icon"></div></a>
                        </div>
                    </div>
                    <div class="element">
                        <div class="uni-aligner-vertical"></div>
                        <div class="element-wrapper">
                            <a class="sort-item<?=$sSort == 'popular' ? ' ui-state-active' : ''?> <?=$sOrder?>" href="<?=
                                $sSort == 'popular' ?
                                $APPLICATION->GetCurPageParam('sort=popular&order='.$sOrderInvert, array('sort', 'order')) :
                                $APPLICATION->GetCurPageParam('sort=popular&order='.$sOrder, array('sort', 'order'))                      
                            ?>"><span class="sort-item-text"><?=GetMessage('SECTION_SORT_POPULAR')?></span><div class="sort-item-icon"></div></a>
                        </div>
                    </div>
                    <div class="element">
                        <div class="uni-aligner-vertical"></div>
                        <div class="element-wrapper">
                            <a class="sort-item<?=$sSort == null ? ' ui-state-active' : ''?>" href="<?=$APPLICATION->GetCurPageParam('', array('sort', 'order'))?>">
                                <span class="sort-item-text"><?=GetMessage('SECTION_SORT_NONE')?></span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="part views right">
                <div class="part-wrapper">
                    <div class="uni-aligner-vertical"></div>
					<?/*
                    <div class="caption">
                        <div class="uni-aligner-vertical"></div>
                        <?=GetMessage('SECTION_VIEW_TITLE')?>
                    </div>
					*/?>
                    <div class="element">
                        <div class="uni-aligner-vertical"></div>
                        <div class="element-wrapper">
                            <a class="view list<?=$sView == 'list' ? ' ui-state-active' : ''?>" href="<?=$APPLICATION->GetCurPageParam('view=list', array('view'))?>"></a>
                        </div>
                    </div>
                    <div class="element">
                        <div class="uni-aligner-vertical"></div>
                        <div class="element-wrapper">
                            <a class="view tile<?=$sView == 'tile' ? ' ui-state-active' : ''?>" href="<?=$APPLICATION->GetCurPageParam('view=tile', array('view'))?>"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="uni-indents-vertical indent-15"></div>
    <?
        if ($sSort == 'price')
            $sSort = 'catalog_PRICE_1';
            
        if ($sSort == 'popular')
            $sSort = 'show_counter';
    ?>
<?else:?>
	<?$sView = "tile";?>
<?endif;?>
<?
    $arFlags['SECTION_VIEW'] = $sView;
    $arFlags['SECTION_SORT_FIELD'] = !empty($sSort) ? $sSort : $arParams['ELEMENT_SORT_FIELD'];
    $arFlags['SECTION_SORT_ORDER'] = !empty($sSort) ? $sOrder : $arParams['ELEMENT_SORT_ORDER'];
    unset($sSort, $sOrder, $sOrderInvert, $sView);
?>
