<?if ($arItem['CAN_BUY']):?>
    <div class="button-buy">
        <a rel="nofollow" class="uni-button solid_button button addToBasket addToBasket<?=$arItem["ID"]?>"
    		onclick="SaleActions.addToBasket('<?=$arItem['ID']?>', '<?=$arItem['MIN_PRICE']['CURRENCY']?>', <?=$arItem['CATALOG_MEASURE_RATIO']?>, SaleWrapper.updateAfterBasketAction);"
    	><?=GetMessage("CATALOG_ADD")?>
    	</a>
    	<a rel="nofollow" href="<?=$arParams["BASKET_URL"];?>" class="uni-button solid_button button removeFromBasket removeFromBasket<?=$arItem["ID"]?>" style="display: none;">
    		<?=GetMessage("CATALOG_ADDED")?>
    	</a>
    </div>
<?else:?>
    <div class="button-buy">
        <a rel="nofollow" class="button solid_text"
			href="<?=$arItem['DETAIL_PAGE_URL']?>"
		><?=GetMessage("CATALOG_NOT_AVAILABLE")?>
		</a>
    </div>
<?endif;?>