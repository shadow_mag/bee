<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

global $APPLICATION;

$aMenuLinksExt = array();

/* Параметры */
$sIBlockType = '#CATALOG_IBLOCK_TYPE#';
$iIBlockID = '#CATALOG_IBLOCK_ID#';
$sSelectedSectionVariable = 'CATALOG_SECTION';
/*-Параметры-*/

if (CModule::IncludeModule('iblock')) {
    if (!empty($sSelectedSectionVariable)) {
        $sSelectedSectionValue = null;
    
        $arRootSections = array();
        $rsRootSections = CIBlockSection::GetList(array('NAME' => 'ASC'), array(
            'IBLOCK_TYPE' => $sIBlockType,
            'IBLOCK_ID' => $iIBlockID,
            'SECTION_ID' => false,
            'ACTIVE' => 'Y'
        ));
        
        while ($arRootSection = $rsRootSections->GetNext())
            $arRootSections[] = $arRootSection;
    
        if (!empty($_SESSION[$sSelectedSectionVariable]))
            $sSelectedSectionValue = $_SESSION[$sSelectedSectionVariable];
            
        if (empty($sSelectedSectionValue) && !empty($arRootSections)) {
            $arFirstSection = current($arRootSections);
            $sSelectedSectionValue = $arFirstSection['ID'];
            //unset($arFirstSection);
        }
    }
    
    $aMenuLinksExt = $APPLICATION->IncludeComponent("intec:menu.sections", "", array(
        "IBLOCK_TYPE" => $sIBlockType,
        "IBLOCK_ID" => $iIBlockID,
        "SECTION_ID" => $sSelectedSectionValue,
        "SECTION_URL" => "",
        "COUNT_ELEMENTS" => "Y",
        "CACHE_TYPE" => "A",
  		"CACHE_TIME" => "36000000"
    ), false);
}

$aMenuLinks = array_merge($aMenuLinks, $aMenuLinksExt);
?>