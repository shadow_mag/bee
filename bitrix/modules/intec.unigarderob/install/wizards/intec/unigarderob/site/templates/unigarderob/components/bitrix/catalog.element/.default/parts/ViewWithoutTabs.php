<?if ($arFlags['SHOW_DESCRIPTION_DETAIL']): // ��������� ��������?>
	<div class="uni-indents-vertical indent-50"></div>
    <div class="row">
		<div class="title"><?=GetMessage('PRODUCT_DESCRIPTION')?></div>
        <div class="uni-indents-vertical indent-25"></div>
		<div id="description" class="item_description uni-text-default">
			<?=$arResult['DETAIL_TEXT']?>
		</div>
		<div class="clear"></div>
	</div>
<?endif;?>
<?if ($arFlags['SHOW_PROPERTIES']):?>
	<div class="uni-indents-vertical indent-50"></div>
	<div class="row">
		<div class="title"><?=GetMessage('PRODUCT_PROPERTIES')?></div>
		<div class="uni-indents-vertical indent-25"></div>
		<div id="properties" class="item_description">
			<div class="properties">
				<?foreach ($arResult['DISPLAY_PROPERTIES'] as $arProperty):?>
					<div class="property">
						<div class="name">
							<?=$arProperty['NAME']?>
						</div>
						<?if (!is_array($arProperty['VALUE'])) {?>
						<div class="value">
							<?=$arProperty['VALUE']?>
						</div>
						<?} else {?>
							<div class="value">
							<?=implode(', ', $arProperty['VALUE'])?>
						</div>
						<?}?>
					</div>
				<?endforeach;?>
			</div>
		</div>
		<div class="clear"></div>
	</div>
<?endif;?>
<?if ($arFlags['SHOW_DOCUMENTS']):?>
	<div class="uni-indents-vertical indent-50"></div>
	<div class="row">
		<div class="title"><?=GetMessage('PRODUCT_DOCUMENTS')?></div>
		<div class="uni-indents-vertical indent-25"></div>
        <?include('Documents.php')?>
		<div class="clear"></div>
	</div>
<?endif;?>
<?if ($arFlags['SHOW_EXPANDABLES']):?>
	<div class="uni-indents-vertical indent-50"></div>
	<div class="row">
		<div id="expandables" class="item_description">
			<?include('ProductsExpandables.php')?>
		</div>
		<div class="clear"></div>
	</div>
<?endif;?>