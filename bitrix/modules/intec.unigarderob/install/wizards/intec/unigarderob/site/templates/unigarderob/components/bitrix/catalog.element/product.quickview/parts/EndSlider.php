<?if ($arResult['SHOW_SLIDER']){?>
	<div class="end_slider">
		<div class="image-slider image-slider-pic">
			<div class="list" id='ProductSlider'>
				<div class="buttons hidden">
					<div class="valign"></div>
					<div class="wrapper">
						<div class="button uni-slider-button-small uni-slider-button-left" id="left" onclick="return ProductSlider.SlideRight();"><div class="icon"></div></div>
						<div class="button uni-slider-button-small uni-slider-button-right" id="right" onclick="return ProductSlider.SlideLeft();"><div class="icon"></div></div>
					</div>
				</div>
				<div class="items">
					<?if (count($arResult['MORE_PHOTO']) > 1) {?>
						<?foreach($arResult['MORE_PHOTO'] as $photo) {?>
							<div class="image">
								<div class="wrapper">
									<div>
										<div>
											<div class="valign"></div>
											<img src="<?=$photo['SRC']?>" />
										</div>
									</div>
								</div>
							</div>
						<?}?>
					<?}else{?>
						<script>
							$(".end_slider").hide();
						</script>
					<?}?>
				</div>
				<script type="text/javascript">
					var ProductSlider = new UNISlider({
						'SLIDER': '.image-slider .list#ProductSlider .items',
						'ELEMENT': '.image-slider .list#ProductSlider .items .image',
						'OFFSET': 5,
						'INFINITY_SLIDE': true,
						'ANIMATE': true,
						'ANIMATE_SPEED': 400,
						"EVENTS": {
							"onAdaptabilityChange": function ($oSlider) {
								if ($oSlider.Settings.OFFSET < $oSlider.GetElementsCount()) {
									$('.image-slider .list#ProductSlider .buttons').removeClass('hidden');
								} else {
									$('.image-slider .list#ProductSlider .buttons').addClass('hidden');
								}  
							}            
						},
					});
					
					$('.image-slider .list#ProductSlider .items .image').on('click', function () {
						$('.image-slider .list#ProductSlider .items .image').removeClass('selected');
						$(this).addClass('selected');
						$('.image-slider .image-box .slider-images#ProductSliderBox .image')
							.hide()
							.eq($(this).index()).css("display","block");
					}).eq(0).addClass('selected');
				</script>
			</div>
			<?if ($arFlags['SHOW_OFFERS_SLIDER']){?>
				<?foreach ($arResult['JS_OFFERS'] as $arOffer){?>
					<?if ($arOffer['SLIDER_COUNT'] > 0){?>
						<div class="list" id="SKUProductSlider<?=$arOffer['ID']?>" style="display: none;">
							<div class="buttons hidden">
								<div class="valign"></div>
								<div class="wrapper">
									<div class="button uni-slider-button-small uni-slider-button-left" onclick="return SKUProductSlider<?=$arOffer['ID']?>.SlideRight();"><div class="icon"></div></div>
									<div class="button uni-slider-button-small uni-slider-button-right" id="right" onclick="return SKUProductSlider<?=$arOffer['ID']?>.SlideLeft();"><div class="icon"></div></div>
								</div>
							</div>
							<div class="items">
								<?if (count($arOffer['SLIDER']) > 1) {?>
									<?foreach($arOffer['SLIDER'] as $photo) {?>
										<div class="image">
											<div class="wrapper">
												<div>
													<div>
														<div class="valign"></div>
														<img src="<?=$photo['SRC']?>"/>
													</div>
												</div>
											</div>
										</div>
									<?}?>
								<?}?>
							</div>
						</div>
						<script type="text/javascript">
							var SKUProductSlider<?=$arOffer['ID']?> = new UNISlider({
								'SLIDER': '.image-slider .list#SKUProductSlider<?=$arOffer['ID']?> .items',
								'ELEMENT': '.image-slider .list#SKUProductSlider<?=$arOffer['ID']?> .items .image',
								'OFFSET': 5,
								'INFINITY_SLIDE': true,
								'ANIMATE': true,
								'ANIMATE_SPEED': 400,
								"EVENTS": {
									"onAdaptabilityChange": function ($oSlider) {
										if ($oSlider.Settings.OFFSET < $oSlider.GetElementsCount()) {
											$('.image-slider .list#SKUProductSlider<?=$arOffer['ID']?> .buttons').removeClass('hidden');
										} else {
											$('.image-slider .list#SKUProductSlider<?=$arOffer['ID']?> .buttons').addClass('hidden');
										}  
									}            
								},
							});
							
							$('.image-slider .list#SKUProductSlider<?=$arOffer['ID']?> .items .image').on('click', function () {
								$('.image-slider .list#SKUProductSlider<?=$arOffer['ID']?> .items .image').removeClass('selected');
								$(this).addClass('selected');
								$('.image-slider .image-box .slider-images#SKUProductSliderBox<?=$arOffer['ID']?> .image')
									.hide()
									.eq($(this).index()).css("display","block");
							}).eq(0).addClass('selected');
						</script>
					<?}?>
				<?}?>
				<script>
					$(".end_slider").show();
				</script>
			<?}?>
		</div>
	</div>
<?}?>