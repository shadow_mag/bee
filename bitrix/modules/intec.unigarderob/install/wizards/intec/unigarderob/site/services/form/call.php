<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(!CModule::IncludeModule("form"))
	return;

$iFormSID = "CALL_".WIZARD_SITE_ID; // ������������� SID �����
$arForm = CForm::GetBySID($iFormSID)->Fetch(); // ���� ����� �� SID
$bitrixTemplateDir = $_SERVER["DOCUMENT_ROOT"].BX_PERSONAL_ROOT."/templates/".WIZARD_TEMPLATE_ID."_".WIZARD_SITE_ID;

if($arForm) // ���� ����� ����������
{
	CWizardUtil::ReplaceMacros($bitrixTemplateDir.'/ajax/popup.order.call.php', array("CALL_FORM_ID" => $arForm["ID"]));
	return;
}

unset($arForm); // ���������� ���������� �����


$rsSite = CSite::GetByID(WIZARD_SITE_ID);
if($arSite = $rsSite->Fetch())
	$lang = $arSite["LANGUAGE_ID"]; // ����� ���� �����

unset($rsSite); // ������� rsSite

if(strlen($lang) <= 0) $lang = "ru"; // ������������� ����, ���� �� �� ����������
	
WizardServices::IncludeServiceLang("call.php", $lang); // ��������� �������� ����

// ������� ������ � ����� ������
$arFields = array(
	"NAME"				=> GetMessage("FORM_NAME"),
	"SID"				=> $iFormSID,
	"C_SORT"			=> 100,
	"BUTTON"			=> GetMessage("BUTTON_NAME"),
	"DESCRIPTION"		=> "",
	"DESCRIPTION_TYPE"	=> "text",
	"STAT_EVENT1"		=> "form",
	"STAT_EVENT2"		=> "",
	"arSITE"			=> array( WIZARD_SITE_ID ),
	"arMENU"			=> array( "ru" => GetMessage("FORM_NAME") ),
	"arGROUP"			=> array( "2" => "10" )
);

$iFormID = CForm::Set($arFields); // ������� ����� �����

if($iFormID < 0) // ���� ������ ����� ����� �� �����
	return; // ����

/* ��������� ������� */

$arANSWER = array(); // ���� ���
$arANSWER[] = array( "MESSAGE" => " ", "C_SORT" => 100, "ACTIVE" => "Y", "FIELD_TYPE" => "text", "FIELD_PARAM" => "" );
$arField = array( "FORM_ID" => $iFormID, "ACTIVE" => "Y", "TITLE" => GetMessage("FORM_QUESTION_1"), "TITLE_TYPE" => "text", "SID" => "NAME", "C_SORT" => 100, "ADDITIONAL" => "N", "REQUIRED" => "Y", "arANSWER" => $arANSWER );
CFormField::Set($arField);

$arANSWER = array(); // ��� �������
$arANSWER[] = array( "MESSAGE" => " ", "C_SORT" => 200, "ACTIVE" => "Y", "FIELD_TYPE" => "text", "FIELD_PARAM" => "" );
$arField = array( "FORM_ID" => $iFormID, "ACTIVE" => "Y", "TITLE" => GetMessage("FORM_QUESTION_2"), "TITLE_TYPE" => "text", "SID" => "PHONE", "C_SORT" => 200, "ADDITIONAL" => "N", "REQUIRED" => "Y", "arANSWER" => $arANSWER );
CFormField::Set($arField);

/** ��������� ������� **/

/* ��������� ������ */
$arField = array( "FORM_ID" => $iFormID, "C_SORT" => 100, "ACTIVE" => "Y", "TITLE" => "DEFAULT", "DEFAULT_VALUE" => "Y", "arPERMISSION_VIEW" => array(2), "arPERMISSION_MOVE" => array(2), "arPERMISSION_EDIT" => array(2), "arPERMISSION_DELETE" => array(2) );
CFormStatus::Set($arField);

// ��������� ��� ���-����� ����� ���� ��������� �������
// ��� ���� ������� �������� �������
$arTemplates = CForm::SetMailTemplate($iFormID);
// ����������� ����� ��������� �������� ������� ������ ���-�����
CForm::Set(array("arMAIL_TEMPLATE" => $arTemplates), $iFormID);

CWizardUtil::ReplaceMacros($bitrixTemplateDir.'/ajax/popup.order.call.php', array("CALL_FORM_ID" => $iFormID));
?>