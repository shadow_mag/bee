<?
    include('.utils.php');
    $iIBlockID = __importIBlockFile('catalog_offers.xml', 'offers', 'unigarderob_catalog_offers');
	__importIBlockFile('catalog_offers_prices.xml', 'offers', 'unigarderob_catalog_offers_prices');
    
    $arVars = __getVars();
    $arVars['MACROS']['CATALOG_OFFERS_IBLOCK_ID'] = $iIBlockID;
    $arVars['MACROS']['CATALOG_OFFERS_IBLOCK_TYPE'] = 'offers';
    __setVars($arVars);
?>