<?
    include('.utils.php');
    $iIBlockID = __importIBlockFile('catalog.xml', 'catalog', 'unigarderob_catalog');
	__importIBlockFile('catalog_prices.xml', 'catalog', 'unigarderob_catalog_prices');
    
    $arVars = __getVars();
    $arVars['MACROS']['CATALOG_IBLOCK_ID'] = $iIBlockID;
    $arVars['MACROS']['CATALOG_IBLOCK_TYPE'] = 'catalog';
    __setVars($arVars);
?>