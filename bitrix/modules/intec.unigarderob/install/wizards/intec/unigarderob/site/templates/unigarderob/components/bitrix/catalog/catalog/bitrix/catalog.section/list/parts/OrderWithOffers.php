<?foreach ($arItem['OFFERS'] as $arOffer):?>
    <?if ($arOffer['CAN_BUY']):?>
        <div class="button-buy-click SKUBuyButtonClick SKUBuyButtonClick<?=$arOffer['ID']?>" style="display: none;">
            <?$sBuyBittonClickImage = !empty($arOffer['DETAIL_PICTURE']['SRC']) ? $arOffer['DETAIL_PICTURE']['SRC'] : $arOffer['PREVIEW_PICTURE']['SRC']?>
            <?$sBuyBittonClickImage = empty($sBuyBittonClickImage) ? $arItem['DETAIL_PICTURE']['SRC'] : $sBuyBittonClickImage?>
            <?$sBuyBittonClickImage = empty($sBuyBittonClickImage) ? $arItem['PREVIEW_PICTURE']['SRC'] : $sBuyBittonClickImage?>
			<?$arBuyButtonClickParams = array (
				"ID" => $arOffer["ID"],
				"Name" => $arItem["NAME"],
				"IBlockType" => $arParams["IBLOCK_TYPE"],
				"IBlockID" => $arItem["IBLOCK_ID"],
				"PriceCurrent" => $arOffer['MIN_PRICE']['PRINT_DISCOUNT_VALUE'],
				"PriceOld" => "",
				"Image" => $sBuyBittonClickImage,
                "Currency" => $arOffer['MIN_PRICE']['CURRENCY'],
                "PriceID" => $arOffer['MIN_PRICE']['PRICE_ID'],
				"Price" => $arOffer['MIN_PRICE']['VALUE']
			);?>
    		<a 
    			href="javascript:void(0);" 
    			class="uni-button button" 
    			onclick="Popups.OneClickBuy(<?=CUtil::PhpToJSObject($arBuyButtonClickParams);?>)"
    		><?=GetMessage("CATALOG_ONE_CLICK_BUY")?></a>
        </div>
        <div class="button-buy SKUBuyButton SKUBuyButton<?=$arOffer['ID']?>" style="display: none;">
            <a rel="nofollow" class="uni-button solid_button button addToBasket addToBasket<?=$arOffer['ID']?>"
    			onclick="SaleActions.addToBasket('<?=$arOffer['ID']?>', '<?=$arOffer['MIN_PRICE']['CURRENCY']?>', <?=$arOffer['CATALOG_MEASURE_RATIO']?>, SaleWrapper.updateAfterBasketAction);"
    		><?=GetMessage("CATALOG_ADD")?>
    		</a>
    		<a rel="nofollow" href="<?=$arParams["BASKET_URL"];?>" class="uni-button solid_button button removeFromBasket removeFromBasket<?=$arOffer["ID"]?>" style="display: none;">
    			<?=GetMessage("CATALOG_ADDED")?>
    		</a>
        </div>
    <?endif;?>
    <div class="min-buttons SKUMinButtons SKUMinButtons<?=$arOffer['ID']?>" style="display: none;">
        <div class="min-buttons-wrapper">
    		<?if($arParams["DISPLAY_COMPARE"] == "Y"):?>
    			<div class="min-button compare">
    				<div class="add addToCompare addToCompare<?=$arOffer["ID"]?>"
    					onclick="SaleActions.addToCompare('<?=$arOffer['COMPARE_URL']?>', SaleWrapper.updateAfterCompareAction);"
    					title="<?=GetMessage('COMPARE_TEXT_DETAIL')?>"					
    				>
    				</div>
    				<div class="remove removeFromCompare removeFromCompare<?=$arOffer["ID"]?>"
    					style="display:none"
    					onclick="SaleActions.removeFromCompare('<?=$arOffer['COMPARE_REMOVE_URL']?>', SaleWrapper.updateAfterCompareAction);"
    					title="<?=GetMessage('COMPARE_DELETE_TEXT_DETAIL')?>"
    				>
    				</div>
    			</div>
    		<?endif?>
    		<?if ($arOffer['CAN_BUY']):?>
    			<div class="min-button like">
    				<div class="add addToWishList addToWishList<?=$arOffer["ID"]?>"								
    					onclick="SaleActions.addToWishList('<?=$arOffer['ID']?>', '<?=$arOffer['MIN_PRICE']['CURRENCY']?>', 1, SaleWrapper.updateAfterBasketAction);"
    					title="<?=GetMessage('LIKE_TEXT_DETAIL')?>"
    				>
    				</div>
    				<div class="remove removeFromWishList removeFromWishList<?=$arOffer["ID"]?>"
    					style="display:none"							
    					onclick="SaleActions.removeFromWishList('<?=$arOffer['ID']?>', SaleWrapper.updateAfterBasketAction);"
    					title="<?=GetMessage('LIKE_DELETE_TEXT_DETAIL')?>"
    				>
    				</div>
    			</div>
    		<?endif;?>
        </div>
    </div>
<?endforeach;?>