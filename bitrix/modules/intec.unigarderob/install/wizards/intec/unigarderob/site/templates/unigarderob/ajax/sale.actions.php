<?
    if (!empty($_POST['SITE_ID']) && !empty($_POST['SITE_DIR'])) {
        define(SITE_ID, $_POST['SITE_ID']);
        define(SITE_DIR, $_POST['SITE_DIR']);
    }
?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?
    $options = array();
        
    $sAction = $_POST['ACTION'];
	
    
    if($sAction == 'UpdateBasket' || $sAction == 'UpdateCompare') {
        CModule::IncludeModule("iblock");
        if (CModule::IncludeModule("intec.unigarderob")) {
            UniGarderob::InitProtection();
            UniGarderob::ShowInclude(SITE_ID);
            $options = UniGarderob::getOptionsValue(SITE_ID);
        }
    }
    
    if (empty($sAction))
        die();
        
    if ($sAction == 'UpdateButtons')
        include('../parts/sale.buttons.php');
        
    if ($sAction == 'UpdateBasket')
        include('../parts/sale.basket.php');
        
    if ($sAction == 'UpdateCompare')
        include('../parts/sale.compare.php');
	
	 if ($sAction == 'UpdateCompareFly')
        include('../parts/sale.compare.fly.php');

    if (($sAction == 'AddToWishList' || $sAction == 'RemoveFromWishList' || $sAction == 'AddToBasket' || $sAction == 'RemoveFromBasket' || $sAction == 'SetQuantity') && CModule::IncludeModule("sale") && CModule::IncludeModule("catalog")) {
        
        $iProductID = intval($_POST['ID']);
        
        if ($iProductID <= 0)
            die();
        
        $arBasketItem = CSaleBasket::GetList(
			array(
				"NAME" => "ASC",
				"ID" => "ASC"
			),
			array(
				"PRODUCT_ID" => $iProductID,
				"FUSER_ID" => CSaleBasket::GetBasketUserID(),
				"LID" => SITE_ID,
				"ORDER_ID" => false
			),
			false,
			false,
			array("ID", "DELAY")
		)->Fetch();
        
        $arProduct = null;
        if ($dbProduct = CIBlockElement::GetByID($iProductID)->GetNextElement()) {
            $arProduct = $dbProduct->GetFields();
            $arProduct['PROPERTIES'] = $dbProduct->getProperties();
            unset($dbProduct);
        }
        
        if ($sAction == 'RemoveFromWishList' || $sAction == 'RemoveFromBasket')
            if (!empty($arBasketItem))
                CSaleBasket::Delete($arBasketItem['ID']);
                
        if ($sAction == 'AddToWishList' || $sAction == 'AddToBasket') {
            $arAddFields = array();
            $arUpdateFields = array();
            $fQuantity = floatval($_POST['QUANTITY']);
            
            if (!empty($_POST['CURRENCY']))
                $arAddFields["CURRENCY"] = $_POST["CURRENCY"];
            
            if ($fQuantity <= 0)
                $fQuantity = 1;
            
            if ($sAction == 'AddToWishList') {
                $arAddFields['DELAY'] = 'Y';
                $arUpdateFields['DELAY'] = 'Y';
            } else {
                $arAddFields['DELAY'] = 'N';
                $arUpdateFields['DELAY'] = 'N';
            }
            
            if (!empty($arBasketItem)) {
                CSaleBasket::Update($arBasketItem['ID'], $arUpdateFields);
            } else if (!empty($arProduct)) {
                $arIBlockInfo = CCatalogSKU::GetInfoByOfferIBlock($arProduct['IBLOCK_ID']);
                $arBasketProperties = array();
                
                if (!empty($arIBlockInfo)) {
                    foreach ($arProduct['PROPERTIES'] as $arProperty) {
                        $arLinkProperty = CCatalogSKU::GetInfoByLinkProperty($arProperty['ID']);
                        if ($arLinkProperty == false && !empty($arProperty['VALUE']) && !is_array($arProperty['VALUE']))
                            $arBasketProperties[] = array(
                                'NAME' => $arProperty['NAME'],
                                'CODE' => $arProperty['CODE'],
                                'VALUE' => $arProperty['VALUE'],
                                'SORT' => $arProperty['SORT']
                            );
                    }
                    
                    
                }
                
                Add2BasketByProductID($iProductID, $fQuantity, $arAddFields, $arBasketProperties);
            }
        }
        
        if ($sAction == 'SetQuantity') {
            $fQuantity = floatval($_POST['QUANTITY']);
            
            if (!empty($arBasketItem) && $fQuantity > 0)
                CSaleBasket::Update($arBasketItem['ID'], array('QUANTITY' => $fQuantity));
                
            $arBasketItem = $dbBasketItem = CSaleBasket::GetList(
    			array(
    				"NAME" => "ASC",
    				"ID" => "ASC"
    			),
    			array(
    				"PRODUCT_ID" => $iProductID,
    				"FUSER_ID" => CSaleBasket::GetBasketUserID(),
    				"LID" => SITE_ID,
    				"ORDER_ID" => false
    			),
    			false,
    			false,
    			array("QUANTITY")
    		)->Fetch();
            
            echo $arBasketItem["QUANTITY"];
        }  
    }
?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");?>