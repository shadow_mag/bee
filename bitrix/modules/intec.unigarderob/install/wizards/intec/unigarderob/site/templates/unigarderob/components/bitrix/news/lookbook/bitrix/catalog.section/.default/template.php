<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>
<?
    $this->setFrameMode(true);
?>
<?if (!empty($arResult['ITEMS'])):?>
    <?
        $sItemEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT");
    	$sItemDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE");
    	$arItemDeleteParams = array("CONFIRM" => GetMessage('N_L_CS_DEFAULT_ACTION_DELETE_QUESTION'));
    ?>
    <div class="lookbook-pictures">
        <div class="lookbook-pictures-wrapper">
            <?foreach ($arResult['ITEMS'] as $arItem):?>
                <?
                    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $sItemEdit);
            		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], $sItemDelete, $arItemDeleteParams);
            		$sItemEditAreaID = $this->GetEditAreaId($arItem['ID']);
                ?>
                <div class="lookbook-picture uni-20">
                    <div class="lookbook-picture-wrapper hover_shadow" id="<?=$sItemEditAreaID?>">
                        <a rel="lookbook-pictures" class="lookbook-picture-wrapper-wrapper fancy" href="<?=$arItem['DETAIL_PICTURE']['SRC']?>">
                            <div class="lookbook-information">
                                <div class="lookbook-name">
                                    <div class="lookbook-name-wrapper">
                                        <?=$arItem["NAME"]?>
                                    </div>
                                </div>
                                <div class="lookbook-icon">
                                    <div class="uni-aligner-vertical"></div>
                                    <div class="lookbook-icon-wrapper"></div>
                                </div>
                            </div>
                            <div class="lookbook-picture-image">
                                <div class="lookbook-picture-image-wrapper" style="background-image: url('<?=$arItem['PREVIEW_PICTURE']['SRC']?>');"></div>
                            </div>
                        </a>
                    </div>
                </div>
            <?endforeach;?>
        </div>
    </div>
    <div class="clear"></div>
<?endif;?>