<?
    $MESS['BASKET_FLY_SECTIONS_BASKET_HEADER'] = 'Товаров в корзине #COUNT# на <span class="basket-sum">#SUM#</span>';
    $MESS['BASKET_FLY_SECTIONS_WISH_LIST_HEADER'] = 'Отложенных товаров #COUNT# на #SUM#';
    $MESS['BASKET_FLY_SECTIONS_TABLE_HEADER_NAME'] = 'Наименование';
    $MESS['BASKET_FLY_SECTIONS_TABLE_HEADER_COUNT'] = 'Количество';
    $MESS['BASKET_FLY_SECTIONS_TABLE_HEADER_PRICE'] = 'Цена';
    $MESS['BASKET_FLY_SECTIONS_TABLE_HEADER_TOTAL'] = 'Итого';
    
    $MESS['BASKET_FLY_SECTIONS_BASKET_NOTIFY_TEXT'] = 'Ваша корзина пуста';
    $MESS['BASKET_FLY_SECTIONS_WISH_LIST_NOTIFY_TEXT'] = 'Список отложенных товаров пуст';
    
    $MESS['BASKET_FLY_BUTTONS_GO_BUY'] = 'Перейти к покупкам';
    $MESS['BASKET_FLY_BUTTONS_GO_BASKET'] = 'Перейти в корзину';
    $MESS['BASKET_FLY_BUTTONS_GO_ORDER'] = 'Оформить заказ';
    $MESS['BASKET_FLY_BUTTONS_CONTINUE_BUY'] = 'Продолжить покупки';
    $MESS['BASKET_FLY_BUTTONS_CLOSE'] = 'Закрыть';
?>