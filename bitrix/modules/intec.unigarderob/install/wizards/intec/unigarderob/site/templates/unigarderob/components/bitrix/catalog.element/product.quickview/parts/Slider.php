<?reset($arResult['MORE_PHOTO']);?>
<div class="image-slider">
	<div class="image-box">
		<div class="wrapper">
			<div class="marks">
				<?if( $arResult["MIN_PRICE"]["DISCOUNT_DIFF_PERCENT"] && $arParams['SHOW_DISCOUNT_PERCENT'] == 'Y' ){?>
					<span class="mark action">- <?=$arResult["MIN_PRICE"]["DISCOUNT_DIFF_PERCENT"];?> %</span>
				<?}?>
				<?if( $arResult["PROPERTIES"]["HIT"]["VALUE"] ){?>
					<span class="mark hit"><?=GetMessage('MARK_HIT')?></span>
				<?}?>			
				<?if( $arResult["PROPERTIES"]["NEW"]["VALUE"] ){?>
					<span class="mark new"><?=GetMessage('MARK_NEW')?></span>
				<?}?>
				<?if( $arResult["PROPERTIES"]["RECOMMEND"]["VALUE"] ){?>
					<span class="mark recommend"><?=GetMessage('MARK_RECOMEND')?></span>
				<?}?>
			</div>
			<div class="slider-images" id="ProductSliderBox">
				<?$noimg = false;
				$first  = true;
				if (empty($arResult['PREVIEW_PICTURE']) && empty($arResult['DETAIL_PICTURE'])) $noimg = true;?>
				<?foreach ($arResult['MORE_PHOTO'] as $photo){?>
               <?if ($first && $noimg){?>
						<?$photo['SRC'] = SITE_TEMPLATE_PATH."/images/noimg/no-img.png"?>
                  <a class="image noimg">
							<img class="noimg" src="<?=$photo['SRC']?>" />
							<div class="valign"></div>
                  </a>
               <?}else{?>
						<a 
							rel='images' 
							<?if ($options['CATALOG_PRODUCT_IMAGE_VIEW']['ACTIVE_VALUE'] != 'WITHOUT_EFFECTS'):?>
								<?=$options['CATALOG_PRODUCT_IMAGE_VIEW']['ACTIVE_VALUE'] == 'WITH_FANCY'?' href="'.$photo['SRC'].'"':''?>
							<?endif;?>
							class="image<?=$options['CATALOG_PRODUCT_IMAGE_VIEW']['ACTIVE_VALUE'] == 'WITH_FANCY'?' fancy':''?><?=$options['CATALOG_PRODUCT_IMAGE_VIEW']['ACTIVE_VALUE'] == 'WITH_ZOOM'?' zoom':''?><?=$options['CATALOG_PRODUCT_IMAGE_VIEW']['ACTIVE_VALUE'] == 'WITHOUT_EFFECTS'?' noeffect':''?>"
						>
							<img src="<?=$photo['SRC']?>" alt="<?=htmlspecialcharsbx($arResult['NAME'])?>" title="<?=htmlspecialcharsbx($arResult['NAME'])?>"/>
							<div class="valign"></div>
						</a>
					<?}?>
               <?$first = false?>
				<?}?>
			</div>
			<?if ($arFlags['SHOW_OFFERS_SLIDER']):?>
				<?foreach ($arResult['JS_OFFERS'] as $arOffer):?>
					<?if ($arOffer['SLIDER_COUNT'] > 0):?>
						<div class="slider-images" id="SKUProductSliderBox<?=$arOffer['ID']?>" style="display: none;">
							<?foreach($arOffer['SLIDER'] as $photo){?>
								<a 
									rel='images<?=$arOffer['ID']?>' 
									<?if ($options['CATALOG_PRODUCT_IMAGE_VIEW']['ACTIVE_VALUE'] != 'WITHOUT_EFFECTS'):?>
										<?=$options['CATALOG_PRODUCT_IMAGE_VIEW']['ACTIVE_VALUE'] == 'WITH_FANCY'?' href="'.$photo['SRC'].'"':''?>
									<?endif;?> 
									class="image<?=$options['CATALOG_PRODUCT_IMAGE_VIEW']['ACTIVE_VALUE'] == 'WITH_FANCY'?' fancy':''?><?=$options['CATALOG_PRODUCT_IMAGE_VIEW']['ACTIVE_VALUE'] == 'WITH_ZOOM'?' zoom':''?><?=$options['CATALOG_PRODUCT_IMAGE_VIEW']['ACTIVE_VALUE'] == 'WITHOUT_EFFECTS'?' noeffect':''?>"
								>
									<img src="<?=$photo['SRC']?>" />
									<div class="valign"></div>
								</a>
							<?}?>
						</div>
					<?endif;?>
				<?endforeach;?>
			<?endif;?>
			<?if ($options['CATALOG_PRODUCT_IMAGE_VIEW']['ACTIVE_VALUE'] == 'WITH_ZOOM'):?>
				<script type="text/javascript">
                    $(document).ready(function(){
                        $('.image-slider .image-box .wrapper a.image').not('.noimg').zoom({magnify:1.5});
                    });
				</script>
			<?endif;?>
		</div>
	</div>
	<div class="clear"></div>
</div>