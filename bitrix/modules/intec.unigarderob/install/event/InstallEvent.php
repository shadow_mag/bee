<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?
	global $MESS;
	$sRootPath = str_replace("\\", "/", __FILE__);
	$sRootPath = substr($sRootPath, 0, strlen($sRootPath)-strlen("/install/event/InstallEvant.php"));
	include(GetLangFileName($sRootPath."/lang/", "/install/event/InstallEvent.php"));
	
	$EVENT_NAME = "ONECLICKBUY_ORDER";
	$LID = "ru";
	$NAME = GetMessage('TYPE_NAME');
	$DESCRIPTION = '#USER_NAME# - '.GetMessage('USER_NAME').'
					#USER_PHONE# - '.GetMessage('USER_PHONE').'
					#ORDER_ID# - '.GetMessage('ORDER_ID').'
					#ORDER_DESCRIPTION# - '.GetMessage('ORDER_DESCRIPTION').'
					#ORDER_LIST# - '.GetMessage('ORDER_LIST').'
					#PRICE# - '.GetMessage('PRICE');
	
	$rsET = CEventType::GetByID("ADV_BANNER_STATUS_CHANGE", "ru");
	if ( !($arET = $rsET->Fetch()) ) {
	
	$et = new CEventType;
    $et->Add(array(
        "LID"           => $LID,
        "EVENT_NAME"    => $EVENT_NAME,
        "NAME"          => $NAME,
        "DESCRIPTION"   => $DESCRIPTION
        ));
	}
		
?>