<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Loader;

if (!Loader::includeModule('iblock'))
	return;

$arIBlockType = CIBlockParameters::GetIBlockTypes();

$arIBlock = array();
$iblockFilter = (
	!empty($arCurrentValues['IBLOCK_TYPE'])
	? array('TYPE' => $arCurrentValues['IBLOCK_TYPE'], 'ACTIVE' => 'Y')
	: array('ACTIVE' => 'Y')
);
$rsIBlock = CIBlock::GetList(array('SORT' => 'ASC'), $iblockFilter);
while ($arr = $rsIBlock->Fetch())
	$arIBlock[$arr['ID']] = '['.$arr['ID'].'] '.$arr['NAME'];
unset($arr, $rsIBlock, $iblockFilter);
$arComponentParameters = array(
    "GROUPS" => array(
        "COMPARE" => array(
            "NAME" => GetMessage("TABSLABEL_GROUP_COMPARE")
        )
    ),
	"PARAMETERS" => array(
		"IBLOCK_TYPE" => array(
			"NAME" => GetMessage("IBLOCK_TYPE"),
			"TYPE" => "LIST",
			"VALUES" => $arIBlockType,
			"REFRESH" => "Y",
		),
	)
);
if (!empty($arCurrentValues['IBLOCK_TYPE']))
{
    $arComponentParameters['PARAMETERS']["IBLOCK_ID"] = array(
		"NAME" => GetMessage("IBLOCK_IBLOCK"),
		"TYPE" => "LIST",
		"ADDITIONAL_VALUES" => "Y",
		"VALUES" => $arIBlock,
		"REFRESH" => "Y",
	);
    
    if (is_numeric($arCurrentValues['IBLOCK_ID']))
    {
        $rsProperties = CIBlockProperty::GetList(
        	array('SORT' => 'ASC', 'ID' => 'ASC'),
        	array('IBLOCK_TYPE' => $arCurrentValues['IBLOCK_TYPE'], 'IBLOCK_ID' => $arCurrentValues['IBLOCK_ID'], 'ACTIVE' => 'Y')
        );
        
        $arMultipleListProperties = array();
        while ($arProperty = $rsProperties->Fetch()) {
        	if($arProperty["MULTIPLE"] == "Y" && $arProperty["LIST_TYPE"] == "L") {
        		$arMultipleListProperties[$arProperty["ID"]] = $arProperty["NAME"];
        	}
        }
        unset($arProperty);
        
        $arComponentParameters['PARAMETERS']["PROPERTY_LABEL"] = array(
    		"NAME" => GetMessage("TABSLABEL_PROPERTY_LABEL"),
    		"TYPE" => "LIST",
    		"ADDITIONAL_VALUES" => "Y",
    		"VALUES" => $arMultipleListProperties,
    		"REFRESH" => "Y",
		);
        
        if (!empty($arCurrentValues['PROPERTY_LABEL']))
        {
            $arPropertyEnums = array();
            $rsPropertyEnums = CIBlockPropertyEnum::GetList(array(), array(
                "IBLOCK_ID" => $arCurrentValues['IBLOCK_ID'],
                "PROPERTY_ID" => $arCurrentValues['PROPERTY_LABEL']
            ));
            
            while ($arPropertyEnum = $rsPropertyEnums->GetNext())
                $arPropertyEnums[$arPropertyEnum['ID']] = $arPropertyEnum['VALUE'];
                
            $arComponentParameters['PARAMETERS']["DISPLAYED_LABELS"] = array(
        		"NAME" => GetMessage("TABSLABEL_DISPLAYED_LABELS"),
        		"TYPE" => "LIST",
        		"MULTIPLE" => "Y",
        		"VALUES" => $arPropertyEnums,
    		);
        }
        
		$arComponentParameters['PARAMETERS']["DISPLAY_COMPARE"] = array(
            "PARENT" => "COMPARE",
			"NAME" => GetMessage("TABSLABEL_DISPLAY_COMPARE"),
			"TYPE" => "CHECKBOX",
			"DEFAULT" => "N",
            "REFRESH" => "Y"
		);
        
        if ($arCurrentValues['DISPLAY_COMPARE'] == 'Y')
    		$arComponentParameters['PARAMETERS']["COMPARE_NAME"] = array(
                "PARENT" => "COMPARE",
    			"NAME" => GetMessage("TABSLABEL_COMPARE_NAME"),
    			"TYPE" => "STRING",
    			"DEFAULT" => "CATALOG_COMPARE_LIST"
    		);
        
		$arComponentParameters['PARAMETERS']["LINE_ELEMENT_COUNT"] = array(
            "PARENT" => "VISUAL",
			"NAME" => GetMessage("TABSLABEL_LINE_ELEMENT_COUNT"),
			"TYPE" => "STRING",
			"DEFAULT" => "4"
		);
        
		$arComponentParameters['PARAMETERS']["ELEMENT_COUNT"] = array(
            "PARENT" => "VISUAL",
			"NAME" => GetMessage("TABSLABEL_ELEMENT_COUNT"),
			"TYPE" => "STRING",
			"DEFAULT" => "10"
		);
    }
}

?>