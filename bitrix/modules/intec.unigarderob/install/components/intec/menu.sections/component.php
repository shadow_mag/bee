<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<?
    $arDefaultParams = array(
        "CACHE_TYPE" => "N",
        "CACHE_TIME" => 36000000,
        "COUNT_ELEMENTS" => "N",
        "DEPTH_LEVEL" => null
    );
    
    $arParams = array_merge($arDefaultParams, $arParams);
    $bCountElements = $arParams['COUNT_ELEMENTS'] == 'Y';
    
    if ($this->StartResultCache()) {
        if(!CModule::IncludeModule("iblock")) {
    		$this->AbortResultCache();
    	} else {
            $arFilter = array(
                'IBLOCK_TYPE' => strval($arParams['IBLOCK_TYPE']),
                'IBLOCK_ID' => intval($arParams['IBLOCK_ID']),
                'GLOBAL_ACTIVE' => 'Y',
            );
            
            if (is_numeric($arParams['DEPTH_LEVEL']))
                $arFilter["<=DEPTH_LEVEL"] = $arParams["DEPTH_LEVEL"];
            
            $arFields = array(
                "ID",
    			"DEPTH_LEVEL",
    			"NAME",
    			"SECTION_PAGE_URL",
                "IBLOCK_SECTION_ID"
            );
            
            if ($bCountElements) {
                $arFilter['CNT_ALL'] = 'N';
                $arFilter['CNT_ACTIVE'] = 'Y';
                $arFilter['ELEMENT_SUBSECTIONS'] = 'Y';
                $arFields[] = "ELEMENT_CNT";
            }
           
            $arSections = array();
            $rsSections = CIBlockSection::GetList(
                array('left_margin' => 'ASC'),
                $arFilter,
                $bCountElements,
                $arFields
            );
            
            if (!empty($arParams["SECTION_URL"]))
                $rsSections->SetUrlTemplates("", $arParams["SECTION_URL"]);
            
            while ($arSection = $rsSections->GetNext()) {
                $arResult["SECTIONS"][] = array(
    				"ID" => $arSection["ID"],
    				"DEPTH_LEVEL" => $arSection["DEPTH_LEVEL"],
    				"~NAME" => $arSection["~NAME"],
    				"~SECTION_PAGE_URL" => $arSection["~SECTION_PAGE_URL"],
                    "IBLOCK_SECTION_ID" => $arSection["IBLOCK_SECTION_ID"],
                    "ELEMENT_CNT" => $arSection["ELEMENT_CNT"]
    			);
    			$arResult["ELEMENT_LINKS"][$arSection["ID"]] = array();
            }
            
            if (!empty($arParams['SECTION_ID']) && is_numeric($arParams['SECTION_ID'])) {
                $arNewSections = array();
                $arChildSections = array();
                
                foreach ($arResult['SECTIONS'] as $arSection) {
                    if ($arSection["IBLOCK_SECTION_ID"] == $arParams['SECTION_ID'] || in_array($arSection["IBLOCK_SECTION_ID"], $arChildSections)) {
                        $arChildSections[] = $arSection["ID"];
                        $arNewSections[] = $arSection;
                    } else {
                        unset($arResult["ELEMENT_LINKS"][$arSection["ID"]]);
                    }
                }
                
                $arResult['SECTIONS'] = $arNewSections;
            }
                       
            $this->EndResultCache();
    	}
    }
    
    $arMenuLinksNew = array();
    $iMenuIndex = 0;
    $iMenuPreviousDepthLevel = 1;
    
    foreach ($arResult['SECTIONS'] as $arSection) {
        if ($iMenuIndex > 0)
            $arMenuLinksNew[$iMenuIndex - 1][3]['IS_PARENT'] = $arSection['DEPTH_LEVEL'] > $iMenuPreviousDepthLevel;
            
        $iMenuPreviousDepthLevel = $arSection['DEPTH_LEVEL'];
        
        $arResult["ELEMENT_LINKS"][$arSection["ID"]][] = urldecode($arSection["~SECTION_PAGE_URL"]);
        
        $arMenuLinksNew[$iMenuIndex++] = array(
            htmlspecialcharsbx($arSection["~NAME"]),
            $arSection["~SECTION_PAGE_URL"],
            $arResult["ELEMENT_LINKS"][$arSection["ID"]],
    		array(
                "FROM_IBLOCK" => true,
    			"IS_PARENT" => false,
    			"DEPTH_LEVEL" => $arSection["DEPTH_LEVEL"],
                "ELEMENTS_COUNT" => $arSection["ELEMENT_CNT"],
    		)
    	);
    }
    
    return $arMenuLinksNew;
?>