<?
    $MESS ['CP_IMS_IBLOCK_TYPE'] = "Тип информационного блока";
    $MESS ['CP_IMS_IBLOCK_ID'] = "Код информационного блока";
    $MESS ['CP_IMS_SECTION_ID'] = "Код раздела";
    $MESS ['CP_IMS_SECTION_URL'] = "URL, ведущий на страницу с содержимым раздела";
    $MESS ['CP_IMS_COUNT_ELEMENTS'] = "Подсчитывать количество элементов в разделах";
?>