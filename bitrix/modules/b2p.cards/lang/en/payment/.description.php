<?
$MESS["SPCP_DTITLE"] = "Best2Pay";
$MESS["SPCP_DDESCR"] = "Payments with bank cards via the <a href=\"http://www.best2pay.net\">Best2Pay</a> payment system.";

$MESS["Sector"] = "Sector";
$MESS["Sector_DESCR"] = "Merchant identifier in the Processing Center";
$MESS["Password"] = "Password";
$MESS["Password_DESCR"] = "The key of the digital signature generation";
$MESS["TestMode"] = "Test Mode";
$MESS["TestMode_DESCR"] = "1 - 'Yes', 0 - 'No'";
$MESS["SuccessURL"] = "Success URL";
$MESS["SuccessURL_DESCR"] = "The customer will be redirected here on the successfull payment";
$MESS["FailURL"] = "Fail URL";
$MESS["FailURL_DESCR"] = "The customer will be redirected here when the payment failed";
$MESS["RedirectURL"] = "Redirect URL";
$MESS["RedirectURL_DESCR"] = "The URL of the reverse redirect from the Best2Pay site";
$MESS["KKT"] = "Add param fiscal_positions for KKT";
$MESS["KKT_DESCR"] = "1 - 'Yes', 0 - 'No'";
$MESS["TAX"] = "Choose tax for KKT";
$MESS["TAX_DESCR"] = "1 вЂ“ NDS -  18%
2 вЂ“ NDS - 10%
3 вЂ“ NDS - 18/118
4 вЂ“ NDS - 10/110
5 вЂ“ NDS -  0%
6 вЂ“ NDS is not appearing";
