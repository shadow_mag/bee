<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?><?

include(dirname(__FILE__) . "/common.php");

try {
	if (!CModule::IncludeModule("sale"))
		throw new Exception("Error when initializing sale module");

	$xml = file_get_contents("php://input");
	if (!$xml)
		throw new Exception("Empty data");
	$xml = simplexml_load_string($xml);
	if (!$xml)
		throw new Exception("Non valid XML was received");
	$response = json_decode(json_encode($xml));
	if (!$response)
		throw new Exception("Non valid XML was received");

	if (!orderAsPayed($response))
		exit();
	
	die("ok");

} catch (Exception $ex) {
	error_log($ex->getMessage());
	die($ex->getMessage());
}

?>
