<?

IncludeModuleLangFile(__FILE__);

class b2p_cards extends CModule
{
	const MODULE_ID = 'b2p.cards';
	var $MODULE_ID = 'b2p.cards';
	var $MODULE_VERSION;
	var $MODULE_VERSION_DATE;
	var $MODULE_NAME;
	var $MODULE_DESCRIPTION;
	var $MODULE_CSS;
	var $strError = '';

	function __construct()
	{
		$arModuleVersion = array();
		include(dirname(__FILE__)."/version.php");
		$this->MODULE_VERSION = $arModuleVersion["VERSION"];
		$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
		$this->MODULE_NAME = GetMessage("BEST2PAY_MODULE_NAME");
		$this->MODULE_DESCRIPTION = "http://best2pay.net";

		$this->PARTNER_NAME = GetMessage("BEST2PAY_MODULE_AUTHOR");
		$this->PARTNER_URI = "http://best2pay.net";
	}

	function InstallDB($arParams = array())
	{
		return true;
	}

	function UnInstallDB($arParams = array())
	{
		return true;
	}

	function InstallEvents()
	{
		return true;
	}

	function UnInstallEvents()
	{
		return true;
	}

	function InstallFiles($arParams = array())
	{
        CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/b2p.cards/install/payment/",
            $_SERVER["DOCUMENT_ROOT"]."/bitrix/php_interface/include/sale_payment/b2p.cards/");
        CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/b2p.cards/install/tools/",
            $_SERVER["DOCUMENT_ROOT"]."/bitrix/tools/b2p.cards/");

		return true;
	}

	function UnInstallFiles()
	{
        DeleteDirFilesEx("/bitrix/php_interface/include/sale_payment/b2p.cards");
        DeleteDirFilesEx("/bitrix/tools/b2p.cards");
		return true;
	}

	function DoInstall()
	{
		global $APPLICATION;
		$this->InstallFiles();
		$this->InstallDB();
		RegisterModule(self::MODULE_ID);
	}

	function DoUninstall()
	{
		global $APPLICATION;
		UnRegisterModule(self::MODULE_ID);
		$this->UnInstallDB();
		$this->UnInstallFiles();
	}
}
