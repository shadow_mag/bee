<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Условия доставки");
?>  
  <h4> 
    <div><strong style="text-align: justify;"><font size="4">Как и когда можно получить заказ?</font></strong></div>
   </h4>
 
  <div><strong><font size="4"> 
        <br />
       </font></strong></div>
 
  <p><strong><font size="4">Способ 1. Получение заказа в магазине. Бесплатно!</font></strong></p>
 
  <p><font size="4">Получение заказа в обычном магазине <span style="line-height: 16px;">Magazine</span></font></p>
 
  <h4> 
    <div> 
      <p> </p>
     
      <p style="text-align: justify;"><font size="2" style="font-weight: normal;">Если вы решили приобрести что-то небольшое (малогабаритную кухонную технику, ноутбук, игрушки, посуду &ndash; в общем, товар весом не более 35 кг), вы можете забрать покупку в любом магазине Magazine вашего города уже в тот же день (если товар есть в наличии на складе магазина) или через 1-4 дня в зависимости от региона (если товара на складе магазина в данный момент нет). После вашего заказа товар резервируется на 3 дня - именно столько времени у вас будет, чтобы приехать за долгожданной покупкой. Товары из категории &quot;Мебель&quot;, даже если они весят менее 35 кг, можно заказать только с доставкой на дом. Выбрать ближайший к вам магазин, узнать его график работы и посмотреть схему проезда вы можете на нашем сайте. Услуга доставки заказа в магазин Enterбесплатна!</font></p>
     </div>
   </h4>
 
  <p><font size="4">Получение заказа в пункте выдачи <span style="line-height: 16px; font-weight: normal;">Magazine</span></font></p>
 
  <h4> 
    <div> 
      <p style="text-align: justify;"><span style="font-weight: normal;">Специально для тех, кто ценит свое время, мы открываем магазины в уникальном формате PickUp-Point (пункт выдачи) – в них только терминал для заказа товаров и стойка для выдачи заказов. Пришел – заказал – забрал. Без очередей и лишней траты времени. Выдача товаров работает ежедневно с 16 до 20 часов (а в некоторых магазинах - и до 21 часа). Посмотреть, где находится ближайший к вам пункт выдачи, вы можете на нашем сайте прямо сейчас. И, разумеется, получение заказов в пунктах выдачи Magazine абсолютно бесплатно!</span></p>
     </div>
   </h4>
 
  <p><strong><font size="4">Способ 2. Курьерская доставка</font></strong></p>
 
  <h4> 
    <div> 
      <div style="text-align: justify;"><img src="http://content.enter.ru/wp-content/uploads/2013/08/f1.jpg" align="right" width="300" height="131"  /></div>
     
      <p style="text-align: justify;"><font size="2" style="font-weight: normal;">Собственная компания доставки Magazine F1 гарантирует:</font></p>
     
      <p> </p>
     
      <ul lh15="" pb10"=""> 
        <li style="text-align: justify;"><font size="2" style="font-weight: normal;">Своевременность получения заказа</font></li>
       
        <li style="text-align: justify;"><font size="2" style="font-weight: normal;">Соответствие выбранного товара</font></li>
       
        <li style="text-align: justify;"><font size="2" style="font-weight: normal;">Сохранность товара</font></li>
       </ul>
     
      <p></p>
     
      <p style="text-align: justify;"><font size="2" style="font-weight: normal;">Мы доставляем вам товары даже в снегопад и непогоду! Но если оператор перенесет доставку, пожалуйста, отнеситесь к этому с пониманием. Быстрее всего вы сможете забрать свою покупку самостоятельно в ближайшем магазине &laquo;</font><span style="font-size: small; font-weight: normal;">Magazine</span><font size="2" style="font-weight: normal;">&raquo;.</font></p>
     
      <p style="text-align: justify;"><font size="2" style="font-weight: normal;">При покупке любого товара из ассортимента ООО «</font><span style="font-size: small; font-weight: normal;">Magazine</span><font size="2" style="font-weight: normal;">» на сумму свыше 10 000 рублей доставка в пределах черты города присутствия магазинов ООО «</font><span style="font-size: small; font-weight: normal;">Magazine</span><font size="2" style="font-weight: normal;">» осуществляется БЕСПЛАТНО!</font></p>
     
      <p style="text-align: justify;"><font size="2" style="font-weight: normal;">В Москве эти условия бесплатной доставки действуют в пределах 20 км от МКАД, в областных центрах – в пределах 20 км от черты города, а также в других городах присутствия магазинов «</font><span style="font-size: small; font-weight: normal;">Magazine</span><font size="2" style="font-weight: normal;">» и близлежащих населенных пунктах (в пределах 10 км от города присутствия магазина «</font><span style="font-size: small; font-weight: normal;">Magazine</span><font size="2" style="font-weight: normal;">» в Московской области и в 5 км &mdash; в других областях.</font></p>
     
      <p style="text-align: justify;"><font size="2" style="font-weight: normal;">Чтобы узнать полные условия курьерской доставки товаров, выберите нужный город</font>.</p>
     
      <p> </p>
     
      <div style="text-align: justify;"><select name="region_list" id="region_list"> <option value="0" selected="">Москва и Московская область</option> <option value="1">Санкт-Петербург</option> <option value="2">Абакан</option> <option value="3">Архангельск</option> <option value="4">Астрахань</option> <option value="5">Барнаул</option> <option value="6">Белгород и Белгородская область</option> <option value="7">Брянск и Брянская область</option> <option value="8">Владимир и Владимирская область</option> <option value="9">Воронеж и Воронежская область</option> <option value="10">Екатеринбург</option> <option value="11">Иваново и Ивановская область</option> <option value="12">Краснодар и Краснодарский край</option> <option value="13">Курск и Курская область</option> <option value="14">Липецк и Липецкая область</option> <option value="15">Нижний Новгород и Нижегородская область</option> <option value="16">Новосибирск</option> <option value="17">Орел и Орловская область</option> <option value="18">Ростов-на-Дону и Ростовская область</option> <option value="19">Рязань и Рязанская область</option> <option value="20">Самара</option> <option value="21">Смоленск и Смоленская область</option> <option value="22">Тамбов и Тамбовская область</option> <option value="23">Тверь и Тверская область</option> <option value="24">Тула и Тульская область</option> <option value="25">Уфа</option> <option value="26">Челябинск</option> <option value="27">Ярославль и Ярославская область</option> <option value="28">Альметьевск</option> <option value="29">Благовещенск</option> <option value="30">Братск</option> <option value="31">Великий Новгород</option> <option value="32">Владивосток</option> <option value="33">Волгоград</option> <option value="34">Вологда</option> <option value="35">Другой город</option> <option value="36">Ижевск</option> <option value="37">Иркутск</option> <option value="38">Йошкар-Ола</option> <option value="39">Казань</option> <option value="40">Каменск-Уральский</option> <option value="41">Кемерово</option> <option value="42">Киров</option> <option value="43">Комсомольск-на-Амуре</option> <option value="44">Красноярск</option> <option value="45">Курган</option> <option value="46">Магадан</option> <option value="47">Магнитогорск</option> <option value="48">Мурманск</option> <option value="49">Набережные Челны</option> <option value="50">Нижневартовск</option> <option value="51">Нижнекамск</option> <option value="52">Нижний Тагил</option> <option value="53">Новокузнецк</option> <option value="54">Новый Уренгой</option> <option value="55">Норильск</option> <option value="56">Омск</option> <option value="57">Оренбург</option> <option value="58">Пенза</option> <option value="59">Пермь</option> <option value="60">Петрозаводск</option> <option value="61">Петропавловск-Камчатский</option> <option value="62">Псков</option> <option value="63">Саранск</option> <option value="64">Саратов</option> <option value="65">Сочи</option> <option value="66">Ставрополь</option> <option value="67">Сургут</option> <option value="68">Сыктывкар</option> <option value="69">Тольятти</option> <option value="70">Томск</option> <option value="71">Тюмень</option> <option value="72">Улан-Удэ</option> <option value="73">Ульяновск</option> <option value="74">Хабаровск</option> <option value="75">Ханты-Мансийск</option> <option value="76">Чебоксары</option> <option value="77">Череповец</option> <option value="78">Чита</option> <option value="79">Южно-Сахалинск</option> <option value="80">Якутск</option> </select> </div>
     
      <div style="text-align: justify;"> 
        <br />
       </div>
     
      <p></p>
     </div>
   </h4>
 
  <p><font size="4"><b>Стандартная доставка</b></font></p>
 
  <h4 style="text-align: justify;"> 
    <p><font size="3">Москва и Московская область </font></p>
   
    <div> 
      <div id="region_delivery_option"> 
        <p style="text-align: justify;"><span style="font-weight: normal;">Мы привезем заказ в удобное для вас место уже на следующий день (при заказе до 20.00 текущего дня) – курьерская служба работает с 9 утра до 9 вечера и доставляет заказы точно в срок.</span></p>
       </div>
     </div>
   
    <p><font size="3"> Курьерская доставка по Москве </font></p>
   
    <div> 
      <div> 
        <p style="text-align: justify;"><font size="2" style="font-weight: normal;">Стоимость доставки по Москве - 290 рублей для любых товаров.</font></p>
       
        <p style="text-align: justify;"><font size="2" style="font-weight: normal;">Для удобства вы можете выбрать подходящий интервал доставки по Москве:</font></p>
       
        <p> </p>
       
        <ul lh15="" pb10"=""> 
          <li style="text-align: justify;"><font size="2" style="font-weight: normal;">с 9.00 до 14.00,</font></li>
         
          <li style="text-align: justify;"><font size="2" style="font-weight: normal;">с 14.00 до 18.00,</font></li>
         
          <li style="text-align: justify;"><font size="2" style="font-weight: normal;">с 18.00 до 21.00,</font></li>
         
          <li style="text-align: justify;"><font size="2" style="font-weight: normal;">с 9.00 до 18.00.</font></li>
         </ul>
       
        <p></p>
       </div>
     </div>
   
    <p><font size="3"> Курьерская доставка в пределах 20 км от МКАД, а также 20-40 км от МКАД </font></p>
   
    <div> 
      <div> 
        <p style="text-align: justify;"><span style="font-weight: normal;">Стоимость доставки в пределах 20 км от МКАД - 290 рублей для любых товаров.</span></p>
       
        <p style="text-align: justify;"><span style="font-weight: normal;">Для удобства вы можете выбрать подходящий интервал доставки по Москве:</span></p>
       
        <ul lh15="" pb10"=""> 
          <li style="text-align: justify;"><span style="font-weight: normal;">с 10.00 до 15.00,</span></li>
         
          <li style="text-align: justify;"><span style="font-weight: normal;">с 15.00 до 21.00,</span></li>
         
          <li style="text-align: justify;"><span style="font-weight: normal;">с 10.00 до 21.00.</span></li>
         </ul>
       
        <p style="text-align: justify;"><span style="font-weight: normal;">Стоимость доставки в пределах 20-40 км от МКАД:</span></p>
       
        <ul lh15="" pb10"=""> 
          <li style="text-align: justify;"><span style="font-weight: normal;">950 рублей для малогабаритных товаров (весом не более 25 кг и объемом не более 100 л).</span></li>
         
          <li style="text-align: justify;"><span style="font-weight: normal;">990 рублей для крупногабаритных товаров (весом более 25 кг или объемом более 100 л).</span></li>
         </ul>
       
        <p style="text-align: justify;"><span style="font-weight: normal;">Интервал доставки заказов в пределах 20-40 км от МКАД: с 10.00 до 21.00. Доставка осуществляется только в районные центры и города районного значения.</span></p>
       </div>
     </div>
   
    <p><font size="3"> Доставка до населенных пунктов, расположенных в Московской области более чем в 40 км от МКАД </font></p>
   
    <div> 
      <div> 
        <p style="text-align: justify;"><span style="font-weight: normal;">Стоимость доставки до населенного пункта, расположенного в пределах Московской области более чем в 40 км от Москвы:</span></p>
       
        <ul lh15="" pb10"=""> 
          <li style="text-align: justify;"><span style="font-weight: normal;">1250 рублей для малогабаритных товаров (весом не более 25 кг и объемом не более 100 л).</span></li>
         
          <li style="text-align: justify;"><span style="font-weight: normal;">2400 рублей для крупногабаритных товаров (весом более 25 кг или объемом более 100 л).</span></li>
         </ul>
       
        <p style="text-align: justify;"><span style="font-weight: normal;">Интервал доставки до населенного пункта, расположенного в 40 км от Москвы и далее: с 10.00 до 21.00. Доставка осуществляется только в районные центры и города районного значения. Кстати, наши курьеры принимают оплату не только наличными, но и банковскими картами. А еще любой, даже очень тяжелый товар, мы доставляем до двери, и неважно, что вы живете на 30 этаже :)</span></p>
       </div>
     </div>
   
    <p><font size="2" style="font-weight: normal;"> Города Московской области, которые находятся на расстоянии более 20 км от Москвы, но в которых есть магазин Magazine (районы льготной доставки) Курьерская доставка по Сергиевому Посаду и населенным пунктам, расположенным на расстоянии до 10 км от черты города </font></p>
   
    <div> 
      <div> 
        <p style="text-align: justify;"><font size="2" style="font-weight: normal;">Стоимость доставки по Сергиевому Посаду - 290 рублей для любых товаров</font><span style="font-weight: normal;">.</span></p>
       
        <p style="text-align: justify;"><span style="font-weight: normal;">Интервал доставки по Сергиевому Посаду: с 10 до 20 часов.</span></p>
       </div>
     </div>
   
    <p><font size="3"> Курьерская доставка по Ногинску и населенным пунктам, расположенным на расстоянии до 10 км от черты города </font></p>
   
    <div> 
      <div> 
        <p style="text-align: justify;"><span style="font-weight: normal;">Стоимость доставки по Ногинску - 290 рублей для любых товаров.</span></p>
       
        <p style="text-align: justify;"><span style="font-weight: normal;">Для удобства вы можете выбрать подходящий интервал доставки по Ногинску:</span></p>
       
        <ul lh15="" pb10"=""> 
          <li style="text-align: justify;"><span style="font-weight: normal;">с 9.00 до 21.00,</span></li>
         
          <li style="text-align: justify;"><span style="font-weight: normal;">с 9.00 до 15.00,</span></li>
         
          <li style="text-align: justify;"><span style="font-weight: normal;">с 15.00 до 21.00.</span></li>
         </ul>
       </div>
     </div>
   
    <p><font size="3"> Курьерская доставка по Серпухову и населенным пунктам, расположенным на расстоянии до 10 км от черты города </font></p>
   
    <div> 
      <div> 
        <p style="text-align: justify;"><span style="font-weight: normal;">Стоимость доставки по Серпухову - 290 рублей для любых товаров.</span></p>
       
        <p style="text-align: justify;"><span style="font-weight: normal;">Интервал доставки по Серпухову: с 10 до 20 часов.</span></p>
       </div>
     </div>
   
    <p><font size="3"> Курьерская доставка по Чехову и населенным пунктам, расположенным на расстоянии до 10 км от черты города </font></p>
   
    <div> 
      <div> 
        <p style="text-align: justify;"><span style="font-weight: normal;">Стоимость доставки по Чехову - 290 рублей для любых товаров.</span></p>
       
        <p style="text-align: justify;"><span style="font-weight: normal;">Интервал доставки по Чехову: с 10 до 20 часов.</span></p>
       </div>
     </div>
   
    <p><font size="3"> Курьерская доставка по Егорьевску и населенным пунктам, расположенным на расстоянии до 10 км от черты города </font></p>
   
    <div> 
      <div> 
        <p style="text-align: justify;"><span style="font-weight: normal;">Стоимость доставки по Егорьевску - 290 рублей для любых товаров.</span></p>
       
        <p style="text-align: justify;"><span style="font-weight: normal;">Интервал доставки по Егорьевску: с 10 до 20 часов.</span></p>
       </div>
     </div>
   
    <p><font size="3"> Курьерская доставка по Коломне и населенным пунктам, расположенным на расстоянии до 10 км от черты города </font></p>
   
    <div> 
      <div> 
        <p style="text-align: justify;"><span style="font-weight: normal;">Стоимость доставки по Коломне - 290 рублей для любых товаров.</span></p>
       
        <p style="text-align: justify;"><span style="font-weight: normal;">Интервал доставки по Коломне: с 10 до 20 часов.</span></p>
       </div>
     </div>
   </h4>
 
  <h3 style="text-align: justify;">Экспресс-доставка</h3>
 
  <h4 style="text-align: justify;"> 
    <p>Москва </p>
   
    <div> 
      <div> 
        <p style="text-align: justify;"><span style="font-weight: normal;">Экспресс-доставку мы пока осуществляем только в Москве и в пределах 20 км от МКАД. Если заказ в Magazine вы сделали до 13.15, получить покупку можно в этот же день! Срочные заказы мы доставляем с 16.00 до 20.00 – вполне можно успеть прибыть на вечернее торжество с подарком! Экспресс-доставка пока возможна только для малогабаритных товаров (весом не более 25 кг и объемом не более 100 л), поэтому советуем в подарок выбирать что-нибудь компактное ;). Стоимость экспресс-доставки по Москве и в пределах 20 км от МКАД - 790 рублей.</span></p>
       </div>
     </div>
   </h4>
 
  <h3 style="text-align: justify;"><font size="3">Доставка ювелирных украшений</font></h3>
 
  <h4> 
    <div> 
      <div> 
        <p style="text-align: justify;"><span style="font-weight: normal;">Дорогие ювелирные украшения мы доставляем только по будням с 9 часов утра до 6 часов вечера – это связано с безопасностью. Поэтому на кольца, серьги, подвески и другие драгоценности заказы с доставкой на следующий рабочий день принимаются только до 16.00 текущего дня. Мы предлагаем несколько интервалов доставки украшений:</span></p>
       
        <ul lh15="" pb10"=""> 
          <li style="text-align: justify;"><span style="font-weight: normal;">с 9.00 до 14.00,</span></li>
         
          <li style="text-align: justify;"><span style="font-weight: normal;">с 14.00 до 18.00.</span></li>
         
          <li style="text-align: justify;"><span style="font-weight: normal;">с 9.00 до 18.00.</span></li>
         </ul>
       
        <p style="text-align: justify;"><span style="font-weight: normal;">Стоимость доставки украшений по Москве и в пределах 20 км от МКАД такая же, как и обычных малогабаритных товаров - 290 рублей. Доставка ювелирных украшений за МКАД осуществляется также только по будним дням в интервале с 9.00 до 18.00. Стоимость доставки украшений в пределах 20 км от МКАД – 290 рублей, в пределах 40 км от МКАД – 950 рублей, а доставка в населенные пункты, расположенные в пределах МО более чем в 40 км от МКАД, стоит 1250 рублей.</span></p>
       </div>
     </div>
   </h4>
 
  <h3 style="text-align: justify;"><font size="3">Доставка мебели</font></h3> 
  
    <div> 
      <div> 
        <p style="text-align: justify;"><span style="font-weight: normal;">Большинство шкафов, диванов и тумбочек мы доставляем всего в течение 7 дней с момента заказа, максимальный же срок доставки мебели - 15 дней (что, согласитесь, тоже немного). Точный срок исполнения заказа вы можете уточнить у специалистов нашего Контакт z. Они с радостью вам все расскажут!</span></p>
       </div>
     </div>
  
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>