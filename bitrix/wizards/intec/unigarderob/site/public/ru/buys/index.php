<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Покупки");
?><h4></h4>
<h4 style="text-align: justify;">Оформление заказа</h4>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
 <b style="color: #333333; font-family: Tahoma, Verdana, Helvetica, sans-serif; font-size: 13px;">УВАЖАЕМЫЕ ПОКУПАТЕЛИ!</b>
</p>
<p style="text-align: justify;">
	<b style="color: #333333; font-family: Tahoma, Verdana, Helvetica, sans-serif; font-size: 13px;"><br>
	</b>
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
 <b>Для совершения покупки в интернет-магазине</b> вам необходимо:
</p>
<p style="text-align: justify;">
 <br>
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 1. Зарегистрироваться в интернет-магазине. Если вы не хотитите регистрироваться, то данный пункт вы можете пропустить и переходить к п.2.
</p>
<p style="text-align: justify;">
 <br>
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 2. Поместить интересующий вас товар в «корзину» и оформить заказ.
</p>
<p style="text-align: justify;">
 <br>
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 3. Выбрать тип доставки:
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
</p>
<ul style="text-align: justify;">
	<li>
	<p>
		 самовывоз из магазина;
	</p>
 </li>
	<li>
	<p>
		 доставка по г. Челябинск (стоимость доставки см. ниже);
	</p>
 </li>
	<li>
	<p>
		 доставка транспортной компанией в другие города.
	</p>
 </li>
</ul>
<p>
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
 <br>
</p>
<p style="text-align: justify;">
	 4. Выбрать вид оплаты:
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
</p>
<ul>
	<li>
	<p style="text-align: justify;">
		 наличные (оплата в магазине при получении товара или оплата курьеру по г. Челябинск);&nbsp;
	</p>
 </li>
	<li>
	<p style="text-align: justify;">
		 оплата банковской картой (принимаются карты Visa, MasterCard);
	</p>
 </li>
	<li>счет для оплаты (оператор высылает Вам на электронный адрес счет на оплату с доставкой, который Вы можете оплатить на почте, в Сбербанке и т.д.).<br>
 </li>
</ul>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
 <br>
</p>
<p style="text-align: justify;">
	 5. Ваш заказ оформлен. Ждите ответа оператора по электронной почте.
</p>
<p style="text-align: justify;">
 <br>
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 При оплате товара банковской картой Вы автоматически перенаправляетесь на сайт системы обработки платежей. Далее нажимаете кнопку "Перейти" и заполняете приводимую ниже форму, после чего нажимаете "Оплатить". Предварительно, уточните наличие товара на складе у опретора. При получении товара методом самовывоза - необходимо личное присутствие держателя карты. При себе иметь документ, удостоверяющий личность. Если Вы заказываете товар с доставкой, то сумму доставки надо уточнить у оператора.
</p>
<p style="text-align: justify;">
 <br>
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 При выборе платежной системы "Счет на оплату" с Вами связывается оператор и высылает на указанный электронный адрес счет на оплату с учетом доставки товара до вашего города. Товар резервируется за Вами сроком до 3-х дней с момента выставления счета. После поступления оплаты на наш р/с, Вам отсылается товар. Во избежание недоразумений подтверждайте свою оплату: либо по телефону, либо по электронке.
</p>
<p style="text-align: justify;">
</p><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>