<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>
<?if (!CModule::IncludeModule('intec.unigarderob')) return;?>
<?
	UniGarderob::InitProtection();

	$wizard =& $this->GetWizard();
	$themeID = $wizard->GetVar('themeID');

	if (!empty($themeID)) {
		UniGarderob::setOption(WIZARD_SITE_ID, 'COLOR_THEME', $themeID, true);
	} else {
		UniGarderob::setOption(WIZARD_SITE_ID, 'COLOR_THEME', 'BLUE', true);
	}
?>