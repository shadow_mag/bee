<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<?

	COption::SetOptionString("fileman", "propstypes", serialize(array("description"=>GetMessage("MAIN_OPT_DESCRIPTION"), "keywords"=>GetMessage("MAIN_OPT_KEYWORDS"), "title"=>GetMessage("MAIN_OPT_TITLE"), "keywords_inner"=>GetMessage("MAIN_OPT_KEYWORDS_INNER"))), false, $siteID);
	COption::SetOptionInt("search", "suggest_save_days", 250);
	COption::SetOptionString("search", "use_tf_cache", "Y");
	COption::SetOptionString("search", "use_word_distance", "Y");
	COption::SetOptionString("search", "use_social_rating", "Y");
	COption::SetOptionString("iblock", "use_htmledit", "Y");

	COption::SetOptionString("main", "captcha_registration", "N");

	if (COption::GetOptionString("socialservices", "auth_services") == "")
	{
		$bRu = (LANGUAGE_ID == 'ru');
		
		$arServices = array(
			"VKontakte" => "Y",  
			"MyMailRu" => "N",
			"Twitter" => "N",
			"Facebook" => "N",
			"Livejournal" => "Y",
			"YandexOpenID" => ($bRu? "Y":"N"),
			"Rambler" => ($bRu? "Y":"N"),
			"MailRuOpenID" => ($bRu? "Y":"N"),
			"Liveinternet" => ($bRu? "Y":"N"),
			"Blogger" => "Y",
			"OpenID" => "Y",
			"LiveID" => "N",
		);
		
		COption::SetOptionString("socialservices", "auth_services", serialize($arServices));
	}

	if (CModule::IncludeModule('intec.unigarderob') && WIZARD_INSTALL_DEMO_DATA)
	{
		UniGarderob::InitProtection();
		UniGarderob::setOption(WIZARD_SITE_ID, 'SHOW_PANEL_SETTING', "Y", true);
		UniGarderob::setOption(WIZARD_SITE_ID, 'HIDE_MAIN_BANNER', "Y", true);
		UniGarderob::setOption(WIZARD_SITE_ID, 'TYPE_PHONE', "header", true);
		UniGarderob::setOption(WIZARD_SITE_ID, 'TYPE_BASKET', "header", true);
		UniGarderob::setOption(WIZARD_SITE_ID, 'TYPE_TOP_MENU', "solid", true);
		UniGarderob::setOption(WIZARD_SITE_ID, 'MENU_WIDTH_SIZE', "Y", true);
		UniGarderob::setOption(WIZARD_SITE_ID, 'POSITION_TOP_MENU', "top", true);
		UniGarderob::setOption(WIZARD_SITE_ID, 'HEADER_WIDTH_SIZE', "Y", true);
		UniGarderob::setOption(WIZARD_SITE_ID, 'SHOW_BUTTON_TOP', "Y", true);
		UniGarderob::setOption(WIZARD_SITE_ID, 'ADAPTIV', "Y", true);
		UniGarderob::setOption(WIZARD_SITE_ID, 'CATALOG_VIEW', "tile", true);
		UniGarderob::setOption(WIZARD_SITE_ID, 'CATALOG_SECTION_DEFAULT_VIEW', "tile", true);
		UniGarderob::setOption(WIZARD_SITE_ID, 'CATALOG_PRODUCT_VIEW', "WITH_TABS", true);
		UniGarderob::setOption(WIZARD_SITE_ID, 'CATALOG_PRODUCT_MENU', "N", true);
		UniGarderob::setOption(WIZARD_SITE_ID, 'CATALOG_SKU_VIEW', "DYNAMIC", true);
		UniGarderob::setOption(WIZARD_SITE_ID, 'CATALOG_PRODUCT_IMAGE_VIEW', "WITH_FANCY", true);
		UniGarderob::setOption(WIZARD_SITE_ID, 'SERVICES_VIEW', "WITH_TABS", true);
		UniGarderob::setOption(WIZARD_SITE_ID, 'SERVICES_CATALOG_DEFAULT_VIEW', "TILE", true);
		UniGarderob::setOption(WIZARD_SITE_ID, 'SERVICES_SECTION_DEFAULT_VIEW', "tile", true);
	}
	//почтовый шаблон для Купить в 1 клик
	$arr["ACTIVE"] = "Y";
	$arr["EVENT_NAME"] = "ONECLICKBUY_ORDER";
	$arr["LID"] = WIZARD_SITE_ID;
	$arr["EMAIL_FROM"] = "#DEFAULT_EMAIL_FROM#";
	$arr["EMAIL_TO"] = "#DEFAULT_EMAIL_FROM#";
	$arr["SUBJECT"] = "#SITE_NAME#: ".GetMessage('NEW_ORDER')." N#ORDER_ID#";
	$arr["BODY_TYPE"] = "html";
	$arr["MESSAGE"] = 
		GetMessage('INFO')." #SITE_NAME#<br>
		------------------------------------------<br>
		<br>
		".GetMessage('TMP_ORDER_ID')." #ORDER_ID#.<br>
		<br>
		".GetMessage('USER_NAME').": #USER_NAME#<br>
		".GetMessage('USER_PHONE').": #USER_PHONE#<br>
		".GetMessage('ORDER_DESCRIPTION').": #ORDER_DESCRIPTION#<br>
		".GetMessage('PRICE').": #PRICE# ".GetMessage('RUB')."<br>
		<br>
		".GetMessage('ORDER_LIST').":<br>
		#ORDER_LIST#<br>
	";

	$emess = new CEventMessage;
	$emess->Add($arr);
?>