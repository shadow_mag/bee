var Popups = {};

Popups.OneClickBuy = function ($oParameters){		
	var $oPopup = BX.PopupWindowManager.create("Poups.OneClickBuy" + $oParameters.ID, null, {
		autoHide: true,			
		offsetLeft: 0,
		offsetTop: 0,
		overlay : true,
		draggable: {restrict:true},
		closeByEsc: true,
		closeIcon: { right : "20px", top : "11px"},
		content: '<div style="width:586px;height:435px; text-align: center;"><span style="position:absolute;left:50%; top:50%"><img src="<?=SITE_DIR?>images/please_wait.gif"/></span></div>',
		events: {
			onAfterPopupShow: function() {
			     BX.ajax.post(
					SITE_TEMPLATE_PATH + '/ajax/popup.buy.oneclick.php',
                    { 
						"IBLOCK_TYPE": $oParameters.IBlockType,
						"IBLOCK_ID": $oParameters.IBlockID,
						"ELEMENT_ID": $oParameters.ID,
						"NAME_PRODUCT": $oParameters.Name,
						"NEW_PRICE": $oParameters.PriceCurrent,
						"OLD_PRICE": $oParameters.PriceOld,
						"IMAGE": $oParameters.Image,
                        "CURRENCY": $oParameters.Currency,
                        "PRICE_ID": $oParameters.PriceID,
						"PRICE": $oParameters.Price
					},
                    BX.delegate(function($sResponse) {
						this.setContent($sResponse);
					}, this)
				);
			}
		}
	});
    
	$oPopup.show();	
}

Popups.OrderCall = function () {
	var $oPopup = BX.PopupWindowManager.create("Popups.OrderCall", null, {
		autoHide: true,			
		offsetLeft: 0,
		offsetTop: 0,
		overlay : true,
		draggable: {restrict:true},
		closeByEsc: true,
		closeIcon: { right : "32px", top : "23px"},
		content: '<div style="width:307px;height:290px; text-align: center;"><span style="position:absolute;left:50%; top:50%"><img src="/images/please_wait.gif"/></span></div>',
		events: {
			onAfterPopupShow: function()
			{
				BX.ajax.post(
					SITE_TEMPLATE_PATH + '/ajax/popup.order.call.php',
					{},
					BX.delegate(function($sResponse) {
						this.setContent($sResponse);
					}, this)
				);
			}
		},
		buttons: [
               new BX.PopupWindowButton({
                  className: "bx_popup_close" ,
                  events: {click: function(){
                     this.popupWindow.close();
                  }}
               })
        ]
	});
    
	$oPopup.show();
}

Popups.AskQuestion = function () {
	var $oPopup = BX.PopupWindowManager.create("Popups.AskQuestion", null, {
		autoHide: true,			
		offsetLeft: 0,
		offsetTop: 0,
		overlay : true,
		draggable: {restrict:true},
		closeByEsc: true,
		closeIcon: { right : "32px", top : "23px"},
		content: '<div style="width:307px;height:290px; text-align: center;"><span style="position:absolute;left:50%; top:50%"><img src="/images/please_wait.gif"/></span></div>',
		events: {
			onAfterPopupShow: function()
			{
				BX.ajax.post(
					SITE_TEMPLATE_PATH + '/ajax/popup.ask.question.php',
					{},
					BX.delegate(function($sResponse) {
						this.setContent($sResponse);
					}, this)
				);
			}
		},
		buttons: [
               new BX.PopupWindowButton({
                  className: "bx_popup_close" ,
                  events: {click: function(){
                     this.popupWindow.close();
                  }}
               })
        ]
	});
    
	$oPopup.show();
}

Popups.OrderService = function ($sServiceName) {
    var that = this;
    this.ServiceName = $sServiceName;
	var $oPopup = BX.PopupWindowManager.create("Popups.OrderService", null, {
		autoHide: true,			
		offsetLeft: 0,
		offsetTop: 0,
		overlay : true,
		draggable: {restrict:true},
		closeByEsc: true,
		closeIcon: { right : "20px", top : "16px"},
		content: '<div style="width:316px;height:483px; text-align: center;"><span style="position:absolute;left:50%; top:50%"><img src="/images/please_wait.gif"/></span></div>',
		events: {
			onAfterPopupShow: function()
			{
				BX.ajax.post(
					SITE_TEMPLATE_PATH + '/ajax/popup.order.service.php',
					{},
					BX.delegate(function($sResponse) {
						this.setContent($sResponse);
						$('form[name=SERVICE_' + SITE_ID + '] .controls .input .inputtext').eq(0).val(that.ServiceName);
					}, this)
				);
			}
		},
		buttons: [
               new BX.PopupWindowButton({
                  className: "bx_popup_close" ,
                  events: {click: function(){
                     this.popupWindow.close();
                  }}
               })
        ]
	});
    
	$oPopup.show();
}

Popups.Authorize = function ($oParameters) {
    if(window.innerWidth < $oParameters.Width) {
		document.location.href = $oParameters.Data.UrlProfile;
	} else {	
		var authPopup = BX.PopupWindowManager.create("Popups.Authorize", null, {
			autoHide: true,			
			offsetLeft: 0,
			offsetTop: 0,
			overlay : true,
			draggable: {restrict:true},
			closeByEsc: true,
			closeIcon: { right : "32px", top : "23px"},
			content: '<div style="width: ' + $oParameters.Width + 'px; height: ' + $oParameters.Height + 'px; text-align: center;"><span style="position:absolute;left:50%; top:50%"><img src="/images/please_wait.gif"/></span></div>',
			events: {
				onAfterPopupShow: function() {
					BX.ajax.post(
						$oParameters.Url,
						$oParameters.Data,
						BX.delegate(function($sResponse) {
							this.setContent($sResponse);
						}, this)
					);
				}
			}
		});
		authPopup.show();
	}
}

Popups.SendResume = function () {
    var $oPopup = BX.PopupWindowManager.create("Popups.SendResume", null, {
		autoHide: true,			
		offsetLeft: 0,
		offsetTop: 0,
		overlay : true,
		draggable: {restrict:true},
		closeByEsc: true,
		closeIcon: { right : "20px", top : "16px"},
		content: '<div style="width:307px;height:713px; text-align: center;"><span style="position:absolute;left:50%; top:50%"><img src="/images/please_wait.gif"/></span></div>',
		events: {
			onAfterPopupShow: function()
			{
				BX.ajax.post(
					SITE_TEMPLATE_PATH + '/ajax/popup.send.resume.php',
					{},
					BX.delegate(function($sResponse) {
						this.setContent($sResponse);
					}, this)
				);
			}
		},
		buttons: [
               new BX.PopupWindowButton({
                  className: "bx_popup_close" ,
                  events: {click: function(){
                     this.popupWindow.close();
                  }}
               })
        ]
	});
    
	$oPopup.show();
}

Popups.TableOfSizes = function () {
    var $oPopup = BX.PopupWindowManager.create("Popups.TableOfSizes", null, {
		autoHide: true,			
		offsetLeft: 0,
		offsetTop: 0,
		overlay : true,
		draggable: {restrict:true},
		closeByEsc: true,
		closeIcon: { right : "20px", top : "16px"},
		content: '<div style="width:640px;height:524px; text-align: center;"><span style="position:absolute;left:50%; top:50%"><img src="/images/please_wait.gif"/></span></div>',
		events: {
			onAfterPopupShow: function()
			{
				BX.ajax.post(
					SITE_TEMPLATE_PATH + '/ajax/popup.content.tableofsizes.php',
					{},
					BX.delegate(function($sResponse) {
						this.setContent($sResponse);
					}, this)
				);
			}
		}
	});
    
	$oPopup.show();
}

Popups.ProductQuickView = {};
Popups.ProductQuickView.Open = function ($sUrl) {
    if($(window).width() > 960) {
		$("body").append(
            '<div class="uni-popup">' + 
                '<div class="uni-popup-wrapper">' +
                    '<div class="uni-popup-background"></div>' +
                    '<div class="uni-popup-content"></div>' +
                '</div>' +
            '</div>'
        );			
        	
		BX.ajax.post(
			$sUrl + "?quick_view=Y",
			{},
			BX.delegate(function($sResponse) {
				$(".uni-popup .uni-popup-content").html($sResponse);
				$(".uni-popup").fadeIn("slow");
				$("html").addClass("uni-popup-mode");
			},
			this)
		);
	}
}
Popups.ProductQuickView.Close = function () {
    $(".uni-popup").fadeOut("slow",function(){
		$(".uni-popup").remove();
		$("html").removeClass("uni-popup-mode");	
	});
}
$("html").on("click", ".uni-popup .uni-popup-background", function () {
    Popups.ProductQuickView.Close();
});