<?
    $MESS["CATALOG_ADD"] = "В корзину";
    $MESS["CATALOG_ADDED"] = "Добавлено";
    $MESS["CATALOG_ONE_CLICK_BUY"] = "Купить в 1 клик";
    $MESS["CATALOG_DELETE_ELEMENT"] = "Удалить товар?";
    $MESS["COMPARE_TEXT_DETAIL"] = "Добавить в сравнение";
    $MESS["COMPARE_DELETE_TEXT_DETAIL"] = "Убрать из сравнения";
    $MESS["LIKE_TEXT_DETAIL"] = "Отложить товар";
    $MESS["LIKE_DELETE_TEXT_DETAIL"] = "Убрать из отложенных";
	 $MESS["QUICK_VIEW"] = "Быстрый просмотр";
?>