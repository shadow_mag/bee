<?foreach ($arItem['OFFERS'] as $arOffer):?>
    <?if ($arOffer['CAN_BUY']):?>
        <div class="button-buy SKUBuyButton SKUBuyButton<?=$arOffer['ID']?>" style="display: none;">
            <a rel="nofollow" class="uni-button solid_button button addToBasket addToBasket<?=$arOffer["ID"]?>"
        		onclick="SaleActions.addToBasket('<?=$arOffer['ID']?>', '<?=$arOffer['MIN_PRICE']['CURRENCY']?>', <?=$arOffer['CATALOG_MEASURE_RATIO']?>, SaleWrapper.updateAfterBasketAction);"
        	><?=GetMessage("CATALOG_ADD")?>
        	</a>
        	<a rel="nofollow" href="<?=$arParams["BASKET_URL"];?>" class="uni-button solid_button button removeFromBasket removeFromBasket<?=$arOffer["ID"]?>" style="display: none;">
        		<?=GetMessage("CATALOG_ADDED")?>
        	</a>
        </div>
    <?else:?>
        <div class="button-buy SKUBuyButton SKUBuyButton<?=$arOffer['ID']?>" style="display: none;">
            <a rel="nofollow" class="button solid_text"
    			href="<?=$arOffer['DETAIL_PAGE_URL']?>"
    		><?=GetMessage("CATALOG_NOT_AVAILABLE")?>
    		</a>
        </div>
    <?endif;?>
<?endforeach;?>