<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?$this->setFrameMode(true)?>
<?if (!empty($arResult['SECTIONS'])):?>
    <div class="catalog-sections-extend">
        <div class="catalog-sections-extend-wrapper">
            <?foreach ($arResult['SECTIONS'] as $arSection):?>
                <?if ($arSection['DEPTH_LEVEL'] > 1 && $arParams['ROOT_SECTIONS'] == "Y") continue;?>
                <?$this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);?>
                <?$this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);?>
                <div class="section">
                    <div class="section-wrapper" id="<?=$this->GetEditAreaId($arSection['ID']);?>">
                        <a class="image" href="<?=$arSection['SECTION_PAGE_URL']?>" style="background-image: url('<?=$arSection['PICTURE']['SRC']?>');" title="<?=htmlspecialcharsbx($arSection['NAME'])?>"></a>
                        <div class="menu">
                            <div class="menu-wrapper">
                                <a href="<?=$arSection['SECTION_PAGE_URL']?>" class="menu-caption">
                                    <?=$arSection['NAME']?>
                                </a>
                                <?$iCountSubSections = 0;?>
                                <?foreach ($arSection['SECTIONS'] as $arSubSection):?>
                                    <?$this->AddEditAction($arSubSection['ID'], $arSubSection['EDIT_LINK'], $strSectionEdit);?>
                                    <?$this->AddDeleteAction($arSubSection['ID'], $arSubSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);?>
                                    <?$iCountSubSections++;?>
                                    <a href="<?=$arSubSection['SECTION_PAGE_URL']?>" class="menu-item" id="<?=$this->GetEditAreaId($arSubSection['ID']);?>">
                                        <?=$arSubSection['NAME'];?>
                                    </a>
                                    <?if ($iCountSubSections >= 6) break;?>
                                <?endforeach;?>
                                <?unset($iCountSubSections);?>
                                <a href="<?=$arSection['SECTION_PAGE_URL']?>" class="menu-item menu-item-all">
                                    <?=GetMessage('CATALOG_SECTIONS_EXTEND_SHOW_ALL')?>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            <?endforeach;?>
            <div class="clear"></div>
        </div>
    </div>
<?endif;?>