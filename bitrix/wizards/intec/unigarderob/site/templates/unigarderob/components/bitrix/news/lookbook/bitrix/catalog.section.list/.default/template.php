<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>
<?
    $this->setFrameMode(true);
?>
<div class="lookbook-albums">
    <div class="lookbook-albums-wrapper">
        <?foreach ($arResult['SECTIONS'] as $arSection):?>
            <?
                $this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], GetMessage('N_L_CSL_DEFAULT_ACTION_EDIT'));
                $this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], GetMessage('N_L_CSL_DEFAULT_ACTION_DELETE'), GetMessage('N_L_CSL_DEFAULT_ACTION_DELETE_QUESTION'));
            ?>
            <div class="lookbook-album uni-25">
                <div class="lookbook-albums-block hover_shadow" id="<?=$this->GetEditAreaId($arSection['ID']);?>">
					<div class="lookbook-album-wrapper" >
						<div class="lookbook-album-wrapper-wrapper">
							<a class="lookbook-picture" href="<?=$arSection['SECTION_PAGE_URL']?>" style="background-image: url('<?=$arSection['PICTURE']['SRC']?>');"></a>
						</div>
					</div>
					<div class="lookbook-information">
						<div class="lookbook-information-wrapper">
							<a class="lookbook-name" href="<?=$arSection['SECTION_PAGE_URL']?>"><?=$arSection['NAME']?></a>
						</div>
					</div>
				</div>
            </div>
        <?endforeach;?>
    </div>
</div>
<div class="clear"></div>