<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?$this->setFrameMode(true);?>
<?if(!empty($arResult['SECTIONS'])):?>
	<div class="catalog-section section-select">
        <div class="catalog-section-wrapper">
            <?foreach ($arResult['SECTIONS'] as $arSection):?>
                <?if ($arSection['DEPTH_LEVEL'] != 1) continue;?>
                <div class="catalog-section-item<?=$arSection['SELECTED'] ? ' ui-state-active' : ''?>">
                    <a href="<?=$arSection['SECTION_PAGE_URL']?>" class="catalog-section-item-wrapper">
                        <?=$arSection['NAME']?>
                    </a>
                </div>
            <?endforeach;?>
        </div>
    </div>
<?endif;?>