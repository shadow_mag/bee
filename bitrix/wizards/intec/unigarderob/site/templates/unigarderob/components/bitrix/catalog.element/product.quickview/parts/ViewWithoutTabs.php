<div class="row">	
	<div id="properties" class="item_description">
		<div class="properties">
			<table class="prop_table">
				<?foreach ($arResult['DISPLAY_PROPERTIES'] as $arProperty):?>
					<tr>
						<td class="name_prop"><?=$arProperty['NAME']?></td>
						<td class="value_prop">
							<?if(is_array($arProperty['VALUE'])){?>
								<?=implode(",",$arProperty['VALUE']);?>
							<?}else{?>
								<?=$arProperty['VALUE']?>
							<?}?>
						</td>
					</tr>
				<?endforeach;?>
			</table>
		</div>
	</div>
	<div class="clear"></div>
</div>