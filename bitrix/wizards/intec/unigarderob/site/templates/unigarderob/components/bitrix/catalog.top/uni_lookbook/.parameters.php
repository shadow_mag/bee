<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
if(!CModule::IncludeModule("iblock"))
	return;

$arAscDesc = array(
	"asc" => GetMessage("IBLOCK_SORT_ASC"),
	"desc" => GetMessage("IBLOCK_SORT_DESC"),
);
    
$arTemplateParameters = array(	
	"TITLE" => array(
		"NAME" => GetMessage("UNI_POPULAR_TITLE"),
		"TYPE" => "TEXT",
		"DEFAULT" => GetMessage("UNI_POPULAR_TITLE_DEFAULT"),
	),
    "LINE_ELEMENT_COUNT" => array(
        "NAME" => GetMessage("UNI_POPULAR_LINE_ELEMENT_COUNT"),
        "TYPE" => "TEXT",
        "DEFAULT" => "4"
    ),
    "USE_SLIDER" => array(
        "NAME" => GetMessage("UNI_POPULAR_USE_SLIDER"),
        "TYPE" => "CHECKBOX",
        "DEFAULT" => "N",
        "REFRESH" => "Y"
    )
);

if ($arCurrentValues['USE_SLIDER'] == 'Y') {
    $arTemplateParameters["USE_SLIDER_ARROWS"] = array(
        "NAME" => GetMessage("UNI_POPULAR_USE_SLIDER_ARROWS"),
        "TYPE" => "CHECKBOX",
        "DEFAULT" => "N"
    );
    $arTemplateParameters["USE_SLIDER_POINTS"] = array(
        "NAME" => GetMessage("UNI_POPULAR_USE_SLIDER_POINTS"),
        "TYPE" => "CHECKBOX",
        "DEFAULT" => "N"
    );
}
?>