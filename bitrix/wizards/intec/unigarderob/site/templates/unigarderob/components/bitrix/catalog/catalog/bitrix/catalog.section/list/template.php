<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>
<?$this->setFrameMode(true);?>
<?if (!empty($arResult['ITEMS'])):?>
    <?$sItemEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT");
    	$sItemDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE");
    	$arItemDeleteParams = array("CONFIRM" => GetMessage('CATALOG_DELETE_ELEMENT'));
    ?>
    <?if ($arParams["DISPLAY_TOP_PAGER"]):?>
        <div style="padding-top: 20px; padding-bottom: 20px;">
            <?include('parts/Paginator.php');?>
        </div>
    <?endif;?>
    <?$frame = $this->createFrame()->begin();?>
    <div class="catalog-section list">
        <div class="clatalog-section-wrapper">
            <?foreach ($arResult['ITEMS'] as $arItem):?>
                <?
                    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $sItemEdit);
            		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], $sItemDelete, $arItemDeleteParams);
            		$sItemEditAreaID = $this->GetEditAreaId($arItem['ID']);
                ?>
                <div class="product" id="<?=$sItemEditAreaID?>">
                    <div class="product-wrapper hover_shadow" id="Product<?=$arItem['ID']?>">
                        <div class="image">
                            <div class="marks">
                                <?if(!empty($arItem["MIN_PRICE"]["DISCOUNT_DIFF_PERCENT"]) && $arParams['SHOW_DISCOUNT_PERCENT'] == 'Y'){?>
                                    <span class="mark action">-<?=$arItem["MIN_PRICE"]["DISCOUNT_DIFF_PERCENT"];?>%</span>
                				<?}?>
                            </div>
                            <?if (empty($arItem['OFFERS'])):?>
                                <a class="image-wrapper uni-image ProductImage" href="<?=$arItem['DETAIL_PAGE_URL']?>">
                                    <img src="<?=$arItem['PICTURE']?>" alt="<?=htmlspecialcharsbx($arItem['NAME'])?>" title="<?=htmlspecialcharsbx($arItem['NAME'])?>"/>
                                </a>
                            <?else:?>
                                <?include('parts/OffersImages.php');?>
                            <?endif;?>
                            <div class="uni-button-quickview" onclick="Popups.ProductQuickView.Open('<?=$arItem['DETAIL_PAGE_URL']?>')"><i></i>
								<?=GetMessage("QUICK_VIEW");?>
							</div>
                        </div>
                        <div class="information<?=!empty($arItem['BRAND']) ? ' with-brand' : ''?>">
                            <?if (!empty($arItem['BRAND'])):?>
                                <a class="brand-image uni-image" href="<?=$arItem['BRAND']['DETAIL_PAGE_URL']?>">
                                    <div class="uni-aligner-vertical"></div>
                                    <img src="<?=$arItem['BRAND']['PICTURE']?>" alt="<?=$arItem['BRAND']['NAME']?>" />
                                </a>
                            <?endif;?>
                            <div class="nomination">
                                <div class="nomination-wrapper">
                                    <?if (!empty($arItem['BRAND'])):?>
                                        <a class="brand" href="<?=$arItem['BRAND']['DETAIL_PAGE_URL']?>"><?=$arItem['BRAND']['NAME']?></a>
                                    <?endif;?>
                                    <a class="name" href="<?=$arItem['DETAIL_PAGE_URL']?>"><?=$arItem['NAME']?></a>
                                </div>
                            </div>
                            <?if (!empty($arItem['OFFERS'])) include('parts/OffersProperties.php');?>
                            <div class="price ProductPrice">
                                <div class="price-wrapper">
                                    <div class="current"><?=$arItem['MIN_PRICE']['PRINT_DISCOUNT_VALUE']?></div>
                                    <div class="original"<?=$arItem['MIN_PRICE']['DISCOUNT_DIFF'] == 0 ? ' style="display: none;"' : ''?>><?=$arItem['MIN_PRICE']['PRINT_VALUE']?></div>
                                </div>
                            </div>
                            <div class="order">
                                <div class="order-wrapper">
                                    <?if (empty($arItem['OFFERS'])):?>
                                        <?include('parts/Order.php');?>
                                    <?else:?>
                                        <?include('parts/OrderWithOffers.php');?>
                                    <?endif;?>
                                </div>
                            </div>
                            <?if (!empty($arItem['PREVIEW_TEXT'])):?>
                                <div class="description">
                                    <?=$arItem['PREVIEW_TEXT'];?>
                                </div>
                            <?endif;?>
                        </div>
                    </div>
                    <?if (!empty($arItem['OFFERS'])) include('parts/OffersScript.php');?>
                </div>
            <?endforeach;?>
        </div>
    </div>
    <?$frame->end();?>      
    <?if ($arParams["DISPLAY_BOTTOM_PAGER"]):?>
        <div style="padding-top: 20px;">
            <?include('parts/Paginator.php');?>
        </div>
    <?endif;?>
<?endif;?>