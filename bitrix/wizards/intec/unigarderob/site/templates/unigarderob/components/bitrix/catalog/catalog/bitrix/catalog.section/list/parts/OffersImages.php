<?foreach ($arItem['OFFERS'] as $arOffer):?>
    <a class="image-wrapper uni-image SKUProductImage SKUProductImage<?=$arOffer['ID']?>" href="<?=$arItem['DETAIL_PAGE_URL']?>" style="display: none;">
        <div class="uni-aligner-vertical"></div>
        <img src="<?=$arOffer['PICTURE']?>" />
    </a>
<?endforeach;?>
<?unset($sOfferImage, $arOfferImage);?>