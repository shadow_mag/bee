<div class="menu-header-1-item<?=!empty($arMenuItem['SELECTED']) ? ' ui-state-active' : ''?>">
    <div class="menu-header-1-item-wrapper">
        <a class="menu-header-1-item-wrapper-wrapper" href="<?=$arMenuItem['LINK']?>">
            <div class="uni-aligner-vertical"></div>
            <div class="menu-header-1-text">
                <?=$arMenuItem['TEXT']?>
            </div>
        </a>
    </div>
</div>