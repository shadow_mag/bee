<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?global $options;?>
<?include('parts/section/preload.php');?>
<?$arFlags = array(
    'SECTION_VIEW' => '',
    'SECTION_SORT_FIELD' => '',
    'SECTION_SORT_ORDER' => '',
    'SECTIONS_VIEW' => $options['CATALOG_VIEW']['ACTIVE_VALUE'],
    'INCLUDE_SUBSECTIONS' => $arParams['INCLUDE_SUBSECTIONS'] == "Y" || $arParams['INCLUDE_SUBSECTIONS'] == "A",
    'ELEMENTS_COUNT' => 0,
    'SHOW_FILTER' => false,
    'SHOW_PANEL' => false,
    'SHOW_SECTION' => false,
    'SHOW_SECTIONS' => false,
    'SHOW_FILTER' => false        
);?>
<?$arIBlockElementsFilter = array(
    "SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
    "SECTION_CODE" => !empty($arResult["VARIABLES"]["SECTION_CODE"]) ? $arResult["VARIABLES"]["SECTION_CODE"] : false,
    "INCLUDE_SUBSECTIONS" => $arFlags['INCLUDE_SUBSECTIONS'] ? 'Y' : 'N',
    "SECTION_SCOPE" => "IBLOCK"
);?>
<?if ($arParams['INCLUDE_SUBSECTIONS'] == 'A') {
    $arIBlockElementsFilter["SECTION_ACTIVE"] = "Y";
    $arIBlockElementsFilter["SECTION_GLOBAL_ACTIVE"] = "Y";
}?>
<?$arFlags['ELEMENTS_COUNT'] = intval(CIBlockElement::GetList(
	array(),
	$arIBlockElementsFilter,
	array(),
	array()
)); unset($arIBlockElementsFilter);?>
<?$arFlags['SHOW_FILTER'] = $arFlags['ELEMENTS_COUNT'] > 0;?>
<?$arFlags['SHOW_PANEL'] = $arFlags['ELEMENTS_COUNT'] > 0;?>
<?$arFlags['SHOW_SECTION'] = $arFlags['ELEMENTS_COUNT'] > 0;?>
<?$arFlags['SHOW_SECTIONS'] = !$arFlags['INCLUDE_SUBSECTIONS'];?>
<?$arFlags['SHOW_FILTER'] = $arParams["USE_FILTER"] == "Y" && $arFlags['ELEMENTS_COUNT'] > 0;?>
<?$this->setFrameMode(true)?>
<div class="left_col">
    <?include('parts/section/menu.php')?>
    <div class="clear"></div>
    <div class="uni-indents-vertical indent-25"></div>
	<?include('parts/section/filter.php')?>
</div>
<div class="right_col inner_section_list">
    <?include('parts/section/sections.php')?>
	<?include('parts/section/panel.sort.php');?>
    <?include('parts/section/section.php')?>	
	<div class="clear"></div>
</div>
<div class="clear"></div>
