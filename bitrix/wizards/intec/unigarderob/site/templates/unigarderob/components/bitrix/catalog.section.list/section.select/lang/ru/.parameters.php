<?
    $MESS['CSL_SS_SELECTED_SECTION_VARIABLE'] = "Имя переменной, которая хранит в себе выбранный раздел";
    $MESS['CSL_SS_SEF_SECTION'] = "Раздел";
    $MESS['CSL_SS_SEF_ELEMENT'] = "Элемент";
    $MESS['CSL_SS_VARIABLE_ALIASES_SECTION_ID'] = "ID раздела";
    $MESS['CSL_SS_VARIABLE_ALIASES_SECTION_CODE'] = "Код раздела";
    $MESS['CSL_SS_VARIABLE_ALIASES_ELEMENT_ID'] = "ID элемента";
    $MESS['CSL_SS_VARIABLE_ALIASES_ELEMENT_CODE'] = "Код элемента";
?>