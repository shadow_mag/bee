<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?$this->setFrameMode(true)?>
<?
    $sSort = null;
    $sOrder = 'asc';
    $sOrderInvert = 'desc';
	
    if (isset($_GET['sort']))
        if (in_array($_GET['sort'], array('name', 'date')))
            $sSort = $_GET['sort'];
          
    if (isset($_GET['order']))
        if (in_array($_GET['order'], array('asc', 'desc')))
            $sOrder = $_GET['order'];

    if ($sOrder == 'desc')
        $sOrderInvert = 'asc';	
?>
<div class="uni-panel-sort">
    <div class="uni-panel-sort-wrapper">
        <div class="part sort">
            <div class="part-wrapper">
                <div class="element">
                    <div class="uni-aligner-vertical"></div>
                    <div class="element-wrapper">
                        <a class="sort-item<?=$sSort == 'name' ? ' ui-state-active' : ''?> <?=$sOrder?>" href="<?=
                            $sSort == 'name' ?
                            $APPLICATION->GetCurPageParam('sort=name&order='.$sOrderInvert, array('sort', 'order')) :
                            $APPLICATION->GetCurPageParam('sort=name&order='.$sOrder, array('sort', 'order'))                      
                        ?>"><span class="sort-item-text"><?=GetMessage("SECTION_SORT_NAME")?></span><div class="sort-item-icon"></div></a>
                    </div>
                </div>
                <div class="element">
                    <div class="uni-aligner-vertical"></div>
                    <div class="element-wrapper">
                        <a class="sort-item<?=$sSort == 'date' ? ' ui-state-active' : ''?> <?=$sOrder?>" href="<?=
                            $sSort == 'date' ?
                            $APPLICATION->GetCurPageParam('sort=date&order='.$sOrderInvert, array('sort', 'order')) :
                            $APPLICATION->GetCurPageParam('sort=date&order='.$sOrder, array('sort', 'order'))                      
                        ?>"><span class="sort-item-text"><?=GetMessage("SECTION_SORT_DATE")?></span><div class="sort-item-icon"></div></a>
                    </div>
                </div>
                <div class="element">
                    <div class="uni-aligner-vertical"></div>
                    <div class="element-wrapper">
                        <a class="sort-item<?=$sSort == null ? ' ui-state-active' : ''?>" href="<?=$APPLICATION->GetCurPageParam('', array('sort', 'order'))?>">
                            <span class="sort-item-text"><?=GetMessage('SECTION_SORT_NONE')?></span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?
    if ($sSort == 'date')
        $sSort = 'ACTIVE_FROM';
?>
<?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"",
	Array(
		"IBLOCK_TYPE"	=>	$arParams["IBLOCK_TYPE"],
		"IBLOCK_ID"	=>	$arParams["IBLOCK_ID"],
		"SORT_BY1" => !empty($sSort) ? $sSort : $arParams["SORT_BY1"],
		"SORT_ORDER1" => !empty($sSort) ? $sOrder : $arParams["SORT_ORDER1"],
		"NEWS_COUNT"	=>	$arParams["NEWS_COUNT"],
		"SORT_BY2"	=>	$arParams["SORT_BY2"],
		"SORT_ORDER2"	=>	$arParams["SORT_ORDER2"],
		"FIELD_CODE"	=>	$arParams["LIST_FIELD_CODE"],
		"PROPERTY_CODE"	=>	$arParams["LIST_PROPERTY_CODE"],
		"DETAIL_URL"	=>	$arResult["FOLDER"].$arResult["URL_TEMPLATES"]["detail"],
		"DISPLAY_PANEL"	=>	$arParams["DISPLAY_PANEL"],
		"SET_TITLE"	=>	$arParams["SET_TITLE"],
		"SET_STATUS_404" => $arParams["SET_STATUS_404"],
		"INCLUDE_IBLOCK_INTO_CHAIN"	=>	$arParams["INCLUDE_IBLOCK_INTO_CHAIN"],
		"CACHE_TYPE"	=>	$arParams["CACHE_TYPE"],
		"CACHE_TIME"	=>	$arParams["CACHE_TIME"],
		"CACHE_FILTER"	=>	$arParams["CACHE_FILTER"],
		"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
		"DISPLAY_TOP_PAGER"	=>	$arParams["DISPLAY_TOP_PAGER"],
		"DISPLAY_BOTTOM_PAGER"	=>	$arParams["DISPLAY_BOTTOM_PAGER"],
		"PAGER_TITLE"	=>	$arParams["PAGER_TITLE"],
		"PAGER_TEMPLATE"	=>	$arParams["PAGER_TEMPLATE"],
		"PAGER_SHOW_ALWAYS"	=>	$arParams["PAGER_SHOW_ALWAYS"],
		"PAGER_DESC_NUMBERING"	=>	$arParams["PAGER_DESC_NUMBERING"],
		"PAGER_DESC_NUMBERING_CACHE_TIME"	=>	$arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
		"PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
		"DISPLAY_DATE"	=>	$arParams["DISPLAY_DATE"],
		"DISPLAY_NAME"	=>	"Y",
		"DISPLAY_PICTURE"	=>	$arParams["DISPLAY_PICTURE"],
		"DISPLAY_PREVIEW_TEXT"	=>	$arParams["DISPLAY_PREVIEW_TEXT"],
		"PREVIEW_TRUNCATE_LEN"	=>	$arParams["PREVIEW_TRUNCATE_LEN"],
		"ACTIVE_DATE_FORMAT"	=>	$arParams["LIST_ACTIVE_DATE_FORMAT"],
		"USE_PERMISSIONS"	=>	$arParams["USE_PERMISSIONS"],
		"GROUP_PERMISSIONS"	=>	$arParams["GROUP_PERMISSIONS"],
		"FILTER_NAME"	=>	$arParams["FILTER_NAME"],
		"HIDE_LINK_WHEN_NO_DETAIL"	=>	$arParams["HIDE_LINK_WHEN_NO_DETAIL"],
		"CHECK_DATES"	=>	$arParams["CHECK_DATES"],
	),
	$component
);?>
