<?$arSkuProps = array();?>
<?if (!empty($arResult['SKU_PROPS'])):?>
    <div class="offers properties">
        <div class="properties-wrapper">
            <?foreach ($arResult['SKU_PROPS'] as &$arProp):?>
            	<?
                    if (!isset($arItem['OFFERS_PROP'][$arProp['CODE']]))
            		    continue;
                        
                    $arSkuProps[] = array(
            			'ID' => $arProp['ID'],
            			'SHOW_MODE' => $arProp['SHOW_MODE'],
            			'VALUES_COUNT' => $arProp['VALUES_COUNT']
            		);
                ?>
                <?if ($arProp['SHOW_MODE'] == 'TEXT'):?>
            		<div class="property text">
            			<div class="property-name"><?=htmlspecialcharsex($arProp['NAME'])?></div>
        				<div class="values SKUProductProperties<?=$arProp['ID']?>">
                            <div class="values-wrapper">
            					<?foreach ($arProp['VALUES'] as $arOneValue):?>
            						<div class="value SKUProductPropertyPROP_<?=$arProp['ID'].$arOneValue['ID']?>" onclick="return SKUProduct<?=$arItem['ID']?>.SetOfferByProperty('<?='PROP_'.$arProp['ID']?>', '<?=$arOneValue['ID']?>');">
            							<span>
            								<?=htmlspecialcharsbx($arOneValue['NAME'])?>
            							</span>
            						</div>
            					<?endforeach;?>
                            </div>
        				</div>
            		</div>
            	<?elseif ($arProp['SHOW_MODE'] == 'PICT'):?>
            		<div class="property picture">
            			<div class="property-name"><?=htmlspecialcharsex($arProp['NAME'])?></div>
        				<div class="values">
                            <div class="values-wrapper">
            					<?foreach ($arProp['VALUES'] as $arOneValue):?>
            						<div class="value SKUProductPropertyPROP_<?=$arProp['ID'].$arOneValue['ID']?>" onclick="return SKUProduct<?=$arItem['ID']?>.SetOfferByProperty('<?='PROP_'.$arProp['ID']?>', '<?=$arOneValue['ID']?>');">
            							<?if ($arOneValue['NA']):?>
            								<span class="na">
            									-
            								</span>
            							<?else:?>
            								<div class="value-image">
            									<img src="<?=$arOneValue['PICT']['SRC']?>"/>
            								</div>
                                            <div class="sprite"></div>
            							<?endif;?>
            						</div>
            					<?endforeach;?>
                            </div>
        				</div>
            		</div>
            	<?endif;?>
            <?endforeach;?>
        </div>
    </div>
<?endif;?>