<div id="tabs" class="uni-tabs" style="position: static;">
	<ul class="tabs">
		<?if ($arFlags['SHOW_DESCRIPTION_DETAIL']):?>
			<li class="tab"><a href="#description"><?=GetMessage("PRODUCT_DESCRIPTION")?></a></li>
		<?endif;?>
		<?if ($arFlags['SHOW_PROPERTIES']):?>
			<li class="tab"><a href="#properties"><?=GetMessage("PRODUCT_PROPERTIES")?></a></li>
		<?endif;?>
		<?if ($arFlags['SHOW_EXPANDABLES']):?>
			<li class="tab"><a href="#expandables"><?=GetMessage("PRODUCT_ACCESSORIES")?></a></li>
		<?endif;?>
		<?if ($arFlags['SHOW_REVIEWS']):?>
			<li class="tab"><a href="#reviews"><?=GetMessage("PRODUCT_REVIEWS")?></a></li>
		<?endif;?>
		<?if ($arFlags['SHOW_DOCUMENTS']):?>
			<li class="tab"><a href="#documents"><?=GetMessage("PRODUCT_DOCUMENTS")?></a></li>
		<?endif;?>
		<div class="bottom-line"></div>
	</ul>
	<div class="clear"></div>
	<?if ($arFlags['SHOW_DESCRIPTION_DETAIL']): // ��������� ��������?>
		<div id="description" class="description uni-text-default">
			<?=$arResult['DETAIL_TEXT']?>
		</div>
	<?endif;?>
	<?if ($arFlags['SHOW_PROPERTIES']):?>
		<div id="properties" class="item_description">
			<div class="properties">
				<?foreach ($arResult['DISPLAY_PROPERTIES'] as $arProperty):?>
					<div class="property">
						<div class="name">
							<?=$arProperty['NAME']?>
						</div>
						<?if (!is_array($arProperty['VALUE'])) {?>
						<div class="value">
							<?=$arProperty['VALUE']?>
						</div>
						<?} else {?>
							<div class="value">
							<?=implode(', ', $arProperty['VALUE'])?>
						</div>
						<?}?>
					</div>
				<?endforeach;?>
			</div>
		</div>
	<?endif;?>
	<?if ($arFlags['SHOW_EXPANDABLES']):?>
		<div id="expandables" class="item_description">
			<?include('ProductsExpandables.php')?>
		</div>
	<?endif;?>
	<?if ($arFlags['SHOW_REVIEWS']):?>
		<div id="reviews" class="item_description">
			<?include('Reviews.php')?>
		</div>
	<?endif;?>
	<?if ($arFlags['SHOW_DOCUMENTS']):?>
		<div id="documents" class="item_description">
			<?include('Documents.php')?>
		</div>
	<?endif;?>
	<script type="text/javascript">
		$(document).ready(function(){
			$("#tabs").tabs({
				show: function(event, ui) { $(window).trigger('resize'); }
			});
		})
	</script>
</div>