<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();?>
<?
    $arTemplateParameters = array(
        "LINE_ELEMENT_COUNT" => array(
            "PARENT" => "VISUAL",
            "NAME" => GetMessage('LINE_ELEMENT_COUNT'),
            "TYPE" => "STRING",
            "DEFAULT" => "6"
        )
    )
?>