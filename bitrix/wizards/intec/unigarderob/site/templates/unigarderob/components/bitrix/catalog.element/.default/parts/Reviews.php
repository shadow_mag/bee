<?$APPLICATION->IncludeComponent(
	"intec:reviews", 
	"reviews", 
	array(
		"IBLOCK_TYPE" => "reviews",
		"IBLOCK_ID" => $arParams['REVIEWS_IBLOCK_ID'],
		"ELEMENT_ID" => $arResult['ID'],
		"DISPLAY_REVIEWS_COUNT" => $arParams['MESSAGES_PER_PAGE']
	),
	$component
);?>