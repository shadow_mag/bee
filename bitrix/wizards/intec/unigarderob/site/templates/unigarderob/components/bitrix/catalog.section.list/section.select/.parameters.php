<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
    $arTemplateParameters = array(
        "SELECTED_SECTION_VARIABLE" => array(
            "NAME" => GetMessage("CSL_SS_SELECTED_SECTION_VARIABLE"),
            "TYPE" => "STRING",
            "DEFAULT" => "CATALOG_SECTION"
        ),
        "VARIABLE_ALIASES" => array(
            "SECTION_ID" => array(
                "NAME" => GetMessage("CSL_SS_VARIABLE_ALIASES_SECTION_ID"),
                "TEMPLATE" => "#SECTION_ID#"
            ),
            "SECTION_CODE" => array(
                "NAME" => GetMessage("CSL_SS_VARIABLE_ALIASES_SECTION_CODE"),
                "TEMPLATE" => "#SECTION_CODE#"
            ),
            "ELEMENT_ID" => array(
                "NAME" => GetMessage("CSL_SS_VARIABLE_ALIASES_ELEMENT_ID"),
                "TEMPLATE" => "#ELEMENT_ID#"
            ),
            "ELEMENT_CODE" => array(
                "NAME" => GetMessage("CSL_SS_VARIABLE_ALIASES_ELEMENT_CODE"),
                "TEMPLATE" => "#ELEMENT_CODE#"
            )
        ),
        "SEF_MODE" => array(
			"section" => array(
				"NAME" => GetMessage("CSL_SS_SEF_SECTION"),
				"DEFAULT" => "#SECTION_ID#/",
				"VARIABLES" => array(
					"SECTION_ID",
					"SECTION_CODE"
				)
			),
            "element" => array(
				"NAME" => GetMessage("CSL_SS_SEF_ELEMENT"),
				"DEFAULT" => "#SECTION_ID#/#ELEMENT_ID#/",
				"VARIABLES" => array(
					"SECTION_ID",
					"SECTION_CODE",
                    "ELEMENT_ID",
					"ELEMENT_CODE"
				)
			)
		)
    );
?>