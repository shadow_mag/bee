<?$APPLICATION->IncludeComponent(
	"intec:buttons.min.updater", 
	".default", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"IBLOCK_TYPE" => "",
		"IBLOCK_ID" => "",
		"COMPARE_USE" => "Y",
		"COMPARE_NAME" => "CATALOG_COMPARE_LIST",
		"COMPARE_ADD_MASK" => ".addToCompare#ID#",
		"COMPARE_ADDED_MASK" => ".removeFromCompare#ID#",
		"COMPARE_ADD_SELECTOR" => ".addToCompare",
		"COMPARE_ADDED_SELECTOR" => ".removeFromCompare",
		"WISH_LIST_USE" => "Y",
		"WISH_LIST_ADD_MASK" => ".addToWishList#ID#",
		"WISH_LIST_ADDED_MASK" => ".removeFromWishList#ID#",
		"WISH_LIST_ADD_SELECTOR" => ".addToWishList",
		"WISH_LIST_ADDED_SELECTOR" => ".removeFromWishList",
		"BASKET_USE" => "Y",
		"BASKET_ADD_MASK" => ".addToBasket#ID#",
		"BASKET_ADDED_MASK" => ".removeFromBasket#ID#",
		"BASKET_ADD_SELECTOR" => ".addToBasket",
		"BASKET_ADDED_SELECTOR" => ".removeFromBasket"
	),
	$component,
	array(
		"HIDE_ICONS" => "N"
	)
);?>