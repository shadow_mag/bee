<?
    if(!CModule::IncludeModule("iblock"))
	return;

    $arIBlockTypes = CIBlockParameters::GetIBlockTypes(Array("all"=>" "));

    $arComponentParameters = array(
        "GROUPS" => array(),
        "PARAMETERS" => array(
            "IBLOCK_TYPE" => array(
    			"PARENT" => "BASE",
    			"NAME" => GetMessage("CP_IMS_IBLOCK_TYPE"),
    			"TYPE" => "LIST",
    			"VALUES" => $arIBlockTypes,
    			"DEFAULT" => "",
    			"ADDITIONAL_VALUES" => "Y",
    			"REFRESH" => "Y",
    		),
    		"IBLOCK_ID" => array(
    			"PARENT" => "BASE",
    			"NAME" => GetMessage("CP_IMS_IBLOCK_ID"),
    			"TYPE" => "LIST",
    			"VALUES" => $arIBlocks,
    			"DEFAULT" => '',
    			"MULTIPLE" => "N",
    			"ADDITIONAL_VALUES" => "Y",
    			"REFRESH" => "Y",
    		),
            "SECTION_ID" => array(
    			"PARENT" => "BASE",
    			"NAME" => GetMessage("CP_IMS_SECTION_ID"),
    			"TYPE" => "STRING"
    		),
            "SECTION_URL" => CIBlockParameters::GetPathTemplateParam(
    			"SECTION",
    			"SECTION_URL",
    			GetMessage("CP_IMS_SECTION_URL"),
    			"",
    			"BASE"
    		),
            "COUNT_ELEMENTS" => array(
    			"PARENT" => "BASE",
    			"NAME" => GetMessage("CP_IMS_COUNT_ELEMENTS"),
    			"TYPE" => "CHECKBOX",
                "DEFAULT" => "N",
    		),
            "CACHE_TIME" => array("DEFAULT" => 36000000)
        )
    )
?>