<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("TABSLABEL_COMPONENT_NAME"),
	"DESCRIPTION" => GetMessage("TABSLABEL_COMPONENT_DESCRIPTION"),
	"ICON" => "/images/cat_list.gif",
	"CACHE_PATH" => "Y",
	"SORT" => 30,
	"PATH" => array(
		"ID" => "intec",
        "NAME" => GetMessage("INTEC_NAME"),
		"CHILD" => array(
			"ID" => "content",
			"NAME" => GetMessage("INTEC_CONTENT_NAME"),
			"SORT" => 30,
		),
	),
);

?>