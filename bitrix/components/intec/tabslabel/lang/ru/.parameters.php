<?
    $MESS["TABSLABEL_GROUP_COMPARE"] = "Сравнение товаров";

    $MESS["IBLOCK_TYPE"] = "Тип инфоблока";
    $MESS["IBLOCK_IBLOCK"] = "Инфоблок";
    $MESS["TABSLABEL_PROPERTY_LABEL"] = "Свойство c ярлыками";
    $MESS["TABSLABEL_DISPLAYED_LABELS"] = "Отображаемые ярлычки";
    $MESS["TABSLABEL_DISPLAY_COMPARE"] = "Ипользователь сравнение";
    $MESS["TABSLABEL_COMPARE_NAME"] = "Имя списка сравнения";
    $MESS["TABSLABEL_LINE_ELEMENT_COUNT"] = "Количество отображаемых элементов, или элементов в 1 строке";
    $MESS["TABSLABEL_ELEMENT_COUNT"] = "Количество элементов";
?>