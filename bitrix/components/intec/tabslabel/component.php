<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>
<?
    $arDefaultParams = array(
    
    );
    
    $arParams = array_merge($arParams, $arDefaultParams);
    
    $arLabelPropertyEnums = array();
    
    if(!empty($arParams["PROPERTY_LABEL"]) && !empty($arParams["IBLOCK_ID"])) {	
        $arFilter = array(
            "IBLOCK_ID" => $arParams["IBLOCK_ID"],
            "PROPERTY_ID" => $arParams["PROPERTY_LABEL"]
        );
        
        if (!empty($arParams['DISPLAYED_LABELS']) && is_array($arParams['DISPLAYED_LABELS']))
            $arFilter['ID'] = $arParams['DISPLAYED_LABELS'];
        
    	$rsLabelPropertyEnums = CIBlockPropertyEnum::GetList(array(
                "DEF" => "DESC",
                "SORT" => "ASC"
            ), 
            $arFilter
        );
        
        while ($arLabelPropertyEnum = $rsLabelPropertyEnums->GetNext())
            $arLabelPropertyEnums[$arLabelPropertyEnum['ID']] = $arLabelPropertyEnum;
    }
    $arResult["LABELS"] = $arLabelPropertyEnums;
    $this->IncludeComponentTemplate();
?>