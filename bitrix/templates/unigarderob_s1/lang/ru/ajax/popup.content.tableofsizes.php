<?
    $MESS['GTZ_TITLE'] = "Таблица размеров";
    $MESS['GTZ_HEADER_SIZE_RU'] = "Российский размер";
    $MESS['GTZ_HEADER_SISE_INTERNATIONAL'] = "Международный размер";
    $MESS['GTZ_HEADER_SISE_EU'] = "Европейский размер";
    $MESS['GTZ_HEADER_BUST'] = "Обхват груди";
    $MESS['GTZ_HEADER_WAIST'] = "Обхват талии";
    $MESS['GTZ_HEADER_GIRTH'] = "Обхват бедер";
?>