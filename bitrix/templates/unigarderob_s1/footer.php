<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
					<?if(!$isFrontPage){?>
						<?if($flag_lm){?>
							</div><!--right_col-->
						<?}?>
					<?}?>
					<div class="clear"></div>
				</div> <!-- bx_content_section -->
			</div> <!-- worakarea_wrap_container workarea-->
		</div> <!-- workarea_wrap -->
		<div class="clear"></div>
		<div class="bg_footer">
			<div class="footer">
				<div class="cookie-block">
					<svg width="27" height="27" viewBox="0 0 27 27" fill="none" xmlns="http://www.w3.org/2000/svg">
						<circle cx="13.5" cy="13.5" r="13.5" fill="white"/>
						<path d="M14.672 10.136C14.32 10.136 14.0213 10.0133 13.776 9.768C13.5307 9.52267 13.408 9.224 13.408 8.872C13.408 8.52 13.5307 8.22133 13.776 7.976C14.0213 7.72 14.32 7.592 14.672 7.592C15.024 7.592 15.3227 7.72 15.568 7.976C15.824 8.22133 15.952 8.52 15.952 8.872C15.952 9.224 15.824 9.52267 15.568 9.768C15.3227 10.0133 15.024 10.136 14.672 10.136ZM13.584 19.096C13.072 19.096 12.656 18.936 12.336 18.616C12.0267 18.296 11.872 17.816 11.872 17.176C11.872 16.9093 11.9147 16.5627 12 16.136L13.088 11H15.392L14.24 16.44C14.1973 16.6 14.176 16.7707 14.176 16.952C14.176 17.1653 14.224 17.32 14.32 17.416C14.4267 17.5013 14.5973 17.544 14.832 17.544C15.024 17.544 15.1947 17.512 15.344 17.448C15.3013 17.9813 15.1093 18.392 14.768 18.68C14.4373 18.9573 14.0427 19.096 13.584 19.096Z" fill="#CEA073"/>
					</svg>


					Сайт использует файлы cookie. Если вы не против, просто продолжайте им пользоваться
					<div class="cookie-close">
						<svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
							<path d="M1 1L14.5 14.5" stroke="white" stroke-width="2"/>
							<path d="M1 14.5L14.5 1" stroke="white" stroke-width="2"/>
						</svg>
					</div>
				</div>
				<div class="footer-top">
					<div class="contacts">
						<a href="/"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/logo_footer.php"), false);?></a>
					</div>
					<div class="footer-top__menu">
						<?$APPLICATION->IncludeComponent("bitrix:menu", "bottom_menu", array(
							"ROOT_MENU_TYPE" => "bottom",
							"MENU_CACHE_TYPE" => "A",
							"MENU_CACHE_TIME" => "600000",
							"MENU_CACHE_USE_GROUPS" => "N",
							"MENU_CACHE_GET_VARS" => array(
							),
							"MAX_LEVEL" => "2",
							"CHILD_MENU_TYPE" => "left",
							"USE_EXT" => "N",
							"DELAY" => "N",
							"ALLOW_MULTI_SELECT" => "N"
							), false
						);?>
					</div>
					<div class="phone">
						<?$APPLICATION->IncludeComponent("bitrix:main.include","",Array(
								"AREA_FILE_SHOW" => "file",
								"PATH" => SITE_DIR."include/company_phone.php",
							)
						);?>
						<div class="call_button">
							<span class="open_call" onclick="Popups.OrderCallQuestion()"><?=GetMessage("CALL_TEXT")?></span>
						</div>
					</div>



					<div class="social_buttons">
						<?$facebookLink = $APPLICATION->GetFileContent($_SERVER["DOCUMENT_ROOT"].SITE_DIR."include/socnet_facebook.php");
						$twitterLink = $APPLICATION->GetFileContent($_SERVER["DOCUMENT_ROOT"].SITE_DIR."include/socnet_twitter.php");
						$vkLink = $APPLICATION->GetFileContent($_SERVER["DOCUMENT_ROOT"].SITE_DIR."include/socnet_vk.php");
						$inLink  = $APPLICATION->GetFileContent($_SERVER["DOCUMENT_ROOT"].SITE_DIR."include/socnet_in.php");?>
						<ul>
							<?if ($inLink):?>
								<li class="in"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/socnet_in.php"), false);?></li>
							<?endif?>
							<?if ($facebookLink):?>
								<li class="fb"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/socnet_facebook.php"), false);?></li>
							<?endif?>
							<?if ($twitterLink):?>
								<li class="tw"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/socnet_twitter.php"), false);?></li>
							<?endif?>
							<?if (LANGUAGE_ID=="ru" && $vkLink):?>
								<li class="vk"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/socnet_vk.php"), false);?></li>
							<?endif?>
						</ul>
					</div>
				</div>
				<div class="footer-bottom">
					<div class="footer-txt">
						Цены, вкл НДС, но указаны без учета стоимости доставки <a href="/buys/delivery">Стоимость доставки</a> <br>
						Сервисные условия | Защита данных | Юридические данные <br>
						Все эксклюзивные права на сайт принадлежат ООО "ГМГ" и защищены Законом РФ &copy;
					</div>
					<div class="logo-block">
						<?$APPLICATION->IncludeComponent("bitrix:main.include","",Array(
								"AREA_FILE_SHOW" => "file",
								"PATH" => SITE_DIR."include/company_logo.php",
							)
						);?>
                    </div>
				</div>


            <div class="clear"></div>
				<div class="uni-indents-vertical indent-25"></div>
				<div id="bx-composite-banner"></div>
			</div>
			<div class="copyright">
				<div class="copy_wrap clearfix">
					<div class="copy">
						<?$APPLICATION->IncludeComponent("bitrix:main.include","",Array(
								"AREA_FILE_SHOW" => "file",
								"PATH" => SITE_DIR."include/copy.php",
							)
						);?>
					</div>
					<div class="address">
						<?$APPLICATION->IncludeComponent("bitrix:main.include","",Array(
								"AREA_FILE_SHOW" => "file",
								"PATH" => SITE_DIR."include/address.php",
							)
						);?>
					</div>
					<div class="email">
						<?$APPLICATION->IncludeComponent("bitrix:main.include","",Array(
								"AREA_FILE_SHOW" => "file",
								"PATH" => SITE_DIR."include/email.php",
							)
						);?>
					</div>
				</div>
			</div>
		</div>
	</div><!--wrap-->

	<!-- feedback question start -->



	<!-- feedback question end -->

	<?if ($options['SHOW_BUTTON_TOP']['ACTIVE_VALUE'] == 'Y'):?>
		<div class="button_up solid_button">
			<i></i>
		</div>
	<?endif;?>

	<!-- Subscribe Modal -->
	<div class="modal-subscribe">
		<div class="modal-subscribe__mask"></div>
		<?$APPLICATION->IncludeComponent("bitrix:subscribe.form", "modal_subscribe", Array(

			),
			false
		);?>
	</div>



	<script type="text/javascript">
		$('.nbs-flexisel-nav-left').addClass('uni-slider-button-small').addClass('uni-slider-button-left').html('<div class="icon"></div>');
		$('.nbs-flexisel-nav-right').addClass('uni-slider-button-small').addClass('uni-slider-button-right').html('<div class="icon"></div>');
	</script>
</body>
</html>