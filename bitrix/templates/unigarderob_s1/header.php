<!DOCTYPE html>
<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile($_SERVER["DOCUMENT_ROOT"]."/bitrix/templates/".SITE_TEMPLATE_ID."/header.php");?>
<html>
<head>
    <?if (CModule::IncludeModule("intec.unigarderob")) {
        UniGarderob::InitProtection();
		UniGarderob::ShowInclude(SITE_ID);
	}
	$options = UniGarderob::getOptionsValue(SITE_ID);?>
	<title><?$APPLICATION->ShowTitle()?></title>
	<link rel="shortcut icon" type="image/x-icon" href="<?=SITE_DIR?>favicon.ico" />
   <?if ($options['ADAPTIV']['ACTIVE_VALUE'] == 'Y'):?>
        <meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, width=device-width"/>
    <?endif;?>
    <?include('parts/javascript.constants.php');?>
    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/uni.js')?>
    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/sale.actions.js')?>
    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/sale.wrapper.js')?>
    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/popups.js')?>
    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/cookie.js')?>
	<?$APPLICATION->ShowHead();?>
	<?include('parts/sale.buttons.php');?>
</head>
<body class="<?=$options['ADAPTIV']['ACTIVE_VALUE'] == 'Y'?'adaptiv':'no-adaptiv'?>">
<?$APPLICATION->IncludeComponent(
	"intec:garderob.panel.themeselector",
	".default",
	array(
		"COMPONENT_TEMPLATE" => ".default"
	),
	$component,
	array('HIDE_ICONS' => 'Y')
);?>
<?if(CSite::InDir(SITE_DIR.'index.php')){
	$isFrontPage = true;
}
// left_menu
$lm = new CMenu("left");
$lm->Init($APPLICATION->GetCurDir(), true);
$flag_lm=(count($lm->arMenu)>0?true:false);
// top_custom
$tc = new CMenu("topcustom");
$tc->Init($APPLICATION->GetCurDir(), true);
$flag_tc=(count($tc->arMenu)>0?true:false);
?>
<div id="panel"><?$APPLICATION->ShowPanel();?></div>
<?
    $menuClasses = array();

    if ($options["TYPE_MENU"]["ACTIVE_VALUE"] == 'standard') $menuClasses[] = 'menu-top-standard';
    if ($options["TYPE_MENU"]["ACTIVE_VALUE"] == 'catalog') $menuClasses[] = 'menu-top-catalog';

    $menuClasses = implode(' ', $menuClasses);
?>
<div class="wrap">
	<div class="top_panel">
		<div class="top_panel_wrap">
			<?if($options["TYPE_BASKET"]["ACTIVE_VALUE"] == "top") { ?>
				<div class="basket_wrap right">
                    <div class="basket_wrap_wrap">
    					<?include('parts/sale.compare.php')?>
    					<?include('parts/sale.basket.php')?>
                    </div>
				</div>
			<?}?>
			<div class="top_personal right">
				<?$APPLICATION->IncludeComponent(
					"bitrix:system.auth.form",
					".default",
					array(
						"REGISTER_URL" => SITE_DIR."personal/profile/",
						"PROFILE_URL" => SITE_DIR."personal/profile/",
						"SHOW_ERRORS" => "N",
						"AUTH_FORGOT_PASSWORD_URL" => SITE_DIR."auth/?forgot_password=yes",
						"FORGOT_PASSWORD_URL" => SITE_DIR."personal/profile/",
						"COMPONENT_TEMPLATE" => ".default"
					),
					false
				);?>
			</div>
			<?$ph_style="";?>
			<?if($options["TYPE_PHONE"]["ACTIVE_VALUE"] !== 'top') {$ph_style= 'display: none';?><?}?>
				<div class="phone_block right" style="<?=$ph_style;?>">
					<div class="phone">
						<?$APPLICATION->IncludeComponent("bitrix:main.include","",Array(
								"AREA_FILE_SHOW" => "file",
								"PATH" => SITE_DIR."include/company_phone.php",
							)
						);?>
					</div>
					<div class="call_button">
						<span class="open_call" onclick="Popups.OrderCallQuestion()"><?=GetMessage("CALL_TEXT")?></span>
					</div>
				</div>

			<?if ($options["TYPE_MENU"]["ACTIVE_VALUE"] == 'catalog'):?>
				<?$APPLICATION->IncludeComponent(
					"bitrix:menu",
					"top_horizontal_menu",
					array(
						"ROOT_MENU_TYPE" => "top",
						"MENU_CACHE_TYPE" => "A",
						"MENU_CACHE_TIME" => "36000000",
						"MENU_CACHE_USE_GROUPS" => "Y",
						"MENU_CACHE_GET_VARS" => array(
						),
						"MAX_LEVEL" => "1",
						"CHILD_MENU_TYPE" => "",
						"USE_EXT" => "Y",
						"DELAY" => "N",
						"ALLOW_MULTI_SELECT" => "N",
						"IBLOCK_CATALOG_TYPE" => "catalog",
						"IBLOCK_CATALOG_ID" => "4",
						"IBLOCK_CATALOG_DIR" => SITE_DIR."catalog/",
						"TYPE_MENU" => "catalog",
						"WIDTH_MENU" => $options["WIDTH_MENU"]["ACTIVE_VALUE"],
						"COMPONENT_TEMPLATE" => "top_horizontal_menu",
						"SMOOTH_COLUMNS" => 'Y'
					),
					false
				);?>
			<?endif;?>
			<?if(($options["TYPE_PHONE"]["ACTIVE_VALUE"] == 'header') && ($options["TYPE_BASKET"]["ACTIVE_VALUE"] == 'header') && ($options["TYPE_MENU"]["ACTIVE_VALUE"] == 'catalog')) {?>
				<div class="search_wrap" style="width: 40%">
					<?$APPLICATION->IncludeComponent("bitrix:search.title", "header_search", array(
						"NUM_CATEGORIES" => "1",
						"TOP_COUNT" => "5",
						"ORDER" => "date",
						"USE_LANGUAGE_GUESS" => "Y",
						"CHECK_DATES" => "N",
						"SHOW_OTHERS" => "N",
						"PAGE" => SITE_DIR."catalog/",
						"CATEGORY_0_TITLE" => GetMessage("SEARCH_GOODS"),
						"CATEGORY_0" => array(
						),
						"CATEGORY_0_iblock_catalog" => array(
							0 => "all",
						),
						"SHOW_INPUT" => "Y",
						"INPUT_ID" => "title-search-input",
						"CONTAINER_ID" => "search",
						"PRICE_CODE" => array(
							0 => "BASE",
						),
						"PRICE_VAT_INCLUDE" => "Y",
						"PREVIEW_TRUNCATE_LEN" => "",
						"SHOW_PREVIEW" => "Y",
						"PREVIEW_WIDTH" => "300",
						"PREVIEW_HEIGHT" => "300",
						"CONVERT_CURRENCY" => "Y",
						"CURRENCY_ID" => "RUB"
						),
						false
					);?>
				</div>
			<?}?>
			<div class="clear"></div>
		</div>
	</div><!--end top_panel-->

	<div class="header_wrap">
		<div class="header_wrap_information">
			<table class="header_wrap_container <?=$menuClasses?>">
				<tr>
					<td class="logo_wrap">
						<?$APPLICATION->IncludeComponent("bitrix:main.include","",Array(
								"AREA_FILE_SHOW" => "file",
								"PATH" => SITE_DIR."include/logo.php",
							)
						);?>
					</td>
					<td class="right_wrap">
						<table class="table_wrap">
							<tr>
								<?if ($options["TYPE_MENU"]["ACTIVE_VALUE"] == 'catalog'):?>
									<td style="width: 45%;">
										<div class="top_level_catalog">
											<?$APPLICATION->IncludeComponent(
												"bitrix:catalog.section.list",
												"section.select",
												array(
													"COMPONENT_TEMPLATE" => "section.select",
													"IBLOCK_TYPE" => "catalog",
													"IBLOCK_ID" => "4",
													"SECTION_ID" => false,
													"SECTION_CODE" => false,
													"COUNT_ELEMENTS" => "N",
													"TOP_DEPTH" => "1",
													"SECTION_FIELDS" => array(
														0 => "",
														1 => "",
													),
													"SECTION_USER_FIELDS" => array(
														0 => "",
														1 => "",
													),
													"SECTION_URL" => "",
													"CACHE_TYPE" => "N",
													"CACHE_TIME" => "36000000",
													"CACHE_GROUPS" => "N",
													"ADD_SECTIONS_CHAIN" => "N",
													"SELECTED_SECTION_VARIABLE" => "CATALOG_SECTION",
													"SEF_MODE" => "Y",
													"SEF_FOLDER" => "/catalog/",
													"SEF_URL_TEMPLATES" => array(
														"section" => "#SECTION_CODE#/",
														"element" => "#SECTION_CODE#/#ELEMENT_CODE#/",
													)
												),
												false
											);?>
										</div>
									</td>
								<?endif;?>
								<?if(($options["TYPE_PHONE"]["ACTIVE_VALUE"] != 'header') || ($options["TYPE_BASKET"]["ACTIVE_VALUE"] != 'header') || ($options["TYPE_MENU"]["ACTIVE_VALUE"] != 'catalog')) {?>
									<td style="width: 100%;">
									  <div class="search_wrap">
										<?$APPLICATION->IncludeComponent("bitrix:search.title", "header_search", array(
											"NUM_CATEGORIES" => "1",
											"TOP_COUNT" => "5",
											"ORDER" => "date",
											"USE_LANGUAGE_GUESS" => "Y",
											"CHECK_DATES" => "N",
											"SHOW_OTHERS" => "N",
											"PAGE" => SITE_DIR."catalog/",
											"CATEGORY_0_TITLE" => GetMessage("SEARCH_GOODS"),
											"CATEGORY_0" => array(
											),
											"CATEGORY_0_iblock_catalog" => array(
												0 => "all",
											),
											"SHOW_INPUT" => "Y",
											"INPUT_ID" => "title-search-input",
											"CONTAINER_ID" => "search",
											"PRICE_CODE" => array(
												0 => "BASE",
											),
											"PRICE_VAT_INCLUDE" => "Y",
											"PREVIEW_TRUNCATE_LEN" => "",
											"SHOW_PREVIEW" => "Y",
											"PREVIEW_WIDTH" => "300",
											"PREVIEW_HEIGHT" => "300",
											"CONVERT_CURRENCY" => "Y",
											"CURRENCY_ID" => "RUB"
											),
											false
										);?>
									  </div>
									</td>
								<?}?>
								<?if($options["TYPE_PHONE"]["ACTIVE_VALUE"] == 'header') {?>
									<td>
										<div class="phone_wrap">
											<div class="phone">
												<?$APPLICATION->IncludeComponent("bitrix:main.include","",Array(
														"AREA_FILE_SHOW" => "file",
														"PATH" => SITE_DIR."include/company_phone.php",
													)
												);?>
											</div>
											<div class="call_button">
												<!--<span class="open_call" onclick="Popups.OrderCallQuestion()"><?=GetMessage("CALL_TEXT")?></span>-->
												<div class="call_button__title">Обратная связь</div>
												<div class="buttons buttons--question clearfix">
													<div class="border_button" onclick="Popups.OrderCall()"><img src="/images/ico3.png" alt="ico">Закажите звонок</div>
													<div class="border_button" onclick="Popups.OrderCallText()"><img src="/images/ico1.png" alt="ico"> Напишите нам </div>
												</div>
											</div>
										</div>
									</td>
								<?}?>
								<?if($options["TYPE_BASKET"]["ACTIVE_VALUE"] == "header") { ?>
									<td>
										<div class="basket_wrap<?=$options["TYPE_BASKET"]["ACTIVE_VALUE"] == "fly"?' fly':''?>">
                                            <div class="basket_wrap_wrap">
                                                <?include('parts/sale.compare.php')?>
                                                <?include('parts/sale.basket.php')?>
                                            </div>
										</div>
									</td>
								<?}?>
								<?if($options["TYPE_BASKET"]["ACTIVE_VALUE"] == "fly") { ?>
									<td>
										<div class="basket_wrap<?=$options["TYPE_BASKET"]["ACTIVE_VALUE"] == "fly"?' fly':''?>">
                                            <div class="basket_wrap_wrap">
                                                <span class="compare-hidden"><?include('parts/sale.compare.php')?></span>
                                                <?include('parts/sale.basket.php')?>
                                            </div>
										</div>
									</td>
								<?}?>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
		<div class="top <?=$menuClasses?>">
			<?$APPLICATION->IncludeComponent(
				"bitrix:menu",
				"top_horizontal_menu",
				array(
					"ROOT_MENU_TYPE" => "top",
					"MENU_CACHE_TYPE" => "A",
					"MENU_CACHE_TIME" => "36000000",
					"MENU_CACHE_USE_GROUPS" => "Y",
					"MENU_CACHE_GET_VARS" => array(
					),
					"MAX_LEVEL" => "3",
					"CHILD_MENU_TYPE" => "left",
					"USE_EXT" => "Y",
					"DELAY" => "N",
					"ALLOW_MULTI_SELECT" => "N",
					"IBLOCK_CATALOG_TYPE" => "catalog",
					"IBLOCK_CATALOG_ID" => "4",
					"IBLOCK_CATALOG_DIR" => SITE_DIR."catalog/",
					"MENU_IN" => "after-header",
					"TYPE_MENU" => $options["TYPE_TOP_MENU"]["ACTIVE_VALUE"],
					"MENU_WIDTH_SIZE" => $options["MENU_WIDTH_SIZE"]["ACTIVE_VALUE"],
					"SMOOTH_COLUMNS" => "Y",
					"COMPONENT_TEMPLATE" => "top_horizontal_menu"
				),
				false
			);?>
			<script>
				$(document).ready(function () {
				 $('.adaptiv .top .top_menu .parent .mobile_link').click(function(){
					if ( $(this).parent().hasClass('open') ) {
						$(this).siblings(".submenu_mobile").slideUp();
						$(this).parent().removeClass('open');
					} else {
						$(this).siblings(".submenu_mobile").slideDown();
						$(this).parent().addClass('open');
					}
					return false;
				 });
				});
			</script>
		</div>
		<?if ($options["TYPE_MENU"]["ACTIVE_VALUE"] == 'catalog'):?>
			<?$APPLICATION->IncludeComponent(
				"bitrix:menu",
				"top_catalog",
				array(
					"ROOT_MENU_TYPE" => "section",
					"MENU_CACHE_TYPE" => "A",
					"MENU_CACHE_TIME" => "36000000",
					"MENU_CACHE_USE_GROUPS" => "Y",
					"MENU_CACHE_GET_VARS" => array(
					),
					"MAX_LEVEL" => "1",
					"CHILD_MENU_TYPE" => "",
					"USE_EXT" => "Y",
					"DELAY" => "N",
					"ALLOW_MULTI_SELECT" => "N",
				),
				false
			);?>
		<?endif;?>
	</div>

	<!-- Banners -->
    <?
        $currentUrlPage = $_SERVER['REQUEST_URI'];
	    $currentUrlPageArr = explode("/", $currentUrlPage);
	?>

	<? if ($currentUrlPageArr[1] == "company" && $currentUrlPageArr[2] == "mission") {?>
        <p><img src="/images/banner2.jpg" alt="banner" style="width: 100%;"></p>
	<?} else if ($currentUrlPageArr[1] == "company" && $currentUrlPageArr[2] == "staff") {?>
        <p><img src="/images/banner1.jpg" alt="banner" style="width: 100%;"></p>
	<?} else if ($currentUrlPageArr[1] == "company" && $currentUrlPageArr[2] == "") {?>
		<p><img src="/images/banner3.jpg" alt="banner" style="width: 100%;"></p>
	<?} else if ($currentUrlPageArr[1] == "buys" && $currentUrlPageArr[2] == "delivery") {?>
        <p><img src="/images/banner5.jpg" alt="banner" style="width: 100%;"></p>
	<?}?>


	<?if($isFrontPage || (!$isFrontPage && $options["HIDE_MAIN_BANNER"]["ACTIVE_VALUE"] != "Y")){?>
		<?$APPLICATION->IncludeComponent(
			"bitrix:news.list",
			"main_slider",
			array(
				"IBLOCK_TYPE" => "#SLIDER_IBLOCK_CODE#",
				"IBLOCK_ID" => "17",
				"NEWS_COUNT" => "20",
				"SORT_BY1" => "SORT",
				"SORT_ORDER1" => "DESC",
				"SORT_BY2" => "SORT",
				"SORT_ORDER2" => "ASC",
				"FILTER_NAME" => "",
				"FIELD_CODE" => array(
					0 => "",
					1 => "",
				),
				"PROPERTY_CODE" => array(
					0 => "TARGET",
					1 => "HEADER",
					2 => "HEADER2",
					3 => "POSITION",
					4 => "BANNER_HREF",
					5 => "TEXT",
					6 => "COLOR_TEXT",
					7 => "",
				),
				"CHECK_DATES" => "Y",
				"DETAIL_URL" => "",
				"AJAX_MODE" => "N",
				"AJAX_OPTION_JUMP" => "N",
				"AJAX_OPTION_STYLE" => "Y",
				"AJAX_OPTION_HISTORY" => "N",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "36000000",
				"CACHE_FILTER" => "N",
				"CACHE_GROUPS" => "N",
				"PREVIEW_TRUNCATE_LEN" => "",
				"ACTIVE_DATE_FORMAT" => "d.m.Y",
				"SET_STATUS_404" => "N",
				"SET_TITLE" => "N",
				"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
				"ADD_SECTIONS_CHAIN" => "N",
				"HIDE_LINK_WHEN_NO_DETAIL" => "N",
				"INCLUDE_SUBSECTIONS" => "Y",
				"PAGER_TEMPLATE" => "",
				"DISPLAY_TOP_PAGER" => "N",
				"DISPLAY_BOTTOM_PAGER" => "N",
				"PAGER_SHOW_ALWAYS" => "N",
				"PAGER_DESC_NUMBERING" => "N",
				"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
				"PAGER_SHOW_ALL" => "N",
				"DISPLAY_DATE" => "N",
				"DISPLAY_NAME" => "N",
				"DISPLAY_PICTURE" => "Y",
				"DISPLAY_PREVIEW_TEXT" => "N",
				"AJAX_OPTION_ADDITIONAL" => "",
				"COMPONENT_TEMPLATE" => "main_slider",
				"SET_BROWSER_TITLE" => "Y",
				"SET_META_KEYWORDS" => "Y",
				"SET_META_DESCRIPTION" => "Y",
				"SET_LAST_MODIFIED" => "N",
				"PAGER_BASE_LINK_ENABLE" => "N",
				"SHOW_404" => "N",
				"MESSAGE_404" => "",
				"PARENT_SECTION" => "",
				"PARENT_SECTION_CODE" => "",
				"MODE_SLIDER" => "fade",
				"SPEED_SLIDER" => "800",
				"USE_AUTOSCROLL" => "Y",
				"PAUSE_AUTOSCROLL" => "8000"
			),
			false
		);?>
	<?}?>
	<div class="clear"></div>

	<div class="workarea_wrap">
		<div class="worakarea_wrap_container workarea">
			<div class="bx_content_section">
				<?if(!$isFrontPage){?>
					<?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "elegante_bread", Array(
						"START_FROM" => "0",
						"PATH" => "",
						"SITE_ID" => SITE_ID,
						),
						false
					);?>
					<h1 class="header_grey"><?$APPLICATION->ShowTitle("header")?></h1>
					<?if($flag_tc){?>
						<div class="top_custom">
							<?$APPLICATION->IncludeComponent(
								"bitrix:menu",
								"custom_menu",
								array(
									"ROOT_MENU_TYPE" => "topcustom",
									"MENU_THEME" => "site",
									"MENU_CACHE_TYPE" => "N",
									"MENU_CACHE_TIME" => "3600",
									"MENU_CACHE_USE_GROUPS" => "Y",
									"MENU_CACHE_GET_VARS" => array(
									),
									"MAX_LEVEL" => "1",
									"CHILD_MENU_TYPE" => "left",
									"USE_EXT" => "N",
									"DELAY" => "N",
									"ALLOW_MULTI_SELECT" => "N",
									"COMPONENT_TEMPLATE" => "custom_menu",
									"HIDE_CATALOG" => "Y",
									"COUNT_ITEMS_CATALOG" => "8"
								),
								false
							);?>
						</div>
					<?}?>
					<?if($flag_lm){?>

						<div class="left_col">
							<?$APPLICATION->IncludeComponent("bitrix:menu", "left_menu", array(
	"ROOT_MENU_TYPE" => "left",
		"MENU_THEME" => "site",
		"MENU_CACHE_TYPE" => "N",
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => "",
		"MAX_LEVEL" => "1",
		"CHILD_MENU_TYPE" => "left",
		"USE_EXT" => "N",
		"DELAY" => "N",
		"ALLOW_MULTI_SELECT" => "N",
		"COMPONENT_TEMPLATE" => "left_menu",
		"HIDE_CATALOG" => "Y",
		"COUNT_ITEMS_CATALOG" => "8"
	),
	false,
	array(
	"ACTIVE_COMPONENT" => "Y"
	)
);?>
						</div>
						<div class="right_col">

					<?}?>
				<?}?>