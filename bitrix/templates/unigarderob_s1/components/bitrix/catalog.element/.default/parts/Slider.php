<?reset($arResult['MORE_PHOTO']);?>
<div class="image-slider">
	<div class="image-box">
		<div class="wrapper">
			<div class="marks">
				<?if($arResult["MIN_PRICE"]["DISCOUNT_DIFF_PERCENT"] && $arParams['SHOW_DISCOUNT_PERCENT'] == 'Y'){?>
					<span class="mark action">- <?=$arResult["MIN_PRICE"]["DISCOUNT_DIFF_PERCENT"];?> %</span>
				<?}?>
				<?if( $arResult["PROPERTIES"]["HIT"]["VALUE"] ){?>
					<span class="mark hit"><?=GetMessage('MARK_HIT')?></span>
				<?}?>			
				<?if( $arResult["PROPERTIES"]["NEW"]["VALUE"] ){?>
					<span class="mark new"><?=GetMessage('MARK_NEW')?></span>
				<?}?>
				<?if( $arResult["PROPERTIES"]["RECOMMEND"]["VALUE"] ){?>
					<span class="mark recommend"><?=GetMessage('MARK_RECOMEND')?></span>
				<?}?>
			</div>
			<div class="slider-images" id="<?=$sUniqueID?>ProductSliderBox">
                <? 
                    $noimg = false;
                    $first  = true;
                    if (empty($arResult['PREVIEW_PICTURE']) && empty($arResult['DETAIL_PICTURE'])) $noimg = true;
                ?>
				<?foreach ($arResult['MORE_PHOTO'] as $photo):?>
                    <?if ($first && $noimg):?>
						<?$photo['SRC'] = SITE_TEMPLATE_PATH."/images/noimg/no-img.png"?>
                        <a class="image noimg">
							<img class="noimg" src="<?=$photo['SRC']?>"/>
							<div class="valign"></div>
                        </a>
                    <?else:?>
						<a 
							rel='images' 
							<?if ($options['CATALOG_PRODUCT_IMAGE_VIEW']['ACTIVE_VALUE'] != 'WITHOUT_EFFECTS'):?>
								<?=$options['CATALOG_PRODUCT_IMAGE_VIEW']['ACTIVE_VALUE'] == 'WITH_FANCY'?' href="'.$photo['SRC'].'"':''?>
							<?endif;?>
							class="image<?=$options['CATALOG_PRODUCT_IMAGE_VIEW']['ACTIVE_VALUE'] == 'WITH_FANCY'?' fancy':''?><?=$options['CATALOG_PRODUCT_IMAGE_VIEW']['ACTIVE_VALUE'] == 'WITH_ZOOM'?' zoom':''?><?=$options['CATALOG_PRODUCT_IMAGE_VIEW']['ACTIVE_VALUE'] == 'WITHOUT_EFFECTS'?' noeffect':''?>"
						>
							<img src="<?=$photo['SRC']?>" alt="<?=htmlspecialcharsbx($arResult['NAME'])?>" title="<?=htmlspecialcharsbx($arResult['NAME'])?>"/>
							<div class="valign"></div>
						</a>
                    <?endif;?>
                    <?$first = false?>
				<?endforeach;?>
			</div>
			<?if ($arFlags['SHOW_OFFERS_SLIDER']):?>
				<?foreach ($arResult['JS_OFFERS'] as $arOffer):?>
					<?if ($arOffer['SLIDER_COUNT'] > 0):?>
						<div class="slider-images" id="<?=$sUniqueID?>SKUProductSliderBox<?=$arOffer['ID']?>" style="display: none;">
							<?foreach($arOffer['SLIDER'] as $photo):?>
								<a 
									rel='images<?=$arOffer['ID']?>' 
									<?if ($options['CATALOG_PRODUCT_IMAGE_VIEW']['ACTIVE_VALUE'] != 'WITHOUT_EFFECTS'):?>
										<?=$options['CATALOG_PRODUCT_IMAGE_VIEW']['ACTIVE_VALUE'] == 'WITH_FANCY'?' href="'.$photo['SRC'].'"':''?>
									<?endif;?> 
									class="image<?=$options['CATALOG_PRODUCT_IMAGE_VIEW']['ACTIVE_VALUE'] == 'WITH_FANCY'?' fancy':''?><?=$options['CATALOG_PRODUCT_IMAGE_VIEW']['ACTIVE_VALUE'] == 'WITH_ZOOM'?' zoom':''?><?=$options['CATALOG_PRODUCT_IMAGE_VIEW']['ACTIVE_VALUE'] == 'WITHOUT_EFFECTS'?' noeffect':''?>"
								>
									<img src="<?=$photo['SRC']?>" />
									<div class="valign"></div>
								</a>
							<?endforeach;?>
						</div>
					<?endif;?>
				<?endforeach;?>
			<?endif;?>
			<?if ($options['CATALOG_PRODUCT_IMAGE_VIEW']['ACTIVE_VALUE'] == 'WITH_ZOOM'):?>
				<script type="text/javascript">
                    $(document).ready(function(){
                        $('.image-slider .image-box .wrapper a.image').not('.noimg').zoom({magnify:1.5});
                    });
				</script>
			<?endif;?>
		</div>
	</div>
	<div class="clear"></div>
	<?if ($arResult['SHOW_SLIDER']):?>
		<div class="list" id='<?=$sUniqueID?>ProductSlider'>
			<div class="buttons hidden">
				<div class="valign"></div>
				<div class="wrapper">
					<div class="button uni-slider-button-small uni-slider-button-left" id="left" onclick="return <?=$sUniqueID?>ProductSlider.SlideRight();"><div class="icon"></div></div>
					<div class="button uni-slider-button-small uni-slider-button-right" id="right" onclick="return <?=$sUniqueID?>ProductSlider.SlideLeft();"><div class="icon"></div></div>
				</div>
			</div>
			<div class="items">
				<?if (count($arResult['MORE_PHOTO']) > 1) {?>
					<?foreach($arResult['MORE_PHOTO'] as $photo) {?>
						<div class="image">
							<div class="wrapper">
								<div>
									<div>
										<div class="valign"></div>
										<img src="<?=$photo['SRC']?>" />
									</div>
								</div>
							</div>
						</div>
					<?}?>
				<?}?>
			</div>
            <script type="text/javascript">
    			var <?=$sUniqueID?>ProductSlider = new UNISlider({
                    'SLIDER': '.image-slider .list#<?=$sUniqueID?>ProductSlider .items',
                    'ELEMENT': '.image-slider .list#<?=$sUniqueID?>ProductSlider .items .image',
                    'OFFSET': 4,
                    'INFINITY_SLIDE': true,
                    'ANIMATE': true,
                    'ANIMATE_SPEED': 400,
                    "EVENTS": {
                        "onAdaptabilityChange": function ($oSlider) {
                            if ($oSlider.Settings.OFFSET < $oSlider.GetElementsCount()) {
                                $('.image-slider .list#<?=$sUniqueID?>ProductSlider .buttons').removeClass('hidden');
                            } else {
                                $('.image-slider .list#<?=$sUniqueID?>ProductSlider .buttons').addClass('hidden');
                            }  
                        }            
                    },
                    <?if ($arFlags['USE_ADAPTIV']):?>
                        <?if ($arFlags['SHOW_MENU_LEFT']):?>
                            "ADAPTABILITY": [{
                                "WIDTH": "DEFAULT",
                                "SETTINGS": {"OFFSET":"2"}
                            },{
                                "WIDTH": "750",
                                "SETTINGS": {"OFFSET":"4"}
                            },{
                                "WIDTH": "400",
                                "SETTINGS": {"OFFSET":"2"}
                            }]
                        <?else:?>
                            "ADAPTABILITY": [{
                                "WIDTH": "DEFAULT",
                                "SETTINGS": {"OFFSET":"4"}
                            },{
                                "WIDTH": "1150",
                                "SETTINGS": {"OFFSET":"2"}
                            },{
                                "WIDTH": "750",
                                "SETTINGS": {"OFFSET":"4"}
                            },{
                                "WIDTH": "400",
                                "SETTINGS": {"OFFSET":"2"}
                            }]
                        <?endif;?>
                    <?endif;?>
    			});
                
                $('.image-slider .list#<?=$sUniqueID?>ProductSlider .items .image').on('click', function () {
                    $('.image-slider .list#<?=$sUniqueID?>ProductSlider .items .image').removeClass('selected');
                    $(this).addClass('selected');
                    $('.image-slider .image-box .slider-images#<?=$sUniqueID?>ProductSliderBox .image')
                        .hide()
                        .eq($(this).index()).css({'display':'block'});
                }).eq(0).addClass('selected');
    		</script>
		</div>
		<?if ($arFlags['SHOW_OFFERS_SLIDER']):?>
			<?foreach ($arResult['JS_OFFERS'] as $arOffer):?>
				<?if ($arOffer['SLIDER_COUNT'] > 0):?>
					<div class="list" id="<?=$sUniqueID?>SKUProductSlider<?=$arOffer['ID']?>" style="display: none;">
						<div class="buttons hidden">
							<div class="valign"></div>
							<div class="wrapper">
								<div class="button uni-slider-button-small uni-slider-button-left" onclick="return <?=$sUniqueID?>SKUProductSlider<?=$arOffer['ID']?>.SlideRight();"><div class="icon"></div></div>
								<div class="button uni-slider-button-small uni-slider-button-right" id="right" onclick="return <?=$sUniqueID?>SKUProductSlider<?=$arOffer['ID']?>.SlideLeft();"><div class="icon"></div></div>
							</div>
						</div>
						<div class="items">
							<?if (count($arOffer['SLIDER']) > 1) {?>
								<?foreach($arOffer['SLIDER'] as $photo) {?>
									<div class="image">
										<div class="wrapper">
											<div>
												<div>
													<div class="valign"></div>
													<img src="<?=$photo['SRC']?>"/>
												</div>
											</div>
										</div>
									</div>
								<?}?>
							<?}?>
						</div>
					</div>
                    <script type="text/javascript">
            			var <?=$sUniqueID?>SKUProductSlider<?=$arOffer['ID']?> = new UNISlider({
                            'SLIDER': '.image-slider .list#<?=$sUniqueID?>SKUProductSlider<?=$arOffer['ID']?> .items',
                            'ELEMENT': '.image-slider .list#<?=$sUniqueID?>SKUProductSlider<?=$arOffer['ID']?> .items .image',
                            'OFFSET': 4,
                            'INFINITY_SLIDE': true,
                            'ANIMATE': true,
                            'ANIMATE_SPEED': 400,
                            "EVENTS": {
                                "onAdaptabilityChange": function ($oSlider) {
                                    if ($oSlider.Settings.OFFSET < $oSlider.GetElementsCount()) {
                                        $('.image-slider .list#<?=$sUniqueID?>SKUProductSlider<?=$arOffer['ID']?> .buttons').removeClass('hidden');
                                    } else {
                                        $('.image-slider .list#<?=$sUniqueID?>SKUProductSlider<?=$arOffer['ID']?> .buttons').addClass('hidden');
                                    }  
                                }            
                            },
                            <?if ($arFlags['USE_ADAPTIV']):?>
                                <?if ($arFlags['SHOW_MENU_LEFT']):?>
                                    "ADAPTABILITY": [{
                                        "WIDTH": "DEFAULT",
                                        "SETTINGS": {"OFFSET":"3"}
                                    },{
                                        "WIDTH": "750",
                                        "SETTINGS": {"OFFSET":"4"}
                                    },{
                                        "WIDTH": "400",
                                        "SETTINGS": {"OFFSET":"2"}
                                    }]
                                <?else:?>
                                    "ADAPTABILITY": [{
                                        "WIDTH": "DEFAULT",
                                        "SETTINGS": {"OFFSET":"4"}
                                    },{
                                        "WIDTH": "1150",
                                        "SETTINGS": {"OFFSET":"2"}
                                    },{
                                        "WIDTH": "750",
                                        "SETTINGS": {"OFFSET":"4"}
                                    },{
                                        "WIDTH": "400",
                                        "SETTINGS": {"OFFSET":"2"}
                                    }]
                                <?endif;?>
                            <?endif;?>
            			});
                        
                        $('.image-slider .list#<?=$sUniqueID?>SKUProductSlider<?=$arOffer['ID']?> .items .image').on('click', function () {
                            $('.image-slider .list#<?=$sUniqueID?>SKUProductSlider<?=$arOffer['ID']?> .items .image').removeClass('selected');
                            $(this).addClass('selected');
                            $('.image-slider .image-box .slider-images#<?=$sUniqueID?>SKUProductSliderBox<?=$arOffer['ID']?> .image')
                                .hide()
                                .eq($(this).index()).css({'display':'block'});
                        }).eq(0).addClass('selected');
            		</script>
				<?endif;?>
			<?endforeach;?>
		<?endif;?>
	<?endif;?>
</div>