<?/*$APPLICATION->IncludeComponent(
	"bitrix:sale.viewed.product", 
	"slider", 
	array(
		"VIEWED_COUNT" => "8",
		"VIEWED_NAME" => "Y",
		"VIEWED_IMAGE" => "Y",
		"VIEWED_PRICE" => "Y",
		"VIEWED_CURRENCY" => (($arParams['CONVERT_CURRENCY'] == 'Y') ? $arParams['VIEWED_CURRENCY'] : 'default'),
		"VIEWED_CANBUY" => "N",
		"VIEWED_CANBASKET" => "N",
		"VIEWED_IMG_HEIGHT" => "60",
		"VIEWED_IMG_WIDTH" => "70",
		"BASKET_URL" => "/personal/basket.php",
		"ACTION_VARIABLE" => "action",
		"PRODUCT_ID_VARIABLE" => "id",
		"SET_TITLE" => "N",
		"COMPONENT_TEMPLATE" => "slider",
		"LINE_ELEMENT_COUNT" => $arFlags['SHOW_MENU_LEFT']?'4':'6',
		"VIEWED_TITLE" => GetMessage('PRODUCT_YOU_LOOKED')
	),
	$component
);*/?>
<?session_start();?>
<?if (!is_array($_SESSION["VIEWED_PRODUCTS"])) $_SESSION["VIEWED_PRODUCTS"] = array();?>
<?if (!in_array($arResult['ID'], $_SESSION["VIEWED_PRODUCTS"])):?>
	<?$_SESSION["VIEWED_PRODUCTS"][] = $arResult['ID']?>
<?endif;?>
<?if (is_array($_SESSION["VIEWED_PRODUCTS"]) && count($_SESSION["VIEWED_PRODUCTS"]) > 0):?>
	<div class="startshop-indents-vertical indent-50"></div>
	<div class="startshop-row">
		<?$GLOBALS["arrFilterGlobal"] = array("ID" => $_SESSION["VIEWED_PRODUCTS"])?> 		 	
		<?$APPLICATION->IncludeComponent(
			"bitrix:catalog.section",
			"slider",
			Array(
				"AJAX_MODE" => "N",
				"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
				"IBLOCK_ID" => $arParams["IBLOCK_ID"],
				"SECTION_ID" => "",
				"SECTION_CODE" => "",
				'TITLE' => GetMessage('PRODUCT_YOU_LOOKED'),
				"SECTION_USER_FIELDS" => array(),
				"ELEMENT_SORT_FIELD" => "sort",
				"ELEMENT_SORT_ORDER" => "asc",
				"FILTER_NAME" => "arrFilterGlobal",
				"FLEXISEL_ID" => "viewedList",
				"INCLUDE_SUBSECTIONS" => "Y",
				"SHOW_ALL_WO_SECTION" => "Y",
				"SECTION_URL" => "",
				"DETAIL_URL" => "",
				"BASKET_URL" => "/personal/cart/",
				"ACTION_VARIABLE" => "action",
				"PRODUCT_ID_VARIABLE" => "id",
				"PRODUCT_QUANTITY_VARIABLE" => "quantity",
				"PRODUCT_PROPS_VARIABLE" => "prop",
				"SECTION_ID_VARIABLE" => "SECTION_ID",
				"META_KEYWORDS" => "-",
				"META_DESCRIPTION" => "-",
				"BROWSER_TITLE" => "-",
				"ADD_SECTIONS_CHAIN" => "N",
				"DISPLAY_COMPARE" => "N",
				"SET_TITLE" => "N",
				"SET_STATUS_404" => "N",
				"PAGE_ELEMENT_COUNT" => "10",
				"LINE_ELEMENT_COUNT" => $options['CATALOG_PRODUCT_MENU']['ACTIVE_VALUE'] == "Y"?'4':'6',
				"PROPERTY_CODE" => array(0=>"HIT",1=>"RECOMMEND",2=>"NEW",3=>"",),
				"OFFERS_FIELD_CODE" => array("ID"),
				"OFFERS_PROPERTY_CODE" => array(),
				"OFFERS_SORT_FIELD" => "sort",
				"OFFERS_SORT_ORDER" => "asc",
				"OFFERS_LIMIT" => "2",
				"PRICE_CODE" => array(0=>"BASE"),
				"USE_PRICE_COUNT" => "N",
				"SHOW_PRICE_COUNT" => "1",
				"PRICE_VAT_INCLUDE" => "Y",
				"USE_PRODUCT_QUANTITY" => "N",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "36000000",
				"CACHE_FILTER" => "N",
				"CACHE_GROUPS" => "Y",
				"DISPLAY_TOP_PAGER" => "N",
				"DISPLAY_BOTTOM_PAGER" => "N",
				"PAGER_TITLE" => "",
				"PAGER_SHOW_ALWAYS" => "N",
				"PAGER_TEMPLATE" => "shop",
				"PAGER_DESC_NUMBERING" => "N",
				"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
				"PAGER_SHOW_ALL" => "N",
				"CONVERT_CURRENCY" => "N",
				"OFFERS_CART_PROPERTIES" => array(),
				"AJAX_OPTION_JUMP" => "N",
				"AJAX_OPTION_STYLE" => "Y",
				"AJAX_OPTION_HISTORY" => "N"
			)
		);?>
	</div>
<?endif;?>