<div class="row">
	<div class="price minimal">
		<div class="current" id="<?=$sUniqueID?>SKUPriceCurrent">
            <?=$arResult['MIN_PRICE']['PRINT_DISCOUNT_VALUE']?>
        </div>
		<?if ($arParams['SHOW_OLD_PRICE'] == 'Y'):?>
			<div class="old" id="<?=$sUniqueID?>SKUPriceOld"<?=!$arFlags['SHOW_PRICE_DISCOUNT'] ? ' style="display: none;"' : ''?>><?=$arResult['MIN_PRICE']['PRINT_VALUE']?></div>
		<?endif;?>
	</div>
	<div class="uni-indents-vertical indent-25"></div>
    <?$arSkuProps = array();?>
    <div class="offers properties">
        <div class="properties-wrapper">
            <?foreach ($arResult['SKU_PROPS'] as &$arProp):?>
            	<?
                    if (!isset($arResult['OFFERS_PROP'][$arProp['CODE']]))
            		    continue;

                    $arSkuProps[] = array(
            			'ID' => $arProp['ID'],
            			'SHOW_MODE' => $arProp['SHOW_MODE'],
            			'VALUES_COUNT' => $arProp['VALUES_COUNT']
            		);
                ?>
                <?if ($arProp['SHOW_MODE'] == 'TEXT'):?>
            		<div class="property text">
            			<div class="property-name"><?=htmlspecialcharsex($arProp['NAME'])?></div>
        				<div class="values SKUProductProperties">
                            <div class="values-wrapper">
            					<?foreach ($arProp['VALUES'] as $arOneValue):?>
            						<div class="value SKUProductPropertyPROP_<?=$arProp['ID'].$arOneValue['ID']?>" onclick="return <?=$sUniqueID?>SKUProduct.SetOfferByProperty('<?='PROP_'.$arProp['ID']?>', '<?=$arOneValue['ID']?>');">
            							<span>
            								<?=htmlspecialcharsbx($arOneValue['NAME'])?>
            							</span>
            						</div>
            					<?endforeach;?>
                            </div>
        				</div>
            		</div>
            	<?elseif ($arProp['SHOW_MODE'] == 'PICT'):?>
            		<div class="property picture">
            			<div class="property-name"><?=htmlspecialcharsex($arProp['NAME'])?></div>
        				<div class="values">
                            <div class="values-wrapper">
            					<?foreach ($arProp['VALUES'] as $arOneValue):?>
            						<div class="value SKUProductPropertyPROP_<?=$arProp['ID'].$arOneValue['ID']?>" onclick="return <?=$sUniqueID?>SKUProduct.SetOfferByProperty('<?='PROP_'.$arProp['ID']?>', '<?=$arOneValue['ID']?>');">
            							<?if ($arOneValue['NA']):?>
            								<span class="na">
            									-
            								</span>
            							<?else:?>
            								<div class="value-image">
            									<img src="<?=$arOneValue['PICT']['SRC']?>"/>
            								</div>
                                            <div class="sprite"></div>
            							<?endif;?>
            						</div>
            					<?endforeach;?>
                            </div>
        				</div>
            		</div>
            	<?endif;?>
            <?endforeach;?>
			<?if ($arFlags['SHOW_TABLE_OF_SIZES']):?>
                <div class="uni-indents-vertical indent-30"></div>
                <div class="table-sizes">
                    <div class="table-sizes-notify"><?=GetMessage('TABLE_OF_SIZES_NOTIFY')?></div>
                    <div class="table-sizes-selector">&#8250;</div>
                    <div class="uni-button table-sizes-button solid_button" onclick="Popups.TableOfSizes();"><?=GetMessage('TABLE_OF_SIZES_BUTTON')?></div>
                </div>
            <?endif;?>
        </div>
    </div>
</div>
<div class="uni-indents-vertical indent-25"></div>
<div class="order">
	<div class="valign"></div>
	<div class="count" id="<?=$sUniqueID?>SKUCount">
        <?
            $arJSNumeric = array(
                'VALUE' => floatval($arResult['CATALOG_MEASURE_RATIO']),
                'MINIMUM' => floatval($arResult['CATALOG_MEASURE_RATIO']),
                'MAXIMUM' => floatval($arResult['CATALOG_QUANTITY']),
                'RATIO' => floatval($arResult['CATALOG_MEASURE_RATIO']),
                'UNLIMITED' => $arFlags['USE_COUNT_UNLIMITED'],
                'VALUE_TYPE' => 'FLOAT'
            );
        ?>
		<button id="decrease" onclick="return <?=$sUniqueID?>SKUProductCount.Decrease()" class="uni-button">-</button>
		<input type="text" id="<?=$sUniqueID?>SKUProductCount" onchange="return <?=$sUniqueID?>SKUProductCount.SetValue($(this).val())" name="count" value="<?=$arResult['CATALOG_MEASURE_RATIO']?>" />
		<button id="increase" onclick="return <?=$sUniqueID?>SKUProductCount.Increase()" class="uni-button">+</button>
		<script type="text/javascript">
            var <?=$sUniqueID?>SKUProductCount = new UNINumericUpDown(<?=CUtil::PhpToJSObject($arJSNumeric)?>);
            <?=$sUniqueID?>SKUProductCount.Settings.EVENTS.onValueChange = function ($oNumeric) {
                $('#<?=$sUniqueID?>SKUProductCount').val($oNumeric.GetValue());
            }
		</script>
	</div>
	<?foreach ($arResult['OFFERS'] as $arOffer):?>
        <div class="buy-button-click SKUBuyButtonClick<?=$arOffer['ID']?>" style="display: none;">
			<?if (!empty($arOffer['PREVIEW_PICTURE'])):?>
				<?$photo = CFile::ResizeImageGet($arOffer['PREVIEW_PICTURE'], array('width' => 170, 'height' => 170), BX_RESIZE_IMAGE_PROPORTIONAL)?>
			<?else:?>
				<?$photo = array()?>
				<?if (!empty($arResult['DETAIL_PICTURE'])):?>
					<?$photo['src'] = $arResult['DETAIL_PICTURE']['SRC']?>
				<?elseif(!empty($arResult['PREVIEW_PICTURE'])):?>
					<?$photo['src'] = $arResult['PREVIEW_PICTURE']['SRC']?>
				<?else:?>
					<?$photo['src'] = SITE_TEMPLATE_PATH.'/images/noimg/noimg_quadro.jpg'?>
				<?endif;?>
			<?endif;?>
			<?
				$off_det = is_array($arOffer['DETAIL_PICTURE']) ? $arOffer['DETAIL_PICTURE']['SRC'] : CFile::GetPath($arOffer['DETAIL_PICTURE']);
				$off_prev = is_array($arOffer['PREVIEW_PICTURE']) ? $arOffer['PREVIEW_PICTURE']['SRC'] : CFile::GetPath($arOffer['PREVIEW_PICTURE']);
			?>
            <?$sBuyBittonClickImage = !empty($off_det) ? $off_det : $off_prev?>
            <?$sBuyBittonClickImage = empty($sBuyBittonClickImage) ? $arResult['DETAIL_PICTURE']['SRC'] : $sBuyBittonClickImage?>
            <?$sBuyBittonClickImage = empty($sBuyBittonClickImage) ? $arResult['PREVIEW_PICTURE']['SRC'] : $sBuyBittonClickImage?>
			<?$arBuyButtonClickParams = array (
				"ID" => $arOffer["ID"],
				"Name" => $arResult["NAME"],
				"IBlockType" => $arParams["IBLOCK_TYPE"],
				"IBlockID" => $arResult["IBLOCK_ID"],
				"PriceCurrent" => $arOffer['MIN_PRICE']['PRINT_DISCOUNT_VALUE'],
				"PriceOld" => "",
				"Image" => $sBuyBittonClickImage,
                "Currency" => $arOffer['MIN_PRICE']['CURRENCY'],
                "PriceID" => $arOffer['MIN_PRICE']['PRICE_ID'],
				"Price" => $arOffer['MIN_PRICE']['VALUE']
			);?>
			<a
				href="javascript:void(0);"
				id="one_click_buy_<?=$arOffer['ID']?>"
				class="uni-button button"
				onclick="Popups.OneClickBuy(<?=CUtil::PhpToJSObject($arBuyButtonClickParams);?>)"
			><?=GetMessage("ONE_CLICK_BUY")?></a>
        </div>
        <div class="buy-button SKUBuyButton<?=$arOffer['ID']?>" style="display: none;">
			<a rel="nofollow" class="uni-button solid_button button addToBasket addToBasket<?=$arOffer["ID"]?>"
					onclick="SaleActions.addToBasket('<?=$arOffer['ID']?>', '<?=$arOffer['MIN_PRICE']['CURRENCY']?>', <?=$sUniqueID?>SKUProductCount.GetValue(), SaleWrapper.updateAfterBasketAction);"
				><?=GetMessage("CATALOG_ADD_TO_BASKET")?>
			</a>
			<a rel="nofollow" href="<?=$arParams["BASKET_URL"];?>" class="uni-button solid_button button removeFromBasket removeFromBasket<?=$arOffer["ID"]?>" style="display: none;">
				<?=GetMessage("CATALOG_ADDED")?>
			</a>
        </div>
		<div class="min-buttons SKUMinButtons<?=$arOffer['ID']?>" style="display: none;">
			<?if($arParams["DISPLAY_COMPARE"]=="Y"):?>
				<div class="min-button compare">
					<div class="add addToCompare addToCompare<?=$arOffer["ID"]?>"
						onclick="SaleActions.addToCompare('<?=$arOffer['COMPARE_URL']?>', SaleWrapper.updateAfterCompareAction);"
						title="<?=GetMessage('COMPARE_TEXT_DETAIL')?>"
					>
					</div>
					<div  class="remove removeFromCompare removeFromCompare<?=$arOffer["ID"]?>"
						style="display:none"
						onclick="SaleActions.removeFromCompare('<?=$arOffer['COMPARE_REMOVE_URL']?>', SaleWrapper.updateAfterCompareAction);"
						title="<?=GetMessage('COMPARE_DELETE_TEXT_DETAIL')?>"
					>
					</div>
				</div>
			<?endif?>
            <?if ($arOffer['CAN_BUY']):?>
    			<div class="min-button like">
    				<div class="add addToWishList addToWishList<?=$arOffer["ID"]?>"
    					onclick="SaleActions.addToWishList('<?=$arOffer['ID']?>', '<?=$arOffer['MIN_PRICE']['CURRENCY']?>', <?=$sUniqueID?>SKUProductCount.GetValue(), SaleWrapper.updateAfterBasketAction);"
    					id="like_<?=$arOffer["ID"]?>"
    					title="<?=GetMessage('LIKE_TEXT_DETAIL')?>"
    				>
    				</div>
    				<div class="remove removeFromWishList removeFromWishList<?=$arOffer["ID"]?>"
    					style="display:none"
    					onclick="SaleActions.removeFromWishList('<?=$arOffer['ID']?>', SaleWrapper.updateAfterBasketAction);"
    					title="<?=GetMessage('LIKE_DELETE_TEXT_DETAIL')?>"
    				>
    				</div>
    			</div>
            <?endif;?>
		</div>
	<?endforeach;?>
</div>