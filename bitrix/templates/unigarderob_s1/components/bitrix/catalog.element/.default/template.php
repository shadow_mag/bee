<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>
<?$this->setFrameMode(true);?>
<?$sUniqueID = 'item'.spl_object_hash($this);?>
<?$frame = $this->createFrame()->begin();?>
<?global $options;?>
<?if (!CModule::IncludeModule("sale")) return;?>
<?$arFlags = array(
    'SHOW_PRICE_DISCOUNT' => ($arResult['MIN_PRICE']['VALUE'] != $arResult['MIN_PRICE']['DISCOUNT_VALUE']),
    'SHOW_MENU_LEFT' => ($options['CATALOG_PRODUCT_MENU']['ACTIVE_VALUE'] == 'Y'),
    'SHOW_TITLE' => ($arParams['SHOW_TITLE'] == 'Y'),
    'SHOW_PRODUCT_OF_DAY' => ($arParams['PRODUCT_OF_DAY_SHOW'] == 'Y'),
    'SHOW_ARTICLE' => !empty($arResult['PROPERTIES']['CML2_ARTICLE']['VALUE']),
    'SHOW_OFFERS' => !empty($arResult['OFFERS']),
    'SHOW_OFFERS_SLIDER' => ($options['CATALOG_SKU_VIEW']['ACTIVE_VALUE'] == 'DYNAMIC' && !empty($arResult['JS_OFFERS'])),
    'SHOW_OFFERS_DYNAMIC' => (!empty($arResult['OFFERS']) && $options['CATALOG_SKU_VIEW']['ACTIVE_VALUE'] == 'DYNAMIC'),
    'SHOW_OFFERS_LIST' => (!empty($arResult['OFFERS']) && $options['CATALOG_SKU_VIEW']['ACTIVE_VALUE'] == 'LIST'),
    'SHOW_QUANTITY_STATUS' => ($options['CATALOG_SKU_VIEW']['ACTIVE_VALUE'] != 'LIST' || (empty($arResult['OFFERS']) && $arResult['CATALOG_QUANTITY_TRACE'] == "Y")),
    'SHOW_PROPERTIES_MINIMAL' => (is_array($arResult['DISPLAY_PROPERTIES']) && !empty($arResult['DISPLAY_PROPERTIES'])),
    'SHOW_PROPERTIES' => false,//!empty($arResult['DISPLAY_PROPERTIES']),
    'SHOW_DOCUMENTS' => !empty($arResult['PROPERTIES']['INSTRUCTIONS']['VALUE']),
    'SHOW_EXPANDABLES' => !empty($arResult["PROPERTIES"]["CML2_EXPANDABLES"]["VALUE"]),
    'SHOW_REVIEWS' => (is_numeric($arParams['REVIEWS_IBLOCK_ID']) && $arParams['USE_REVIEW'] == 'Y'),
    'SHOW_DESCRIPTION_PREVIEW' => !empty($arResult['PREVIEW_TEXT']),
    'SHOW_DESCRIPTION_DETAIL' => !empty($arResult['DETAIL_TEXT']),
    'SHOW_ASSOCIATED' => !empty($arResult["PROPERTIES"]["CML2_ASSOCIATED"]["VALUE"]),
    'SHOW_TABLE_OF_SIZES' => ($arParams['SHOW_TABLE_OF_SIZES'] == "Y"),
    'USE_COUNT_UNLIMITED' => ($arResult['CATALOG_CAN_BUY_ZERO'] == "Y" || $arResult['CATALOG_QUANTITY_TRACE'] == "N"),
    'USE_ADAPTIV' => ($options['ADAPTIV']['ACTIVE_VALUE'] == 'Y'),
    'USE_TABS' => ($options['CATALOG_PRODUCT_VIEW']['ACTIVE_VALUE'] == "WITH_TABS"),
);?>
<?if ($arFlags['SHOW_MENU_LEFT']):?>
    <div class="left_col">
        <?$APPLICATION->IncludeComponent("bitrix:menu", "catalog.left.1", array(
    		"ROOT_MENU_TYPE" => "catalog",
    		"MENU_CACHE_TYPE" => "N",
    		"MENU_CACHE_TIME" => "3600",
    		"MENU_CACHE_USE_GROUPS" => "Y",
    		"MENU_CACHE_GET_VARS" => array(
    		),
    		"MAX_LEVEL" => "2",
    		"CHILD_MENU_TYPE" => "catalog",
    		"USE_EXT" => "Y",
    		"DELAY" => "N",
    		"ALLOW_MULTI_SELECT" => "N",
    		"HIDE_CATALOG" => "Y",
    		"COUNT_ITEMS_CATALOG" => "8"
    		),
    		false
    	);?>
    	<div class="clear"></div>
    </div>
    <div class="right_col">
<?endif;?>
<div class="item<?=$arFlags['SHOW_MENU_LEFT']?' with-menu':''?>" id="<?=$sUniqueID?>">
	<?if ($arFlags['SHOW_TITLE']):?>
		<div class="row">
			<div class="title" style="font-size: 28px;">
				<?=$arResult["NAME"];?>
			</div>
		</div>
		<div class="uni-indents-vertical indent-20"></div>
	<?endif;?>
	<?if ($arFlags['SHOW_PRODUCT_OF_DAY']):?>
		<?if (!empty($arResult["PROPERTIES"]["DAY_PROD"]["VALUE"])):?>
			<?include('parts/ProductOfDay.php')?>
		<?endif;?>
	<?endif;?>
	<div class="row">
		<?include('parts/Slider.php')?>
		<div class="information">
            <?if (!empty($arResult['PROPERTIES']['BRAND']['VALUE'])):?>
                <?$brand = $arResult['PROPERTIES']['BRAND']['VALUE']?>
                <a class="brand" href="<?=$brand['DETAIL_PAGE_URL']?>">
                    <div class="uni-aligner-vertical"></div>
                    <img src="<?=$brand['PREVIEW_PICTURE']['SRC']?>" />
                </a>
            <?endif;?>
			<?if ($arFlags['SHOW_ARTICLE'] || $arFlags['SHOW_QUANTITY_STATUS']): // ���� ���� ��� Offers ��� ��� SKU �� LIST ��� ���� �������?>
				<div class="row">
					<?if ($arFlags['SHOW_ARTICLE']):?>
						<div class="article"><?=GetMessage('PRODUCT_ARTICLE')?>: <?=$arResult['PROPERTIES']['CML2_ARTICLE']['VALUE']?></div>
					<?endif;?>
					<?if ($arFlags['SHOW_QUANTITY_STATUS']):?>
						<div class="state available" id="<?=$sUniqueID?>SKUQuantityAvailable" style="<?=$arResult['CATALOG_AVAILABLE'] != 'Y'?'display: none;':''?>">
							<div class="icon"></div>
							<?=GetMessage('PRODUCT_HAVE')?>
							<?if ($arParams['SHOW_MAX_QUANTITY'] == 'Y'):?>
								<span id="<?=$sUniqueID?>SKUQuantity" <?=$arResult['CATALOG_QUANTITY'] == 0?'style="display: none;"':''?>><span id="<?=$sUniqueID?>SKUQuantityValue"><?=$arResult['CATALOG_QUANTITY']?></span> <span id="<?=$sUniqueID?>SKUQuantityMeasure"><?=$arResult['CATALOG_MEASURE_NAME']?></span>.</span>
							<?endif;?>
						</div>
						<div class="state unavailable" id="<?=$sUniqueID?>SKUQuantityUnavailable" style="<?=$arResult['CATALOG_AVAILABLE'] == 'Y'?'display: none;':''?>">
							<div class="icon"></div>
							<?=GetMessage('PRODUCT_NOT_HAVE')?>
						</div>
					<?endif;;?>
				</div>
				<div class="uni-indents-vertical indent-25"></div>
			<?endif;?>
			<?if (!$arFlags['SHOW_OFFERS']): // ���� ��� OFFERS?>
				<div class="row">
					<?include('parts/OrderNoOffers.php')?>
				</div>
			<?endif;?>
			<?if ($arFlags['SHOW_OFFERS_LIST']): // ���� ���� Offers � ��� SKU - LIST?>
				<div class="row">
                    <?include('parts/OrderWithOffersSKUList.php')?>
                </div>
			<?endif;?>
			<?if ($arFlags['SHOW_OFFERS_DYNAMIC']): // ���� ���� Offers � ��� SKU - DYNAMIC?>
				<div class="row">
					<?include('parts/OrderWithOffersSKUDynamic.php')?>
				</div>
			<?endif;?>
		</div>
        <?if ($arFlags['SHOW_MENU_LEFT']):?><div class="clear"></div><?endif;?>
        <div class="information with-menu">
            <?if (!empty($arResult['PREVIEW_TEXT'])):?>
				<div class="uni-indents-vertical indent-25"></div>
				<div class="row">
					<div class="description uni-text-default">
						<?= $arResult['PREVIEW_TEXT'] ?>
					</div>
				</div>
			<?endif;?>
            <?if ($arFlags['SHOW_PROPERTIES_MINIMAL']):?>
                <?include('parts/PropertiesMinimal.php')?>
            <?endif;?>

        </div>
		<div class="clear"></div>
	</div>
	<?if ($arFlags['SHOW_OFFERS_LIST']):?>
		<div class="uni-indents-vertical indent-50"></div>
		<div class="row" style="overflow-x: auto;">
			<?include('parts/SKUList.php')?>
		</div>
	<?endif;?>
	<?if ($arFlags['USE_TABS']):?>
		<div class="uni-indents-vertical indent-50"></div>
		<div class="row">
			<?include('parts/ViewWithTabs.php')?>
		</div>
	<?else:?>
		<div class="row">
			<?include('parts/ViewWithoutTabs.php')?>
		</div>
	<?endif;?>
	<?if ($arFlags['SHOW_ASSOCIATED']):?>
		<div class="uni-indents-vertical indent-50"></div>
		<div class="row">
			<?include('parts/ProductsAssociated.php')?>
		</div>
	<?endif;?>
	<div class="uni-indents-vertical indent-50"></div>
	<div class="row">
		<?include('parts/ProductsViewed.php')?>
	</div>
	<?if (!$arFlags['USE_TABS'] && $arFlags['SHOW_REVIEWS']):?>
		<div class="uni-indents-vertical indent-50"></div>
		<div class="row">
			<div class="title"><?=GetMessage('PRODUCT_REVIEWS')?></div>
			<div class="uni-indents-vertical indent-15"></div>
			<div id="reviews" class="item_description">
				<?include('parts/Reviews.php')?>
			</div>
			<div class="clear"></div>
		</div>
	<?endif;?>
</div>
<?if ($arFlags['SHOW_MENU_LEFT']):?>
    </div>
<?endif;?>
<div class="clear"></div>
<?if ($arFlags['SHOW_OFFERS_DYNAMIC']):
    if (isset($arResult['OFFERS']) && !empty($arResult['OFFERS']))
    {
    	$arJSParams = array(
    		'CONFIG' => array(
    			'SHOW_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
    			'SHOW_DISCOUNT_PERCENT' => ('Y' == $arParams['SHOW_DISCOUNT_PERCENT']),
    			'SHOW_OLD_PRICE' => ('Y' == $arParams['SHOW_OLD_PRICE']),
    			'DISPLAY_COMPARE' => ('Y' == $arParams['DISPLAY_COMPARE']),
    			'SHOW_SKU_PROPS' => $arResult['SHOW_OFFERS_PROPS'],
    			'OFFER_GROUP' => $arResult['OFFER_GROUP'],
    			'MAIN_PICTURE_MODE' => $arParams['DETAIL_PICTURE_MODE']
    		),
    		'PRODUCT_TYPE' => $arResult['CATALOG_TYPE'],
    		'VISUAL' => array(
    			'ID' => $arItemIDs['ID'],
    			'CURRENT_PATH' => $this->GetFolder(),
    			'ONE_CLICK_BUY' => $arItemIDs['ONE_CLICK_BUY'],
    		),
    		'DEFAULT_PICTURE' => array(
    			'PREVIEW_PICTURE' => $arResult['DEFAULT_PICTURE'],
    			'DETAIL_PICTURE' => $arResult['DEFAULT_PICTURE']
    		),
    		'PRODUCT' => array(
    			'ID' => $arResult['ID'],
    			'NAME' => $arResult['~NAME']
    		),
    		'BASKET' => array(
    			'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
    			'BASKET_URL' => $arParams['BASKET_URL'],
    			'SKU_PROPS' => $arResult['OFFERS_PROP_CODES']
    		),
    		'OFFERS' => $arResult['JS_OFFERS'],
    		'OFFER_SELECTED' => $arResult['OFFERS_SELECTED'],
    		'TREE_PROPS' => $arSkuProps
    	);
    }
    else
    {
    	$arJSParams = array(
    		'CONFIG' => array(
    			'SHOW_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
    			'SHOW_PRICE' => true,
    			'SHOW_DISCOUNT_PERCENT' => ('Y' == $arParams['SHOW_DISCOUNT_PERCENT']),
    			'SHOW_OLD_PRICE' => ('Y' == $arParams['SHOW_OLD_PRICE']),
    			'DISPLAY_COMPARE' => ('Y' == $arParams['DISPLAY_COMPARE']),
    			'MAIN_PICTURE_MODE' => $arParams['DETAIL_PICTURE_MODE']
    		),
    		'VISUAL' => array(
    			'ID' => $arItemIDs['ID']
    		),
    		'PRODUCT_TYPE' => $arResult['CATALOG_TYPE'],
    		'PRODUCT' => array(
    			'ID' => $arResult['ID'],
    			'PICT' => $arFirstPhoto,
    			'NAME' => $arResult['~NAME'],
    			'SUBSCRIPTION' => true,
    			'PRICE' => $arResult['MIN_PRICE'],
    			'SLIDER_COUNT' => $arResult['MORE_PHOTO_COUNT'],
    			'SLIDER' => $arResult['MORE_PHOTO'],
    			'CAN_BUY' => $arResult['CAN_BUY'],
    			'CHECK_QUANTITY' => $arResult['CHECK_QUANTITY'],
    			'QUANTITY_FLOAT' => is_double($arResult['CATALOG_MEASURE_RATIO']),
    			'MAX_QUANTITY' => $arResult['CATALOG_QUANTITY'],
    			'STEP_QUANTITY' => $arResult['CATALOG_MEASURE_RATIO'],
    			'BUY_URL' => $arResult['~BUY_URL'],
    		),
    		'BASKET' => array(
    			'ADD_PROPS' => ('Y' == $arParams['ADD_PROPERTIES_TO_BASKET']),
    			'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
    			'PROPS' => $arParams['PRODUCT_PROPS_VARIABLE'],
    			'EMPTY_PROPS' => $emptyProductProperties,
    			'BASKET_URL' => $arParams['BASKET_URL']
    		)
    	);
    }
endif;?>
<script type="text/javascript">
    $("#tabs").tabs();

    <?if ($arFlags['SHOW_OFFERS_DYNAMIC']):?>
        var <?=$sUniqueID?>SKUProduct = new UNISku({
            'CATALOG': <?=CUtil::PhpToJSObject($arJSParams, false, true);?>,
            'EVENTS': {
                'onChangeProperty': function ($oParameters) {
                    $('.item#<?=$sUniqueID?> .information .offers.properties .property .values .value')
                        .removeClass('selected')
                        .addClass('hidden')
                        .removeClass('enabled')
                        .removeClass('disabled');

                    $('.item#<?=$sUniqueID?> .information .order .buy-button').css('display', 'none');
                    $('.item#<?=$sUniqueID?> .information .order .buy-button-click').css('display', 'none');
                    $('.item#<?=$sUniqueID?> .information .order .min-buttons').css('display', 'none');

                    $oOffer = $oParameters.OFFER;
                    $arPropDisplayed = $oParameters.PROPERTIES.DISPLAYED;
                    $arPropDisabled = $oParameters.PROPERTIES.DISABLED;
                    $arPropEnabled = $oParameters.PROPERTIES.ENABLED;
                    $arPropSelected = $oParameters.OFFER.TREE;

                    for (var $iNumber = 0; $iNumber < $arPropDisplayed.length; $iNumber++) {
                        $('.item#<?=$sUniqueID?> .information .SKUProductProperty' + $arPropDisplayed[$iNumber]['KEY'] + '' + $arPropDisplayed[$iNumber]['VALUE'])
                            .addClass('hidden');
                    }

                    for (var $iNumber = 0; $iNumber < $arPropDisabled.length; $iNumber++) {
                        $('.item#<?=$sUniqueID?> .information .SKUProductProperty' + $arPropDisabled[$iNumber]['KEY'] + '' + $arPropDisabled[$iNumber]['VALUE'])
                            .removeClass('hidden')
                            .addClass('disabled');
                    }

                    for (var $iNumber = 0; $iNumber < $arPropEnabled.length; $iNumber++) {
                        $('.item#<?=$sUniqueID?> .information .SKUProductProperty' + $arPropEnabled[$iNumber]['KEY'] + '' + $arPropEnabled[$iNumber]['VALUE'])
                            .removeClass('hidden')
                            .addClass('enabled');
                    }

                    for (var $sKey in $arPropSelected) {
                        $('.item#<?=$sUniqueID?> .information .SKUProductProperty' + $sKey + '' + $arPropSelected[$sKey])
                            .addClass('selected');
                    }

                    $('.item#<?=$sUniqueID?> .image-slider .image-box .slider-images').hide();
                    $('.item#<?=$sUniqueID?> .image-slider .list').hide();
                    $('#<?=$sUniqueID?>SKUProductSliderBox' + $oOffer.ID).show();
                    $('#<?=$sUniqueID?>SKUProductSlider' + $oOffer.ID).show();

                    $('.item#<?=$sUniqueID?> .information .order .SKUMinButtons' + $oOffer.ID).css('display', 'inline-block');

                    if ($oOffer.CAN_BUY) {
                        <?=$sUniqueID?>SKUProductCount.Settings.MAXIMUM = $oOffer.MAX_QUANTITY;
                        <?=$sUniqueID?>SKUProductCount.Settings.MINIMUM = $oOffer.STEP_QUANTITY;
                        <?=$sUniqueID?>SKUProductCount.Settings.RATIO = $oOffer.STEP_QUANTITY;

                        $('.item#<?=$sUniqueID?> #<?=$sUniqueID?>SKUQuantityAvailable').show();
                        $('.item#<?=$sUniqueID?> #<?=$sUniqueID?>SKUQuantityUnavailable').hide();

                        if ($oOffer.CHECK_QUANTITY) {
                            <?=$sUniqueID?>SKUProductCount.Settings.UNLIMITED = false;
                            $('.item#<?=$sUniqueID?> #<?=$sUniqueID?>SKUQuantity').show();
                            $('.item#<?=$sUniqueID?> #<?=$sUniqueID?>SKUQuantityValue').html($oOffer.MAX_QUANTITY);
                            $('.item#<?=$sUniqueID?> #<?=$sUniqueID?>SKUQuantityMeasure').html($oOffer.MEASURE);
                        } else {
                            <?=$sUniqueID?>SKUProductCount.Settings.UNLIMITED = true;
                            $('.item#<?=$sUniqueID?> #<?=$sUniqueID?>SKUQuantity').hide();
                            $('.item#<?=$sUniqueID?> #<?=$sUniqueID?>SKUQuantityValue').html('');
                            $('.item#<?=$sUniqueID?> #<?=$sUniqueID?>SKUQuantityMeasure').html('');
                        }

                        <?=$sUniqueID?>SKUProductCount.SetValue($oOffer.STEP_QUANTITY);

                        $('.item#<?=$sUniqueID?> .information .order .SKUBuyButton' + $oOffer.ID).css('display', 'inline-block');
                        $('.item#<?=$sUniqueID?> .information .order .SKUBuyButtonClick' + $oOffer.ID).css('display', 'inline-block');
                        $('.item#<?=$sUniqueID?> #<?=$sUniqueID?>SKUCount').css('display', 'inline-block');
                    } else {
                        <?=$sUniqueID?>SKUProductCount.Settings.MAXIMUM = 0;
                        <?=$sUniqueID?>SKUProductCount.Settings.MINIMUM = 0;
                        <?=$sUniqueID?>SKUProductCount.Settings.RATIO = 0;
                        <?=$sUniqueID?>SKUProductCount.Settings.UNLIMITED = false;
                        <?=$sUniqueID?>SKUProductCount.SetValue(0);

                        $('.item#<?=$sUniqueID?> #<?=$sUniqueID?>SKUQuantityAvailable').hide();
                        $('.item#<?=$sUniqueID?> #<?=$sUniqueID?>SKUQuantity').hide();
                        $('.item#<?=$sUniqueID?> #<?=$sUniqueID?>SKUQuantityValue').html('');
                        $('.item#<?=$sUniqueID?> #<?=$sUniqueID?>SKUQuantityMeasure').html('');
                        $('.item#<?=$sUniqueID?> #<?=$sUniqueID?>SKUQuantityUnavailable').show();
                        $('.item#<?=$sUniqueID?> #<?=$sUniqueID?>SKUCount').hide();
                    }

                    if ($oOffer.PRICE.VALUE == $oOffer.PRICE.DISCOUNT_VALUE) {
                        $('.item#<?=$sUniqueID?> #<?=$sUniqueID?>SKUPriceCurrent').html($oOffer.PRICE.PRINT_VALUE);
                        $('.item#<?=$sUniqueID?> #<?=$sUniqueID?>SKUPriceOld').hide().html('');
                    } else {
                        $('.item#<?=$sUniqueID?> #<?=$sUniqueID?>SKUPriceCurrent').html($oOffer.PRICE.PRINT_DISCOUNT_VALUE);
                        $('.item#<?=$sUniqueID?> #<?=$sUniqueID?>SKUPriceOld').show().html($oOffer.PRICE.PRINT_VALUE);
                    }
                }
            }
        });
    <?endif;?>
</script>
<?$frame->end()?>