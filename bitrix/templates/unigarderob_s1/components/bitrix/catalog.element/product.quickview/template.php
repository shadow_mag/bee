<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>
<?$this->setFrameMode(true);?>
<?$sUniqueID = 'item'.spl_object_hash($this);?>
<?$frame = $this->createFrame()->begin();?>
<?global $options;?>
<?if (!CModule::IncludeModule("sale")) return;?>
<?$arFlags = array(
    'SHOW_PRICE_DISCOUNT' => ($arResult['MIN_PRICE']['VALUE'] != $arResult['MIN_PRICE']['DISCOUNT_VALUE']),      
    'SHOW_ARTICLE' => !empty($arResult['PROPERTIES']['CML2_ARTICLE']['VALUE']),
    'SHOW_OFFERS' => !empty($arResult['OFFERS']),
    'SHOW_OFFERS_SLIDER' => ($options['CATALOG_SKU_VIEW']['ACTIVE_VALUE'] == 'DYNAMIC' && !empty($arResult['JS_OFFERS'])),
    'SHOW_OFFERS_DYNAMIC' => 'DYNAMIC',
    'SHOW_OFFERS_LIST' => (!empty($arResult['OFFERS']) && $options['CATALOG_SKU_VIEW']['ACTIVE_VALUE'] == 'LIST'),
    'SHOW_QUANTITY_STATUS' => ($options['CATALOG_SKU_VIEW']['ACTIVE_VALUE'] != 'LIST' || (empty($arResult['OFFERS']) && $arResult['CATALOG_QUANTITY_TRACE'] == "Y")),
    'SHOW_PROPERTIES_MINIMAL' => ($options['CATALOG_PRODUCT_MIN_PROPERTIES']['ACTIVE_VALUE'] == "Y" && is_array($arResult['DISPLAY_PROPERTIES']) && !empty($arResult['DISPLAY_PROPERTIES'])),
    'SHOW_PROPERTIES' => !empty($arResult['DISPLAY_PROPERTIES']), 
    'SHOW_DESCRIPTION_DETAIL' => !empty($arResult['DETAIL_TEXT']),   
    'USE_COUNT_UNLIMITED' => ($arResult['CATALOG_CAN_BUY_ZERO'] == "Y" || $arResult['CATALOG_QUANTITY_TRACE'] == "N"),   
);
?>
<?
$APPLICATION->ShowAjaxHead();
$APPLICATION->SetAdditionalCSS($templateFolder."/style.css");
?>
<div class="item-quick-view" id="<?=$sUniqueID?>">
	<div class="header_block">
        <div class="header_block-wrapper">
    		<div class="brand-qv">
    			<?if (!empty($arResult['PROPERTIES']['BRAND']['VALUE'])):?>
    				 <?$brand = $arResult['PROPERTIES']['BRAND']['VALUE']?>
    				 <a class="brand uni-image" href="<?=$brand['DETAIL_PAGE_URL']?>">
    					  <div class="uni-aligner-vertical"></div>
    					  <img src="<?=$brand['PREVIEW_PICTURE']['SRC']?>" />
    				 </a>
    			<?endif;?>
    		</div>
    		<div class="name">
    			<?=$arResult["NAME"];?>
    		</div>	
    		<?include('parts/BuyBlock.php')?>
            <div class="close_popup-wrapper">
                <div class='close_popup' onclick="Popups.ProductQuickView.Close();"></div>
            </div>
    		<div class="clear"></div>
        </div>
	</div>	
	<div class="row-w">		
		<?include('parts/Slider.php')?>
		<div style="float:right;width:400px;">
			<div class="information">
				<?if (!empty($arResult['PREVIEW_TEXT'])):?>				
					<div class="row">
						<div class="description uni-text-default">
							<?=$arResult['PREVIEW_TEXT']?>
						</div>
					</div>
				<?endif;?>
			</div>		
			<div class="information">		
				<?if ($arFlags['SHOW_ARTICLE'] || $arFlags['SHOW_QUANTITY_STATUS']): // ���� ���� ��� Offers ��� ��� SKU �� LIST ��� ���� �������?>
					<div class="row">
						<?if ($arFlags['SHOW_ARTICLE']):?>
							<div class="article"><?=GetMessage('PRODUCT_ARTICLE')?>: <?=$arResult['PROPERTIES']['CML2_ARTICLE']['VALUE']?></div>
						<?endif;?>
						<?if ($arFlags['SHOW_QUANTITY_STATUS']):?>
							<div class="state available" id="SKUQuantityAvailable" style="<?=$arResult['CATALOG_AVAILABLE'] != 'Y'?'display: none;':''?>">
								<div class="icon"></div>
								<?=GetMessage('PRODUCT_HAVE')?>
								<?if ($arParams['SHOW_MAX_QUANTITY'] == 'Y'):?>
									<span id="SKUQuantity" <?=$arResult['CATALOG_QUANTITY'] == 0?'style="display: none;"':''?>><span id="SKUQuantityValue"><?=$arResult['CATALOG_QUANTITY']?></span> <span id="SKUQuantityMeasure"><?=$arResult['CATALOG_MEASURE_NAME']?></span>.</span>
								<?endif;?>
							</div>
							<div class="state unavailable" id="SKUQuantityUnavailable" style="<?=$arResult['CATALOG_AVAILABLE'] == 'Y'?'display: none;':''?>">
								<div class="icon"></div>
								<?=GetMessage('PRODUCT_NOT_HAVE')?>
							</div>
						<?endif;?>
					</div>
					<div class="uni-indents-vertical indent-25"></div>
				<?endif;?>				
				<?if ($arFlags['SHOW_OFFERS_DYNAMIC']): // ���� ���� Offers � ��� SKU - DYNAMIC?>
					<div class="row">
						<?include('parts/OrderWithOffersSKUDynamic.php')?>
					</div>
				<?endif;?>
				<?include('parts/ViewWithoutTabs.php')?>			
				<div class="clear"></div>
			</div>	
			<div class="clear"></div>
		</div>
		<div class="clear"></div>		
	</div>
	<?include('parts/EndSlider.php')?>
	<div class="footer_block">
		<div class="footer_block-wrapper">
			<?if ($arParams['SHOW_TABLE_OF_SIZES'] == "Y"){?>
				<div class="change_size" onclick="Popups.TableOfSizes();"><?=GetMessage("CHANGE_SIZE")?></div>
			<?}?>
			<a href="<?=$arResult["DETAIL_PAGE_URL"]?>" class='uni-button solid_button button' onclick="document.location.href = '<?=$arResult["DETAIL_PAGE_URL"]?>'"><?=GetMessage("TEXT_DETAIL")?></a>
			<div class="clear"></div>
		</div>
	</div>
</div>
<div class="clear"></div>
<?if ($arFlags['SHOW_OFFERS_DYNAMIC']):
    if (isset($arResult['OFFERS']) && !empty($arResult['OFFERS']))
    {
    	$arJSParams = array(
    		'CONFIG' => array(
    			'SHOW_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
    			'SHOW_DISCOUNT_PERCENT' => ('Y' == $arParams['SHOW_DISCOUNT_PERCENT']),
    			'SHOW_OLD_PRICE' => ('Y' == $arParams['SHOW_OLD_PRICE']),
    			'DISPLAY_COMPARE' => ('Y' == $arParams['DISPLAY_COMPARE']),
    			'SHOW_SKU_PROPS' => $arResult['SHOW_OFFERS_PROPS'],
    			'OFFER_GROUP' => $arResult['OFFER_GROUP'],
    			'MAIN_PICTURE_MODE' => $arParams['DETAIL_PICTURE_MODE']
    		),
    		'PRODUCT_TYPE' => $arResult['CATALOG_TYPE'],
    		'VISUAL' => array(
    			'ID' => $arItemIDs['ID'],
    			'CURRENT_PATH' => $this->GetFolder(),
    			'ONE_CLICK_BUY' => $arItemIDs['ONE_CLICK_BUY'],
    		),
    		'DEFAULT_PICTURE' => array(
    			'PREVIEW_PICTURE' => $arResult['DEFAULT_PICTURE'],
    			'DETAIL_PICTURE' => $arResult['DEFAULT_PICTURE']
    		),
    		'PRODUCT' => array(
    			'ID' => $arResult['ID'],
    			'NAME' => $arResult['~NAME']
    		),
    		'BASKET' => array(
    			'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
    			'BASKET_URL' => $arParams['BASKET_URL'],
    			'SKU_PROPS' => $arResult['OFFERS_PROP_CODES']
    		),
    		'OFFERS' => $arResult['JS_OFFERS'],
    		'OFFER_SELECTED' => $arResult['OFFERS_SELECTED'],
    		'TREE_PROPS' => $arSkuProps
    	);
    }
    else
    {
    	$arJSParams = array(
    		'CONFIG' => array(
    			'SHOW_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
    			'SHOW_PRICE' => true,
    			'SHOW_DISCOUNT_PERCENT' => ('Y' == $arParams['SHOW_DISCOUNT_PERCENT']),
    			'SHOW_OLD_PRICE' => ('Y' == $arParams['SHOW_OLD_PRICE']),
    			'DISPLAY_COMPARE' => ('Y' == $arParams['DISPLAY_COMPARE']),
    			'MAIN_PICTURE_MODE' => $arParams['DETAIL_PICTURE_MODE']
    		),
    		'VISUAL' => array(
    			'ID' => $arItemIDs['ID']			
    		),
    		'PRODUCT_TYPE' => $arResult['CATALOG_TYPE'],
    		'PRODUCT' => array(
    			'ID' => $arResult['ID'],
    			'PICT' => $arFirstPhoto,
    			'NAME' => $arResult['~NAME'],
    			'SUBSCRIPTION' => true,
    			'PRICE' => $arResult['MIN_PRICE'],
    			'SLIDER_COUNT' => $arResult['MORE_PHOTO_COUNT'],
    			'SLIDER' => $arResult['MORE_PHOTO'],
    			'CAN_BUY' => $arResult['CAN_BUY'],
    			'CHECK_QUANTITY' => $arResult['CHECK_QUANTITY'],
    			'QUANTITY_FLOAT' => is_double($arResult['CATALOG_MEASURE_RATIO']),
    			'MAX_QUANTITY' => $arResult['CATALOG_QUANTITY'],
    			'STEP_QUANTITY' => $arResult['CATALOG_MEASURE_RATIO'],
    			'BUY_URL' => $arResult['~BUY_URL'],
    		),
    		'BASKET' => array(
    			'ADD_PROPS' => ('Y' == $arParams['ADD_PROPERTIES_TO_BASKET']),
    			'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
    			'PROPS' => $arParams['PRODUCT_PROPS_VARIABLE'],
    			'EMPTY_PROPS' => $emptyProductProperties,
    			'BASKET_URL' => $arParams['BASKET_URL']
    		)
    	);
    }
endif;?>
<script type="text/javascript">
    $("#tabs").tabs();

    <?if ($arFlags['SHOW_OFFERS_DYNAMIC']):?>
        var <?=$sUniqueID?>SKUProduct = new UNISku({
            'CATALOG': <?=CUtil::PhpToJSObject($arJSParams, false, true);?>,
            'EVENTS': {
                'onChangeProperty': function ($oParameters) {
                    $('.item-quick-view#<?=$sUniqueID?> .information .offers.properties .property .values .value')
                        .removeClass('selected')
                        .addClass('hidden')
                        .removeClass('enabled')
                        .removeClass('disabled');
                        
                    $('.item-quick-view#<?=$sUniqueID?> .buy-button-qv').css('display', 'none');
                    $('.item-quick-view#<?=$sUniqueID?> .information .order .buy-button-click').css('display', 'none');
                    $('.item-quick-view#<?=$sUniqueID?> .min-buttons-qv').css('display', 'none');
                    
                    $oOffer = $oParameters.OFFER;
                    $arPropDisplayed = $oParameters.PROPERTIES.DISPLAYED;
                    $arPropDisabled = $oParameters.PROPERTIES.DISABLED;
                    $arPropEnabled = $oParameters.PROPERTIES.ENABLED;
                    $arPropSelected = $oParameters.OFFER.TREE;
                    
                    for (var $iNumber = 0; $iNumber < $arPropDisplayed.length; $iNumber++) {
                        $('.item-quick-view#<?=$sUniqueID?> .information .SKUProductProperty' + $arPropDisplayed[$iNumber]['KEY'] + '' + $arPropDisplayed[$iNumber]['VALUE'])
                            .addClass('hidden');
                    }
                    
                    for (var $iNumber = 0; $iNumber < $arPropDisabled.length; $iNumber++) {
                        $('.item-quick-view#<?=$sUniqueID?> .information .SKUProductProperty' + $arPropDisabled[$iNumber]['KEY'] + '' + $arPropDisabled[$iNumber]['VALUE'])
                            .removeClass('hidden')
                            .addClass('disabled');
                    }
                    
                    for (var $iNumber = 0; $iNumber < $arPropEnabled.length; $iNumber++) {
                        $('.item-quick-view#<?=$sUniqueID?> .information .SKUProductProperty' + $arPropEnabled[$iNumber]['KEY'] + '' + $arPropEnabled[$iNumber]['VALUE'])
                            .removeClass('hidden')
                            .addClass('enabled');
                    }
                    
                    for (var $sKey in $arPropSelected) {
                        $('.item-quick-view#<?=$sUniqueID?> .information .SKUProductProperty' + $sKey + '' + $arPropSelected[$sKey])
                            .addClass('selected');
                    }
                    
                    $('.item-quick-view#<?=$sUniqueID?> .image-slider .image-box .slider-images').hide();
                    $('.item-quick-view#<?=$sUniqueID?> .image-slider .list').hide();
                    $('.item-quick-view#<?=$sUniqueID?> #SKUProductSliderBox' + $oOffer.ID).show();
                    $('.item-quick-view#<?=$sUniqueID?> #SKUProductSlider' + $oOffer.ID).show();
                    
                    $('.item-quick-view#<?=$sUniqueID?> .min-buttons-qv.SKUMinButtons' + $oOffer.ID).css('display', 'inline-block');
                    
                    if ($oOffer.CAN_BUY) {                        
                        $('.item-quick-view#<?=$sUniqueID?> #SKUQuantityAvailable').show();
                        $('.item-quick-view#<?=$sUniqueID?> #SKUQuantityUnavailable').hide();
                        
                        if ($oOffer.CHECK_QUANTITY) {
                            $('.item-quick-view#<?=$sUniqueID?> #SKUQuantity').show();
                            $('.item-quick-view#<?=$sUniqueID?> #SKUQuantityValue').html($oOffer.MAX_QUANTITY);
                            $('.item-quick-view#<?=$sUniqueID?> #SKUQuantityMeasure').html($oOffer.MEASURE);
                        } else {
                            $('.item-quick-view#<?=$sUniqueID?> #SKUQuantity').hide();
                            $('.item-quick-view#<?=$sUniqueID?> #SKUQuantityValue').html('');
                            $('.item-quick-view#<?=$sUniqueID?> #SKUQuantityMeasure').html('');
                        }
                        
                        $('.item-quick-view#<?=$sUniqueID?> .buy-button-qv.SKUBuyButton' + $oOffer.ID).css('display', 'inline-block');
                        $('.item-quick-view#<?=$sUniqueID?> .information .order .SKUBuyButtonClick' + $oOffer.ID).css('display', 'inline-block');
                    } else {
                        $('.item-quick-view#<?=$sUniqueID?> #SKUQuantityAvailable').hide();
                        $('.item-quick-view#<?=$sUniqueID?> #SKUQuantity').hide();
                        $('.item-quick-view#<?=$sUniqueID?> #SKUQuantityValue').html('');
                        $('.item-quick-view#<?=$sUniqueID?> #SKUQuantityMeasure').html('');
                        $('.item-quick-view#<?=$sUniqueID?> #SKUQuantityUnavailable').show();
                    }
                    
                    if ($oOffer.PRICE.VALUE == $oOffer.PRICE.DISCOUNT_VALUE) {
                        $('.item-quick-view#<?=$sUniqueID?> #SKUPriceCurrent').html($oOffer.PRICE.PRINT_VALUE);
                        $('.item-quick-view#<?=$sUniqueID?> #SKUPriceOld').hide().html('');
                    } else {
                        $('.item-quick-view#<?=$sUniqueID?> #SKUPriceCurrent').html($oOffer.PRICE.PRINT_DISCOUNT_VALUE);
                        $('.item-quick-view#<?=$sUniqueID?> #SKUPriceOld').show().html($oOffer.PRICE.PRINT_VALUE);
                    }
                }
            }
        });
    <?endif;?>
    
    if (UNI.isFunction(ButtonsMinUpdater))
        ButtonsMinUpdater();
</script>
<?die();?>
<?$frame->end()?>
