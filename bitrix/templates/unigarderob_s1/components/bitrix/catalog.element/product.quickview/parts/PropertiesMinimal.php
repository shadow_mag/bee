<?$arPropertiesMinimal = array_slice($arResult['DISPLAY_PROPERTIES'], 0, 6);?>
<?if (!empty($arPropertiesMinimal)):?>
<div class="uni-indents-vertical indent-40"></div>
<div class="row">
    <div class="properties">
    	<?foreach ($arPropertiesMinimal as $arPropertyMinimal):?>
    		<div class="property"><?=$arPropertyMinimal['NAME']?> &mdash; <?=$arPropertyMinimal['VALUE']?>;</div>
    	<?endforeach;?>
    	<?if (count($arPropertiesMinimal) > 0):?>
    		<a href="#properties" class="all-properties"><?=GetMessage('PRODUCT_PROPERTIES_ALL')?></a>
    		<script type="text/javascript">
    			$(document).ready(function(){
    				$(".properties .all-properties").click(function() {
    					var tabs = $('#tabs');
    					tabs.tabs("select", "#properties");
    				})
    			})
    		</script>
    	<?endif;?>
    </div>
</div>
<?endif;?>