<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>
<div class="form_feedback_under">

<?=$arResult["FORM_NOTE"]?>

<?if ($arResult["isFormNote"] != "Y")
{
?>
<?=$arResult["FORM_HEADER"]?>
<div class="form_feedback">
<?
/***********************************************************************************
					form header
***********************************************************************************/
if ($arResult["isFormDescription"] == "Y" || $arResult["isFormTitle"] == "Y" || $arResult["isFormImage"] == "Y")
{
	if ($arResult["isFormTitle"]){?>
		<div class="header_grey">Обратная связь</div>
	<?}?>
	<?if ($arResult["isFormTitle"]){?>
		<div class="decription_form"><?=$arResult["FORM_DESCRIPTION"]?></div>
	<?}?>
<?}?>
<div class="errors" style="display:none;">
	<?=GetMessage("ERROR_MESSAGE");?>
</div>
<?if ($arResult["isFormErrors"] == "Y"):?><?=$arResult["FORM_ERRORS_TEXT"];?><?endif;
/***********************************************************************************
						form questions
***********************************************************************************/
?>

<style>
	.buttons--question {
		display: flex;
		align-items: center;
		flex-direction: column;
	}
	.buttons--question .border_button {
		display: flex;
        align-items: center;
        margin-bottom: 25px;
        padding: 0 15px;
        box-sizing: border-box;
        width: 251px;
        justify-content: center;
	}
	.buttons--question .solid_button {
		width: 100%;
		cursor: pointer;
	}
	.buttons--question .border_button img {
		width: 40px;
		height: 40px;
		margin-right: 5px;
	}
	.buttons--question .solid_button img {
		width: 35px;
		height: 35px;
	}
	.popup-window-close-icon, .uni-popup .close_popup {
		top: 20px !important;
		right: 20px !important;
	}
</style>

<div class="buttons buttons--question clearfix">
	<div class="border_button" onclick="Popups.OrderCall()"><img src="/images/ico3.png" alt="ico">Закажите звонок</div>
	<div class="solid_button" onclick="Popups.OrderCallText()"><img src="/images/ico1.png" alt="ico"> Напишите нам </div>
</div>
<div>
</div>

</div>
<?=$arResult["FORM_FOOTER"]?>
<?
} //endif (isFormNote)
?>
</div>
<script type="text/javascript">
	function SendAndTestForm(e){
		var flagrequared=1;
		$('.starrequired').each(function(i,el){
			var element=$(el).parent().parent().find('input,textarea');
			if(element.val()==''){
				element.addClass('nofill');
				flagrequared=0;
			}else{
				element.removeClass('nofill');
			}
		})
		if($('.captcha_form').find('input.inputtext').val()==""){
			$('.captcha_form').find('input.inputtext').addClass('nofill');
			flagrequared=0;
		}else{
			$('.captcha_form').find('input.inputtext').removeClass('nofill');
		}
		if(!flagrequared){
			$('.errors').show();
			e.preventDefault();
		}
	}
	$(document).ready(function(){
		$('.close_button').click(function(){
			$('.bx_popup_close').trigger('click');
		});
	})
</script>