var UNIBasketFly = function ($oSettings) {
    this.defaults = {
        'animate': false,
        'animateTime': 500,
        'selectors': {
            'sections': '.sections',
            'section': '.section'
        }
    };
    this.settings = $.extend({}, this.defaults, $oSettings || {});
    this.section = null;
};

UNIBasketFly.prototype.getSectionsContainer = function () {
    return $(this.settings.selectors.sections);
}

UNIBasketFly.prototype.getSections = function () {
    return $(this.settings.selectors.section);
}

UNIBasketFly.prototype.getSectionBySelector = function ($sSelector) {
    return $(this.settings.selectors.section + $sSelector);
};

UNIBasketFly.prototype.getSectionByID = function ($iSectionID) {
    return $(this.settings.selectors.section).eq($iSectionID);
};

UNIBasketFly.prototype.setSectionByID = function ($iSectionID) {
    var $oSectionsContainer = this.getSectionsContainer();
    
    if (this.section !== null)
        var $oSectionCurrent = this.getSectionByID(this.section);
        
    var $oSection = this.getSectionByID($iSectionID);
    
    if (this.section != $iSectionID) {
        if (this.settings.animate) {
            if (this.section !== null) {
                if ($oSectionCurrent.width() > $oSection.width()) {
                    $oSectionsContainer.stop().animate({'width': $oSection.width()}, this.settings.animateTime, function () {
                        $oSectionCurrent.hide();
                        $oSection.show();
                    });
                } else if ($oSectionCurrent.width() < $oSection.width()) {
                    $oSectionCurrent.hide();
                    $oSection.show();
                    $oSectionsContainer.stop().animate({'width': $oSection.width()}, this.settings.animateTime);
                } else {
                    $oSectionCurrent.hide();
                    $oSection.show();
                }
            } else {
                $oSection.show();
                $oSectionsContainer.stop().animate({'width': $oSection.width()}, this.settings.animateTime);
            }
        } else {
            if (this.section !== null)
                $oSectionCurrent.hide();
            
            $oSection.show();
            $oSectionsContainer.css({'width': $oSection.width()});
        }
        this.section = $iSectionID;
    } else {
        this.close();
    }
};

UNIBasketFly.prototype.setSectionBySelector = function ($sSelector) {
    this.setSectionByID(this.getSectionBySelector($sSelector).index());
};

UNIBasketFly.prototype.close = function () {
    if (this.section !== null) {
        var $oSectionsContainer = this.getSectionsContainer();
        var $oSections = this.getSections();
        if (this.settings.animate) {
            $oSectionsContainer.stop().animate({'width': '0px'}, this.settings.animateTime, function () {
                $oSections.hide();
            });
        } else {
            $oSections.hide();
            $oSectionsContainer.css({'width': '0px'});
        }
        
        this.section = null;
    }
    
}