<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
    global $APPLICATION;
    
    $arDefaultParams = array(
        "SELECTED_SECTION_VARIABLE" => "CATALOG_SECTION"
    );
    
    $arParams = array_merge($arDefaultParams, $arParams);
    
    /* SEF */
    $arDefaultUrlTemplates404 = array(
    	"section" => "#SECTION_ID#/"
    );
    
    $arTemplateVariables = array(
    	"SECTION_ID",
    	"SECTION_CODE",
    	"ELEMENT_ID",
    	"ELEMENT_CODE"
    );
    
    $arUrlVariables = array();
    
    if  ($arParams["SEF_MODE"] == "Y") {
        $oEngine = new CComponentEngine($this);
        $arUrlTemplates = CComponentEngine::MakeComponentUrlTemplates(array(), $arParams["SEF_URL_TEMPLATES"]);
        $arVariableAliases = CComponentEngine::MakeComponentVariableAliases(array(), $arParams["VARIABLE_ALIASES"]);
        
        $oEngine->guessComponentPath(
    		$arParams["SEF_FOLDER"],
    		$arUrlTemplates,
    		$arUrlVariables
    	);
    } else {
        $arVariableAliases = CComponentEngine::MakeComponentVariableAliases(array(), $arParams["VARIABLE_ALIASES"]);
        CComponentEngine::InitComponentVariables(false, $arTemplateVariables, $arVariableAliases, $arUrlVariables);
    }
    /* -SEF- */
        
    $sSelectedSectionVariable = $arParams['SELECTED_SECTION_VARIABLE'];
    
    if (!empty($sSelectedSectionVariable)) {
        $sSelectedSectionValue = null;
    
        if (!empty($_SESSION[$sSelectedSectionVariable]))
            $sSelectedSectionValue = $_SESSION[$sSelectedSectionVariable];

        if (empty($sSelectedSectionValue) && !empty($arResult['SECTIONS'])) {
            $arFirstSection = current($arResult['SECTIONS']);
            $sSelectedSectionValue = $arFirstSection['ID'];
            $_SESSION[$sSelectedSectionVariable] = $sSelectedSectionValue;
            unset($arFirstSection);
        }
        
        $iCurrentSection = 0;
        if (!empty($arUrlVariables['SECTION_ID']) || !empty($arUrlVariables['SECTION_CODE'])) {
            $arSectionFilter = array(
                "IBLOCK_TYPE" => $arParams['IBLOCK_TYPE'],
                "IBLOCK_ID" => $arParams['IBLOCK_ID']
            );
            
            if (!empty($arUrlVariables['SECTION_ID']))
                $arSectionFilter['ID'] = $arUrlVariables['SECTION_ID'];
                
            if (!empty($arUrlVariables['SECTION_CODE']))
                $arSectionFilter['CODE'] = $arUrlVariables['SECTION_CODE'];
            
            $arCurrentSection = CIBlockSection::GetList(array(), $arSectionFilter)->Fetch();
            
            $iMaxMargin = 0;
            
            if (!empty($arCurrentSection))
                foreach ($arResult['SECTIONS'] as $arSection)
                    if ($arSection['LEFT_MARGIN'] <= $arCurrentSection['LEFT_MARGIN'] && $arCurrentSection['LEFT_MARGIN'] >= $iMaxMargin) {
                        $sSelectedSectionValue = $arSection['ID'];
                        $_SESSION[$sSelectedSectionVariable] = $sSelectedSectionValue;
                        $iMaxMargin = $arSection['LEFT_MARGIN'];
                    }
        }
            
        unset($rsSections);
            
        foreach ($arResult['SECTIONS'] as &$arSection)
            if ($arSection['ID'] == $sSelectedSectionValue)
                $arSection['SELECTED'] = true;      
    }
?>