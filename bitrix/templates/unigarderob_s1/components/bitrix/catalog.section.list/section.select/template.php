<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?$this->setFrameMode(true);?>
<?if(!empty($arResult['SECTIONS'])):?>
	<div class="catalog-section section-select">
        <div class="catalog-section-wrapper">
            <?foreach ($arResult['SECTIONS'] as $arSection):?>
                <?if ($arSection['DEPTH_LEVEL'] != 1) continue;?>
                <div class="catalog-section-item<?=$arSection['SELECTED'] ? ' ui-state-active' : ''?>">
                    <a href="<?=$arSection['SECTION_PAGE_URL']?>" class="catalog-section-item-wrapper">
                        <?=$arSection['NAME']?>
                    </a>
                </div>
            <?endforeach;?>
				<div class="catalog-section-item catalog-section-item--banner">
					<a href="/printbar.php" class="catalog-section-item__link"><img src="/bitrix/templates/unigarderob_s1/images/ban.png" alt="banner"></a>
				</div>
        </div>
    </div>
<?endif;?>