<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?$this->setFrameMode(true)?>
<?if(!empty($arResult)){?>
    <div class="bottom_menu_wrap">
    	<?foreach( $arResult as $arItem ){?>
    		<div class="bottom_menu">
    			<div class="menu_title"><a class="hover_link" href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></div>
    			<?if(!empty($arItem["ITEMS"])){?>
    				<?foreach( $arItem["ITEMS"] as $arSubItem ){?>
    					<div class="menu_item"><a class="hover_link" href="<?=$arSubItem["LINK"]?>"><?=$arSubItem["TEXT"]?></a></div>
    				<?}?>
    			<?}?>
    		</div>
    	<?}?>

		<!-- Social -->
		<div class="bottom_menu">
			<div class="menu_title">
				<a class="hover_link" href="https://wa.me/79776878161?text=Обращение%20с%20сайта%20bee-ru.ru" target="_blank">
					<img src="/upload/medialibrary/whatsapp.svg" alt="whatsapp">
				<a class="hover_link" href="https://www.instagram.com/bee_ru_ru" style="margin-left: 15px;" target="_blank">
					<img src="/upload/medialibrary/insta.svg" alt="instargam">
				</a>
			</div>
		</div>
    </div>
<?}?>