<div class="menu-catalog-left-1-subitem menu-catalog-left-1-subitem-level-<?=$iCurrentLevel?><?=!empty($arMenuItem['SELECTED']) ? ' ui-state-active' : ''?>">
    <a href="<?=$arMenuItem['LINK']?>" class="menu-catalog-left-1-subitem-wrapper ">
        <div class="uni-aligner-vertical"></div>
        <div class="menu-catalog-left-1-text solid_text">
            <?=$arMenuItem['TEXT']?>
        </div>
        <?if (is_numeric($arMenuItem['PARAMS']['ELEMENTS_COUNT'])):?>
            <div class="menu-catalog-left-1-count">
                <?=$arMenuItem['PARAMS']['ELEMENTS_COUNT']?>
            </div>
        <?endif;?>
    </a>
</div>