<?
    $MESS["CATALOG_ADD"] = "В корзину";
    $MESS["CATALOG_ADDED"] = "Добавлено";
    $MESS["CATALOG_ADVANCED"] = "Подробнее";
    $MESS["CATALOG_NOT_AVAILABLE"] = "Нет в наличии";
    $MESS["CATALOG_PRICE_OFFERS"] = "от #PRICE#";
    $MESS["CATALOG_DELETE_ELEMENT"] = "Удалить товар?";
	$MESS["QUICK_VIEW"] = "Быстрый просмотр";
	$MESS['MARK_HIT'] = "Хит";
	$MESS['MARK_NEW'] = "Новинка";
	$MESS['MARK_RECOMEND'] = "Рекомендуем";
?>