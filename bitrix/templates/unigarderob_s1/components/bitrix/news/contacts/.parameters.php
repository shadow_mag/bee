<?
    if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
    
    $arTemplateParameters = array(
    	"MAIN_ELEMENT_ID" => Array(
    		"NAME" => GetMessage("MAIN_ELEMENT_ID"),
    		"TYPE" => "STRING"
    	),
    	"MAIN_ELEMENT_CODE" => Array(
    		"NAME" => GetMessage("MAIN_ELEMENT_CODE"),
    		"TYPE" => "STRING"
    	),
        "SHOW_SECTION_SHOPS" => array(
            "NAME" => GetMessage("SHOW_SECTION_SHOPS"),
            "TYPE" => "CHECKBOX",
            "DEFAULT" => "Y",
            "REFRESH" => "Y" 
        )
    );
    
    if ($arCurrentValues["SHOW_SECTION_SHOPS"] == "Y") {
        $arTemplateParameters["SECTION_ALL_ENABLED"] = Array(
    		"NAME" => GetMessage("SECTION_ALL_ENABLED"),
    		"TYPE" => "CHECKBOX",
    		"DEFAULT" => "Y"
    	);
    }
?>