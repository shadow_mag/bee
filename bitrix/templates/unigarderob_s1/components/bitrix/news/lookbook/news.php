<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>
<?
    $this->setFrameMode(true);
?>
<?$APPLICATION->IncludeComponent("bitrix:catalog.section.list", ".default", Array(
		"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"SECTION_ID" => "",
		"SECTION_CODE" => "",
		"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
		"TOP_DEPTH" => "1",
		"ADD_SECTIONS_CHAIN" => "N",
		"CACHE_TYPE" => $arParams["CACHE_TYPE"],
		"CACHE_TIME" => $arParams["CACHE_TIME"],
		"CACHE_NOTES" => $arParams["CACHE_NOTES"],
		"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
		"HIDE_SECTION_NAME" => "N"
	),
	$component
);?>