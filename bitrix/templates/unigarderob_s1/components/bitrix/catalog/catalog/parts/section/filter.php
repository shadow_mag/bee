<div id="filter" class="filter_catalog_hide">
<?if($arFlags['SHOW_FILTER']):
	$arFilter = array(
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"ACTIVE" => "Y",
		"GLOBAL_ACTIVE" => "Y",
	);
    
	if (intval($arResult["VARIABLES"]["SECTION_ID"]) > 0)
		$arFilter["ID"] = $arResult["VARIABLES"]["SECTION_ID"];  
	elseif (!empty($arResult["VARIABLES"]["SECTION_CODE"]))
		$arFilter["=CODE"] = $arResult["VARIABLES"]["SECTION_CODE"];

	$obCache = new CPHPCache();
    
	if ($obCache->InitCache(36000, serialize($arFilter), "/iblock/catalog")) {
	   $arCurSection = $obCache->GetVars();
	} elseif ($obCache->StartDataCache()) {
		$arCurSection = array();
		if (\Bitrix\Main\Loader::includeModule("iblock")) {
			$dbIBlockSection = CIBlockSection::GetList(array(), $arFilter, false, array("ID"));

			if(defined("BX_COMP_MANAGED_CACHE")) {
				global $CACHE_MANAGER;
				$CACHE_MANAGER->StartTagCache("/iblock/catalog");

				if ($arCurSection = $dbIBlockSection->Fetch())
					$CACHE_MANAGER->RegisterTag("iblock_id_".$arParams["IBLOCK_ID"]);
     
				$CACHE_MANAGER->EndTagCache();
			} else {
				if(!$arCurSection = $dbIBlockSection->Fetch())
				    $arCurSection = array();
			}
		}
        
		$obCache->EndDataCache($arCurSection);
	}
    
	if (!isset($arCurSection))
		$arCurSection = array();
    
    $APPLICATION->IncludeComponent(
		"bitrix:catalog.smart.filter",
		".default",
		array(
			"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
			"SECTION_ID" => $arCurSection['ID'],
			"FILTER_NAME" => $arParams["FILTER_NAME"],
			"PRICE_CODE" => $arParams["PRICE_CODE"],
			"CACHE_TYPE" => $arParams["CACHE_TYPE"],
			"CACHE_TIME" => $arParams["CACHE_TIME"],
			"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
			"SAVE_IN_SESSION" => "N",
			"XML_EXPORT" => "Y",
			"SECTION_TITLE" => "NAME",
			"SECTION_DESCRIPTION" => "DESCRIPTION",
			'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],
			"POPUP_POSITION" => "right",
			'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
			'CURRENCY_ID' => $arParams['CURRENCY_ID'],
			"SEF_MODE" => $arParams["SEF_MODE"],
			"SEF_RULE" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["smart_filter"],
			"SMART_FILTER_PATH" => $arResult["VARIABLES"]["SMART_FILTER_PATH"],
			"PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
		),
		$component,
		array('HIDE_ICONS' => 'Y')
	);
    
    unset($arFilter, $arCurSection, $dbIBlockSection);
endif;?>
</div> 
<div id="uni-filter-show" class="uni-filter uni-btn-hide uni-btn-show"><?=GetMessage("SECTION_SHOW_FILTER");?><i data-role="prop_angle" class="right fa fa-angle-down"></i></div>
<div id="uni-filter-hide" class="uni-filter uni-btn-hide"><?=GetMessage("SECTION_HIDE_FILTER");?><i data-role="prop_angle" class="right fa fa-angle-up"></i></div>
<script>
	$( "#uni-filter-show").click(function() {
		$('#filter').show(1000);
		$('#uni-filter-hide').show();
		$('#uni-filter-show').hide();
	});
	$( "#uni-filter-hide").click(function() {
		$('#filter').hide(1000);
		$('#uni-filter-hide').hide();
		$('#uni-filter-show').show();
	});
	if ($(window).width() <= '800'){
		$('.filter_catalog_hide').hide();
	} else {
		$('.filter_catalog_hide').show();
	}
	$(window).resize(function() {
		if ($(window).width() <= '800'){
			$('.filter_catalog_hide').hide();
		} else {
			$('.filter_catalog_hide').show();
		}
	});
</script>