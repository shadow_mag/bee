<div id="menu" class="menu_catalog_hide">
<?if ($options["TYPE_MENU"]["ACTIVE_VALUE"] == 'catalog'):?>
    <div class="catalog-section-menu catalog-section-menu-all">
        <?$APPLICATION->IncludeComponent(
            "bitrix:menu",
            "catalog.left.1",
            array(
        		"ROOT_MENU_TYPE" => "catalog",
        		"MENU_CACHE_TYPE" => "N",
        		"MENU_CACHE_TIME" => "3600",
        		"MENU_CACHE_USE_GROUPS" => "Y",
        		"MENU_CACHE_GET_VARS" => array(
        		),
        		"MAX_LEVEL" => "5",
        		"CHILD_MENU_TYPE" => "catalog",
        		"USE_EXT" => "Y",
        		"DELAY" => "N",
        		"ALLOW_MULTI_SELECT" => "N",
        		"HIDE_CATALOG" => "Y"
        	),
        	false
        );?>
    </div>
    <div class="catalog-section-menu catalog-section-menu-current">
        <?$APPLICATION->IncludeComponent(
            "bitrix:menu",
            "catalog.left.1",
            array(
        		"ROOT_MENU_TYPE" => "section",
        		"MENU_CACHE_TYPE" => "N",
        		"MENU_CACHE_TIME" => "3600",
        		"MENU_CACHE_USE_GROUPS" => "Y",
        		"MENU_CACHE_GET_VARS" => array(
        		),
        		"MAX_LEVEL" => "5",
        		"CHILD_MENU_TYPE" => "catalog",
        		"USE_EXT" => "Y",
        		"DELAY" => "N",
        		"ALLOW_MULTI_SELECT" => "N",
        		"HIDE_CATALOG" => "Y"
        	),
        	false
        );?>
    </div>
<?else:?>
    <div class="catalog-section-menu">
        <?$APPLICATION->IncludeComponent(
            "bitrix:menu",
            "catalog.left.1",
            array(
        		"ROOT_MENU_TYPE" => "catalog",
        		"MENU_CACHE_TYPE" => "N",
        		"MENU_CACHE_TIME" => "3600",
        		"MENU_CACHE_USE_GROUPS" => "Y",
        		"MENU_CACHE_GET_VARS" => array(
        		),
        		"MAX_LEVEL" => "5",
        		"CHILD_MENU_TYPE" => "catalog",
        		"USE_EXT" => "Y",
        		"DELAY" => "N",
        		"ALLOW_MULTI_SELECT" => "N",
        		"HIDE_CATALOG" => "Y"
        	),
        	false
        );?>
    </div>
<?endif;?>
</div> 
<div id="uni-menu-show" class="uni-menu uni-btn-hide uni-btn-show"><?=GetMessage("SECTION_SHOW_MENU");?><i data-role="prop_angle" class="right fa fa-angle-down"></i></div>
<div id="uni-menu-hide" class="uni-menu uni-btn-hide"><?=GetMessage("SECTION_HIDE_MENU");?><i data-role="prop_angle" class="right fa fa-angle-up"></i></div>
<script>
	$( "#uni-menu-show").click(function() {
		$('#menu').show(1000);
		$('#uni-menu-hide').show();
		$('#uni-menu-show').hide();
	});
	$( "#uni-menu-hide").click(function() {
		$('#menu').hide(1000);
		$('#uni-menu-hide').hide();
		$('#uni-menu-show').show();
	});
	if ($(window).width() <= '800'){
		$('.menu_catalog_hide').hide();
	} else {
		$('.menu_catalog_hide').show();
	}
	$(window).resize(function() {
		if ($(window).width() <= '800'){
			$('.menu_catalog_hide').hide();
		} else {
			$('.menu_catalog_hide').show();
		}
	});
</script>