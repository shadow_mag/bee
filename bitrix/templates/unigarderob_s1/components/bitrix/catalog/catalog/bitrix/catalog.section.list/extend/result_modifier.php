<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
    if (CModule::IncludeModule('iblock'))
        foreach ($arResult['SECTIONS'] as &$arSection) {
            $dbSubSections = CIBlockSection::GetList(array(), array(
                "SECTION_ID" => $arSection['ID'],
                "DEPTH_LEVEL" => $arSection['DEPTH_LEVEL'] + 1,
				"ACTIVE" => "Y"
            ));
            
            
                        
            $arSection['SECTIONS'] = array();
            
            while ($arSubSection = $dbSubSections->GetNext()) {
                $arPanelButtons = CIBlock::GetPanelButtons(
                    $arSubSection['IBLOCK_ID'],
                    0,
                    $arSubSection['ID'],
                    array("SESSID"=>false, "CATALOG"=>true)
                );
                
                $arSubSection['EDIT_LINK'] = $arPanelButtons['edit']['edit_section']['ACTION_URL'];
                $arSubSection['DELETE_LINK'] = $arPanelButtons['edit']['delete_section']['ACTION_URL'];
                
                $arSection['SECTIONS'][] = $arSubSection;
            }
                
        }
?>