<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
switch($arParams["LINE_ELEMENT_COUNT"]){
	case "1":
		$class_grid = "uni-100";
	break;
	case "2":
		$class_grid = "uni-50";
	break;
	case "3":
		$class_grid = "uni-33";
	break;
	case "4":
		$class_grid = "uni-25";
	break;
	case "5":
		$class_grid = "uni-20";
	break;
	case "6":
		$class_grid = "uni-16.6";
	break;
	default:
		$class_grid = "uni-33";
	break;		
}
$this->setFrameMode(true);?>
<?if(is_array($arResult["ITEMS"]) && !empty($arResult["ITEMS"])){ ?>	
	<div class="main_news_pictures">
		<?$href = str_replace("#SITE_DIR#/", SITE_DIR, $arResult["LIST_PAGE_URL"]);?>
		<h3 class="header_grey"><?=$arParams["TITLE"]?></h3>			
		<?foreach($arResult["ITEMS"] as $arItem):?>
			<?
			$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
			$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
			?>
			<div class="uni_col <?=$class_grid?> news-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">		
				<?if(is_array($arItem["PREVIEW_PICTURE"])){?>
					<?$file = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"],array('width'=>85, 'height'=>85));
					$src = $file['src'];?>
					<div class="left_col">
						<a href="<?echo $arItem["DETAIL_PAGE_URL"]?>" class="img_review" style='display:block;background-image:url(<?=$src?>)'></a>					
					</div>
				<?}?>		
				<div class="right_col">
					<a class="name_news" href="<?echo $arItem["DETAIL_PAGE_URL"]?>"><?echo TruncateText($arItem["NAME"],100)?></a>	
					<?if($arParams["DISPLAY_DATE"]!="N" && $arItem["DISPLAY_ACTIVE_FROM"]):?>
						<div class="news-date-time"><?echo $arItem["DISPLAY_ACTIVE_FROM"]?></div>
					<?endif?>	
				</div>
				<div class="clear"></div>
			</div>
		<?endforeach;?>
		<div class="see_all"><a href="<?=$href?>"><?=GetMessage("ALL_NEWS_PICTURES")?>&nbsp;<span style="font-size:16px;">&#8250;</span></a></div>
		<div class="clearfix"></div>
	</div> 	
<?}?>

