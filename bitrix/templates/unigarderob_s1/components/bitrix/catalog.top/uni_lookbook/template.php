<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>
<?global $options;?>
<?$sUniqueID = 'uni_lookbook'.spl_object_hash($this);?>
<?
    $iGrid = intval($arParams['LINE_ELEMENT_COUNT']);
    
    if ($iGrid < 1) $iGrid = 1;
    if ($iGrid > 6) $iGrid = 6;
    
    $arGrid = array(
        '1' => 'uni-100',
        '2' => 'uni-50',
        '3' => 'uni-33',
        '4' => 'uni-25',
        '5' => 'uni-20',
        '6' => 'uni-16'
    );
    
    $arModifiers = array();
    
    if ($arParams['USE_SLIDER'] == 'Y') $arModifiers[] = 'with-slider';
    if ($arParams['USE_SLIDER'] && $arParams['USE_SLIDER_ARROWS'] == 'Y') $arModifiers[] = 'with-slider-arrows';
    if ($arParams['USE_SLIDER'] && $arParams['USE_SLIDER_POINTS'] == 'Y') $arModifiers[] = 'with-slider-points';
    if ($arParams['USE_SLIDER'] && $arParams['USE_SLIDER_POINTS'] == 'Y') $arModifiers[] = 'with-slider-points';
    if (!empty($arParams['TITLE'])) $arModifiers[] = 'with-title';
?>
<?if (!empty($arResult['ITEMS'])):?>
    <?$frame = $this->createFrame()->begin();?>
    <div class="uni_lookbook<?=!empty($arModifiers) ? ' '.implode(' ', $arModifiers) : ''?>" id="<?=$sUniqueID?>">
        <?if (!empty($arParams['TITLE'])):?>
            <div class="header_grey">
                <?=$arParams['TITLE'];?>
            </div>
        <?endif;?>
        <?if ($arParams['USE_SLIDER_ARROWS'] == "Y"):?>
            <div class="buttons">
                <div class="button button-left uni-slider-button-small uni-slider-button-left" onclick="return $<?=$sUniqueID?>.SlideRight();">
                    <div class="icon"></div>
                </div>
        		<div class="button button-right uni-slider-button-small uni-slider-button-right" onclick="return $<?=$sUniqueID?>.SlideLeft();">
                    <div class="icon"></div>
                </div>	
            </div>
            <div class="clear"></div>
        <?endif;?>
        <div class="indents">
            <div class="indents-wrapper">
                <div class="slider">
                    <?
                		$strElementEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT");
                		$strElementDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE");
                		$arElementDeleteParams = array("CONFIRM" => GetMessage('CT_BCS_TPL_ELEMENT_DELETE_CONFIRM'));
                    ?>
            		<?foreach($arResult["ITEMS"] as $arElement):
            			$this->AddEditAction($arElement['ID'], $arElement['EDIT_LINK'], $strElementEdit);
            			$this->AddDeleteAction($arElement['ID'], $arElement['DELETE_LINK'], $strElementDelete, $arElementDeleteParams);
            		?>
                        <div class="element <?=$arGrid[$iGrid]?>">
                            <div class="element-wrapper hover_shadow" id="<?=$this->GetEditAreaId($arElement['ID'])?>">
                                <a class="marks fancy" href="<?=$arElement['DETAIL_PICTURE']['SRC']?>">
                                    <?if(!empty($arElement["MIN_PRICE"]["DISCOUNT_DIFF_PERCENT"])){?>
                                        <span class="mark action">-<?=$arElement["MIN_PRICE"]["DISCOUNT_DIFF_PERCENT"];?>%</span>
                    				<?}?>
                                </a>
                                <div class="image">
                                    <a class="image-wrapper uni-image fancy" href="<?=$arElement['DETAIL_PICTURE']['SRC']?>">
                                        <div class="uni-indents-vertical"></div>
                                        <img src="<?=$arElement['PICTURE']?>" alt="<?=htmlspecialcharsbx($arElement['NAME'])?>" title="<?=htmlspecialcharsbx($arElement['NAME'])?>"/>
                                    </a>
                                    <a class="uni-button-quickview fancy" href="<?=$arElement['DETAIL_PICTURE']['SRC']?>">
										<div class="model-name"><?=$arElement['NAME'];?></div>
									</a>
									<a class="loop fancy" href="<?=$arElement['DETAIL_PICTURE']['SRC']?>">
										<i></i>
									</a>
                                </div>
       
                            </div>
                        </div>
                    <?endforeach;?>
                </div>
            </div>
        </div>
        <?if ($arParams['USE_SLIDER_POINTS'] == "Y"):?>
            <div class="listener" style="display: none;">
                <div class="listener-wrapper">
                </div>
            </div>
        <?endif;?>
    </div>
    <?if ($arParams['USE_SLIDER'] == "Y"):?>
        <script type="text/javascript">					
            var $<?=$sUniqueID?> = new UNISlider({
                'OFFSET': <?=$iGrid?>,
                'SLIDER': '#<?=$sUniqueID?> .slider',
                'ELEMENT': '#<?=$sUniqueID?> .slider .element',
                'ANIMATE': true,
                'ANIMATE_SPEED': 400,
                'EVENTS': {
                    'onAdaptabilityChange': function ($oSlider) {
                        <?if ($options['ADAPTIV']['ACTIVE_VALUE'] == 'Y'):?>
                            $oSlider.Settings.OFFSET = Math.round($oSlider.GetSliderWidth() / $oSlider.GetElementWidth());
                        <?endif;?>
                        
                        <?if ($arParams['USE_SLIDER_ARROWS'] == "Y"):?>
                            var $oListenerContainer = $('#<?=$sUniqueID?> .listener .listener-wrapper');
                            var $iDisplayedItems = $oSlider.Settings.OFFSET;
                            var $iCountItems = $oSlider.GetElementsCount();
                            var $iListenerButtons = Math.floor($iCountItems / $iDisplayedItems);
                            
                            if ($iDisplayedItems < $iCountItems) {
                                $('#<?=$sUniqueID?> .buttons').show();
                            } else {
                                $('#<?=$sUniqueID?> .buttons').hide();
                            }
                        <?endif;?>
                        
                        <?if ($arParams['USE_SLIDER_POINTS'] == "Y"):?>
                            if ($iDisplayedItems * 2 <= $iCountItems) {
                                $('#<?=$sUniqueID?> .listener').show();
                            } else {
                                $('#<?=$sUniqueID?> .listener').hide();
                            }
                            
                            $oListenerContainer.empty();
                            if ($iListenerButtons > 0) {
                                var $iCurrentPage = Math.floor($oSlider.GetCurrentSlide() / $oSlider.Settings.OFFSET);
                                
                                for (var $i = 0; $i < $iListenerButtons; $i++) {
                                    var $iNumber = $i * $iDisplayedItems;
                                    $('<div class="button" slide="' + $iNumber + '"></div>').click(function() {
                                        $oSlider.SlideTo($(this).attr('slide'));
                                    }).appendTo($oListenerContainer)
                                }
                                
                                $oListenerContainer.children()
                                    .removeClass('state-active')
                                    .eq($iCurrentPage)
                                    .addClass('state-active');
                            }
                        <?endif;?>
                    },
                    'onAfterSlide': function ($oSlider, $oSettings) {
                        <?if ($arParams['USE_SLIDER_POINTS'] == "Y"):?>
                            var $oListenerContainer = $('#<?=$sUniqueID?> .listener .listener-wrapper');
                            var $iCurrentPage = Math.floor($oSettings.SLIDE.NEXT / $oSlider.Settings.OFFSET);
                            
                            $oListenerContainer.children()
                                .removeClass('state-active')
                                .eq($iCurrentPage)
                                .addClass('state-active');
                        <?endif;?>
                    }            
                } 
            });
        </script>
    <?endif;?>
    <?$frame->end();?>
<?endif;?>
