<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>
<?$this->setFrameMode(true);?>
<?$sUniqueID = 'uni-labels-2'.spl_object_hash($this);?>
<?if (empty($arResult["LABELS"])) {?>
	<div class="uni-labels-2" id="<?=$sUniqueID?>">
		<div class="uni-labels-wrapper" id="<?=$sUniqueID.'tabs'?>">
			<div class="tabs">
				<h3 class="header_grey">
					Lookbook
				</h3>
			</div>
            <div style="padding-top: 15px; clear: both;"></div>
<?print_r($arResult);?>
			<?foreach($arResult["LABELS"] as $arLabel){?>
				<div id="<?=$sUniqueID.'tab'.$arLabel["XML_ID"]?>">
					<?$GLOBALS["arr".$arLabel["XML_ID"]] = array("PROPERTY_".$arLabel["PROPERTY_CODE"]."_VALUE" => $arLabel["VALUE"]);?>
					<?$APPLICATION->IncludeComponent(
						"bitrix:catalog.top", 
						"uni_popular", 
						array(
							"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
							"IBLOCK_ID" => $arParams["IBLOCK_ID"],
							"ELEMENT_SORT_FIELD" => "RAND",
							"ELEMENT_SORT_ORDER" => "asc",
							"FILTER_NAME" => "arr".$arLabel["XML_ID"],
							"HIDE_NOT_AVAILABLE" => "N",
							"ELEMENT_COUNT" => $arParams["ELEMENT_COUNT"],
							"LINE_ELEMENT_COUNT" => $arParams["LINE_ELEMENT_COUNT"],
							"OFFERS_SORT_FIELD" => "sort",
                    		"OFFERS_SORT_ORDER" => "asc",
                    		"OFFERS_SORT_FIELD2" => "id",
                    		"OFFERS_SORT_ORDER2" => "desc",
                    		"OFFERS_LIMIT" => "0",
                            "OFFERS_PROPERTY_CODE" => $arParams['OFFERS_PROPERTY_CODE'],
                            "PRICE_CODE" => $arParams['PRICE_CODE'],
							"CACHE_TYPE" => "A",
							"CACHE_TIME" => "36000000",
							"CACHE_GROUPS" => "Y",
							"DISPLAY_COMPARE" => $arParams["DISPLAY_COMPARE"],
							"COMPARE_NAME" => $arParams["COMPARE_NAME"],
							"SEF_MODE" => "N",
                            "USE_SLIDER" => "Y",
                            "USE_SLIDER_ARROWS" => "Y",
                            "USE_SLIDER_POINTS" => "Y",
						),
						false,
						array("HIDE_ICONS" => "Y")
					);?>
				</div>
			<?}?>
		</div>
	</div>
	<script>
		$(document).ready(function(){
			$("#<?=$sUniqueID.'tabs'?>").tabs();
		})
	</script>
<?}?>
