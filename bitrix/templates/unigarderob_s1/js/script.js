$(document).ready(function() {
    resize();
    function resize() {
        var size = $(".bg_footer").outerHeight();
        $("body").css("padding-bottom", size + 50 + "px");
    }
    $(window).resize(function() {
        resize();
    });
    $(".fancy").fancybox();
    $("input[name='PERSONAL_PHONE']").mask("+7 (999) 999-9999");
    $(".button_up").click(function() {
        $("body, html").animate(
            {
                scrollTop: 0
            },
            1000
        );
    });

    var modalSubscr = document.querySelector(".modal-subscribe");
    var urlPage = window.location.href;
    var activeSubscr = 0;
    urlPage = urlPage.split("/");

    urlPage.forEach(el => {
        if (el == "subscribe") {
            activeSubscr = 1;
        }
    });

    if (activeSubscr == 0) {
        if (!$.cookie("subscribe")) {
            setTimeout(function() {
                if (!!modalSubscr) {
                    modalSubscr.classList.add("active");

                    var modalSubscrClose = document.querySelector(
                        ".modal-subscribe__close"
                    );
                    modalSubscrClose.addEventListener("click", function() {
                        modalSubscr.classList.remove("active");
                    });

                    var modalSubscrMask = document.querySelector(
                        ".modal-subscribe__mask"
                    );
                    modalSubscrMask.addEventListener("click", function() {
                        modalSubscr.classList.remove("active");
                    });

                    $.cookie("subscribe", "1", {
                        expires: 30,
                        path: "/"
                    });
                }
            }, 180000);
        }
    }

	//cookie
    if (!$.cookie("cook")) {
        $(".cookie-block").addClass("active");
        $(".cookie-close").click(function() {
            $.cookie("cook", "1", {
                expires: 30,
                path: "/"
            });
            $(".cookie-block").removeClass("active");
        });
    }
});
$(window).scroll(function() {
    var top_show = 300;
    if ($(this).scrollTop() > top_show) {
        $(".button_up").fadeIn();
    } else {
        $(".button_up").fadeOut();
    }
});
