var SaleActions = {};

SaleActions.__isFunction = function ($oFunction) {
    return Object.prototype.toString.call($oFunction) === '[object Function]';
}

SaleActions.addToCompare = function ($sCompareUrl, $fCallback) {
    $.ajax({
		url: $sCompareUrl,
		type: "GET"
	}).done(function ($sResponse) {
	   if (SaleActions.__isFunction($fCallback)) $fCallback($sResponse);
	});
}

SaleActions.removeFromCompare = function ($sCompareUrl, $fCallback) {
    $.ajax({
		url: $sCompareUrl,
		type: "GET"
	}).done(function ($sResponse) {
	   if (SaleActions.__isFunction($fCallback)) $fCallback($sResponse);
	});
}

SaleActions.addToWishList = function ($iProductID, $sCurrency, $iQuantity, $fCallback) {
    $.post(
        SITE_TEMPLATE_PATH + '/ajax/sale.actions.php', {
            "ACTION": "AddToWishList",
            "ID": $iProductID,
            "CURRENCY": $sCurrency,
            "QUANTITY": $iQuantity,
            "SITE_ID": SITE_ID,
            "SITE_DIR": SITE_DIR
    }).done(function ($sResponse) {
        if (SaleActions.__isFunction($fCallback)) $fCallback($sResponse);
    });
}

SaleActions.removeFromWishList = function ($iProductID, $fCallback) {
    $.post(
        SITE_TEMPLATE_PATH + '/ajax/sale.actions.php', {
            "ACTION": "RemoveFromWishList",
            "ID": $iProductID,
            "SITE_ID": SITE_ID,
            "SITE_DIR": SITE_DIR
    }).done(function ($sResponse) {
        if (SaleActions.__isFunction($fCallback)) $fCallback($sResponse);
    });
}

SaleActions.addToBasket = function ($iProductID, $sCurrency, $iQuantity, $fCallback) {
    $.post(
        SITE_TEMPLATE_PATH + '/ajax/sale.actions.php', {
            "ACTION": "AddToBasket",
            "ID": $iProductID,
            "CURRENCY": $sCurrency,
            "QUANTITY": $iQuantity,
            "SITE_ID": SITE_ID,
            "SITE_DIR": SITE_DIR
    }).done(function ($sResponse) {
        if (SaleActions.__isFunction($fCallback)) $fCallback($sResponse);
    });
}

SaleActions.removeFromBasket = function ($iProductID, $fCallback) {
    $.post(
        SITE_TEMPLATE_PATH + '/ajax/sale.actions.php', {
            "ACTION": "RemoveFromBasket",
            "ID": $iProductID,
            "SITE_ID": SITE_ID,
            "SITE_DIR": SITE_DIR
    }).done(function ($sResponse) {
        if (SaleActions.__isFunction($fCallback)) $fCallback($sResponse);
    });
}

SaleActions.setQuantity = function ($iProductID, $iQuantity, $fCallback) {
    $.post(
        SITE_TEMPLATE_PATH + '/ajax/sale.actions.php', {
            "ACTION": "SetQuantity",
            "ID": $iProductID,
            "QUANTITY": $iQuantity,
            "SITE_ID": SITE_ID,
            "SITE_DIR": SITE_DIR
    }).done(function ($sResponse) {
        if (SaleActions.__isFunction($fCallback)) $fCallback($sResponse);
    });
}

SaleActions.updateButtons = function ($fCallback) {
    $.post(
        SITE_TEMPLATE_PATH + '/ajax/sale.actions.php', {
            "ACTION": "UpdateButtons",
            "SITE_ID": SITE_ID,
            "SITE_DIR": SITE_DIR
    }).done(function ($sResponse) {
        if (SaleActions.__isFunction($fCallback)) $fCallback($sResponse);
    });
}

SaleActions.updateCompare = function ($fCallback) {
    $.post(
        SITE_TEMPLATE_PATH + '/ajax/sale.actions.php', {
            "ACTION": "UpdateCompare",
            "SITE_ID": SITE_ID,
            "SITE_DIR": SITE_DIR
    }).done(function ($sResponse) {
        if (SaleActions.__isFunction($fCallback)) $fCallback($sResponse);
    });
	$.post(
        SITE_TEMPLATE_PATH + '/ajax/sale.actions.php', {
            "ACTION": "UpdateCompareFly",
            "SITE_ID": SITE_ID,
            "SITE_DIR": SITE_DIR
    }).done(function ($sResponse) {
        SaleWrapper.updateCompareFly($sResponse);
    });
}

SaleActions.updateBasket = function ($fCallback) {
    $.post(
        SITE_TEMPLATE_PATH + '/ajax/sale.actions.php', {
            "ACTION": "UpdateBasket",
            "SITE_ID": SITE_ID,
            "SITE_DIR": SITE_DIR
    }).done(function ($sResponse) {
        $('.b_basket').replaceWith($sResponse);
        if (SaleActions.__isFunction($fCallback)) $fCallback($sResponse);
    });
}