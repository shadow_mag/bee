<?require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?$APPLICATION->ShowAjaxHead();?>
<?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/ajax/popup.content.tableofsizes.css');?>
<?IncludeTemplateLangFile(__FILE__);?>
<?$arTable = array(
    array('40', '74-80', '60-65', '84-90', 'XS', '32'),
    array('42', '82-85', '66-69', '92-95', 'XS', '34'),
    array('44', '86-89', '70-73', '96-98', 'S', '36'),
    array('46', '90-93', '74-77', '99-101', 'M', '38'),
    array('48', '94-97', '78-71', '102-104', 'M', '40'),
    array('50', '98-102', '82-85', '105-108', 'L', '42'),
    array('52', '103-107', '86-90', '109-112', 'XL', '44'),
    array('54', '108-113', '91-95', '113-116', 'XL', '46'),
    array('56', '114-119', '96-102', '117-121', 'XXL', '48'),
    array('58', '120-125', '103-108', '122-126', 'XXXL', '50'),
    array('60', '126-131', '109-114', '127-132', 'XXXL', '52'),
    array('62', '132-137', '115-121', '133-138', 'XXXL', '54'),
    array('64', '138-143', '122-128', '139-144', 'XXXL', '56'),
    array('66', '144-149', '129-134', '145-150', 'XXXL', '58')
);?>
<div class="garderob-table-sizes">
    <div class="garderob-table-sizes-title"><?=GetMessage('GTZ_TITLE')?></div>
    <div class="garderob-table-sizes-wrapper">
        <table class="garderob-table-sizes-table">
            <tr class="garderob-table-sizes-row garderob-table-sizes-row-header">
                <td class="garderob-table-sizes-column">
                    <div class="garderob-table-sizes-cell">
                        <?=GetMessage('GTZ_HEADER_SIZE_RU')?>
                    </div>
                </td>
                <td class="garderob-table-sizes-column">
                    <div class="garderob-table-sizes-cell">
                        <?=GetMessage('GTZ_HEADER_BUST')?>
                    </div>
                </td>
                <td class="garderob-table-sizes-column">
                    <div class="garderob-table-sizes-cell">
                        <?=GetMessage('GTZ_HEADER_WAIST')?>
                    </div>
                </td>
                <td class="garderob-table-sizes-column">
                    <div class="garderob-table-sizes-cell">
                        <?=GetMessage('GTZ_HEADER_GIRTH')?>
                    </div>
                </td>
                <td class="garderob-table-sizes-column">
                    <div class="garderob-table-sizes-cell">
                        <?=GetMessage('GTZ_HEADER_SISE_INTERNATIONAL')?>
                    </div>
                </td>
                <td class="garderob-table-sizes-column">
                    <div class="garderob-table-sizes-cell">
                        <?=GetMessage('GTZ_HEADER_SISE_EU')?>
                    </div>
                </td>
            </tr>
            <?$bRowEven = true;?>
            <?foreach ($arTable as $arRow):?>
                <tr class="garderob-table-sizes-row<?=$bRowEven ? ' garderob-table-sizes-row-even' : ' garderob-table-sizes-row-odd'?>">
                    <td class="garderob-table-sizes-column">
                        <div class="garderob-table-sizes-cell">
                            <?=$arRow[0]?>
                        </div>
                    </td>
                    <td class="garderob-table-sizes-column">
                        <div class="garderob-table-sizes-cell">
                            <?=$arRow[1]?>
                        </div>
                    </td>
                    <td class="garderob-table-sizes-column">
                        <div class="garderob-table-sizes-cell">
                            <?=$arRow[2]?>
                        </div>
                    </td>
                    <td class="garderob-table-sizes-column">
                        <div class="garderob-table-sizes-cell">
                            <?=$arRow[3]?>
                        </div>
                    </td>
                    <td class="garderob-table-sizes-column">
                        <div class="garderob-table-sizes-cell">
                            <?=$arRow[4]?>
                        </div>
                    </td>
                    <td class="garderob-table-sizes-column">
                        <div class="garderob-table-sizes-cell">
                            <?=$arRow[5]?>
                        </div>
                    </td>
                </tr>
                <?$bRowEven = !$bRowEven;?>
            <?endforeach;?>
        </table>
    </div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");?>