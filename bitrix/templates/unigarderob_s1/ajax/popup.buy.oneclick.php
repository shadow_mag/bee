<?require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$APPLICATION->ShowAjaxHead();?>
<?if (!CModule::IncludeModule('sale') || !CModule::IncludeModule('catalog')) die()?>
<?$_POST = array(
    "IBLOCK_TYPE" => $APPLICATION->ConvertCharset($_POST['IBLOCK_TYPE'], "utf-8", SITE_CHARSET),
    "IBLOCK_ID" => $APPLICATION->ConvertCharset($_POST['IBLOCK_ID'], "utf-8", SITE_CHARSET),
    "ELEMENT_ID" => $APPLICATION->ConvertCharset($_POST['ELEMENT_ID'], "utf-8", SITE_CHARSET),
    "IMAGE" => $APPLICATION->ConvertCharset($_POST['IMAGE'], "utf-8", SITE_CHARSET),
    "NAME_PRODUCT" => $APPLICATION->ConvertCharset($_POST['NAME_PRODUCT'], "utf-8", SITE_CHARSET),
    "NEW_PRICE" => $APPLICATION->ConvertCharset($_POST['NEW_PRICE'], "utf-8", SITE_CHARSET),
    "OLD_PRICE" => $APPLICATION->ConvertCharset($_POST['OLD_PRICE'], "utf-8", SITE_CHARSET),
    "CURRENCY" => $APPLICATION->ConvertCharset($_POST['CURRENCY'], "utf-8", SITE_CHARSET),
    "PRICE_ID" => $APPLICATION->ConvertCharset($_POST['PRICE_ID'], "utf-8", SITE_CHARSET),
    "PRICE" => $APPLICATION->ConvertCharset($_POST['PRICE'], "utf-8", SITE_CHARSET),
);?>
<?$APPLICATION->IncludeComponent("intec:oneclickbuy", "shop", array(
		"IBLOCK_TYPE" => $_POST["IBLOCK_TYPE"],
		"IBLOCK_ID" => $_POST["IBLOCK_ID"],
		"ELEMENT_ID" => $_POST["ELEMENT_ID"],
		"USE_QUANTITY" => "N",
		"SEF_FOLDER" => SITE_DIR."catalog/",
		"PROPERTIES" => array(
			0 => "USER_NAME",
			1 => "PHONE",
			2 => "COMMENT"
		),
		"REQUIRED" => array(
			0 => "USER_NAME",
			1 => "PHONE",
		),
		"DEFAULT_PERSON_TYPE" => "",
		"DEFAULT_DELIVERY" => "0",
		"DEFAULT_PAYMENT" => "0",
		"DEFAULT_CURRENCY" => CCurrency::GetBaseCurrency(),
		"PRICE_ID" => $_POST["PRICE_ID"],
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000",
		"USE_SKU" => "Y",
		"IMAGE" => $_POST["IMAGE"],
		"NAME_PRODUCT" => $_POST["NAME_PRODUCT"],
		"NEW_PRICE" =>$_POST["NEW_PRICE"],
		"OLD_PRICE" => $_POST["OLD_PRICE"],
		"PRICE" => $_POST["PRICE"]
	),
	false
);?> 
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");?>