<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Условия оплаты");
?><b><span style="font-size: 14pt;"><span style="font-size: 12pt;">Какой бы способ доставки вы ни выбрали, вы сможете оплатить заказ: </span><br>
 <span style="font-size: 12pt;"> </span></span></b><br>
 <span style="font-size: 12pt;"> </span><br>
 <span style="font-size: 12pt;"> </span><b><span style="font-size: 14pt;"><span style="font-size: 12pt;"> </span><span style="font-size: 12pt;">Наличными</span></span></b><span style="font-size: 12pt;"> </span>
<h2 style="text-align: justify;"></h2>
 <span style="font-size: 12pt;"> </span>
<h4><span style="font-size: 12pt;"> </span>
<div>
 <span style="font-size: 12pt;"> </span>
	<p style="text-align: justify;">
 <span style="font-size: 12pt;"> </span><span style="font-weight: normal;"><span style="font-size: 12pt;">Оплатить товар наличными вы можете и при курьерской доставке, и если решили забрать товар из нашего шоурума</span><span style="line-height: 16px; text-align: justify; font-size: 12pt;">&nbsp;</span><span style="font-size: 12pt;">самоcтоятельно.</span></span><span style="font-size: 12pt;"> </span>
	</p>
 <span style="font-size: 12pt;"> </span>
	<p style="text-align: justify;">
 <span style="font-size: 12pt;"> </span><span style="font-weight: normal;"><br>
 <span style="font-size: 12pt;"> </span></span><span style="font-size: 12pt;"> </span>
	</p>
 <span style="font-size: 12pt;"> </span>
</div>
 <span style="font-size: 12pt;"> </span></h4>
 <span style="font-size: 12pt;"> </span>
<div>
 <span style="font-size: 12pt;"> </span><b><span style="font-size: 12pt;">Банковской картой<br>
	<br>
	</span></b><span style="font-size: 12pt;"> </span>
</div>
 <span style="font-size: 12pt;"> </span>
<p style="text-align: justify;">
 <span style="font-size: 12pt;"> </span>
</p>
 <span style="font-size: 12pt;"> </span>
<div>
 <span style="font-size: 12pt;">
	К оплате принимаются банковские карты платежных систем Visa и MasterCard. Услуга бесплатная, никаких дополнительных процентов вы не платите. <br>
 </span><br>
 <br>
 <span style="font-size: 12pt;">
	Вы можете оплатить свой заказ банковской картой Visa или MasterCard через процессинговый центр, осуществляющий интернет эквайринг. После подтверждения заказа вы будете перенаправлены на защищенную платежную страницу, где вам необходимо ввести данные вашей банковской карты. После успешной оплаты вы получите электронный чек. Информация, указанная в чеке, содержит все данные о проведенной платежной транзакции. <br>
 <br>
 </span>
</div>
<p style="text-align: justify;">
 <span style="font-size: 12pt;"> </span>
</p>
 <span style="font-size: 12pt;"> </span>
<p>
 <span style="font-size: 12pt;"> </span>
</p>
 <span style="font-size: 12pt;"> </span>
<p>
 <span style="font-size: 12pt;"> </span><span style="font-weight: normal;"><span style="font-size: 13px;"><b><span style="font-size: 12pt;">Гарантии безопасности </span></b><span style="font-size: 12pt;"> </span><br>
 <span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span></span></span><span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span>
</p>
<p>
 <span style="font-weight: normal;"><span style="font-size: 13px;"><b><span style="font-size: 12pt;"><br>
 </span></b></span></span>
</p>
 <span style="font-size: 12pt;">
Сервис-провайдер защищает данные вашей банковской карты, подтвержденные соответствием стандарту безопасности PCI DSS 2.2. Данные карты вводятся на защищенной платежной странице, передача информации в процессинговый центр происходит с применением технологии шифрования SSL. Дальнейшая передача информации происходит по закрытым банковским сетям, имеющим наивысший уровень надежности. Процессинговый центр не передает данные вашей карты магазину и иным третьим лицам. Для дополнительной аутентификации держателя карты используется протокол 3D Secure. Если ваш банк поддерживает данную технологию, вы будете перенаправлены на его сервер для ввода дополнительных реквизитов платежа. </span>
<p>
 <span style="font-size: 12pt;"> </span>
</p>
 <span style="font-size: 12pt;"> </span>
<p>
 <span style="font-size: 12pt;"> </span>
</p>
 <span style="font-size: 12pt;"> </span>
<div>
 <span style="font-size: 12pt;"> </span><br>
 <span style="font-size: 12pt;"> </span>
</div>
<h4>
<div>
</div>
 </h4><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>