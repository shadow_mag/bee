<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Условия доставки");
?><h4>
<div>
 <strong style="text-align: justify;"><span style="font-size: 12pt;">Как и когда можно получить заказ?</span></strong><span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span>
</div>
 <span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span></h4>
 <span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span>
<div>
 <span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span><strong><span style="font-size: 18px;"><span style="font-size: 12pt;"> </span><br>
 <span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span></span></strong><span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span>
</div>
 <span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span>
<p>
 <span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span><strong><span style="font-size: 12pt;">Способ 1. Получение заказа в шоуруме. Бесплатно!</span></strong><span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span>
</p>
<p>
 <strong><span style="font-size: 12pt;"><br>
 </span></strong>
</p>
 <span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span>
<p>
 <span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span><span style="font-size: 12pt;">Получение заказа в нашем шоуруме:</span>
</p>
<p>
 <span style="font-size: 12pt;"><br>
 </span>
</p>
<p>
 <span style="font-size: 12pt;">Наш шоурум находится на территории БЦ&nbsp;"Techno-Loft" по адресу 3-й Угрешский проезд 8с9.&nbsp;<br>
 <br>
	 Добраться можно на автомобиле, или по&nbsp;МЦК до&nbsp;станции&nbsp;Угрешская. Далее пешком&nbsp;650 метров&nbsp;по улице 3-й Угрешский проезд&nbsp; до проходной&nbsp;БЦ. Для прохода на территорию БЦ необходимо оформить пропуск у менеджера по телефону. При себе иметь документ удостоверяющий личность.<br>
 </span>
</p>
<h4>
<div>
 <span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span>
</div>
 <span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span></h4>
 <span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span>
<h4><span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span>
<div>
 <span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span><span style="background-color: #ffff00; font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span>
</div>
 <span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span><span style="background-color: #ffff00; font-size: 12pt;"> </span></h4>
 <span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span><span style="background-color: #ffff00; font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span>
<p>
 <span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span><strong><span style="font-size: 12pt;">Способ 2. Курьерская доставка</span></strong><span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span>
</p>
<p>
 <strong><span style="font-size: 12pt;"><br>
 </span></strong>
</p>
 <span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span>
<p style="text-align: justify;">
 <span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span><span style="font-weight: normal; font-size: 12pt;">Курьерская доставка гарантирует:</span><span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span>
</p>
 <span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span>
<p>
 <span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span>
</p>
 <span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span>
<ul lh15="">
	<li style="text-align: justify;"><span style="font-weight: normal; font-size: 12pt;">Своевременность получения заказа</span></li>
	<li style="text-align: justify;"><span style="font-weight: normal; font-size: 12pt;">Соответствие выбранного товара</span></li>
	<li style="text-align: justify;"><span style="font-weight: normal; font-size: 12pt;">Сохранность товара</span></li>
</ul>
 <span style="font-size: 12pt;"> </span>
<p style="text-align: justify;">
 <span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span>
</p>
 <span style="font-size: 12pt;"> </span><span style="font-size: 12pt;">
Мы привезем заказ в удобное для вас место уже на следующий день <span style="background-color: #ffff00;"><span style="background-color: #ffffff;">(при заказе до 18</span><span style="background-color: #ffffff;">.00 текущего дня) – курьерская служба работает с 9 утра до 21</span><span style="background-color: #ffffff;">&nbsp;</span></span>вечера и доставляет заказы точно в срок.<br>
<br>
 вызов курьера с 09-00 до 15-00 в случае, когда отправление надо принять от Клиента в этот же день;<br>
вызов курьера с 09-00 до 18-00 в случае, когда отправление необходимо принять от Клиента на следующий день.<br>
 </span><br>
 <span style="font-size: 12pt;"> </span>
<p>
 <span style="font-size: 12pt;">
	Для удобства вы можете выбрать подходящий интервал доставки по Москве: </span>
</p>
 <span style="font-size: 12pt;"> </span>
<p>
 <span style="font-size: 12pt;"> </span>
</p>
 <span style="font-size: 12pt;"> </span>
<ul>
	<li><span style="font-size: 12pt;">с 10.00 до 15.00,</span></li>
	<li><span style="font-size: 12pt;">с 15.00 до 21.00,</span></li>
	<li><span style="font-size: 12pt;">с 10.00 до 21.00.</span></li>
</ul>
 <span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span><br>
 <span style="font-size: 12pt;"> </span><span style="font-size: 12pt;">
Стоимость доставки в пределах&nbsp;МКАД - 290 рублей для любых товаров.</span><span style="font-size: 12pt;"> </span>
<ul>
</ul>
 <span style="font-size: 12pt;"> </span><span style="font-size: 12pt;">
Мы доставляем вам товары даже в снегопад и непогоду! Но если курьер&nbsp;перенесет доставку, пожалуйста, отнеситесь к этому с пониманием.</span><br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>