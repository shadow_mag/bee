<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Покупки");
?><h4></h4>
<h4 style="text-align: justify;"></h4>
<div>
 <b><span style="font-size: 12pt;">Оформление заказа</span></b><b><span style="font-size: 12pt;"> </span></b><span style="font-size: 12pt;"> </span><br>
	<span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span><br>
	<span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span><b><span style="font-size: 12pt;">УВАЖАЕМЫЕ ПОКУПАТЕЛИ!</span></b><b><span style="font-size: 12pt;"> </span></b><b><span style="font-size: 12pt;"> </span></b><b><span style="font-size: 12pt;"> </span></b><b><span style="font-size: 12pt;"> </span></b><br>
	<span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span><br>
	<span style="font-size: 12pt;"> </span><span style="font-size: 12pt;">
	Для совершения покупки в интернет-магазине вам необходимо: </span><br>
	<span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span><br>
	<span style="font-size: 12pt;"> </span><span style="font-size: 12pt;">
	1. Зарегистрироваться в интернет-магазине. Если вы не хотитите регистрироваться, то данный пункт вы можете пропустить и переходить к п.2. </span><br>
	<span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span><br>
	<span style="font-size: 12pt;"> </span><span style="font-size: 12pt;">
	2. Поместить интересующий вас товар в «Корзину» и оформить заказ. </span><br>
	<span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span><br>
	<span style="font-size: 12pt;"> </span><span style="font-size: 12pt;">
	3. Выбрать тип доставки:</span><br>
	<span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span>
	<ul>
		<li><span style="font-size: 12pt;">самовывоз из нашего шоурума; </span></li>
		<li><span style="font-size: 12pt;"> доставка курьером&nbsp;по г. Москва.</span></li>
	</ul>
	<span style="font-size: 12pt;"> </span><span style="font-size: 12pt;">
	4. Выбрать вид оплаты:</span><br>
	<span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span>
	<ul>
		<li><span style="font-size: 12pt;">наличные (оплата в шоуруме&nbsp;при получении товара или оплата курьеру по г. Москва);&nbsp; </span></li>
		<li><span style="font-size: 12pt;"> оплата банковской картой (принимаются карты Visa, MasterCard).</span></li>
	</ul>
	<span style="font-size: 12pt;"> </span><span style="font-size: 12pt;">
	5. Ваш заказ оформлен. Оператор&nbsp;свяжется&nbsp;в ближайшее время для уточнения деталей. </span><br>
	<span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span><br>
	<span style="font-size: 12pt;"> </span><span style="font-size: 12pt;">
	При оплате товара банковской картой Вы автоматически перенаправляетесь на сайт системы обработки платежей. Далее нажимаете кнопку "Перейти" и заполняете приводимую ниже форму, после чего нажимаете "Оплатить". При получении товара методом самовывоза - необходимо личное присутствие держателя карты.&nbsp; </span><br>
	<span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span>
	<p style="text-align: justify;">
		<span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span>
	</p>
	<span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span>
</div>
<span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span><span style="font-size: 12pt;"> </span>
<p style="text-align: justify;">
 <br>
</p>
<p style="text-align: justify;">
</p><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>